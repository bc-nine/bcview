#include "window_vk.h"

#define VOLK_IMPLEMENTATION
#include "volk.h"

#include "SDL.h"

#include "util_vk.h"
#include "externalallocators.h"
#include "cameracontrols.h"
#include "session.h"
#include "glm/glm.hpp"
#include "testharness.h"
#include "bclib/stdfilesystem.h"
#include "bclib/fileutils.h"

using namespace glm;

namespace bc9 {

KeyEvent SDLToNativeKeyEvent(const SDL_Event &inevent) {
    KeyEvent event;
    event.shift = SDL_GetModState() & KMOD_SHIFT;
    switch (inevent.key.keysym.sym) {
        case SDLK_UP:    event.key = eUpKey; break;
        case SDLK_DOWN:  event.key = eDownKey; break;
        case SDLK_LEFT:  event.key = eLeftKey; break;
        case SDLK_RIGHT: event.key = eRightKey; break;
        case SDLK_w:     event.key = eWKey; break;
        case SDLK_s:     event.key = eSKey; break;
        case SDLK_a:     event.key = eAKey; break;
        case SDLK_d:     event.key = eDKey; break;
        case SDLK_e:     event.key = eEKey; break;
        case SDLK_c:     event.key = eCKey; break;
        case SDLK_r:     event.key = eRKey; break;
        case SDLK_m:     event.key = eMKey; break;
        case SDLK_o:     event.key = eOKey; break;
        case SDLK_1:     event.key = e1Key; break;
        case SDLK_2:     event.key = e2Key; break;
        case SDLK_3:     event.key = e3Key; break;
        case SDLK_4:     event.key = e4Key; break;
        case SDLK_5:     event.key = e5Key; break;
        case SDLK_6:     event.key = e6Key; break;
        case SDLK_7:     event.key = e7Key; break;
        case SDLK_8:     event.key = e8Key; break;
        case SDLK_9:     event.key = e9Key; break;
        case SDLK_0:     event.key = e0Key; break;
        case SDLK_f:     event.key = eFKey; break;
        case SDLK_t:     event.key = eTKey; break;
        case SDLK_SPACE: event.key = eSpaceKey; break;
    }

    return event;
}

Window::Window(const FString& screenshotdir, const FString& worldname) :
screenshotdir(screenshotdir),
worldname(worldname) {
    VkCheck_(volkInitialize());

    assert(SDL_GetNumAllocations() == 0);
    SDL_SetMemoryFunctions(sdlalloc, sdlcalloc, sdlrealloc, sdlfree);

    SDL_Init(SDL_INIT_VIDEO);
}

void Window::Initialize(const FString& name, uintn width, uintn height) {
    FString title("%s (Vulkan)", name.str());
    sdlobj = SDL_CreateWindow(title.str(),
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              width, height,
                              SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);

    SDL_ShowWindow(sdlobj);
}

void Window::SetName(const FString& name) {
    FString title("%s (Vulkan)", name.str());
    SDL_SetWindowTitle(sdlobj, title.str());
}

void Window::ProcessEvents(Session* session, bool& quit, bool& exitrenderloop,
                           bool& requestrestart, bool& paused) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        WindowEvent winevent(&event);
        if (!session->renderer.ProcessEvent(session->context, winevent)) {
            if (event.type == SDL_QUIT) {
                quit = true;
            }
            else if (event.type == SDL_WINDOWEVENT) {
                if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
                    session->renderer.ViewResized(session->context,
                                                  event.window.data1,
                                                  event.window.data2);
                }
            }
            else if (event.type == SDL_MOUSEBUTTONDOWN) {
                ButtonEvent bevent;
                bevent.position = ivec2(event.button.x, event.button.y);
                session->cameracontrols.LeftButtonDown(session->context, bevent);
            }
            else if (event.type == SDL_MOUSEMOTION) {
                if (event.motion.state & SDL_BUTTON_LMASK) {
                    MotionEvent mevent;
                    mevent.position = ivec2(event.motion.x, event.motion.y);
                    session->cameracontrols.LeftButtonMotion(session->context, mevent);
                }
            }
            else if (event.type == SDL_KEYDOWN) {
                KeyEvent kevent(SDLToNativeKeyEvent(event));
                uintn activecameranode = kInvalidUInt;
                glm::vec3 campos;
                glm::quat camrot;
                switch (event.key.keysym.sym) {
                    case SDLK_r:
                    exitrenderloop = true;
                    requestrestart = true;
                    break;

                    case SDLK_m:
                    activecameranode = session->world.ActiveCameraNode();
                    campos = session->nodecache.NodeTranslation(activecameranode);
                    camrot = session->nodecache.NodeRotation(activecameranode);
                    Print_("        "
                           "\"camera-translation\" : [ %0.6f, %0.6f, %0.6f ],",
                           campos.x, campos.y, campos.z);
                    Print_("        "
                           "\"camera-rotation\"    : [ %0.6f, %0.6f, %0.6f, %0.6f ]",
                            camrot.x, camrot.y, camrot.z, camrot.w);
                    break;

                    case SDLK_f:
                    if (session->testharness && paused)
                        paused = false;
                    else
                        session->cameracontrols.KeyDown(session->context, kevent);
                    break;

                    case SDLK_ESCAPE:
                    session->context.ToggleOverlay();
                    break;

                    case SDLK_0:
                    if (session->testharness)
                        session->testharness->KeyPressed('0');
                    else
                        session->cameracontrols.KeyDown(session->context, kevent);
                    break;

                    case SDLK_1:
                    if (session->testharness)
                        session->testharness->KeyPressed('1');
                    else
                        session->cameracontrols.KeyDown(session->context, kevent);
                    break;

                    case SDLK_2:
                    if (session->testharness)
                        session->testharness->KeyPressed('2');
                    else
                        session->cameracontrols.KeyDown(session->context, kevent);
                    break;

                    case SDLK_3:
                    if (session->testharness)
                        session->testharness->KeyPressed('3');
                    else
                        session->cameracontrols.KeyDown(session->context, kevent);
                    break;

                    case SDLK_SPACE:
                    session->renderer.ToggleRayTraceAccumulation();
                    break;

                    case SDLK_PRINTSCREEN:
                    session->renderer.CaptureImage(session->context,
                                                   [&](RenderState& state, uchar* data,
                                                       uintn stride, uintn width,
                                                       uintn height, uintn format) {
                        bool writeimage = true;
                        uintn channels, texelsize;
                        Ignore_(GetResourceFormatProperties(ResourceFormat(format),
                                                            channels, texelsize));
                        FString imagepath("%s\\%s_%s_%d.png", screenshotdir.str(),
                                          worldname.str(), "winvk", state.frame);
                        if (stdfilesystem::exists(imagepath.str())) {
                            const SDL_MessageBoxButtonData buttons[] = {
                                { SDL_MESSAGEBOX_BUTTON_ESCAPEKEY_DEFAULT, 2, "Cancel" },
                                { SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT, 1, "No" },
                                { /* .flags, .buttonid, .text */        0, 0, "Yes" },
                            };
                            int buttonid;
                            FString msg("%s exists. \nOverwrite?", imagepath.str());
                            const SDL_MessageBoxData messageboxdata = {
                                SDL_MESSAGEBOX_WARNING,
                                NULL,
                                "Warning: File Exists",
                                msg.str(),
                                SDL_arraysize(buttons),
                                buttons
                            };
                            SDL_ShowMessageBox(&messageboxdata, &buttonid);
                            if (buttonid != 0)
                                writeimage = false;
                        }
                        if (writeimage) {
                            Result r = WritePNGImage(imagepath.str(), data, stride, width,
                                                     height, channels);
                            if (!r)
                                SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Error",
                                                         r.Message(), sdlobj);
                        }
                    });
                    break;

                    default:
                    session->cameracontrols.KeyDown(session->context, kevent);
                    break;
                }

            }
            else if (event.type == SDL_KEYUP) {
                KeyEvent kevent(SDLToNativeKeyEvent(event));
                session->cameracontrols.KeyUp(session->context, kevent);
            }
        }
    }
}

void Window::ShowErrorBox(const FString& title, const FString& message) {
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, title.str(), message.str(), sdlobj);
}

void Window::Shutdown() {
    if (sdlobj) {
        SDL_DestroyWindow(sdlobj);
        sdlobj = nullptr;
    }
}

}