#pragma once

#include <climits>
#include "volk.h"
#include "rhitypes.h"
#include "bclib/stdjson.h"

namespace bc9 {

class ShaderBinding {
public:
    // note that a shader binding may be part of multiple stages (vertex, pixel, etc).
    // when created from json, they're initialized with a single stage (since each shader
    // has its own json), but other parts of the code that look at the whole pipeline
    // may see that the same binding is used in multiple shader stages, and there we
    // simply reuse this structure, but with all relevant stages identified.
    ShaderBinding(const stdjson &mjson, ShaderStage stage);
    ShaderBinding();

    bool Valid() const;

    bool operator==(const ShaderBinding& rhs);

    unsigned int set = UINT_MAX;
    unsigned int binding = UINT_MAX;
    unsigned int count = UINT_MAX;
    VkDescriptorType dtype = VK_DESCRIPTOR_TYPE_MAX_ENUM;

    ShaderStages stages = eShaderStageInvalid;
};

}