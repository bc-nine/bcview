#include "shaderasset.h"

namespace bc9 {

Result GetShaderAssetType(const StringID& strtype, ShaderAssetType& type) {
    switch (strtype.Hash()) {
        case "vertex"_sid:
        type = eShaderAssetVertex;
        break;

        case "pixel"_sid:
        type = eShaderAssetPixel;
        break;

        case "compute"_sid:
        type = eShaderAssetCompute;
        break;

        case "ray"_sid:
        type = eShaderAssetRay;
        break;

        case "miss"_sid:
        type = eShaderAssetMiss;
        break;

        case "closest-hit"_sid:
        type = eShaderAssetClosestHit;
        break;

        default:
        return Error("%s: unknown shader type", strtype.str());
        break;
    }

    return Success();
}

}