#pragma once

#include "../renderpass.h"
#include "../constants.h"
#include "../framebuffer.h"
#include "../framebufferlayout.h"
#include "../cachekeys.h"

namespace bc9 {

struct ImageView;
class SessionContext;

struct DisplayConstants {
    uint32 colorbufferidx;
    uint32 pad1;
    uint32 pad2;
    uint32 pad3;
};

class DisplayPass : public RenderPass {
public:
    DisplayPass(SessionContext& scontext);

    void PreRender(SessionContext& scontext);
    void Render(SessionContext& scontext);
    void Update(SessionContext& scontext, float dt);

    void Initialize(SessionContext& scontext);
    void Shutdown(SessionContext& scontext);

    void ViewResized(SessionContext& scontext, uintn width, uintn height);

    void OverrideImage(SessionContext& scontext, ImageView& imageview);
    void ClearImageOverride(SessionContext& scontext);

private:
    void UpdateImages(SessionContext& scontext);

    void RecreateFramebuffer(SessionContext& scontext);
    void CleanupFramebuffer(SessionContext& scontext);

    void OverlayUpdateMenuBar(SessionContext& scontext);
    void OverlayUpdate(SessionContext& scontext);

    BufferResource displaycbuf, xrdisplaycbuf;

    FramebufferLayout fblayout, xrfblayout;
    Framebuffer framebuffer, xrframebuffer;

    ShaderKey vertexkey, xrvertexkey;
    ShaderKey pixelkey, xrpixelkey;

    GraphicsPipelineBase psobase, xrpsobase;
    GraphicsPipelineData psodata, xrpsodata;
};

}