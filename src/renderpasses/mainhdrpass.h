#pragma once

#include "../renderpass.h"
#include "../framebufferlayout.h"
#include "../framebuffer.h"
#include "../rendertarget.h"
#include "../pipelines.h"
#include "../graphicscommands.h"

namespace bc9 {

class SessionContext;
class PlatformContext;

class MainHDRPass : public RenderPass {
public:
    MainHDRPass(SessionContext& scontext);

    void Initialize(SessionContext& scontext);
    void Shutdown(SessionContext& scontext);

    void Update(SessionContext& scontext, float dt);
    void Render(SessionContext& scontext);

    void ViewResized(SessionContext& scontext, uintn width, uintn height);

private:
    void RecreateFramebuffer(SessionContext& scontext);
    void CleanupFramebuffer(SessionContext& scontext);

    ColorTarget colortarget, xrcolortarget;
    DepthStencilTarget dstarget, xrdstarget;
    Framebuffer framebuffer, xrframebuffer;
    FramebufferLayout fblayout, xrfblayout;
    GraphicsPipelineData psodata, xrpsodata;

    ImageView xrviews[kMaxViews];
};

}