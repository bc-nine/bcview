#include "raytracepass.h"
#include "../sessioncontext.h"
#include "../overlay.h"
#include "../world.h"
#include "../util_vk.h"
#include "../drawablecache.h"
#include "../imdraw.h"
#include "../renderstate.h"
#include "../assetcache.h"
#include "../surface.h"
#include "imgui.h"
#include "bclib/mathutils.h"
#include "bclib/stdallocators.h"
#include "../renderer.h"

// vulkan-specific for now
#include "../resourceutil_vk.h"

namespace bc9 {

RayTracePass::RayTracePass(SessionContext& scontext) :
RenderPass("main"),
fblayout(1, scontext.pc.msaasamples, eFramebufferLayoutUseDepthStencilTarget) {
    ColorLayout color("color", eResourceFormatR32G32B32A32_SFLOAT,
                      eColorLayoutLoadOpClear | eColorLayoutStoreOpStore);
    fblayout.AddColorLayout(scontext.pc, color);

    raykey = scontext.assetcache.FindShader("ray-default", eShaderAssetRay);
    misskey = scontext.assetcache.FindShader("miss-default", eShaderAssetMiss);
    chitkey = scontext.assetcache.FindShader("chit-default", eShaderAssetClosestHit);
#if 0
    #if IMDRAW_DEBUG_RAYTRACE
        inputlayout.AddBufferInput("imvertices", eShaderStageClosestHit);
    #endif
#endif

    DepthStencilLayout ds(eResourceFormatD24_UNORM_S8_UINT, eDepthStencilLayoutDepthLoadOpClear);
    fblayout.AddDepthStencilLayout(scontext.pc, ds);

    scontext.passcache.RegisterPass(passname);
    scontext.passcache.RegisterTarget(passname, "color");

    maxaccumulation = scontext.world.GetRayTraceMaxAccum();
    bounces = scontext.world.GetRayTraceBounces();
}

void RayTracePass::UpdateConstants(SessionContext& scontext, PerFrameConstants& pf) {
    pf.accumframes.x = ++accumframes;
    pf.accumframes.y = uint32(maxaccumulation);
    pf.bounces = uint32(bounces);
    pf.accelstructuresidx = tlas.DescriptorIndex();
    const Background& background = scontext.world.GetActiveBackground();
    pf.bggrad1 = background.Color1();
    pf.bggrad2 = background.Color2();
    pf.rtoutputidx = storageimage.rwview.DescriptorIndex();
    pf.rtaccumulationidx = accumimage.rwview.DescriptorIndex();
}

void RayTracePass::CreateImages(SessionContext& scontext) {
    uintn w, h;
    scontext.pc.surface.GetViewSize(w, h);
    CreateImageParams params(eResourceFormatR32G32B32A32_SFLOAT, w, h, 1, "storageimage");
    params.SetUsageCopySource();
    params.SetUsageStorage();
    params.SetUsageSampled();
    params.SetInitialLayout(eImageLayoutShaderReadWrite);
    Throw_(CreateImage(scontext.pc, nullptr, params, storageimage));

    CreateImageParams accumparams(eResourceFormatR32G32B32A32_SFLOAT, w, h, 1, "accumimage");
    accumparams.SetUsageStorage();
    accumparams.SetInitialLayout(eImageLayoutShaderReadWrite);

    Throw_(CreateImage(scontext.pc, nullptr, accumparams, accumimage));
}

void RayTracePass::Initialize(SessionContext& scontext) {
    fblayout.Initialize(scontext.pc, passname);
    scontext.passcache.UpdateFramebufferLayout(passname, fblayout);

    RecreateFramebuffer(scontext);

    rtpipelineproperties.sType =
        VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_PROPERTIES_KHR;
    VkPhysicalDeviceProperties2 deviceProperties2 {};
    deviceProperties2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    deviceProperties2.pNext = &rtpipelineproperties;
    vkGetPhysicalDeviceProperties2(scontext.pc.physicaldevice, &deviceProperties2);

    StdStackAllocator alloc(GlobalStack());
    stdvector<NodeCache::BakedInstance> instances(alloc);
    scontext.nodecache.Bake(scontext, instances);

    CreateTLASParams tlasparams;
    Throw_(CreateTLAS(scontext.pc, instances, scontext.assetcache, scontext.drawablecache,
                      tlasparams, tlas));

    CreateRayTracingPipeline(scontext);
    Throw_(CreateShaderBindingTable(scontext, instances));
}

void RayTracePass::PostInitialize(SessionContext& scontext) {
//#if IMDRAW_DEBUG_RAYTRACE
//    inputset.UpdateBufferOutput(scontext, "imvertices", scontext.imdraw.GPUVertexBuffer());
//#endif
}

void RayTracePass::UpdateImages(SessionContext& scontext) {
}

void RayTracePass::UpdateAccelStructure(SessionContext& scontext) {
}

void RayTracePass::CreateRayTracingPipeline(SessionContext& scontext) {
    StdStackAllocator stackalloc(GlobalStack());
    stdvector<VkPipelineShaderStageCreateInfo> shaderstages(stackalloc);

    {
        ShaderResource rayshader = scontext.assetcache.GetShader(raykey);

        VkPipelineShaderStageCreateInfo stage {};
        stage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        stage.stage = rayshader.stage;
        stage.module = rayshader.module;
        stage.pName = rayshader.entrypoint.str();
        shaderstages.push_back(stage);
        VkRayTracingShaderGroupCreateInfoKHR shadergroup {};
        shadergroup.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
        shadergroup.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
        shadergroup.generalShader = static_cast<uint32>(shaderstages.size()) - 1;
        shadergroup.closestHitShader = VK_SHADER_UNUSED_KHR;
        shadergroup.anyHitShader = VK_SHADER_UNUSED_KHR;
        shadergroup.intersectionShader = VK_SHADER_UNUSED_KHR;
        shadergroups.push_back(shadergroup);
    }

    {
        ShaderResource missshader = scontext.assetcache.GetShader(misskey);

        VkPipelineShaderStageCreateInfo stage {};
        stage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        stage.stage = missshader.stage;
        stage.module = missshader.module;
        stage.pName = missshader.entrypoint.str();
        shaderstages.push_back(stage);
        VkRayTracingShaderGroupCreateInfoKHR shadergroup {};
        shadergroup.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
        shadergroup.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_GENERAL_KHR;
        shadergroup.generalShader = static_cast<uint32>(shaderstages.size()) - 1;
        shadergroup.closestHitShader = VK_SHADER_UNUSED_KHR;
        shadergroup.anyHitShader = VK_SHADER_UNUSED_KHR;
        shadergroup.intersectionShader = VK_SHADER_UNUSED_KHR;
        shadergroups.push_back(shadergroup);
    }

    {
        ShaderResource chitshader = scontext.assetcache.GetShader(chitkey);

        VkPipelineShaderStageCreateInfo stage {};
        stage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
        stage.stage = chitshader.stage;
        stage.module = chitshader.module;
        stage.pName = chitshader.entrypoint.str();
        shaderstages.push_back(stage);
        VkRayTracingShaderGroupCreateInfoKHR shadergroup {};
        shadergroup.sType = VK_STRUCTURE_TYPE_RAY_TRACING_SHADER_GROUP_CREATE_INFO_KHR;
        shadergroup.type = VK_RAY_TRACING_SHADER_GROUP_TYPE_TRIANGLES_HIT_GROUP_KHR;
        shadergroup.generalShader = VK_SHADER_UNUSED_KHR;
        shadergroup.closestHitShader = static_cast<uint32>(shaderstages.size()) - 1;
        shadergroup.anyHitShader = VK_SHADER_UNUSED_KHR;
        shadergroup.intersectionShader = VK_SHADER_UNUSED_KHR;
        shadergroups.push_back(shadergroup);
    }

    VkRayTracingPipelineCreateInfoKHR raytracingpipelineci {};
    raytracingpipelineci.sType = VK_STRUCTURE_TYPE_RAY_TRACING_PIPELINE_CREATE_INFO_KHR;
    raytracingpipelineci.stageCount = static_cast<uint32>(shaderstages.size());
    raytracingpipelineci.pStages = shaderstages.data();
    raytracingpipelineci.groupCount = static_cast<uint32>(shadergroups.size());
    raytracingpipelineci.pGroups = shadergroups.data();
    raytracingpipelineci.maxPipelineRayRecursionDepth = 1;
    raytracingpipelineci.layout = scontext.pc.pipelinelayout;
    VkCheck_(vkCreateRayTracingPipelinesKHR(scontext.pc.device, VK_NULL_HANDLE, VK_NULL_HANDLE,
             1, &raytracingpipelineci, VkAllocators(), &pipeline));
}

Result
RayTracePass::CreateShaderBindingTable(SessionContext& scontext,
                                       const stdvector<NodeCache::BakedInstance>& instances) {
    const uint32 instancecount = scontext.nodecache.DrawableInstanceCount();
    const uint32 userdatasize = sizeof(InstanceConstants);
    const uint32 handlesize = rtpipelineproperties.shaderGroupHandleSize;
    const uint32 handlesizealigned = Align(rtpipelineproperties.shaderGroupHandleSize,
                                           rtpipelineproperties.shaderGroupHandleAlignment);
    const uint32 groupcount = static_cast<uint32>(shadergroups.size());
    hitgroupsize = Align(handlesizealigned + userdatasize,
                         rtpipelineproperties.shaderGroupBaseAlignment);
    missgroupsize = Align(handlesizealigned,
                          rtpipelineproperties.shaderGroupBaseAlignment);
    raygroupsize = Align(handlesizealigned,
                         rtpipelineproperties.shaderGroupBaseAlignment);

    // 1 miss group + 1 raygen group + instancecount hit groups
    const uint32 sbtsize = raygroupsize + missgroupsize + instancecount * hitgroupsize;

    stdvector<uint8> shaderhandlestorage(sbtsize);
    VkCheck_(vkGetRayTracingShaderGroupHandlesKHR(scontext.pc.device, pipeline, 0, groupcount,
                                                  sbtsize, shaderhandlestorage.data()));

    // if you're going to use CreateVkBufferParams, pay attn to instancesize! (see mar 6 2023)
    CreateVkBufferParams params {};
    params.createinfo.usage = VK_BUFFER_USAGE_SHADER_BINDING_TABLE_BIT_KHR |
                              VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    params.properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;

    params.instancesize = raygroupsize;
    params.instances = 1;
    params.name = "sbt ray";
    Return_(CreateBuffer(scontext.pc, params, raysbt));

    params.instancesize = missgroupsize;
    params.instances = 1;
    params.name = "sbt miss";
    Return_(CreateBuffer(scontext.pc, params, misssbt));

    params.name = "sbt hit";
    params.instancesize = hitgroupsize * instancecount;
    params.instances = 1;
    Return_(CreateBuffer(scontext.pc, params, hitsbt));

    memcpy(raysbt.memory.mappedmem, shaderhandlestorage.data(), handlesize);
    memcpy(misssbt.memory.mappedmem, shaderhandlestorage.data() + handlesizealigned, handlesize);

    for (uintn i = 0; i < instancecount; ++i) {
        const NodeCache::BakedInstance& instance = instances[i];
        unsigned char* dst = (unsigned char*)(hitsbt.memory.mappedmem);
        memcpy(dst + i*hitgroupsize,
               shaderhandlestorage.data() + handlesizealigned*2, handlesize);
        glm::vec4 row0 = glm::vec4(instance.transform[0][0],
                                   instance.transform[1][0],
                                   instance.transform[2][0],
                                   instance.transform[3][0]);
        glm::vec4 row1 = glm::vec4(instance.transform[0][1],
                                   instance.transform[1][1],
                                   instance.transform[2][1],
                                   instance.transform[3][1]);
        glm::vec4 row2 = glm::vec4(instance.transform[0][2],
                                   instance.transform[1][2],
                                   instance.transform[2][2],
                                   instance.transform[3][2]);
        static uintn v4size = sizeof(glm::vec4);
        memcpy(dst + i*hitgroupsize + handlesizealigned + 0*v4size, &row0[0], v4size);
        memcpy(dst + i*hitgroupsize + handlesizealigned + 1*v4size, &row1[0], v4size);
        memcpy(dst + i*hitgroupsize + handlesizealigned + 2*v4size, &row2[0], v4size);
        uintn id = instance.drawablekey.index;
        memcpy(dst + i*hitgroupsize + handlesizealigned + 3*v4size + sizeof(glm::vec3),
               &id, sizeof(uintn));
    }

    return Success();
}

void RayTracePass::RecreateFramebuffer(SessionContext& scontext) {
    uintn w, h;
    scontext.pc.surface.GetViewSize(w, h);

    CreateColorTargetParams colorparams("color", w, h);
    Throw_(CreateColorTarget(scontext.pc, fblayout, colorparams, colortarget));

    CreateDepthStencilTargetParams dsparams(w, h);
    Throw_(CreateDepthStencilTarget(scontext.pc, fblayout, dsparams, dstarget));

    framebuffer.Initialize(scontext.pc, &fblayout, &colortarget, 1, &dstarget, w, h);

    CreateImages(scontext);
    UpdateImages(scontext);

    scontext.passcache.UpdateTarget("main", "color", &storageimage.view);
}

void RayTracePass::Update(SessionContext& scontext, float dt) {
    if (!scontext.renderer.RayTraceAccumulationMode())
        accumframes = 0;
}

void RayTracePass::ViewResized(SessionContext& scontext, uintn width, uintn height) {
    CleanupFramebuffer(scontext);
    RecreateFramebuffer(scontext);
    scontext.renderer.SetRayTraceAccumulation(false);
}

void RayTracePass::PostRender(SessionContext& scontext) {
    uintn bufferidx = scontext.pc.bufferidx;
    CommandBuffer cmdbuffer = { .vkobj = scontext.pc.commandbuffers[bufferidx] };
    TransitionVkImageParams params { .computemasks = true };
    params.usecmdbuffer = &cmdbuffer;
    params.barrier.oldLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    params.barrier.newLayout = VK_IMAGE_LAYOUT_GENERAL;
    params.barrier.image = storageimage.vkobj;
    params.srcstagemask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    params.dststagemask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    params.barrier.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
    TransitionImage(scontext.pc, params);
}

void RayTracePass::Render(SessionContext& scontext) {

    /*
        VERY IMPORTANT!
        I removed the interface for setting the clear color at init, since it should
        be set (or at least checked) every frame, in case it has been updated. for
        details, see mainhdrpass.cpp. anyway, when you fix this code up, make sure
        to do the appropriate check to see if the world background color has changed,
        and if so, use the SetClearColor() api on framebuffer to set the clear color
        properly, since otherwise it'll just be stuck at default.
    */

    uintn width, height;
    scontext.pc.surface.GetViewSize(width, height);

    uintn bufferidx = scontext.pc.bufferidx;

    const uint32 instancecount = scontext.nodecache.DrawableInstanceCount();
    const uint32 handlesizealigned = Align(rtpipelineproperties.shaderGroupHandleSize,
                                           rtpipelineproperties.shaderGroupHandleAlignment);
    VkStridedDeviceAddressRegionKHR rayshadersbtentry {};
    rayshadersbtentry.deviceAddress = GetBufferDeviceAddress(scontext.pc, raysbt.vkobj);
    rayshadersbtentry.stride = raygroupsize;
    rayshadersbtentry.size = raygroupsize; // * handlecount? just 1 here so ok

    VkStridedDeviceAddressRegionKHR missshadersbtentry {};
    missshadersbtentry.deviceAddress = GetBufferDeviceAddress(scontext.pc, misssbt.vkobj);
    missshadersbtentry.stride = missgroupsize;
    missshadersbtentry.size = missgroupsize; // see comment above

    VkStridedDeviceAddressRegionKHR hitshadersbtentry {};
    hitshadersbtentry.deviceAddress = GetBufferDeviceAddress(scontext.pc, hitsbt.vkobj);
    hitshadersbtentry.stride = hitgroupsize;
    hitshadersbtentry.size = hitgroupsize * instancecount;

    VkStridedDeviceAddressRegionKHR callableshadersbtentry {};

    vkCmdBindPipeline(scontext.pc.commandbuffers[bufferidx],
        VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR, pipeline);

    vkCmdTraceRaysKHR(scontext.pc.commandbuffers[bufferidx],
                      &rayshadersbtentry,
                      &missshadersbtentry,
                      &hitshadersbtentry,
                      &callableshadersbtentry,
                      width, height, 1);

    CommandBuffer cmdbuffer = { .vkobj = scontext.pc.commandbuffers[bufferidx] };
    TransitionVkImageParams params { .computemasks = true };
    params.usecmdbuffer = &cmdbuffer;
    params.barrier.oldLayout = VK_IMAGE_LAYOUT_GENERAL;
    params.barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    params.barrier.image = storageimage.vkobj;
    params.srcstagemask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    params.dststagemask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    params.barrier.subresourceRange = { VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1 };
    TransitionImage(scontext.pc, params);
}

void RayTracePass::OverlayUpdateMenuBar(SessionContext& scontext) {
    if (ImGui::BeginMenu("Ray Trace")) {
        if (ImGui::InputInt("Path Length", &bounces)) {
            accumframes = 0;
            bounces = Clamp(bounces, 1, 256);
        }
        if (ImGui::InputInt("Max Accumulation", &maxaccumulation)) {
            accumframes = 0;
            maxaccumulation = Clamp(maxaccumulation, 1, INT_MAX);
        }
        ImGui::EndMenu();
    }
}

void RayTracePass::OverlayUpdate(SessionContext& scontext) {

}

void RayTracePass::CleanupFramebuffer(SessionContext& scontext) {
    framebuffer.Shutdown(scontext.pc);
    DestroyColorTarget(scontext.pc, colortarget);
    DestroyDepthStencilTarget(scontext.pc, dstarget);
    DestroyImage(scontext.pc, storageimage);
    DestroyImage(scontext.pc, accumimage);
}

void RayTracePass::Shutdown(SessionContext& scontext) {
    CleanupFramebuffer(scontext);

    DestroyTLAS(scontext.pc, tlas);

    vkDestroyPipeline(scontext.pc.device, pipeline, VkAllocators());

    DestroyBuffer(scontext.pc, indexbuffer);
    DestroyBuffer(scontext.pc, vertexbuffer);
    DestroyBuffer(scontext.pc, transformbuffer);

    DestroyBuffer(scontext.pc, raysbt);
    DestroyBuffer(scontext.pc, misssbt);
    DestroyBuffer(scontext.pc, hitsbt);

    fblayout.Shutdown(scontext.pc);
}

void RayTracePass::CaptureRenderState(RenderState& state) {
    state.rt_pathlength = bounces;
    state.rt_maxaccum = maxaccumulation;
}

}