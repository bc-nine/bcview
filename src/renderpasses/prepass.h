#pragma once

#include "../renderpass.h"
#include "../constants.h"
#include "../framebufferlayout.h"
#include "../framebuffer.h"
#include "../rendertarget.h"
#include "../cachekeys.h"
#include "../skybox.h"

namespace bc9 {

class SessionContext;

struct PreConstants {
    uint32 colorbufferidx;
    uint32 pad1;
    uint32 pad2;
    uint32 pad3;
};

class PrePass : public RenderPass {
public:
    PrePass(SessionContext& scontext);

    void Render(SessionContext& scontext);
    void Update(SessionContext& scontext, float dt);

    void Initialize(SessionContext& scontext);
    void Shutdown(SessionContext& scontext);

private:
    void RecreateFramebuffer(SessionContext& scontext);
    void CleanupFramebuffer(SessionContext& scontext);

    void OverlayUpdateMenuBar(SessionContext& scontext);
    void OverlayUpdate(SessionContext& scontext);

    BufferResource prepasscbuf;

    FramebufferLayout fblayout;

    ColorTarget colortarget;
    Framebuffer framebuffer;

    ShaderKey vertexkey;
    ShaderKey pixelkey;

    GraphicsPipelineBase psobase;
    GraphicsPipelineData psodata;

    Skybox skybox;
};

}