#pragma once

#include "../framebufferlayout.h"
#include "../framebuffer.h"
#include "../renderpass.h"
#include "../rendertarget.h"
#include "../sharedstructs.h"
#include "volk.h"
#include "bclib/stdarray.h"
#include "bclib/stdvector.h"
#include "bclib/podtypes.h"

namespace bc9 {

class SessionContext;

struct Vertex {
    float pos[3];
};

class RayTracePass : public RenderPass {
public:
    RayTracePass(SessionContext& scontext);

    void Render(SessionContext& scontext);
    void PostRender(SessionContext& scontext);
    void Update(SessionContext& scontext, float dt);

    void UpdateConstants(SessionContext& scontext, PerFrameConstants& pf);

    void Initialize(SessionContext& scontext);
    void PostInitialize(SessionContext& scontext);
    void Shutdown(SessionContext& scontext);

    void ViewResized(SessionContext& scontext,unsigned int width, unsigned int height);

    void CaptureRenderState(RenderState& state);

private:
    void RecreateFramebuffer(SessionContext& scontext);
    void CleanupFramebuffer(SessionContext& scontext);

    void CreateImages(SessionContext& scontext);
    void CreateRayTracingPipeline(SessionContext& scontext);
    Result CreateShaderBindingTable(SessionContext& scontext,
                                    const stdvector<NodeCache::BakedInstance>& instances);

    void UpdateImages(SessionContext& scontext);
    void UpdateAccelStructure(SessionContext& scontext);

    void FillGeometry(SessionContext& scontext, stdvector<Vertex>& vertices,
                      stdvector<uint32_t>& indices, uint32_t& maxvertexindex);

    void OverlayUpdateMenuBar(SessionContext& scontext);
    void OverlayUpdate(SessionContext& scontext);

    ColorTarget colortarget;
    DepthStencilTarget dstarget;
    Framebuffer framebuffer;
    FramebufferLayout fblayout;

    stdvector<DrawData> drawdata;

    stdarray<stdvector<DrawData>, eRenderBinTypeCount> drawbuckets;

    // ray tracing stuff
    VkPhysicalDeviceAccelerationStructureFeaturesKHR asfeatures {};
    VkPhysicalDeviceRayTracingPipelinePropertiesKHR  rtpipelineproperties {};

    BufferResource vertexbuffer, indexbuffer, transformbuffer;

    BufferResource raysbt, misssbt, hitsbt;

    ImageResource storageimage, accumimage;

    TLASResource tlas;

    ShaderKey raykey;
    ShaderKey misskey;
    ShaderKey chitkey;

    VkPipeline pipeline;

    intn accumframes = 0;
    intn maxaccumulation = 0;
    intn bounces = 0;

    uint32 hitgroupsize = 0, missgroupsize = 0, raygroupsize = 0;

    stdvector<VkRayTracingShaderGroupCreateInfoKHR> shadergroups {};

    stdvector<Drawable> drawables;
};

}