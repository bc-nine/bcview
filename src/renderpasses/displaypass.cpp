#include "displaypass.h"
#include "imgui.h"
#include "../sessioncontext.h"
#include "../overlay.h"
#include "../world.h"
#include "../imdraw.h"
#include "../graphicscommands.h"
#include "../assetcache.h"
#include "../renderer.h"
#include "../xrheadset.h"
#include "bclib/timeutils.h"

namespace bc9 {

DisplayPass::DisplayPass(SessionContext& scontext) :
RenderPass("display") {
    Throw_(CreateSwapchainFramebufferLayout(scontext.pc, "color", fblayout));
    Throw_(CreateXRSwapchainFramebufferLayout(scontext.pc, "xrcolor", xrfblayout));

    vertexkey = scontext.assetcache.FindShader("display", eShaderAssetVertex);
    pixelkey = scontext.assetcache.FindShader("display", eShaderAssetPixel);

    scontext.passcache.RegisterPass(passname);

    psobase = GraphicsPipelineBase(scontext.pc, vertexkey, pixelkey);

    if (scontext.pc.xrmode > 0) {
        xrvertexkey = scontext.assetcache.FindShader("xrdisplay", eShaderAssetVertex);
        xrpixelkey = scontext.assetcache.FindShader("xrdisplay", eShaderAssetPixel);
        xrpsobase = GraphicsPipelineBase(scontext.pc, xrvertexkey, xrpixelkey);
    }
}

void DisplayPass::Initialize(SessionContext& scontext) {
    fblayout.Initialize(scontext.pc, passname);
    psodata = GraphicsPipelineData(fblayout.layoutdata);
    scontext.passcache.UpdateFramebufferLayout(passname, fblayout);
    CreateConstantBufferParams params(scontext.pc, sizeof(DisplayConstants),
                                      "display-constants");
    Throw_(CreateBuffer(scontext.pc, params, displaycbuf));

    if (scontext.pc.xrmode > 0) {
        xrfblayout.Initialize(scontext.pc, passname);
        xrpsodata = GraphicsPipelineData(xrfblayout.layoutdata);

        FString cname("xr-display-constants");
        CreateConstantBufferParams params(scontext.pc, sizeof(DisplayConstants), cname.str());
        Throw_(CreateBuffer(scontext.pc, params, xrdisplaycbuf));
    }

    UpdateImages(scontext);
    RecreateFramebuffer(scontext);
}

void DisplayPass::RecreateFramebuffer(SessionContext& scontext) {
    Throw_(CreateSwapchainFramebuffer(scontext.pc, fblayout, framebuffer));

    if (scontext.pc.xrmode > 0)
        Throw_(CreateXRSwapchainFramebuffer(scontext.pc, xrfblayout, xrframebuffer));
}

void DisplayPass::Update(SessionContext& scontext, float dt) {
}

void DisplayPass::UpdateImages(SessionContext& scontext) {

    if (scontext.pc.xrmode > 0) {
        ImageView* imgview = nullptr;
        scontext.passcache.GetTarget("main", "xrcolor", &imgview);
        DisplayConstants constants { imgview->DescriptorIndex() };
        xrdisplaycbuf.UpdateAndSwap(&constants);

        // clean this shit up
        //scontext.passcache.GetTarget("main", "color-0", &imgview);
        //DisplayConstants constants { imgview->DescriptorIndex() };
        //displaycbuf.UpdateAndSwap(&constants);
    }
    else {
        ImageView* imgview;
        scontext.passcache.GetTarget("main", "color", &imgview);
        DisplayConstants constants { imgview->DescriptorIndex() };
        displaycbuf.UpdateAndSwap(&constants);
    }
}

void DisplayPass::OverrideImage(SessionContext& scontext, ImageView& imageview) {
    DisplayConstants constants { imageview.DescriptorIndex() };
    displaycbuf.UpdateAndSwap(&constants);
}

void DisplayPass::ClearImageOverride(SessionContext& scontext) {
    UpdateImages(scontext);
}

void DisplayPass::ViewResized(SessionContext& scontext, uintn width, uintn height) {
    CleanupFramebuffer(scontext);
    RecreateFramebuffer(scontext);
    UpdateImages(scontext);
}

void DisplayPass::OverlayUpdateMenuBar(SessionContext& scontext) {
    bool test;
    if (ImGui::BeginMenu("Display")) {
        ImGui::Checkbox("test2", &test);
        ImGui::EndMenu();
    }
}

void DisplayPass::OverlayUpdate(SessionContext& scontext) {
}

void DisplayPass::PreRender(SessionContext& scontext) {
    //scontext.imdraw.PreRender(scontext);
}

void DisplayPass::Render(SessionContext& scontext) {
    if (scontext.pc.xrmode == 0) {
        Graphics::BeginRenderPass(scontext.pc, framebuffer);
        Pipeline pipeline(scontext.pipelinecache.GetGraphicsPipeline(scontext.pc,
                                                                     psobase, psodata));
        Graphics::Render(scontext.pc, pipeline, { &displaycbuf }, { .vertexcount = 3 });

    #if IMDRAW_DEBUG_RAYTRACE
        scontext.imdraw.DrawLines(scontext);
        scontext.imdraw.Render(scontext);
    #endif

        scontext.renderer.RenderImGui(scontext);
        Graphics::EndRenderPass(scontext.pc, framebuffer);
    }

    if (scontext.pc.xrmode > 0 && scontext.pc.headset.ShouldRender()) {
        scontext.pc.headset.BeginRenderPass(scontext.pc, xrframebuffer);

        Pipeline pipeline(scontext.pipelinecache.GetGraphicsPipeline(scontext.pc, xrpsobase,
                                                                     xrpsodata));

        Graphics::Render(scontext.pc, pipeline, { &xrdisplaycbuf }, { .vertexcount = 3 });

        scontext.pc.headset.EndRenderPass(scontext.pc, xrframebuffer);
    }
}

void DisplayPass::CleanupFramebuffer(SessionContext& scontext) {
    framebuffer.Shutdown(scontext.pc);
}

void DisplayPass::Shutdown(SessionContext& scontext) {
    DestroyBuffer(scontext.pc, displaycbuf);
    if (scontext.pc.xrmode > 0) {
        DestroyBuffer(scontext.pc, xrdisplaycbuf);
        xrframebuffer.Shutdown(scontext.pc);
        xrfblayout.Shutdown(scontext.pc);
    }

    CleanupFramebuffer(scontext);

    fblayout.Shutdown(scontext.pc);
}

}