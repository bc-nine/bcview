#include "mainhdrpass.h"
#include "../sessioncontext.h"
#include "../world.h"
#include "../renderer.h"
#include "../assetcache.h"
#include "../xrheadset.h"
#include "../surface.h"

namespace bc9 {

MainHDRPass::MainHDRPass(SessionContext& scontext) :
RenderPass("main"),
fblayout(1, scontext.pc.msaasamples, eFramebufferLayoutUseDepthStencilTarget),
xrfblayout(1, scontext.pc.msaasamples, eFramebufferLayoutUseDepthStencilTarget, eMultiview2Layer) {
    ColorLayout color("color", eResourceFormatR32G32B32A32_SFLOAT,
                      eColorLayoutShaderReadable |
                      eColorLayoutLoadOpClear    |
                      eColorLayoutStoreOpStore);
    fblayout.AddColorLayout(scontext.pc, color);
    ColorLayout xrcolor("xrcolor", eResourceFormatR32G32B32A32_SFLOAT,
                      eColorLayoutShaderReadable |
                      eColorLayoutLoadOpClear    |
                      eColorLayoutStoreOpStore);
    xrfblayout.AddColorLayout(scontext.pc, xrcolor);

    DepthStencilLayout ds(eResourceFormatD24_UNORM_S8_UINT, eDepthStencilLayoutDepthLoadOpClear);
    fblayout.AddDepthStencilLayout(scontext.pc, ds);
    xrfblayout.AddDepthStencilLayout(scontext.pc, ds);

    scontext.passcache.RegisterPass(passname);

    scontext.passcache.RegisterTarget(passname, "color");

    if (scontext.pc.xrmode > 0)
        scontext.passcache.RegisterTarget(passname, "xrcolor");
}

void MainHDRPass::Initialize(SessionContext& scontext) {
    fblayout.Initialize(scontext.pc, passname);
    psodata = GraphicsPipelineData(fblayout.layoutdata);
    scontext.passcache.UpdateFramebufferLayout(passname, fblayout);

    if (scontext.pc.xrmode > 0) {
        xrfblayout.Initialize(scontext.pc, passname);
        xrpsodata = GraphicsPipelineData(xrfblayout.layoutdata);
    }

    RecreateFramebuffer(scontext);
}

void MainHDRPass::CleanupFramebuffer(SessionContext& scontext) {
    framebuffer.Shutdown(scontext.pc);
    DestroyColorTarget(scontext.pc, colortarget);
    DestroyDepthStencilTarget(scontext.pc, dstarget);

    if (scontext.pc.xrmode > 0) {
        xrframebuffer.Shutdown(scontext.pc);
        DestroyColorTarget(scontext.pc, xrcolortarget);
        DestroyDepthStencilTarget(scontext.pc, xrdstarget);
    }
}

void MainHDRPass::RecreateFramebuffer(SessionContext& scontext) {
    uintn w, h;
    scontext.pc.surface.GetViewSize(w, h);

    CreateColorTargetParams colorparams("color", w, h);
    Throw_(CreateColorTarget(scontext.pc, fblayout, colorparams, colortarget));

    CreateDepthStencilTargetParams dsparams(w, h);
    Throw_(CreateDepthStencilTarget(scontext.pc, fblayout, dsparams, dstarget));

    scontext.passcache.UpdateTarget("main", "color", &colortarget.target.view);

    framebuffer.Initialize(scontext.pc, &fblayout, &colortarget, 1, &dstarget, w, h);
    framebuffer.SetClearColor("color", scontext.world.GetClearColor());

    if (scontext.pc.xrmode > 0) {
        uintn w, h;
        scontext.pc.headset.GetViewSize(w, h);

        ColorLayout clayout;
        fblayout.GetColorLayout("color", clayout);

        uintn viewcount = scontext.pc.headset.GetViewCount();
        CreateColorTargetParams xrcolorparams("xrcolor", w, h, viewcount);
        Throw_(CreateColorTarget(scontext.pc, xrfblayout, xrcolorparams, xrcolortarget));

        CreateDepthStencilTargetParams xrdsparams(w, h, viewcount);
        Throw_(CreateDepthStencilTarget(scontext.pc, xrfblayout, xrdsparams, xrdstarget));

        scontext.passcache.UpdateTarget("main", "xrcolor", &xrcolortarget.target.view);

        xrframebuffer.Initialize(scontext.pc, &xrfblayout, &xrcolortarget, 1, &xrdstarget, w, h);
    }
}

void MainHDRPass::ViewResized(SessionContext& scontext, uintn width, uintn height) {
    CleanupFramebuffer(scontext);
    RecreateFramebuffer(scontext);
}

void MainHDRPass::Shutdown(SessionContext& scontext) {
    CleanupFramebuffer(scontext);

    fblayout.Shutdown(scontext.pc);

    if (scontext.pc.xrmode > 0)
        xrfblayout.Shutdown(scontext.pc);
    scontext.passcache.ResetPass(passname);
}

void MainHDRPass::Update(SessionContext& scontext, float dt) {
}

void MainHDRPass::Render(SessionContext& scontext) {
    Framebuffer& fb = (scontext.pc.xrmode == 0) ? framebuffer : xrframebuffer;
    GraphicsPipelineData& gpd = (scontext.pc.xrmode == 0) ? psodata : xrpsodata;

    Graphics::BeginRenderPass(scontext.pc, fb);

    psodata.state.cullmode = eCullFront;
    scontext.renderer.RenderBin(scontext, eRenderBinEnvironment, gpd);

    psodata.state.cullmode = eCullBack;
    scontext.renderer.RenderBin(scontext, eRenderBinOpaque, gpd);

    Graphics::EndRenderPass(scontext.pc, fb);
}

}