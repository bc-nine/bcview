#include "prepass.h"
#include "imgui.h"
#include "../sessioncontext.h"
#include "../overlay.h"
#include "../renderer.h"
#include "../world.h"
#include "../imdraw.h"
#include "../graphicscommands.h"
#include "../assetcache.h"
// TODO: get rid of this platform-specific code!
#include "../util_vk.h"
#include "../resourceutil_vk.h"
#include "imgui_impl_vulkan.h"

namespace bc9 {

PrePass::PrePass(SessionContext& scontext) :
RenderPass("prepass"),
fblayout(1, 1, eFramebufferLayoutFlagsNone, eMultiview2Layer),
skybox(scontext) {
    ColorLayout color("color", eResourceFormatR8G8B8A8_SRGB,
                      eColorLayoutStoreOpStore | eColorLayoutShaderReadable);
    fblayout.AddColorLayout(scontext.pc, color);

    vertexkey = scontext.assetcache.FindShader("skybox-irradiance", eShaderAssetVertex);
    pixelkey = scontext.assetcache.FindShader("skybox-irradiance", eShaderAssetPixel);

    scontext.passcache.RegisterPass(passname);

    psobase = GraphicsPipelineBase(scontext.pc, vertexkey, pixelkey);
}

void PrePass::Initialize(SessionContext& scontext) {
    fblayout.Initialize(scontext.pc, passname);
    psodata = GraphicsPipelineData(fblayout.layoutdata);
    scontext.passcache.UpdateFramebufferLayout(passname, fblayout);

    CreateConstantBufferParams params(scontext.pc, sizeof(PreConstants), "prepass-constants");
    Throw_(CreateBuffer(scontext.pc, params, prepasscbuf));

    RecreateFramebuffer(scontext);

    skybox.Initialize(scontext, fblayout.layoutdata);
}

void PrePass::RecreateFramebuffer(SessionContext& scontext) {
    uintn w = 512;
    uintn h = 512;

    CreateColorTargetParams colorparams("color", w, h, 2);
    Throw_(CreateColorTarget(scontext.pc, fblayout, colorparams, colortarget));

    framebuffer.Initialize(scontext.pc, &fblayout, &colortarget, 1, nullptr, w, h);
}

void PrePass::Update(SessionContext& scontext, float dt) {
    uint64 worldupdate = scontext.world.Update();

    if (worldupdate & eWorldBackground) {
        skybox.Update(scontext);
        framebuffer.SetClearColor("color", scontext.world.GetClearColor());
    }

    if (skybox.Active())
        scontext.renderer.AddDrawData(skybox.drawdata, eRenderBinEnvironment);
}

void PrePass::OverlayUpdateMenuBar(SessionContext& scontext) {
}

void PrePass::OverlayUpdate(SessionContext& scontext) {
    #if 0
    //ImageKey key = scontext.assetcache.FindImage("default-environment-map");
    //ImageResource image = scontext.assetcache.GetImage(key);
    //ImageAsset asset = scontext.assetcache.GetImageAsset(key);

    static VkDescriptorSet imageds = nullptr;
    if (!imageds) {
        VkSampler& sampler = scontext.pc.samplers[0];
        VkImageViewCreateInfo viewinfo{ VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };
        viewinfo.image = colortarget.target.vkobj;
        viewinfo.viewType = VK_IMAGE_VIEW_TYPE_2D;

        VkFormat format;
        Ignore_(GetVkFormat(eResourceFormatR8G8B8A8_SRGB, format));
        viewinfo.format = format;

        viewinfo.components = {
            VK_COMPONENT_SWIZZLE_R,
            VK_COMPONENT_SWIZZLE_G,
            VK_COMPONENT_SWIZZLE_B,
            VK_COMPONENT_SWIZZLE_A
        };
        viewinfo.subresourceRange = {};
        viewinfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        viewinfo.subresourceRange.baseMipLevel = 0;
        viewinfo.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
        viewinfo.subresourceRange.baseArrayLayer = 0;
        viewinfo.subresourceRange.layerCount = 1;

        VkImageView imageview;
        Ignore_(CreateImageView(scontext.pc, viewinfo, "cube-test", imageview));
        imageds = ImGui_ImplVulkan_AddTexture(sampler, imageview,
                                              VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);
    }

    ImGui::Begin("Vulkan Texture");
    ImGui::Image((ImTextureID)imageds, ImVec2(512.0, 512.0));
    ImGui::End();
    #endif
}

void PrePass::Render(SessionContext& scontext) {
    Graphics::BeginRenderPass(scontext.pc, framebuffer);

    Pipeline pipeline(scontext.pipelinecache.GetGraphicsPipeline(scontext.pc, psobase, psodata));

    Graphics::Render(scontext.pc, pipeline, { &prepasscbuf }, { .vertexcount = 3 });

    Graphics::EndRenderPass(scontext.pc, framebuffer);
}

void PrePass::CleanupFramebuffer(SessionContext& scontext) {
    framebuffer.Shutdown(scontext.pc);
    DestroyColorTarget(scontext.pc, colortarget);
}

void PrePass::Shutdown(SessionContext& scontext) {
    DestroyBuffer(scontext.pc, prepasscbuf);

    CleanupFramebuffer(scontext);

    skybox.Shutdown(scontext);

    fblayout.Shutdown(scontext.pc);
}

}