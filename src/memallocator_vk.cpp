#include "memallocator_vk.h"
#include "allocator_vk.h"

#define VMA_IMPLEMENTATION
#include "vk_mem_alloc.h"

#include "platformcontext.h"

// when turning this on, beware an exception on shutdown, not sure what's going on here,
// but if it's a problem, just disable vulkan validation layers temporarily. UPDATE:
// since fixing my issue with garbage pointers (see nov 22 2021) this seems to work
// without crashing now on exit.
#define DEBUG_VMA_MEM_USAGE 0

namespace bc9 {

void MemAllocatorVk::CreateBuffer(PlatformContext& context, VkMemoryPropertyFlags properties,
                                  const VkBufferCreateInfo* bufferinfo, VkBuffer* buffer,
                                  Allocation& alloc, const char* name)  {
    assert(name && name[0] != 0);
    VmaAllocationCreateInfo alloccreateinfo = {};
    alloccreateinfo.requiredFlags = properties;
    alloccreateinfo.flags |= VMA_ALLOCATION_CREATE_USER_DATA_COPY_STRING_BIT;
    alloccreateinfo.pUserData = (void*) name;
    if (properties & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        alloccreateinfo.flags |= VMA_ALLOCATION_CREATE_MAPPED_BIT;

    VmaAllocationInfo allocinfo;
    VkCheck_(vmaCreateBuffer(allocator, bufferinfo, &alloccreateinfo, buffer, &alloc.vma,
                             &allocinfo));
    alloc.memory = allocinfo.deviceMemory;
    alloc.size = allocinfo.size;
    alloc.offset = allocinfo.offset;
    if (properties & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
        alloc.mappedmem = allocinfo.pMappedData;
        if (properties & VK_MEMORY_PROPERTY_HOST_CACHED_BIT)
            alloc.coherent = (properties & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
        else
            alloc.coherent = true;
    }
}

void MemAllocatorVk::CreateImage(PlatformContext& context, VkMemoryPropertyFlags properties,
                                 const VkImageCreateInfo* imageinfo, VkImage* image,
                                 Allocation& alloc, const char* name)  {
    assert(name && name[0] != 0);
    VmaAllocationCreateInfo alloccreateinfo = {};
    alloccreateinfo.requiredFlags = properties;
    alloccreateinfo.flags |= VMA_ALLOCATION_CREATE_USER_DATA_COPY_STRING_BIT;
    alloccreateinfo.pUserData = (void*) name;
    if (properties & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT)
        alloccreateinfo.flags |= VMA_ALLOCATION_CREATE_MAPPED_BIT;

    VmaAllocationInfo allocinfo;
    VkCheck_(vmaCreateImage(allocator, imageinfo, &alloccreateinfo, image, &alloc.vma,
                            &allocinfo));
    alloc.memory = allocinfo.deviceMemory;
    alloc.size = allocinfo.size;
    alloc.offset = allocinfo.offset;
    if (properties & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
        // i want to be notified when this is nonzero, do i need to add the offset to pMappedData?
        Assert_(allocinfo.offset == 0);
        alloc.mappedmem = allocinfo.pMappedData;
        if (properties & VK_MEMORY_PROPERTY_HOST_CACHED_BIT)
            alloc.coherent = (properties & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);
        else
            alloc.coherent = true;
    }
}

void MemAllocatorVk::DestroyBuffer(PlatformContext& context, VkBuffer buffer, Allocation& alloc) {
    vmaDestroyBuffer(allocator, buffer, alloc.vma);
}

void MemAllocatorVk::DestroyImage(PlatformContext& context, VkImage image, Allocation& alloc) {
    vmaDestroyImage(allocator, image, alloc.vma);
}

void MemAllocatorVk::Initialize(PlatformContext& context) {
    VmaAllocatorCreateInfo allocatorinfo = {};
    allocatorinfo.flags = VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT;
    allocatorinfo.physicalDevice = context.physicaldevice;
    allocatorinfo.device = context.device;
    allocatorinfo.instance = context.instance;
    allocatorinfo.vulkanApiVersion = VK_API_VERSION_1_3;
    #if DEBUG_VMA_MEM_USAGE
        VmaRecordSettings recordsettings{};
        recordsettings.pFilePath = "C:\\Users\\bcollins\\bcview-vma.csv";
        allocatorinfo.pRecordSettings = &recordsettings;
    #endif
    allocatorinfo.pAllocationCallbacks = VkAllocators();

    // there is now a pVulkanFunctions member, but we'll leave it as null, and define
    // VMA_STATIC_VULKAN_FUNCTIONS to 1 (see memallocator_vk.h). as i understand it,
    // volk has declarations for the function pointers, which replace the prototypes
    // provided by vulkan_core.h (this is why we use VK_NO_PROTOTYPES). volk declares
    // function pointers and then points them to driver implementations at runtime,
    // as opposed to declaring prototypes and linking statically with vulkan-1.lib.
    // it appears that inside vma, the pointers it sees are the ones from volk, which
    // means using static is fine, so that's what i'm going with for now.
    VkCheck_(vmaCreateAllocator(&allocatorinfo, &allocator));
}

void MemAllocatorVk::Shutdown(PlatformContext& context) {
    // enable when trying to e.g. find memory leaks
    #if DEBUG_VMA_MEM_USAGE
        char* stats;
        vmaBuildStatsString(allocator, &stats, true);
        printf("%s\n", stats);
        vmaFreeStatsString(allocator, stats);
    #endif
    vmaDestroyAllocator(allocator);
}

}
