#pragma once

#include "shaderresource.h"
#include "shaderasset.h"
#include "assetcache.h"
#include "pipelinestate.h"
#include "pipelinekeys.h"
#include "pipelines.h"
#include "bclib/stdjson.h"
#include "bclib/stdutility.h"
#include "bclib/stdmap.h"
#include "bclib/stdarray.h"

namespace bc9 {

class SessionContext;

class PipelineCache {
public:
    PipelineCache() {}

    void Initialize(const SessionContext &scontext);
    void Shutdown(const SessionContext &scontext);

    PipelineBaseKey RegisterBaseGraphicsPipeline(const SessionContext &scontext,
                                                 const GraphicsPipelineBase &base,
                                                 const GraphicsPipelineState &state);

    Pipeline GetGraphicsPipeline(const SessionContext &scontext, PipelineBaseKey basekey,
                                 const GraphicsPipelineState &state);

    PipelineBaseKey RegisterBaseComputePipeline(const SessionContext &scontext,
                                                const ComputePipelineBase &base);

    Pipeline GetComputePipeline(const SessionContext &scontext, PipelineBaseKey basekey,
                                const ComputePipelineState &state);

    typedef stdpair<PipelineBaseKey, PipelineStateKey> PipelineKeys;
    typedef stdpair<GraphicsPipelineBase, Pipeline> BaseGraphicsPipelinePair;
    stdmap<PipelineKeys, Pipeline> pipelines;
    stdmap<PipelineBaseKey, BaseGraphicsPipelinePair> basegraphicspipelines;
};

}
