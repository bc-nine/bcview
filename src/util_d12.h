#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h> // For HRESULT
#include <exception>
#include <dxgiformat.h>
#include <d3dcommon.h>
#include "d3d12.h"
#include "bclib/errorutils.h"
#include "rhitypes.h"

namespace bc9 {

class PlatformContext;

Result GetD12CullMode(CullMode mode, D3D12_CULL_MODE& d12mode);

Result GetD12CompareOp(CompareOp op, D3D12_COMPARISON_FUNC& d12func);

Result GetD12StencilOp(StencilOp op, D3D12_STENCIL_OP& d12op);

Result GetResourceState(ImageLayout layout, D3D12_RESOURCE_STATES& state);

Result GetPolygonMode(PolygonMode mode, D3D12_FILL_MODE& d12mode);

Result GetDescriptorRangeType(D3D_SHADER_INPUT_TYPE inputtype,
                              D3D12_DESCRIPTOR_RANGE_TYPE &rangetype);

Result GetDXGIFormat(const PlatformContext& context, const ResourceFormat format,
                     DXGI_FORMAT& dxgiformat);

Result GetShaderInputType(const StringID& strtype, D3D_SHADER_INPUT_TYPE& type);

Result GetSRVDimension(const StringID& strdim, D3D_SRV_DIMENSION& dim);

}