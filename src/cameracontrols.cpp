#include "cameracontrols.h"
#include "world.h"
#include "bclib/mathutils.h"
#include "sessioncontext.h"
#include "nodecache.h"
#include "renderer.h"

using namespace glm;

namespace bc9 {

void ExtractYawPitch(const quat& rot, float &yaw, float &pitch) {
    // see notes for an explanation
    vec3 fwd(Camera::GetForwardVector(rot));
    vec3 p(-fwd);
    float phi = atan2(p.x, p.z);
    float theta = acos(p.y);
    yaw = phi;
    pitch = -(1.570796f - theta);
}

CameraControls::CameraControls(const stdjson& bundlejson) {
    bool found = false;
    Assert_(bundlejson["world"].contains("camera-controls"));
    const stdjson& controlsjson = bundlejson["world"]["camera-controls"];
    StringID mode;
    Throw_(OptString(controlsjson, "mode", mode, found));
    if (found) {
        if (mode == "ttz"_sid)
            cameracontroller = &ttzcontrols;
        else
            cameracontroller = &flycontrols;
    }
    else {
        cameracontroller = &flycontrols;
    }

    Throw_(OptFloat(controlsjson, "linear-speed", speed.x, found));
    Throw_(OptFloat(controlsjson, "angular-speed", speed.y, found));

    stdjson cameranodejson = controlsjson["nodes"][0];
    Throw_(GetString(cameranodejson, activecameranode));
}

void CameraControls::Initialize(SessionContext& scontext) {
    uintn cameranodeidx = scontext.nodecache.FindNode(activecameranode);
    uint worldnodeidx = scontext.nodecache.FindNode("world");

    ttzcontrols.SetSpeed(scontext, speed);
    ttzcontrols.SetCameraNode(scontext, cameranodeidx);
    ttzcontrols.SetWorldNode(scontext, worldnodeidx);

    flycontrols.SetSpeed(scontext, speed);
    flycontrols.SetCameraNode(scontext, cameranodeidx);
}

void CameraControls::Shutdown(SessionContext& scontext) {
}

void CameraControls::LeftButtonDown(SessionContext& scontext, const ButtonEvent& event) {
    cameracontroller->LeftButtonDown(scontext, event);
}

void CameraControls::LeftButtonMotion(SessionContext& scontext, const MotionEvent& event) {
    cameracontroller->LeftButtonMotion(scontext, event);
}

void CameraControls::KeyUp(SessionContext& scontext, const KeyEvent& event) {
    cameracontroller->KeyUp(scontext, event);
}

void CameraControls::KeyDown(SessionContext& scontext, const KeyEvent& event) {
    switch (event.key) {
        case e0Key:
        break;

        case e1Key:
        break;

        case e2Key:
        break;

        case e3Key:
        break;

        case e4Key:
        break;

        case eFKey:
        cameracontroller = &flycontrols;
        break;

        case eTKey:
        cameracontroller = &ttzcontrols;
        break;

        default:
        cameracontroller->KeyDown(scontext, event);
        break;
    }
}

void CameraControls::Update(SessionContext& scontext, float dt) {
    cameracontroller->Update(scontext, dt);
}

FlyControls::FlyControls() :
fwdinterp(0.5f),
rightinterp(0.5f),
upinterp(0.5f),
yawinterp(0.5f),
pitchinterp(0.5f) {
}

void FlyControls::SetSpeed(SessionContext& scontext, const glm::vec2& speed) {
    fwdinterp.SetMinMax(speed.x);
    rightinterp.SetMinMax(speed.x);
    upinterp.SetMinMax(speed.x);
    yawinterp.SetMinMax(speed.y);
    pitchinterp.SetMinMax(speed.y);
}

void FlyControls::LeftButtonDown(SessionContext& scontext, const ButtonEvent& event) {
    prevposition = event.position;
    scontext.renderer.SetRayTraceAccumulation(false);
};

void FlyControls::LeftButtonMotion(SessionContext& scontext, const MotionEvent& event) {
    if (prevposition != event.position) {
        vec2 mvec(event.position.x-prevposition.x, prevposition.y-event.position.y);
        mvec *= 0.001f;
        yaw -= mvec.x;
        pitch += mvec.y;
        prevposition = event.position;
        scontext.renderer.SetRayTraceAccumulation(false);
    }
}

void FlyControls::KeyDown(SessionContext& scontext, const KeyEvent& event) {
    bool mod = true;
    switch (event.key) {
        case eUpKey:    pitchinterp.Start(true); break;
        case eDownKey:  pitchinterp.Start(false); break;
        case eLeftKey:  yawinterp.Start(true); break;
        case eRightKey: yawinterp.Start(false); break;
        case eWKey:     fwdinterp.Start(true); break;
        case eSKey:     fwdinterp.Start(false); break;
        case eDKey:     rightinterp.Start(true); break;
        case eAKey:     rightinterp.Start(false); break;
        case eEKey:     upinterp.Start(true); break;
        case eCKey:     upinterp.Start(false); break;
        default: mod = false; break;
    }

    if (mod)
        scontext.renderer.SetRayTraceAccumulation(false);
}

void FlyControls::KeyUp(SessionContext& scontext, const KeyEvent& event) {
    bool mod = true;
    switch (event.key) {
        case eUpKey:
        case eDownKey:  pitchinterp.Stop(); mod = true; break;
        case eLeftKey:
        case eRightKey: yawinterp.Stop(); mod = true; break;
        case eWKey:
        case eSKey:     fwdinterp.Stop(); mod = true; break;
        case eDKey:
        case eAKey:     rightinterp.Stop(); mod = true; break;
        case eEKey:
        case eCKey:     upinterp.Stop(); mod = true; break;
        default: mod = false; break;
    }

    if (mod)
        scontext.renderer.SetRayTraceAccumulation(false);
}

void FlyControls::Update(SessionContext& scontext, float dt) {
    vec3 pos = scontext.nodecache.NodeTranslation(cameranodeidx);
    quat rot = scontext.nodecache.NodeRotation(cameranodeidx);

    if (fwdinterp.Active())
        pos += Camera::GetForwardVector(rot)*fwdinterp.Update(dt);
    if (rightinterp.Active())
        pos += Camera::GetRightVector(rot)*rightinterp.Update(dt);
    if (upinterp.Active())
        pos += vec3(0.f, 1.f, 0.f)*upinterp.Update(dt);
    if (yawinterp.Active())
        yaw += yawinterp.Update(dt);
    if (pitchinterp.Active())
        pitch += pitchinterp.Update(dt);

    pitch = Clamp(pitch, -1.5f, 1.5f);

    quat q(angleAxis(yaw, vec3(0.f,1.f,0.f)) * angleAxis(pitch, vec3(1.f,0.f,0.f)));
    scontext.nodecache.SetNodeRotation(cameranodeidx, q);
    scontext.nodecache.SetNodeTranslation(cameranodeidx, pos);
}

void FlyControls::SetCameraNode(SessionContext& scontext, uintn nodeidx) {
    cameranodeidx = nodeidx;
    quat rot = scontext.nodecache.NodeRotation(cameranodeidx);
    ExtractYawPitch(rot, yaw, pitch);
}

TiltTwirlControls::TiltTwirlControls() :
startorient(quat(1.f, 0.f, 0.f, 0.f)) {
}

void TiltTwirlControls::SetSpeed(SessionContext& scontext, const glm::vec2& speed) {
    fwdinterp.SetMinMax(speed.x);
}

void TiltTwirlControls::SetCameraNode(SessionContext& scontext, uintn nodeidx) {
    cameranodeidx = nodeidx;
}

void TiltTwirlControls::SetWorldNode(SessionContext& scontext, uintn nodeidx) {
    worldnodeidx = nodeidx;
}

void TiltTwirlControls::LeftButtonDown(SessionContext& scontext, const ButtonEvent& event) {
    prevposition = event.position;
    if (!objectspace) {
        tilt = twirl = 0.f;
        startorient = scontext.nodecache.NodeRotation(worldnodeidx);
    }
    else {
        startorient = quat(1.f,0.f,0.,0.f);
    }
    scontext.renderer.SetRayTraceAccumulation(false);
}

void TiltTwirlControls::LeftButtonMotion(SessionContext& scontext, const MotionEvent& event) {
    if (prevposition != event.position) {
        // we want rightward and downward movement to be positive,
        // because that follows right hand rule for quaternions.
        vec2 mvec(event.position.x-prevposition.x, event.position.y-prevposition.y);
        mvec *= 0.005f;
        tilt += mvec.y;
        twirl += mvec.x;
        prevposition = event.position;
        //PrintI_("tilt: %0.4f  twirl %0.4f", tilt, twirl);
        scontext.renderer.SetRayTraceAccumulation(false);
    }
}

void TiltTwirlControls::KeyDown(SessionContext& scontext, const KeyEvent& event) {
    bool mod = false;
    switch (event.key) {
        case eUpKey:    break;
        case eDownKey:  break;
        case eLeftKey:  break;
        case eRightKey: break;
        case eWKey:     fwdinterp.Start(true); mod = true; break;
        case eSKey:     fwdinterp.Start(false); mod = true; break;
        case eDKey:     break;
        case eAKey:     break;
        case eEKey:     break;
        case eCKey:     break;
        case eOKey:     ToggleObjectSpace(scontext); mod = true; break;
    }

    if (mod)
        scontext.renderer.SetRayTraceAccumulation(false);
}

void TiltTwirlControls::KeyUp(SessionContext& scontext, const KeyEvent& event) {
    bool mod = false;
    switch (event.key) {
        case eUpKey:
        case eDownKey: break;
        case eLeftKey:
        case eRightKey: break;
        case eWKey:
        case eSKey:     fwdinterp.Stop(); mod = true; break;
        case eDKey:
        case eAKey: break;
        case eEKey:
        case eCKey: break;
    }

    if (mod)
        scontext.renderer.SetRayTraceAccumulation(false);
}

void TiltTwirlControls::Update(SessionContext& scontext, float dt) {
    vec3 pos = scontext.nodecache.NodeTranslation(cameranodeidx);
    quat rot = scontext.nodecache.NodeRotation(cameranodeidx);
    if (fwdinterp.Active())
        pos += Camera::GetForwardVector(rot)*fwdinterp.Update(dt);
    quat q(angleAxis(twirl, vec3(0.f,1.f,0.f)) * angleAxis(tilt, vec3(1.f,0.f,0.f)));
    quat rotation = q * startorient;
    scontext.nodecache.SetNodeRotation(worldnodeidx, rotation);
    scontext.nodecache.SetNodeTranslation(cameranodeidx, pos);

    mat4 R = toMat4(rotation);
    mat4 view = transpose(R);
    view[3] = vec4(-dot(vec3(R[0]), pos), -dot(vec3(R[1]), pos), -dot(vec3(R[2]), pos), 1.0f);
}

void TiltTwirlControls::UseObjectSpace(SessionContext& scontext, bool mode) {
    objectspace = mode;
    tilt = twirl = 0.f;
    scontext.nodecache.SetNodeRotation(worldnodeidx, quat(1.f, 0.f, 0.f, 0.f));
    scontext.renderer.SetRayTraceAccumulation(false);
}

void TiltTwirlControls::ToggleObjectSpace(SessionContext& scontext) {
    UseObjectSpace(scontext, !objectspace);
}

}