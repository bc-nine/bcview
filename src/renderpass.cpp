#include "renderpass.h"
#include "sessioncontext.h"

namespace bc9 {

RenderPass::RenderPass(const StringID& name) :
passname(name) {
}

void RenderPass::Initialize(SessionContext& scontext) {
}

void RenderPass::PostInitialize(SessionContext& scontext) {
}

void RenderPass::Shutdown(SessionContext& scontext) {
}

void RenderPass::PreRender(SessionContext& scontext) {
}

void RenderPass::Render(SessionContext& scontext) {
}

void RenderPass::PostRender(SessionContext& scontext) {
}

void RenderPass::Update(SessionContext& scontext, float dt) {
}

void RenderPass::Begin(SessionContext& scontext) {
}

void RenderPass::End(SessionContext& scontext) {
}

void RenderPass::OverlayUpdateMenuBar(SessionContext& scontext) {
}

void RenderPass::OverlayUpdate(SessionContext& scontext) {
}

void RenderPass::ViewResized(SessionContext& scontext, uintn width, uintn height){
}

void RenderPass::CaptureRenderState(RenderState& state) {
}

}