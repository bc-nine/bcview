#pragma once

#if winvk_
#include "shaderbinding_vk.h"
#elif wind12_
#include "shaderbinding_d12.h"
#endif