#pragma once

#include "volk.h"

namespace bc9 {

class SessionContext;

struct CommandBuffer {
	void BeginGraphics(SessionContext& scontext);
	void EndGraphics(SessionContext& scontext);

    VkCommandBuffer vkobj = nullptr;
};

}