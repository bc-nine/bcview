#pragma once

#include "glm/glm.hpp"
#include <wrl.h>
#include <d3d12.h>
#include "types_d12.h"
#include "bclib/errorutils.h"
#include "bclib/stringid.h"
#include "bclib/stdvector.h"

namespace bc9 {

class ColorTarget;
class DepthStencilTarget;
class PlatformContext;
class FramebufferLayout;

class Framebuffer {
public:
    // platform-independent interface
    Framebuffer();

    void Initialize(PlatformContext& context, const FramebufferLayout* layout,
                    ColorTarget* colortargets, unsigned int numcolortargets,
                    DepthStencilTarget* dstarget, unsigned int width, unsigned int height,
                    unsigned int layers = 1);
    void Shutdown(PlatformContext& context);

    void SetColorClear(const StringID& name, const glm::vec4 clear);
    void SetDepthClear(float depth);
    void SetStencilClear(unsigned int stencil);
    void SetStencilReference(uint32 ref);

    // platform-dependent interface
    void Bind(PlatformContext& context);
    void Clear(PlatformContext& context);

    void PreTransitions(PlatformContext& context);
    void PostTransitions(PlatformContext& context);

    const FramebufferLayout* layout = nullptr;

    stdvector<ColorTarget*> colortargets;
    DepthStencilTarget* dstarget = nullptr;

    RTVHandle rtvhandle;
    DSVHandle dsvhandle;

    // TODO: move clearcolor, cleardepth, clearstencil here like we're doing with stencilref
    uint32 stencilref = 0;
};

}