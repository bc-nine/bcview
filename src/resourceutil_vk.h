#pragma once

namespace bc9 {

struct CommandBuffer;

struct TransitionVkImageParams {
    CommandBuffer* usecmdbuffer = nullptr;
    VkImageMemoryBarrier barrier = { .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                                     .srcAccessMask = kInvalidUInt32,
                                     .dstAccessMask = kInvalidUInt32,
                                     .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                     .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED };
    VkPipelineStageFlags srcstagemask = kInvalidUInt32;
    VkPipelineStageFlags dststagemask = kInvalidUInt32;

    bool computemasks = false;
};

extern Result CreateImage(PlatformContext& context, CreateVkImageParams& params,
                          ImageResource& image);

extern Result CreateImageView(PlatformContext& context, const VkImageViewCreateInfo& createinfo,
                              const char* name, VkImageView& imageview);

extern Result CreateBuffer(PlatformContext& context, const CreateVkBufferParams& params,
                           BufferResource& buffer);

extern Result CreateBufferView(PlatformContext& context, VkBufferViewCreateInfo& createinfo,
                               const char* name, VkBufferView& bufferview);

extern void TransitionImage(PlatformContext& context, TransitionVkImageParams& params);

}