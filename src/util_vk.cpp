#include "util_vk.h"
#include <assert.h>
#include <stdexcept>
#include "platformcontext.h"
#include "allocator_vk.h"
#include "resourceutil_vk.h"
#include "rhitypes.h"
#include "bclib/stdset.h"
#include "bclib/stdvector.h"
#include "bclib/stdallocators.h"

namespace bc9 {

Result GetDescriptorType(const StringID &strtype, VkDescriptorType& dtype) {
    switch (strtype.Hash()) {
        case "VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER"_sid:
        dtype = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        break;

        case "VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC"_sid:
        dtype = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
        break;

        case "VK_DESCRIPTOR_TYPE_STORAGE_BUFFER"_sid:
        dtype = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
        break;

        case "VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC"_sid:
        dtype = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
        break;

        case "VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER"_sid:
        dtype = VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
        break;

        case "VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER"_sid:
        dtype = VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
        break;

        case "VK_DESCRIPTOR_TYPE_SAMPLER"_sid:
        dtype = VK_DESCRIPTOR_TYPE_SAMPLER;
        break;

        case "VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE"_sid:
        dtype = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
        break;

        case "VK_DESCRIPTOR_TYPE_STORAGE_IMAGE"_sid:
        dtype = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
        break;

        case "VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR"_sid:
        dtype = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
        break;

        case "VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER"_sid:
        dtype = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
        break;

        default:
        return Error("%s: unknown descriptor type", strtype.str());
        break;
    }

    return Success();
}

Result GetLocationFormat(const StringID &strformat, VkFormat& format) {
    switch (strformat.Hash()) {
        case "VK_FORMAT_R32_SFLOAT"_sid:
        format = VK_FORMAT_R32_SFLOAT;
        break;

        case "VK_FORMAT_R32G32_SFLOAT"_sid:
        format = VK_FORMAT_R32G32_SFLOAT;
        break;

        case "VK_FORMAT_R32G32B32_SFLOAT"_sid:
        format = VK_FORMAT_R32G32B32_SFLOAT;
        break;

        case "VK_FORMAT_R32G32B32A32_SFLOAT"_sid:
        format = VK_FORMAT_R32G32B32A32_SFLOAT;
        break;

        default:
        return Error("%s: unknown location format", strformat.str());
        break;
    }

    return Success();
}

// no need for VK_MEMORY_PROPERTY_HOST_COHERENT_BIT unless you also have _CACHED set
Result GetMemoryPropertyFlags(BufferCPUAccessMode accessmode, VkMemoryPropertyFlags& flags) {
    if (accessmode == eBufferCPUAccessRead || accessmode == eBufferCPUAccessWrite) {
        flags |= VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
    }
    else {
        flags |= VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    }

    return Success();
}

// no need for VK_MEMORY_PROPERTY_HOST_COHERENT_BIT unless you also have _CACHED set
Result GetMemoryPropertyFlags(ImageCPUAccessMode accessmode, VkMemoryPropertyFlags& flags) {
    if (accessmode == eImageCPUAccessRead || accessmode == eImageCPUAccessWrite) {
        flags |= VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
    }
    else {
        flags |= VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    }

    return Success();
}

Result GetVkPrimitiveTopology(PrimitiveTopology topology, VkPrimitiveTopology& vktopology) {
    switch (topology) {
        case ePrimitiveTriangleList:
        vktopology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
        break;

        case ePrimitiveLineList:
        vktopology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
        break;

        case ePrimitivePointList:
        vktopology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
        break;

        default:
        return Error("invalid primitive topology specified");
        break;
    }

    return Success();
}

Result GetVkPolygonMode(PolygonMode mode, VkPolygonMode& vkmode) {
    switch (mode) {
        case ePolygonModeFill:
        vkmode = VK_POLYGON_MODE_FILL;
        break;

        case ePolygonModeLine:
        vkmode = VK_POLYGON_MODE_LINE;
        break;

        case ePolygonModePoint:
        vkmode = VK_POLYGON_MODE_POINT;
        break;

        default:
        return Error("invalid polygon mode specified");
        break;
    }

    return Success();
}

Result GetVkImageLayout(ImageLayout layout, VkImageLayout& vklayout) {
    switch (layout) {
        case eImageLayoutUndefined:
        vklayout = VK_IMAGE_LAYOUT_UNDEFINED;
        return Success();
        break;

        case eImageLayoutPresent:
        vklayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        return Success();
        break;

        case eImageLayoutColorOutput:
        vklayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        return Success();
        break;

        case eImageLayoutShaderReadOnly:
        vklayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        return Success();
        break;

        case eImageLayoutShaderReadWrite:
        vklayout = VK_IMAGE_LAYOUT_GENERAL;
        return Success();
        break;

        case eImageLayoutDepthStencilOutput:
        vklayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
        return Success();
        break;

        case eImageLayoutDepthStencilReadOnly:
        vklayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
        return Success();
        break;

        case eImageLayoutTransferSource:
        vklayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        return Success();
        break;

        case eImageLayoutTransferDestination:
        vklayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        return Success();
        break;
    }

    return Error("invalid image layout specified");
}

Result GetPipelineBindPoint(PipelineType pipelinetype, VkPipelineBindPoint &bindpoint) {
    if (pipelinetype == ePipelineGraphics)
        bindpoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    else if (pipelinetype == ePipelineCompute)
        bindpoint = VK_PIPELINE_BIND_POINT_COMPUTE;
    else if (pipelinetype == ePipelineRayTracing)
        bindpoint = VK_PIPELINE_BIND_POINT_RAY_TRACING_KHR;

    return bindpoint == ePipelineTypeCount ? Error("invalid pipeline bind point") : Success();
}

Result GetVkAttachmentLoadOp(ColorLayoutFlags flags, VkAttachmentLoadOp& vkop) {
    if (flags & eColorLayoutLoadOpClear) {
        assert((flags & eColorLayoutLoadOpPreserve) == 0);
        vkop = VK_ATTACHMENT_LOAD_OP_CLEAR;
    }
    else if (flags & eColorLayoutLoadOpPreserve) {
        assert((flags & eColorLayoutLoadOpClear) == 0);
        vkop = VK_ATTACHMENT_LOAD_OP_LOAD;
    }
    else {
        vkop = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    }

    return Success();
}

Result GetVkAttachmentDepthLoadOp(DepthStencilLayoutFlags flags, VkAttachmentLoadOp& vkop) {
    if (flags & eDepthStencilLayoutDepthLoadOpClear) {
        assert((flags & eDepthStencilLayoutDepthLoadOpPreserve) == 0);
        vkop = VK_ATTACHMENT_LOAD_OP_CLEAR;
    }
    else if (flags & eDepthStencilLayoutDepthLoadOpPreserve) {
        assert((flags & eDepthStencilLayoutDepthLoadOpClear) == 0);
        vkop = VK_ATTACHMENT_LOAD_OP_LOAD;
    }
    else {
        vkop = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    }

    return Success();
}

Result GetVkAttachmentStencilLoadOp(DepthStencilLayoutFlags flags, VkAttachmentLoadOp& vkop) {
    if (flags & eDepthStencilLayoutStencilLoadOpClear) {
        assert((flags & eDepthStencilLayoutStencilLoadOpPreserve) == 0);
        vkop = VK_ATTACHMENT_LOAD_OP_CLEAR;
    }
    else if (flags & eDepthStencilLayoutStencilLoadOpPreserve) {
        assert((flags & eDepthStencilLayoutStencilLoadOpClear) == 0);
        vkop = VK_ATTACHMENT_LOAD_OP_LOAD;
    }
    else {
        vkop = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    }

    return Success();
}

Result GetVkStencilOp(StencilOp op, VkStencilOp& vkop) {
    vkop = (VkStencilOp) op;

    return Success();
}

Result GetVkCullMode(CullMode mode, VkCullModeFlags& flags) {
    switch (mode) {
        case eCullNone:
        flags = VK_CULL_MODE_NONE;
        break;

        case eCullFront:
        flags = VK_CULL_MODE_FRONT_BIT;
        break;

        case eCullBack:
        flags = VK_CULL_MODE_BACK_BIT;
        break;

        default:
        return Error("only none, front, and back cull modes are supported");
        break;
    }

    return Success();
}

Result GetVkAttachmentStoreOp(ColorLayoutFlags flags, VkAttachmentStoreOp& vkop) {
    if (flags & eColorLayoutStoreOpStore)
        vkop = VK_ATTACHMENT_STORE_OP_STORE;
    else
        vkop = VK_ATTACHMENT_STORE_OP_DONT_CARE;

    return Success();
}

Result GetVkAttachmentDepthStoreOp(DepthStencilLayoutFlags flags, VkAttachmentStoreOp& vkop) {
    if (flags & eDepthStencilLayoutDepthStoreOpStore)
        vkop = VK_ATTACHMENT_STORE_OP_STORE;
    else
        vkop = VK_ATTACHMENT_STORE_OP_DONT_CARE;

    return Success();
}

Result GetVkAttachmentStencilStoreOp(DepthStencilLayoutFlags flags, VkAttachmentStoreOp& vkop) {
    if (flags & eDepthStencilLayoutStencilStoreOpStore)
        vkop = VK_ATTACHMENT_STORE_OP_STORE;
    else
        vkop = VK_ATTACHMENT_STORE_OP_DONT_CARE;

    return Success();
}

Result GetVkCompareOp(CompareOp op, VkCompareOp& vkop) {
    vkop = (VkCompareOp) op;

    return Success();
}

Result GetVkSampleCount(unsigned int numsamples, VkSampleCountFlagBits& bits) {
    switch (numsamples) {
        case 1:
        bits = VK_SAMPLE_COUNT_1_BIT;
        return Success();
        break;

        case 2:
        bits = VK_SAMPLE_COUNT_2_BIT;
        return Success();
        break;

        case 4:
        bits = VK_SAMPLE_COUNT_4_BIT;
        return Success();
        break;

        case 8:
        bits = VK_SAMPLE_COUNT_8_BIT;
        return Success();
        break;

        case 16:
        bits = VK_SAMPLE_COUNT_16_BIT;
        return Success();
        break;

        case 32:
        bits = VK_SAMPLE_COUNT_32_BIT;
        return Success();
        break;

        case 64:
        bits = VK_SAMPLE_COUNT_64_BIT;
        return Success();
        break;
    }

    return Error("%u: invalid value for numsamples", numsamples);
}

Result GetVkFormat(ResourceFormat format, VkFormat& vkformat) {
    if (format == eResourceFormatInvalid)
        return Error("invalid resource format");

    vkformat = (VkFormat) format;

    return Success();
}

Result GetResourceFormat(VkFormat vkformat, ResourceFormat& format) {
    format = (ResourceFormat) vkformat;

    return Success();
}

Result GetVkSamplerFilter(SamplerFilter& filter, VkFilter& vkfilter) {
    switch (filter) {
        case eSamplerNearest:
        vkfilter = VK_FILTER_NEAREST;
        break;

        case eSamplerLinear:
        vkfilter = VK_FILTER_LINEAR;
        break;

        default:
        return Error("GetVkSamplerFilter(): unknown filter");
        break;
    }

    return Success();
}

Result GetVkSamplerMipmapFilter(SamplerMipmapFilter& filter, VkSamplerMipmapMode& vkfilter) {
    switch (filter) {
        case eSamplerNearest:
        vkfilter = VK_SAMPLER_MIPMAP_MODE_NEAREST;
        break;

        case eSamplerLinear:
        vkfilter = VK_SAMPLER_MIPMAP_MODE_LINEAR;
        break;

        default:
        return Error("GetVkSamplerMipmapFilter(): unknown mipmap filter");
        break;
    }

    return Success();
}

Result GetVkSamplerMode(SamplerMode& mode, VkSamplerAddressMode& vkmode) {
    switch (mode) {
        case eSamplerRepeat:
        vkmode = VK_SAMPLER_ADDRESS_MODE_REPEAT;
        break;

        case eSamplerMirroredRepeat:
        vkmode = VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;
        break;

        case eSamplerClampToEdge:
        vkmode = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;
        break;

        case eSamplerClampToBorder:
        vkmode = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;
        break;

        case eSamplerMirrorClampToEdge:
        vkmode = VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
        break;

        default:
        return Error("GetVkSamplerMode(): unknown sampler mode");
        break;
    }

    return Success();
}

Result GetVkShaderStage(ShaderAssetType type, VkShaderStageFlagBits& stage) {
    switch (type) {
        case eShaderAssetVertex:
        stage = VK_SHADER_STAGE_VERTEX_BIT;
        break;

        case eShaderAssetPixel:
        stage = VK_SHADER_STAGE_FRAGMENT_BIT;
        break;

        case eShaderAssetCompute:
        stage = VK_SHADER_STAGE_COMPUTE_BIT;
        break;

        case eShaderAssetRay:
        stage = VK_SHADER_STAGE_RAYGEN_BIT_KHR;
        break;

        case eShaderAssetMiss:
        stage = VK_SHADER_STAGE_MISS_BIT_KHR;
        break;

        case eShaderAssetClosestHit:
        stage = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR;
        break;

        default:
        return Error("GetVkShaderStage(): unknown shader type");
        break;
    }

    return Success();
}

Result CreateSampler(PlatformContext& context, VkSamplerCreateInfo& createinfo,
                     VkSampler& sampler, const StringID& name) {
    VkCheck_(vkCreateSampler(context.device, &createinfo, VkAllocators(), &sampler));

    NameVulkanObject(context.device, VK_OBJECT_TYPE_SAMPLER, sampler, name);

    return Success();
}

Result CreatePipelineLayout(PlatformContext& context, VkPipelineLayoutCreateInfo& createinfo,
                            VkPipelineLayout& layout, const StringID& name) {
    VkCheck_(vkCreatePipelineLayout(context.device, &createinfo, VkAllocators(),
                                    &layout));

    NameVulkanObject(context.device, VK_OBJECT_TYPE_PIPELINE_LAYOUT, layout, name);

    return Success();
}

Result CreateDescriptorSetLayout(PlatformContext& context,
                                 VkDescriptorSetLayoutCreateInfo& createinfo,
                                 VkDescriptorSetLayout& layout, const StringID& name) {
    VkCheck_(vkCreateDescriptorSetLayout(context.device, &createinfo, VkAllocators(),
                                         &layout));

    NameVulkanObject(context.device, VK_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT, layout, name);

    return Success();
}

Result CreateRenderPass(PlatformContext& context, VkRenderPassCreateInfo& createinfo,
                        VkRenderPass& renderpass, const StringID& name) {
    VkCheck_(vkCreateRenderPass(context.device, &createinfo, VkAllocators(), &renderpass));

    NameVulkanObject(context.device, VK_OBJECT_TYPE_RENDER_PASS, renderpass, name);

    return Success();
}

static Result FindSupportedFormat(VkPhysicalDevice physicaldevice,
                                  const stdvector<VkFormat>& candidates,
                                  VkImageTiling tiling, VkFormatFeatureFlags features,
                                  VkFormat& outformat) {
    for (VkFormat format : candidates) {
        VkFormatProperties props;
        vkGetPhysicalDeviceFormatProperties(physicaldevice, format, &props);

        if (tiling == VK_IMAGE_TILING_LINEAR &&
            (props.linearTilingFeatures & features) == features) {
            outformat = format;
            return Success();
        }
        else if (tiling == VK_IMAGE_TILING_OPTIMAL &&
            (props.optimalTilingFeatures & features) == features) {
            outformat = format;
            return Success();
        }
    }

    return Error("failed to find supported format");
}

Result GetMemoryType(VkPhysicalDeviceMemoryProperties availableproperties,
                     uint32_t typebits, VkMemoryPropertyFlags properties, uint32_t &index) {
    for (uint32_t i = 0; i < availableproperties.memoryTypeCount; i++) {
        if ((typebits & 1) == 1) {
            if ((availableproperties.memoryTypes[i].propertyFlags & properties) == properties) {
                index = i;
                return Success();
            }
        }
        typebits >>= 1;
    }

    return Error("failed to find matching memory type");
}

uint64_t GetBufferDeviceAddress(PlatformContext& context, VkBuffer buffer) {
    VkBufferDeviceAddressInfoKHR bufferdeviceai{};
    bufferdeviceai.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    bufferdeviceai.buffer = buffer;
    return vkGetBufferDeviceAddress(context.device, &bufferdeviceai);
}

Result CreateAccelStructureBuffer(PlatformContext& context,
                                  BufferResource &accelbuffer,
                                  VkAccelerationStructureBuildSizesInfoKHR& buildsizeinfo) {
    CreateVkBufferParams params {};
    params.instancesize = buildsizeinfo.accelerationStructureSize;
    params.instances = 1;
    params.createinfo.usage = VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR |
                              VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    params.properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    params.name = "accel buffer";
    Return_(CreateBuffer(context, params, accelbuffer));

    return Success();
}

Result CreateScratchBuffer(PlatformContext& context, VkDeviceSize size,
                           RayTracingScratchBuffer& scratchbuffer) {
    CreateVkBufferParams params {};
    params.instancesize = size;
    params.instances = 1;

    params.createinfo.usage = VK_BUFFER_USAGE_STORAGE_BUFFER_BIT |
                              VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT;
    params.properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    params.name = "scratch buffer";

    Return_(CreateBuffer(context, params, scratchbuffer.buffer));

    VkBufferDeviceAddressInfoKHR bufferdeviceaddressinfo {};
    bufferdeviceaddressinfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    bufferdeviceaddressinfo.buffer = scratchbuffer.buffer.vkobj;
    scratchbuffer.deviceaddress = GetBufferDeviceAddress(context, scratchbuffer.buffer.vkobj);

    return Success();
}

void DeleteScratchBuffer(PlatformContext& context, RayTracingScratchBuffer& scratchbuffer) {
    if (scratchbuffer.buffer.vkobj)
        DestroyBuffer(context, scratchbuffer.buffer);
}

VkTransformMatrixKHR GetTransformMatrix(glm::mat4x3 m) {
    VkTransformMatrixKHR transform;

    transform.matrix[0][0] = m[0][0];
    transform.matrix[0][1] = m[1][0];
    transform.matrix[0][2] = m[2][0];
    transform.matrix[0][3] = m[3][0];

    transform.matrix[1][0] = m[0][1];
    transform.matrix[1][1] = m[1][1];
    transform.matrix[1][2] = m[2][1];
    transform.matrix[1][3] = m[3][1];

    transform.matrix[2][0] = m[0][2];
    transform.matrix[2][1] = m[1][2];
    transform.matrix[2][2] = m[2][2];
    transform.matrix[2][3] = m[3][2];

    return transform;
}

uint64 AlignedInstanceSize(PlatformContext& context, BufferResourceType type, uint64 size) {
    uint64 alignedsize = size;

    switch (type) {
        case eBufferResourceStructuredInput:
        case eBufferResourceStructuredOutput:
        case eBufferResourceByteAddressInput:
        case eBufferResourceByteAddressOutput:
        alignedsize = Align<uint64>(size,
            context.deviceproperties.limits.minStorageBufferOffsetAlignment);
        break;

        case eBufferResourceFormattedInput:
        alignedsize = Align<uint64>(size,
            context.deviceproperties.limits.minTexelBufferOffsetAlignment);
        break;

        case eBufferResourceFormattedOutput:
        alignedsize = Align<uint64>(size,
            context.deviceproperties.limits.minTexelBufferOffsetAlignment);
        break;

        case eBufferResourceConstant:
        alignedsize = Align<uint64>(size,
            context.deviceproperties.limits.minUniformBufferOffsetAlignment);
        break;

        default:
        break;
    }

    return alignedsize;
}

void PrintDeviceExtensions(VkPhysicalDevice device) {
    stdvector<const char*> deviceextensions;

    uint32_t extensioncount;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensioncount, nullptr);

    stdvector<VkExtensionProperties> availableextensions(extensioncount);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensioncount,
                                         availableextensions.data());
    PrintI_("\navailable device extensions:\n");
    for (const auto& extension : availableextensions)
        PrintI_("\t%s\n", extension.extensionName);
}

Result FindQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR vksurface,
                         QueueFamilyIndices& indices) {
    uint32 queuefamilycount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queuefamilycount, nullptr);

    StdStackAllocator alloc(GlobalStack());
    stdvector<VkQueueFamilyProperties> queuefamilies(queuefamilycount, alloc);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queuefamilycount, queuefamilies.data());

    int i = 0;
    for (const auto& queuefamily : queuefamilies) {
        VkBool32 presentsupport = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, vksurface, &presentsupport);
        if (queuefamily.queueCount > 0 && queuefamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
            indices.graphicsfamily = i;
        if (queuefamily.queueCount > 0 && presentsupport)
            indices.presentfamily = i;

        if (indices.IsComplete())
            break;

        ++i;
    }

    if (!indices.IsComplete())
        return Error("couldn't find suitable queue families for present and/or graphics");

    //Throw_(FindAsyncComputeQueueFamily(device, indices.asynccomputefamily));

    return Success();
}

bool CheckDeviceExtensionSupport(VkPhysicalDevice device,
                                 const stdvector<const char*>& deviceextensions) {
    uint32_t extensioncount;
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensioncount, nullptr);

    stdvector<VkExtensionProperties> availableextensions(extensioncount);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &extensioncount,
                                         availableextensions.data());

    stdset<FString> requiredextensions(deviceextensions.begin(), deviceextensions.end());

    for (const auto& extension : availableextensions)
        requiredextensions.erase(extension.extensionName);

    return requiredextensions.empty();
}

SwapchainSupportDetails QuerySwapchainSupport(VkPhysicalDevice device, VkSurfaceKHR vksurface) {
    SwapchainSupportDetails details;

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, vksurface, &details.capabilities);

    uint32_t formatcount;
    vkGetPhysicalDeviceSurfaceFormatsKHR(device, vksurface, &formatcount, nullptr);

    if (formatcount != 0) {
        details.formats.resize(formatcount);
        vkGetPhysicalDeviceSurfaceFormatsKHR(device, vksurface, &formatcount, details.formats.data());
    }

    uint32_t presentmodecount;
    vkGetPhysicalDeviceSurfacePresentModesKHR(device, vksurface, &presentmodecount, nullptr);

    if (presentmodecount != 0) {
        details.presentmodes.resize(presentmodecount);
        vkGetPhysicalDeviceSurfacePresentModesKHR(device, vksurface, &presentmodecount,
                                                  details.presentmodes.data());
    }

    return details;
}

bool IsDeviceSuitable(VkPhysicalDevice device, VkSurfaceKHR vksurface,
                      const stdvector<const char*>& deviceextensions) {
    VkPhysicalDeviceProperties deviceproperties;
    VkPhysicalDeviceFeatures devicefeatures;
    vkGetPhysicalDeviceProperties(device, &deviceproperties);
    vkGetPhysicalDeviceFeatures(device, &devicefeatures);

    //return deviceproperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU;
    //return deviceproperties.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU;

    QueueFamilyIndices indices;
    Throw_(FindQueueFamilies(device, vksurface, indices));

    bool extensionssupported = CheckDeviceExtensionSupport(device, deviceextensions);

    bool swapchainadequate = false;
    if (extensionssupported) {
        SwapchainSupportDetails swapchainsupport = QuerySwapchainSupport(device, vksurface);
        swapchainadequate = !swapchainsupport.formats.empty() &&
                            !swapchainsupport.presentmodes.empty();
    }

    VkPhysicalDeviceFeatures supportedfeatures;
    vkGetPhysicalDeviceFeatures(device, &supportedfeatures);

    return indices.IsComplete() &&
           extensionssupported &&
           swapchainadequate &&
           supportedfeatures.samplerAnisotropy; // not sure if I *actually* need this, but...
}

VkPhysicalDevice PickPhysicalDevice(VkInstance instance, VkSurfaceKHR vksurface,
                                    stdvector<const char*>& deviceextensions) {
    VkPhysicalDevice physicaldevice = VK_NULL_HANDLE;
    uint32 devicecount = 0;
    vkEnumeratePhysicalDevices(instance, &devicecount, nullptr);

    if (devicecount == 0)
        Throw_(Error("failed to find GPUs with Vulkan support!"));

    StdStackAllocator alloc(GlobalStack());
    stdvector<VkPhysicalDevice> devices(devicecount, alloc);
    vkEnumeratePhysicalDevices(instance, &devicecount, devices.data());

    for (const auto& device : devices) {
        if (IsDeviceSuitable(device, vksurface, deviceextensions)) {
            physicaldevice = device;
            break;
        }
    }

    if (physicaldevice == VK_NULL_HANDLE) {
        Throw_(Error("failed to find a suitable GPU!"));
    }

    return physicaldevice;
}

bool ChoosePhysicalDevice(VkInstance instance, VkSurfaceKHR vksurface,
                          stdvector<const char*>& deviceextensions,
                          VkPhysicalDevice& physicaldevice) {
    physicaldevice = PickPhysicalDevice(instance, vksurface, deviceextensions);
    PrintDeviceExtensions(physicaldevice);

    VkPhysicalDeviceProperties physicaldeviceproperties;
    vkGetPhysicalDeviceProperties(physicaldevice, &physicaldeviceproperties);
    PrintI_("Active Device: %s\n", physicaldeviceproperties.deviceName);

    return true;
}

}