#pragma once

#include "platformcontext.h"
#include "imageasset.h"
#include "shaderasset.h"
#include "imageresource.h"
#include "bufferresource.h"
#include "shaderresource.h"
#include "accelresource.h"
#include "resourceparam.h"
#include "drawable.h"
#include "geodata.h"
#include "bclib/geoasset.h"
#include "nodecache.h"

namespace bc9 {

class AssetCache;
struct ColorLayout;
class DrawableCache;
struct CommandBuffer;

    class Framebuffer;
    class ColorTarget;
    class DepthStencilTarget;
    class FramebufferLayout;

    struct CreateBLASParams {
    };

    struct CreateTLASParams {
    };

    Result CreateShader(PlatformContext& context, const ShaderAsset& asset,
                        ShaderResource& shader);
    void DestroyShader(PlatformContext& context, ShaderResource& shader);

    Result CreateBLAS(PlatformContext& context, const GeoAsset& asset,
                      const CreateBLASParams& asparams, BLASResource& blas);
    Result CreateTLAS(PlatformContext& context,
                      const stdvector<NodeCache::BakedInstance>& instances,
                      const AssetCache& assetcache, const DrawableCache& drawablecache,
                      const CreateTLASParams& params, TLASResource& tlas);

    void DestroyBLAS(PlatformContext& context, BLASResource& blas);
    void DestroyTLAS(PlatformContext& context, TLASResource& tlas);

    Result CreateBuffer(PlatformContext& context, const CreateBufferParams& params,
                        BufferResource& buffer);

    Result CreateIndexBuffer(PlatformContext& context, const stdvector<GeoAsset>& geoassets,
                             stdvector<GeoData>& geodatas, BufferResource& buffer);
    Result CreateVertexBuffers(PlatformContext& context,
                               const stdvector<GeoAsset>& geoassets,
                               stdvector<GeoData>& geodatas,
                               BufferResource& pntbuffer,
                               BufferResource& vcolorbuffer,
                               BufferResource& srgbvcolorbuffer,
                               BufferResource& tex1coordbuffer,
                               BufferResource& tangentbuffer);

    void DestroyBuffer(PlatformContext& context, BufferResource& buffer);

    // one thing to be aware of here, if you have multiple FramebufferLayouts,
    // is that only one can be used to create a color/ds target, so pick the
    // layout that is the most appropriate. for example, if you have one
    // FramebufferLayout that sets the load/store ops one way, and another
    // that sets it a different way, in that case it probably doesn't matter,
    // but you'll need to examine the code right now to ensure there are no
    // surprises. as another example, if one FramebufferLayout sets msaasamples
    // to 1 (because, say, it doesn't care), but the other FramebufferLayout
    // sets it to 4 (or whatever) and you use the wrong one when creating the
    // color target, then you could have problems.
    Result CreateColorTarget(PlatformContext& context, FramebufferLayout& layout,
                             CreateColorTargetParams& params, ColorTarget& target);
    void DestroyColorTarget(PlatformContext& context, ColorTarget& color);

    Result CreateDepthStencilTarget(PlatformContext& context, FramebufferLayout& layout,
                                    CreateDepthStencilTargetParams& params,
                                    DepthStencilTarget& target);
    void DestroyDepthStencilTarget(PlatformContext& context, DepthStencilTarget& target);

    Result CreateImage(PlatformContext& context, CommandBuffer* usecmdbuffer,
                       const CreateImageParams& params, ImageResource& image);
    Result CreateImage(PlatformContext& context, ImageAsset& asset, ImageResource& image);
    void DestroyImage(PlatformContext& context, ImageResource& image);

    Result CreateImageViewsFromArray(PlatformContext& context, ColorTarget& color,
                                     ColorLayout& layout, ImageView* imageviews, uintn viewcount);

    Result CreateSwapchainFramebufferLayout(PlatformContext& context, const StringID& name,
                                            FramebufferLayout& fblayout);
    Result CreateXRSwapchainFramebufferLayout(PlatformContext& context, const StringID& name,
                                            FramebufferLayout& fblayout);
    Result CreateSwapchainFramebuffer(PlatformContext& context,
                                      const FramebufferLayout& fblayout,
                                      Framebuffer& framebuffer);
    Result CreateXRSwapchainFramebuffer(PlatformContext& context,
                                        const FramebufferLayout& fblayout,
                                        Framebuffer& framebuffer);
}
