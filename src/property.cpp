#include "property.h"

namespace bc9 {

void PrintProperty(const StringID& name, const Property& property) {
    switch (property.Type()) {
        case ePropertyTypeUInt:
        printf("%s: %u\n", name.str(), property.UInt());
        break;

        case ePropertyTypeInt:
        printf("%s: %d\n", name.str(), property.Int());
        break;
    }
}

}
