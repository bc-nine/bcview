#pragma once

#include "glm/glm.hpp"

// BE AWARE OF PADDING ISSUES WITH THESE STRUCTS! VULKAN/DX12 HAVE DIFFERENT RULES THAN C++!

namespace bc9 {

struct PerFrameConstants {
    glm::mat4 view[kMaxMultiviewLayers];
    glm::mat4 proj[kMaxMultiviewLayers];
    glm::mat4 invview[kMaxMultiviewLayers];
    glm::mat4 invproj[kMaxMultiviewLayers];

    float aperturesize;
    float focusdistance;
    uint32 drawablesidx;
    uint32 verticesidx;

    uint32 vertexcolorsidx;
    uint32 srgbvertexcolorsidx;
    uint32 tex1coordsidx;
    uint32 tangentsidx;

    uint32 materialsidx;
    uint32 modelnodesidx;
    uint32 indicesidx;
    uint32 accelstructuresidx;

    glm::uvec2 accumframes;
    uint32 bounces;
    uint32 rtoutputidx;

    glm::vec3 bggrad1;
    uint32 rtaccumulationidx;

    glm::vec3 bggrad2;
    uint32 pad;
};

struct DrawableData {
    // must stay in sync with Drawable in global.hlsli!
    glm::uint32 materialidx;
    glm::uint32 normalsense = 1;
    // these guys should probably be moved to a constant buffer, since it doesn't
    // actually make sense to ever change them at run-time, but i don't currently
    // have a mechanism for "per-frame small buffer" allocation, where e.g. you
    // can allocate a sub-buffer, fill it in with this data, one for each draw
    // call, and then reclaim the memory at the end of the frame. so for now, we'll
    // simply store it here.
    // hmm, except, these are used for ray-tracing, where you couldn't do the
    // cbuffer approach anyway. ok, well that settles it, we'll stick with this.

    glm::uint32 indexoffset;
    glm::uint32 pntoffset;
    glm::uint32 coloroffset;
    glm::uint32 srgbcoloroffset;
    glm::uint32 tex1coordoffset;
    glm::uint32 tangentoffset;
};

struct ModelNodeData {
    // must stay in sync with ModelNode in global.hlsli!
    glm::mat4x4 transform;
    glm::mat4x4 invtransform;
};

struct MaterialData {
    uint32 basecolormap  = 0;
    uint32 emissivemap = 0;
    uint32 normalmap = 0;
    uint32 bsdf = 0;

    float roughness = 1.0;
    float metalness = 0.0;
    glm::vec2 pad1;

    glm::vec4 basecolor = glm::vec4(1.0, 1.0, 1.0, 1.0);

    glm::vec3 emissive = glm::vec3(0., 0., 0.);
    float alphacutoff;

    uint32 alphamode;
    float refractionidx = 1.0;
    glm::vec2 pad;
};

struct InstanceConstants {
    glm::vec4 xformrow0;
    glm::vec4 xformrow1;
    glm::vec4 xformrow2;
    glm::uvec4 pad;
};

}