#include "xrheadset.h"
#include "framebuffer.h"
#include "platformcontext.h"
#include "allocator_vk.h"
#include "xrutils.h"
#include "surface.h"
#include "window.h"

#define XR_USE_PLATFORM_WIN32

#include <windows.h>
#include "openxr/openxr_platform.h"
#include "bclib/miscutils.h"
#include "bclib/stdallocators.h"
#include "bclib/mathutils.h"

namespace bc9 {

XRHeadset::XRHeadset() {
}

#define LOAD_XR_FUNC(function) \
    xrGetInstanceProcAddr(xrinstance, #function, (PFN_xrVoidFunction *)(&function))

namespace Math {
namespace Pose {
XrPosef Identity() {
    XrPosef t{};
    t.orientation.w = 1;
    return t;
}

XrPosef Translation(const XrVector3f& translation) {
    XrPosef t = Identity();
    t.position = translation;
    return t;
}

XrPosef RotateCCWAboutYAxis(float radians, XrVector3f translation) {
    XrPosef t = Identity();
    t.orientation.x = 0.f;
    t.orientation.y = std::sin(radians * 0.5f);
    t.orientation.z = 0.f;
    t.orientation.w = std::cos(radians * 0.5f);
    t.position = translation;
    return t;
}
}  // namespace Pose
}  // namespace Math

static XrBool32 XRDebugUtilsCallback(XrDebugUtilsMessageSeverityFlagsEXT severity,
                                     XrDebugUtilsMessageTypeFlagsEXT type,
                                     const XrDebugUtilsMessengerCallbackDataEXT* callbackdata,
                                     void* userdata)
{
    printf("openxr validation: ");

    switch (type)
    {
        case XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT :
        printf("general ");
        break;
        case XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT :
        printf("validation ");
        break;
        case XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT :
        printf("performance ");
        break;
        case XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT :
        printf("conformance ");
        break;
    }

    switch (severity)
    {
        case XR_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT :
        printf("(verbose): ");
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT :
        printf("(info): ");
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
        printf("(warning): ");
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        printf("(error): ");
        break;
    }

    printf("%s\n", callbackdata->message);

    return XR_FALSE;
}

static XrReferenceSpaceCreateInfo GetXrReferenceSpaceCreateInfo(const StringID& spacestr) {
    XrReferenceSpaceCreateInfo createinfo { XR_TYPE_REFERENCE_SPACE_CREATE_INFO };
    createinfo.poseInReferenceSpace = Math::Pose::Identity();
    if (spacestr == "View"_sid) {
        createinfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_VIEW;
    } else if (spacestr == "ViewFront"_sid) {
        // Render head-locked 2m in front of device.
        createinfo.poseInReferenceSpace = Math::Pose::Translation({0.f, 0.f, -2.f}),
        createinfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_VIEW;
    } else if (spacestr == "Local"_sid) {
        createinfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_LOCAL;
    } else if (spacestr == "Stage"_sid) {
        createinfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_STAGE;
    } else if (spacestr == "StageLeft"_sid) {
        createinfo.poseInReferenceSpace = Math::Pose::RotateCCWAboutYAxis(0.f, {-2.f, 0.f, -2.f});
        createinfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_STAGE;
    } else if (spacestr == "StageRight"_sid) {
        createinfo.poseInReferenceSpace = Math::Pose::RotateCCWAboutYAxis(0.f, {2.f, 0.f, -2.f});
        createinfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_STAGE;
    } else if (spacestr == "StageLeftRotated"_sid) {
        createinfo.poseInReferenceSpace = Math::Pose::RotateCCWAboutYAxis(3.14f / 3.f,
                                                                          {-2.f, 0.5f, -2.f});
        createinfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_STAGE;
    } else if (spacestr == "StageRightRotated"_sid) {
        createinfo.poseInReferenceSpace = Math::Pose::RotateCCWAboutYAxis(-3.14f / 3.f,
                                                                          {2.f, 0.5f, -2.f});
        createinfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_STAGE;
    } else {
        Throw_(Error("Unknown reference space type '%s'", spacestr.str()));
    }

    return createinfo;
}

static void SetupXRDebugUtilsCallback(XRGraphics& xrgraphics,
                                      XrDebugUtilsMessengerEXT& debugmessenger) {
    XrDebugUtilsMessengerCreateInfoEXT createinfo{};
    createinfo.type = XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createinfo.messageSeverities = XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
                                   XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                   XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createinfo.messageTypes = XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                              XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                              XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT |
                              XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT;
    createinfo.userCallback = XRDebugUtilsCallback;
    createinfo.userData = nullptr;

    DECL_XR_FUNC(xrgraphics, xrCreateDebugUtilsMessengerEXT);

    XrCheck_(xrCreateDebugUtilsMessengerEXT(xrgraphics.xrinstance, &createinfo, &debugmessenger));
}

uintn XRHeadset::InitViewConfigurations() {
    XrViewConfigurationType viewconfigtype = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
    uint32 viewconfigurationcount;
    XrCheck_(xrEnumerateViewConfigurations(xrgraphics.xrinstance, xrgraphics.xrsystemid, 0,
                                           &viewconfigurationcount, nullptr));

    Assert_(viewconfigurationcount == 1); // for now...
    stdvector<XrViewConfigurationType> viewconfigurations(viewconfigurationcount);
    XrCheck_(xrEnumerateViewConfigurations(xrgraphics.xrinstance, xrgraphics.xrsystemid,
                                           viewconfigurationcount, &viewconfigurationcount,
                                           viewconfigurations.data()));
    Assert_(viewconfigurations[0] == viewconfigtype); // for now...

    XrViewConfigurationProperties properties = { .type = XR_TYPE_VIEW_CONFIGURATION_PROPERTIES };
    XrCheck_(xrGetViewConfigurationProperties(xrgraphics.xrinstance, xrgraphics.xrsystemid,
                                              viewconfigurations[0], &properties));

    uint32 viewcount = 0;
    XrCheck_(xrEnumerateViewConfigurationViews(xrgraphics.xrinstance, xrgraphics.xrsystemid,
                                               viewconfigtype, 0, &viewcount, nullptr));

    views.resize(viewcount);
    configviews.resize(viewcount);
    projlayerviews.resize(viewcount);
    for (uint32 i = 0; i < viewcount; ++i) {
        views[i] = { XR_TYPE_VIEW };
        configviews[i] = { XR_TYPE_VIEW_CONFIGURATION_VIEW };
        projlayerviews[i] = { XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW };
    }

    XrCheck_(xrEnumerateViewConfigurationViews(xrgraphics.xrinstance, xrgraphics.xrsystemid,
                                               viewconfigtype, viewcount, &viewcount,
                                               configviews.data()));

    // make sure all views have the same dimensions
    for (uint32 i = 0; i < viewcount; ++i) {
        if (i == 0) {
            xrgraphics.xrswapchaindim = { configviews[i].recommendedImageRectWidth,
                                       configviews[i].recommendedImageRectHeight };
        }
        else {
            Assert_(xrgraphics.xrswapchaindim.x == configviews[i].recommendedImageRectWidth &&
                    xrgraphics.xrswapchaindim.y == configviews[i].recommendedImageRectHeight);
        }
    }

    return viewcount;
}

void XRHeadset::CreateSwapchain(PlatformContext& context) {
    Assert_(xrgraphics.xrviewcount > 0);
    Assert_(xrsession != XR_NULL_HANDLE);

    uint32 swapchainformatcount;
    XrCheck_(xrEnumerateSwapchainFormats(xrsession, 0, &swapchainformatcount, nullptr));
    stdvector<int64> swapchainformats(swapchainformatcount);
    XrCheck_(xrEnumerateSwapchainFormats(xrsession, uint32_(swapchainformats.size()),
                                         &swapchainformatcount, swapchainformats.data()));
    int64 swapchainformat = xrgraphics.SetColorSwapchainFormat(swapchainformats);

    const XrViewConfigurationView& vp = configviews[0];

    XrSwapchainCreateInfo createinfo { XR_TYPE_SWAPCHAIN_CREATE_INFO };
    createinfo.arraySize = xrgraphics.xrviewcount;
    createinfo.format = swapchainformat;
    createinfo.width = vp.recommendedImageRectWidth;
    createinfo.height = vp.recommendedImageRectHeight;
    createinfo.mipCount = 1;
    createinfo.faceCount = 1;
    createinfo.sampleCount = xrgraphics.GetSupportedSwapchainSampleCount(vp);
    createinfo.usageFlags = XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;

    swapchain.width = createinfo.width;
    swapchain.height = createinfo.height;
    XrCheck_(xrCreateSwapchain(xrsession, &createinfo, &swapchain.handle));

    uintn imagecount;
    XrCheck_(xrEnumerateSwapchainImages(swapchain.handle, 0, &imagecount, nullptr));
    xrgraphics.xrswapchainimagecount = imagecount;

    xrgraphics.LoadSwapImages(context, swapchain.handle, imagecount);

    for (uint32 i = 0; i < xrgraphics.xrviewcount; ++i) {
        projlayerviews[i].subImage.swapchain = swapchain.handle;
        projlayerviews[i].subImage.imageArrayIndex = i;
        projlayerviews[i].subImage.imageRect.offset = { 0, 0 };
        projlayerviews[i].subImage.imageRect.extent = {
            int32_(configviews[i].recommendedImageRectWidth),
            int32_(configviews[i].recommendedImageRectHeight)
        };
    }
}

Result XRHeadset::Initialize(PlatformContext& context, Surface& surface, Window& window) {
    uint32 layercount = 0;
    xrEnumerateApiLayerProperties(0, &layercount, nullptr);

    stdvector<const char*> uselayers;
    const char* asklayers[] = {
        "XR_APILAYER_LUNARG_core_validation"
    };

    stdvector<XrApiLayerProperties> availablelayers(layercount);
    for (uintn i = 0; i < layercount; ++i)
        availablelayers[i] = { XR_TYPE_API_LAYER_PROPERTIES };
    xrEnumerateApiLayerProperties(layercount, &layercount, availablelayers.data());
    printf("OpenXR API layers available:\n");
    for (uintn i = 0; i < layercount; ++i) {
        printf("* %s\n", availablelayers[i].layerName);

        for (uintn ask = 0; ask < _countof(asklayers); ask++) {
            if (strcmp(asklayers[ask], availablelayers[i].layerName) == 0) {
                uselayers.push_back(asklayers[ask]);
                break;
            }
        }
    }
    printf("\n");

    uint32_t extcount = 0;
    xrEnumerateInstanceExtensionProperties(nullptr, 0, &extcount, nullptr);
    stdvector<XrExtensionProperties> xrexts(extcount);
    for (auto& p : xrexts)
        p.type = XR_TYPE_EXTENSION_PROPERTIES;
    xrEnumerateInstanceExtensionProperties(nullptr, extcount, &extcount, xrexts.data());

    stdvector<const char*> useextensions;
    const char *askextensions[] = {
        XR_KHR_VULKAN_ENABLE2_EXTENSION_NAME,
        XR_EXT_DEBUG_UTILS_EXTENSION_NAME,
    };

    printf("OpenXR extensions available:\n");
    for (sizen i = 0; i < xrexts.size(); ++i) {
        printf("* %s\n", xrexts[i].extensionName);

        for (uintn ask = 0; ask < _countof(askextensions); ask++) {
            if (strcmp(askextensions[ask], xrexts[i].extensionName) == 0) {
                useextensions.push_back(askextensions[ask]);
                break;
            }
        }
    }

    if (useextensions.size() != ArraySize_(askextensions))
        return Error("some required extensions not supported");

    XrInstanceCreateInfo createinfo = { XR_TYPE_INSTANCE_CREATE_INFO };
    createinfo.enabledApiLayerCount = uint32_(uselayers.size());
    createinfo.enabledApiLayerNames = uselayers.data();
    createinfo.enabledExtensionCount = uint32_(useextensions.size());
    createinfo.enabledExtensionNames = useextensions.data();
    createinfo.applicationInfo.apiVersion = XR_CURRENT_API_VERSION;
    createinfo.applicationInfo.applicationVersion =static_cast<uint32_t>(XR_MAKE_VERSION(0, 1, 0));
    createinfo.applicationInfo.engineVersion = static_cast<uint32_t>(XR_MAKE_VERSION(0, 1, 0));
    strcpy_s(createinfo.applicationInfo.applicationName, "bcview");
    xrCreateInstance(&createinfo, &xrgraphics.xrinstance);

#if ENABLE_VALIDATION
    SetupXRDebugUtilsCallback(xrgraphics, debugmessenger);
#endif

    if (xrgraphics.xrinstance == nullptr)
        return Error("unable to create OpenXR instance");

    // Request a form factor from the device (HMD, Handheld, etc.)
    XrSystemGetInfo systeminfo = { XR_TYPE_SYSTEM_GET_INFO };
    systeminfo.formFactor = appconfigform;
    XrCheck_(xrGetSystem(xrgraphics.xrinstance, &systeminfo, &xrgraphics.xrsystemid));

    uint32 blendcount = 0;
    xrEnumerateEnvironmentBlendModes(xrgraphics.xrinstance, xrgraphics.xrsystemid, appconfigview,
                                     1, &blendcount, &xrblend);
    Assert_(xrblend == XR_ENVIRONMENT_BLEND_MODE_OPAQUE); // for now

    xrgraphics.Initialize(context, surface, window);

    XrSessionCreateInfo sessioncreateinfo { XR_TYPE_SESSION_CREATE_INFO };
    sessioncreateinfo.next = xrgraphics.GetGraphicsBinding();
    sessioncreateinfo.systemId = xrgraphics.xrsystemid;
    XrCheck_(xrCreateSession(xrgraphics.xrinstance, &sessioncreateinfo, &xrsession));

    xrgraphics.xrviewcount = InitViewConfigurations();

    CreateSwapchain(context);

    uint32 spacecount;
    XrCheck_(xrEnumerateReferenceSpaces(xrsession, 0, &spacecount, nullptr));
    stdvector<XrReferenceSpaceType> spaces(spacecount);
    XrCheck_(xrEnumerateReferenceSpaces(xrsession, spacecount, &spacecount,spaces.data()));

    StringID vspaces[] = { "ViewFront", "Local", "Stage",
                           "StageLeft", "StageRight",
                           "StageLeftRotated", "StageRightRotated" };

    for (const auto& vspace : vspaces) {
        XrReferenceSpaceCreateInfo createinfo = GetXrReferenceSpaceCreateInfo(vspace);
        XrSpace space;
        XrResult res = xrCreateReferenceSpace(xrsession, &createinfo, &space);
        if (XR_SUCCEEDED(res)) {
            visualizedspaces.push_back(space);
        }
        else {
            PrintE_("failed to create reference space %s with error %d",
                    vspace.str(), res);
        }
    }

    {
        XrReferenceSpaceCreateInfo createinfo = GetXrReferenceSpaceCreateInfo("Stage");
        XrCheck_(xrCreateReferenceSpace(xrsession, &createinfo, &appspace));
    }

    shouldrender = false;

    return Success();
}

const XrEventDataBaseHeader* XRHeadset::TryReadNextEvent(PlatformContext& context) {
    XrEventDataBaseHeader* baseheader = reinterpret_cast<XrEventDataBaseHeader*>(&eventdatabuffer);
    *baseheader = { XR_TYPE_EVENT_DATA_BUFFER };
    const XrResult xr = xrPollEvent(xrgraphics.xrinstance, &eventdatabuffer);
    if (xr == XR_SUCCESS) {
        if (baseheader->type == XR_TYPE_EVENT_DATA_EVENTS_LOST) {
            const XrEventDataEventsLost* const eventslost =
                reinterpret_cast<const XrEventDataEventsLost*>(baseheader);
            PrintW_(FString("%d events lost", eventslost->lostEventCount).str());
        }

        return baseheader;
    }
    if (xr == XR_EVENT_UNAVAILABLE)
        return nullptr;

    XrCheck_(xr);

    return nullptr;
}

void
XRHeadset::HandleSessionStateChangedEvent(PlatformContext& context,
                                          const XrEventDataSessionStateChanged& statechangedevent,
                                          bool& exitrenderloop, bool& requestrestart) {
    const XrSessionState oldstate = sessionstate;
    sessionstate = statechangedevent.state;

    PrintI_(FString("XrEventDataSessionStateChanged: state %u->%u session=%lld time=%lld",
                    oldstate, sessionstate, statechangedevent.session,
                    statechangedevent.time).str());

    if ((statechangedevent.session != XR_NULL_HANDLE) &&
        (statechangedevent.session != xrsession)) {
        PrintE_(FString("XrEventDataSessionStateChanged for unknown session").str());
        return;
    }

    switch (sessionstate) {
        case XR_SESSION_STATE_READY: {
            Assert_(xrsession != XR_NULL_HANDLE);
            XrSessionBeginInfo sessionbegininfo { XR_TYPE_SESSION_BEGIN_INFO };
            sessionbegininfo.primaryViewConfigurationType =
                XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
            XrCheck_(xrBeginSession(xrsession, &sessionbegininfo));
            sessionrunning = true;
            break;
        }
        case XR_SESSION_STATE_STOPPING: {
            Assert_(xrsession != XR_NULL_HANDLE);
            sessionrunning = false;
            XrCheck_(xrEndSession(xrsession));
            break;
        }
        case XR_SESSION_STATE_EXITING: {
            exitrenderloop = true;
            // Do not attempt to restart because user closed this session.
            requestrestart = false;
            break;
        }
        case XR_SESSION_STATE_LOSS_PENDING: {
            exitrenderloop = true;
            // Poll for a new instance.
            requestrestart = true;
            break;
        }
        default:
            break;
    }
}

void XRHeadset::LogActionSourceName(PlatformContext& context, XrAction action,
                                    const StringID& actionname) const {
    XrBoundSourcesForActionEnumerateInfo getinfo = {
        XR_TYPE_BOUND_SOURCES_FOR_ACTION_ENUMERATE_INFO
    };
    getinfo.action = action;
    uint32 pathcount = 0;
    XrCheck_(xrEnumerateBoundSourcesForAction(xrsession, &getinfo, 0, &pathcount,nullptr));
    stdvector<XrPath> paths(pathcount);
    XrCheck_(xrEnumerateBoundSourcesForAction(xrsession, &getinfo, uint32_(paths.size()),
                                              &pathcount, paths.data()));

    bool boundto = false;
    PrintW_("%s action is bound to: ", actionname.str());
    for (uint32_t i = 0; i < pathcount; ++i) {
        constexpr XrInputSourceLocalizedNameFlags all =
            XR_INPUT_SOURCE_LOCALIZED_NAME_USER_PATH_BIT |
            XR_INPUT_SOURCE_LOCALIZED_NAME_INTERACTION_PROFILE_BIT |
            XR_INPUT_SOURCE_LOCALIZED_NAME_COMPONENT_BIT;

        XrInputSourceLocalizedNameGetInfo nameinfo = {
            XR_TYPE_INPUT_SOURCE_LOCALIZED_NAME_GET_INFO
        };
        nameinfo.sourcePath = paths[i];
        nameinfo.whichComponents = all;

        uint32 size = 0;
        XrCheck_(xrGetInputSourceLocalizedName(xrsession, &nameinfo, 0, &size, nullptr));
        if (size == 0)
            continue;

        stdvector<char> grabsource(size);
        XrCheck_(xrGetInputSourceLocalizedName(xrsession, &nameinfo,
                                               uint32_(grabsource.size()), &size,
                                               grabsource.data()));
        boundto = true;
        PrintW_("\n  * %s", grabsource.data());
    }

    if (!boundto)
        PrintW_(" <nothing>\n");
    else
        PrintW_("\n");
}

bool XRHeadset::SessionRunning() {
    return sessionrunning;
}

bool XRHeadset::ShouldRender() {
    return SessionRunning() && shouldrender;
}

void XRHeadset::BeginRenderPass(PlatformContext& context, Framebuffer& framebuffer) {
    xrgraphics.BeginRenderPass(context, framebuffer);
}

void XRHeadset::EndRenderPass(PlatformContext& context, Framebuffer& framebuffer) {
    xrgraphics.EndRenderPass(context);
}

bool XRHeadset::BeginFrame(PlatformContext& context, glm::mat4* viewmats, glm::mat4* invviewmats,
                           glm::mat4* projmats, glm::mat4* invprojmats, uintn matcount) {
    Assert_(xrsession != XR_NULL_HANDLE);

    XrCheck_(xrWaitFrame(xrsession, &framewaitinfo, &framestate));

    XrFrameBeginInfo framebegininfo { XR_TYPE_FRAME_BEGIN_INFO };
    XrCheck_(xrBeginFrame(xrsession, &framebegininfo));

    shouldrender = (framestate.shouldRender == XR_TRUE);

    if (shouldrender) {
        XrViewState viewstate { XR_TYPE_VIEW_STATE };
        uint32 viewcapacityinput = uint32_(views.size());

        XrViewLocateInfo viewlocateinfo{XR_TYPE_VIEW_LOCATE_INFO};
        viewlocateinfo.viewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
        viewlocateinfo.displayTime = framestate.predictedDisplayTime;
        viewlocateinfo.space = appspace;

        uint32 viewcountoutput;
        XrCheck_(xrLocateViews(xrsession, &viewlocateinfo, &viewstate, viewcapacityinput,
                 &viewcountoutput, views.data()));
        Assert_(viewcountoutput == xrgraphics.xrviewcount);

        if ((viewstate.viewStateFlags & XR_VIEW_STATE_POSITION_VALID_BIT) == 0 ||
            (viewstate.viewStateFlags & XR_VIEW_STATE_ORIENTATION_VALID_BIT) == 0) {
            Assert_(0);
            shouldrender = false;
            return false;
        }

        for (uintn i = 0; i < xrgraphics.xrviewcount; ++i) {
            projlayerviews[i].pose = views[i].pose;
            projlayerviews[i].fov = views[i].fov;

            XrFovf& fov = views[i].fov;
            XrQuaternionf& q(views[i].pose.orientation);
            XrVector3f& p(views[i].pose.position);
            invviewmats[i] = glm::mat4(Transform3D(glm::vec3(p.x, p.y, p.z),
                                                   glm::quat(q.w, q.x, q.y, q.z),
                                                   glm::vec3(1., 1., 1.)));
            viewmats[i] = InverseTransform3D(glm::vec3(p.x, p.y, p.z),
                                             glm::quat(q.w, q.x, q.y, q.z),
                                             glm::vec3(1., 1., 1.));
            projmats[i] = InfReversedZPerspective(fov.angleLeft, fov.angleRight,
                                                  fov.angleUp, fov.angleDown, 0.1f);
            invprojmats[i] = inverse(projmats[i]);
        }

        XrSwapchainImageAcquireInfo acquireinfo { XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO };
        XrCheck_(xrAcquireSwapchainImage(swapchain.handle, &acquireinfo, &xrgraphics.xrswapidx));

        XrSwapchainImageWaitInfo waitinfo { XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO };
        waitinfo.timeout = XR_INFINITE_DURATION;
        XrCheck_(xrWaitSwapchainImage(swapchain.handle, &waitinfo));
    }

    return shouldrender;
}

void XRHeadset::EndFrame(PlatformContext& context) {
    XrCompositionLayerProjection layer { XR_TYPE_COMPOSITION_LAYER_PROJECTION };

    StdStackAllocator alloc(GlobalStack());
    stdvector<XrCompositionLayerBaseHeader*> layers(alloc);
    if (ShouldRender()) {
        XrSwapchainImageReleaseInfo releaseinfo { XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO };
        XrCheck_(xrReleaseSwapchainImage(swapchain.handle, &releaseinfo));

        layer.space = appspace;
        layer.layerFlags = 0;
        layer.viewCount = (uint32)projlayerviews.size();
        layer.views = projlayerviews.data();
        layers.push_back(reinterpret_cast<XrCompositionLayerBaseHeader*>(&layer));
    }

    XrFrameEndInfo frameendinfo { XR_TYPE_FRAME_END_INFO };
    frameendinfo.displayTime = framestate.predictedDisplayTime;
    frameendinfo.environmentBlendMode = xrblend;
    frameendinfo.layerCount = uint32_(layers.size());
    frameendinfo.layers = layers.data();
    XrCheck_(xrEndFrame(xrsession, &frameendinfo));
}

void XRHeadset::ProcessEvents(PlatformContext& context, bool& exitrenderloop,
                              bool& requestrestart) {
    exitrenderloop = requestrestart = false;

    while (const XrEventDataBaseHeader* event = TryReadNextEvent(context)) {
        switch (event->type) {
            case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING: {
                const auto& instancelosspending =
                    *reinterpret_cast<const XrEventDataInstanceLossPending*>(event);
                PrintW_(FString("XrEventDataInstanceLossPending by %lld",
                                instancelosspending.lossTime).str());
                exitrenderloop = true;
                requestrestart = true;
                return;
            }
            case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED: {
                auto sessionstatechangedevent =
                    *reinterpret_cast<const XrEventDataSessionStateChanged*>(event);
                HandleSessionStateChangedEvent(context, sessionstatechangedevent,
                                               exitrenderloop, requestrestart);
                break;
            }
            case XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED:
            //    LogActionSourceName(m_input.grabAction, "Grab");
            //    LogActionSourceName(m_input.quitAction, "Quit");
            //    LogActionSourceName(m_input.poseAction, "Pose");
            //    LogActionSourceName(m_input.vibrateAction, "Vibrate");
                break;
            case XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING:
            default: {
                PrintI_(FString("ignoring event type %d", event->type).str());
                break;
            }
        }
    }
}

ResourceFormat XRHeadset::GetSwapchainFormat() const {
    return xrgraphics.xrswapchainformat;
}

void XRHeadset::GetViewSize(uintn& width, uintn& height) const {
    width = xrgraphics.xrswapchaindim.x;
    height = xrgraphics.xrswapchaindim.y;
}

uintn XRHeadset::GetViewCount() const {
    return xrgraphics.xrviewcount;
}

uintn XRHeadset::GetSwapchainImageCount() const {
    return xrgraphics.xrswapchainimagecount;
}

ImageResource XRHeadset::GetSwapchainImage(uintn idx) const {
    return xrgraphics.xrswapchainimages[idx];
}

Result XRHeadset::Shutdown(PlatformContext& context) {
#if ENABLE_VALIDATION
    DECL_XR_FUNC(xrgraphics, xrDestroyDebugUtilsMessengerEXT);
    if (debugmessenger)
        xrDestroyDebugUtilsMessengerEXT(debugmessenger);
#endif

    xrDestroySwapchain(swapchain.handle);

    for (XrSpace visualizedspace : visualizedspaces)
        xrDestroySpace(visualizedspace);

    if (appspace != XR_NULL_HANDLE)
        xrDestroySpace(appspace);

    if (xrsession != XR_NULL_HANDLE)
        xrDestroySession(xrsession);

    if (xrgraphics.xrinstance != XR_NULL_HANDLE)
        xrDestroyInstance(xrgraphics.xrinstance);

    xrgraphics.Shutdown(context);

    return Success();
}

}