#include "resourceparam_vk.h"
#include "platformcontext_vk.h"
#include "util_vk.h"
#include "bclib/miscutils.h"

namespace bc9 {

    Result CreateBufferParams::Get(PlatformContext& context, CreateVkBufferParams& params) const {
    params = vkparams;

    Assert_(accessmode != eBufferCPUAccessModeCount);

    // for VkBufferCreateInfo, we really only need to set size and usage here,
    // since everything else is initialized in CreateVkBufferParams struct.
    bool uniformbuffer = false;
    bool texelbuffer = false;
    bool storagebuffer = false;

    // ok just looked at this a bit more and i believe it's actually correct. so
    // minUniformBufferOffsetAlignment is the minimum offset you can use in a
    // VkDescriptorBufferInfo, meaning, if you had 2 instances of the buffer, each instance
    // must be an integer multiple of (say) 64 bytes, so you can say offset = 0 for the first,
    // offset = k * 64 for the second, etc. if the subbuffers were smaller, you wouldn't be
    // able to do things as easily (you'd have to pad/offset blah blah). so as long as the
    // offset is an integer multiple of this alignment, you're good. again, note that the
    // alignment requirements of the buffer itself are more strict, but vma should handle
    // that for us. what matters to us is sub buffer size (the instances).

    switch (type) {
        case eBufferResourceGeneral:
        break;

        case eBufferResourceIndex:
        params.createinfo.usage |= (VK_BUFFER_USAGE_INDEX_BUFFER_BIT |
                                    VK_BUFFER_USAGE_STORAGE_BUFFER_BIT);
        break;

        case eBufferResourceStructuredInput:
        case eBufferResourceStructuredOutput:
        case eBufferResourceByteAddressInput:
        case eBufferResourceByteAddressOutput:
        storagebuffer = true;
        params.createinfo.usage |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
        break;

        case eBufferResourceFormattedInput:
        texelbuffer = true;
        params.createinfo.usage |= VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;
        break;

        case eBufferResourceFormattedOutput:
        texelbuffer = true;
        params.createinfo.usage |= VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT;
        break;

        case eBufferResourceConstant:
        uniformbuffer = true;
        Assert_(accessmode == eBufferCPUAccessWrite);
        params.createinfo.usage |= VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
        break;

        default:
        case eBufferResourceTypeCount:
        return Error("unknown buffer type");
        break;
    }
    Assert_(type == eBufferResourceGeneral || params.createinfo.usage);
    Assert_(accessmode != eBufferCPUAccessModeCount, "invalid buffer access mode");
    if (storagebuffer) {
        if (texelbuffer || uniformbuffer) {
            return Error("storage buffers cannot also be texel/uniform");
        }
    }

    Throw_(GetMemoryPropertyFlags(accessmode, params.properties));

    return Success();
}

CreateConstantBufferParams::CreateConstantBufferParams(PlatformContext& context, uint64 size,
                                                       const char* name) :
CreateBufferParams(eBufferResourceConstant, name) {
    vkparams.instances = kFrameRenderBufferCount;
    vkparams.instancesize = AlignedInstanceSize(context, type, size);
    elements = 1;
    stride = size;
    SetAccessMode(eBufferCPUAccessWrite);
}

CreateMappedBufferParams::CreateMappedBufferParams(PlatformContext& context,
                                                   BufferResourceType type,
                                                   uint64 elements,
                                                   uint64 elementsize,
                                                   const char* name) :
CreateBufferParams(type, name) {
    vkparams.instances = 1;
    vkparams.instancesize = AlignedInstanceSize(context, type, elements * elementsize);
    elements = elements;
    stride = elementsize;
    SetAccessMode(eBufferCPUAccessWrite);
}

CreateMappedBufferParams::CreateMappedBufferParams(PlatformContext& context,
                                                   BufferResourceType type,
                                                   uint64 size,
                                                   const char* name) :
CreateBufferParams(type, name) {
    vkparams.instances = 1;
    vkparams.instancesize = AlignedInstanceSize(context, type, size);
    elements = 1;
    stride = size;
    SetAccessMode(eBufferCPUAccessWrite);
}

CreateInstancedBufferParams::CreateInstancedBufferParams(PlatformContext& context,
                                                         BufferResourceType type,
                                                         uint64 elements,
                                                         uint64 elementsize,
                                                         const char* name) :
CreateBufferParams(type, name) {
    vkparams.instances = kFrameRenderBufferCount;
    vkparams.instancesize = AlignedInstanceSize(context, type, elements * elementsize);
    elements = elements;
    stride = elementsize;
    SetAccessMode(eBufferCPUAccessWrite);
}

CreateInstancedBufferParams::CreateInstancedBufferParams(PlatformContext& context,
                                                         BufferResourceType type,
                                                         uint64 size,
                                                         const char* name) :
CreateBufferParams(type, name) {
    vkparams.instances = kFrameRenderBufferCount;
    vkparams.instancesize = AlignedInstanceSize(context, type, size);
    elements = 1;
    stride = size;
    SetAccessMode(eBufferCPUAccessWrite);
}

CreateBufferParams::CreateBufferParams(BufferResourceType type, const char* name) :
type(type) {
    vkparams.name = name;
    vkparams.viewcreateinfo.format = VkFormat(kInvalidUInt32);
}

CreateBufferParams::CreateBufferParams(PlatformContext& context, BufferResourceType type,
                                       uint64 size, const char* name) :
type(type) {
    vkparams.name = name;
    vkparams.viewcreateinfo.format = VkFormat(kInvalidUInt32);

    vkparams.instances = 1;
    vkparams.instancesize = AlignedInstanceSize(context, type, size);
    elements = 1;
    stride = size;
}

CreateBufferParams::CreateBufferParams(PlatformContext& context, BufferResourceType type,
                                       uint64 elements, uint64 elementsize, const char* name) :
type(type) {
    vkparams.name = name;
    vkparams.viewcreateinfo.format = VkFormat(kInvalidUInt32);

    vkparams.instances = 1;
    vkparams.instancesize = AlignedInstanceSize(context, type, elements * elementsize);
    elements = elements;
    stride = elementsize;
}

uint64 CreateBufferParams::GetElements() const {
    return elements;
}

uint64 CreateBufferParams::GetStride() const {
    return stride;
}

void CreateBufferParams::SetName(const char* buffername) {
    vkparams.name = buffername;
}

void CreateBufferParams::SetVisibilityVertex(bool visible) {
    // nothing for vulkan
}

void CreateBufferParams::SetVisibilityPixel(bool visible) {
    // nothing for vulkan
}

void CreateBufferParams::SetAccessMode(BufferCPUAccessMode mode) {
    accessmode = mode;
}

void CreateBufferParams::SetUsageCopySource(bool cs) {
    if (cs)
        vkparams.createinfo.usage |= VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    else
        vkparams.createinfo.usage &= ~VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
}
void CreateBufferParams::SetUsageCopyDestination(bool cd) {
    if (cd)
        vkparams.createinfo.usage |= VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    else
        vkparams.createinfo.usage &= ~VK_BUFFER_USAGE_TRANSFER_DST_BIT;
}

void CreateBufferParams::SetInitialData(void* data, uint64 size) {
    vkparams.initdata = data;
    vkparams.initdatasize = size ? size : vkparams.instancesize;
}

Result CreateBufferParams::SetFormat(ResourceFormat format) {
    Assert_(type == eBufferResourceFormattedInput || type == eBufferResourceFormattedOutput);

    Return_(GetVkFormat(format, vkparams.viewcreateinfo.format));

    return Success();
}

Result CreateImageParams::Get(PlatformContext& context, CreateVkImageParams& params) const {
    params = vkparams;

    if (accessmode == eImageCPUAccessNone || accessmode == eImageCPUAccessRead) {
        Return_(GetMemoryPropertyFlags(accessmode, params.properties));
    }
    else if (accessmode == eImageCPUAccessWrite) {
        return Error("write not yet implemented");
    }
    else {
        return Error("invalid image access mode");
    }

    // initialize the subresourceRange first, because this is used elsewhere,
    // e.g. in TransitionImage() calls, even if a view won't actually be created.
    params.viewcreateinfo.subresourceRange.aspectMask = aspect;
    params.viewcreateinfo.subresourceRange.baseMipLevel = 0;
    params.viewcreateinfo.subresourceRange.levelCount = params.createinfo.mipLevels;
    params.viewcreateinfo.subresourceRange.baseArrayLayer = 0;
    params.viewcreateinfo.subresourceRange.layerCount = params.createinfo.arrayLayers;

    params.createview = false;
    if (accessmode == eImageCPUAccessRead) {
        Assert_(params.createinfo.usage & VK_IMAGE_USAGE_TRANSFER_DST_BIT);
        // currently we're assuming this image will be a transfer
        // destination only, the recipient of some some gpu-initiated
        // copy command. in this case, no view is needed. and obviously,
        // if/when this is no longer sufficient, will need to address.
        // e.g. maybe cpu read images could be used in other contexts?
        return Success();
    }

    // it's possible to create an image that doesn't have any VkImageUsageFlagBits
    // set (e.g. creating readbackimg that's used for taking a screenshot). in this
    // case, there's no need to create a VkImageView.
    VkImageUsageFlags usage = params.createinfo.usage;
    if ((usage & VK_IMAGE_USAGE_SAMPLED_BIT) ||
        (usage & VK_IMAGE_USAGE_STORAGE_BIT) ||
        (usage & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT) ||
        (usage & VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT) ||
        (usage & VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT) ||
        (usage & VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT) ||
        (usage & VK_IMAGE_USAGE_FRAGMENT_SHADING_RATE_ATTACHMENT_BIT_KHR) ||
        (usage & VK_IMAGE_USAGE_FRAGMENT_DENSITY_MAP_BIT_EXT)) {
        params.createview = true;
    }

    if (!params.createview)
        return Success();

    // everything from here on out should be imageview related, otherwise
    // important stuff might be missed due to return above.
    if (arrayimage) {
        if (params.createinfo.flags & VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT)
            params.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
        else if (params.createinfo.imageType == VK_IMAGE_TYPE_1D)
            params.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_1D_ARRAY;
        else if (params.createinfo.imageType == VK_IMAGE_TYPE_2D)
            params.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
        else
            return Error("CreateImage() internal error");
    }
    else {
        if (params.createinfo.flags & VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT)
            params.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
        else if (params.createinfo.imageType == VK_IMAGE_TYPE_1D)
            params.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_1D;
        else if (params.createinfo.imageType == VK_IMAGE_TYPE_2D)
            params.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        else if (params.createinfo.imageType == VK_IMAGE_TYPE_3D)
            params.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
        else
            return Error("CreateImage() internal error");
    }
    params.viewcreateinfo.format = params.createinfo.format;
    params.viewcreateinfo.components = { VK_COMPONENT_SWIZZLE_R, VK_COMPONENT_SWIZZLE_G,
                                         VK_COMPONENT_SWIZZLE_B, VK_COMPONENT_SWIZZLE_A };

    return Success();
}

CreateImageParams::CreateImageParams() {
}

CreateImageParams::CreateImageParams(ResourceFormat format, uintn width, uintn height,
                                     uintn samples, const char* name) {
    vkparams.createinfo.imageType = VK_IMAGE_TYPE_2D;

    vkparams.createinfo.extent.width = width;
    vkparams.createinfo.extent.height = height;
    vkparams.createinfo.extent.depth = 1;

    vkparams.createinfo.mipLevels = 1;
    vkparams.createinfo.arrayLayers = 1;

    Populate(format, samples, name);
}

CreateImageParams::CreateImageParams(ResourceFormat format, uintn width, uintn height,
                                     uintn depth, uintn miplevels, uintn arraylayers,
                                     uintn samples, bool cube, const char* name) :
arrayimage(arraylayers) {
    assert(depth && (depth == 1 || arraylayers == 0));
    assert(height >= 1 || depth == 1 || depth == 0);
    assert(!cube || depth == 1);
    assert(!cube || (arraylayers % 6 == 0));

    if (height == 0)
        vkparams.createinfo.imageType = VK_IMAGE_TYPE_1D;
    else if (depth == 1)
        vkparams.createinfo.imageType = VK_IMAGE_TYPE_2D;
    else
        vkparams.createinfo.imageType = VK_IMAGE_TYPE_3D;

    vkparams.createinfo.extent.width = width;
    vkparams.createinfo.extent.height = height;
    vkparams.createinfo.extent.depth = depth;

    vkparams.createinfo.mipLevels = miplevels;
    vkparams.createinfo.arrayLayers = (arraylayers == 0) ? 1 : arraylayers;

    if (cube) {
        assert(arraylayers % 6 == 0);
        vkparams.createinfo.flags |= VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
    }

    Populate(format, samples, name);
}

void CreateImageParams::Populate(ResourceFormat format, uintn samples, const char* name) {
    Throw_(GetVkFormat(format, vkparams.createinfo.format));
    Throw_(GetVkSampleCount(samples, vkparams.createinfo.samples));
    vkparams.name = name;

    vkparams.createinfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    vkparams.createinfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    vkparams.createinfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    // TODO: fix this for concurrent usage (e.g. with compute queue)
    vkparams.createinfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    vkparams.createinfo.queueFamilyIndexCount = 0;
    vkparams.createinfo.pQueueFamilyIndices = nullptr;
}

void CreateImageParams::SetInitialLayout(ImageLayout layout) {
    Throw_(GetVkImageLayout(layout, vkparams.initiallayout));
}

void CreateImageParams::SetAccessMode(ImageCPUAccessMode mode) {
    accessmode = mode;

    if (mode == eImageCPUAccessRead)
        vkparams.createinfo.tiling = VK_IMAGE_TILING_LINEAR;
    else
        vkparams.createinfo.tiling = VK_IMAGE_TILING_OPTIMAL;
}

void CreateImageParams::SetUsageCopySource() {
    vkparams.createinfo.usage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
}

void CreateImageParams::SetUsageCopyDestination() {
    vkparams.createinfo.usage |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;
}

void CreateImageParams::SetUsageSampled() {
    vkparams.createinfo.usage |= VK_IMAGE_USAGE_SAMPLED_BIT;
}

void CreateImageParams::SetUsageStorage() {
    vkparams.createinfo.usage |= VK_IMAGE_USAGE_STORAGE_BIT;
}

void CreateImageParams::SetUsageColorTarget() {
    aspect |= VK_IMAGE_ASPECT_COLOR_BIT;
    aspect &= ~VK_IMAGE_ASPECT_DEPTH_BIT;
    aspect &= ~VK_IMAGE_ASPECT_STENCIL_BIT;
    vkparams.createinfo.usage |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
}

void CreateImageParams::SetUsageStencilTarget() {
    aspect &= ~VK_IMAGE_ASPECT_COLOR_BIT;
    aspect |= VK_IMAGE_ASPECT_STENCIL_BIT;
    vkparams.createinfo.usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
}

void CreateImageParams::SetUsageDepthTarget() {
    aspect &= ~VK_IMAGE_ASPECT_COLOR_BIT;
    aspect |= VK_IMAGE_ASPECT_DEPTH_BIT;
    vkparams.createinfo.usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
}

void CreateImageParams::SetName(const char* imagename) {
    vkparams.name = imagename;
}

void CreateImageParams::SetQueueUsage(PlatformContext& context, bool graphics, bool compute) {
    // TODO
}

}