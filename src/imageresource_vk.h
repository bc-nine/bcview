#pragma once

#include "volk.h"
#include "imageview_vk.h"
#include "memallocator_vk.h"

namespace bc9 {

struct ImageResource {
    VkImage vkobj = nullptr;
    Allocation memory;

    ImageView view = {};
    ImageView rwview = {};
};

}
