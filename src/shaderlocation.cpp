#include "shaderlocation.h"

#if winvk_
#include "util_vk.h"
#endif

namespace bc9 {

ShaderLocation::ShaderLocation(const stdjson &ljson) {
#if winvk_
    location = ljson["location"].get<uint32_t>();
    StringID strformat;
    Throw_(ReqString(ljson, "format", strformat));
    Throw_(GetLocationFormat(strformat, format));
#endif
}

ShaderLocation::ShaderLocation() {
}

}
