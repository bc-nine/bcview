#include "framebufferlayout.h"
#include "platformcontext.h"
#include "resourceutil.h"
#include "util_d12.h"

namespace bc9 {

FramebufferLayout::FramebufferLayout() {
}

FramebufferLayout::FramebufferLayout(unsigned int colortargets, unsigned int msaasamples,
                                     FramebufferLayoutFlags flags) :
msaasamples(msaasamples),
colorcount(colortargets),
flags(flags) {
    depthcount = (flags & eFramebufferLayoutUseDepthStencilTarget) ? 1 : 0;
}

void FramebufferLayout::GetColorLayout(unsigned int idx, ColorLayout& color) const {
    assert(idx < colorlayouts.size());
    color = colorlayouts[idx];
}

void FramebufferLayout::GetColorDescription(unsigned int idx,
                                              D3D12_RENDER_TARGET_VIEW_DESC& desc) const {
    assert(idx < colordescs.size());
    desc = colordescs[idx];
}

void FramebufferLayout::GetDepthDescription(D3D12_DEPTH_STENCIL_VIEW_DESC& desc) const {
    desc = depthdesc;
}

void FramebufferLayout::GetColorLayout(const StringID& name, ColorLayout& color) const {
    unsigned int idx;
    GetColorIndex(name, idx);
    GetColorLayout(idx, color);
}

void FramebufferLayout::GetDepthStencilLayout(DepthStencilLayout& _dslayout) const {
    _dslayout = dslayout;
}

void FramebufferLayout::AddColorLayout(PlatformContext& context, ColorLayout& color) {
    bool colorpresenting = color.flags & eColorLayoutPresenting;

    D3D12_RENDER_TARGET_VIEW_DESC desc = {};
    Throw_(GetDXGIFormat(context, color.format, desc.Format));
    if (msaasamples == 1) {
        desc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
        desc.Texture2D.MipSlice = 0;
        desc.Texture2D.PlaneSlice = 0;
    }
    else {
        desc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2DMS;
    }
    colordescs.push_back(desc);

    colorlayouts.push_back(color);
    if (colorpresenting)
        presenting = true;
    if (msaasamples > 1 && color.NoResolve() == false)
        resolving = true;
}

unsigned int FramebufferLayout::GetDepthStencilLayoutCount() const {
    return  depthcount;
}

unsigned int FramebufferLayout::GetColorLayoutCount() const {
    return (unsigned int) colorlayouts.size();
}

bool FramebufferLayout::Multisampled() const {
    return msaasamples > 1;
}

unsigned int
FramebufferLayout::GetSampleCount() const {
    return msaasamples;
}

void
FramebufferLayout::AddDepthStencilLayout(PlatformContext& context, DepthStencilLayout& _dslayout) {
    dslayout = _dslayout;

    depthdesc = {};
    Throw_(GetDXGIFormat(context, dslayout.format, depthdesc.Format));
    if (msaasamples == 1) {
        depthdesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
        depthdesc.Flags = D3D12_DSV_FLAG_NONE;
        depthdesc.Texture2D.MipSlice = 0;
    }
    else {
        depthdesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2DMS;
        depthdesc.Flags = D3D12_DSV_FLAG_NONE;
    }
}

void FramebufferLayout::GetColorIndex(const StringID& name, unsigned int& index) const {
    for (unsigned int i = 0; i < colorlayouts.size(); ++i) {
        if (colorlayouts[i].name == name) {
            index = i;
            return;
        }
    }

    Throw_(Error("FramebufferLayout: couldn't find color layout '%s'", name.str()));
}

uint64_t FramebufferLayout::GetHash() const {
    return hashkey;
}

void FramebufferLayout::Initialize(PlatformContext& context, const StringID& name) {
    if (colorlayouts.size() != colorcount)
        Throw_(Error("%s: color layout count mismatch", name.str()));

    if (depthcount && !dslayout.Valid())
        Throw_(Error("%s: invalid depth layout", name.str()));

    context.hasher.Reset();
    for (unsigned int i = 0; i < colorcount; ++i) {
        // i've commented out the ones i'm unsure about here. it seems that
        // if the pipeline doesn't actually use this data as part of its
        // creation, then these shouldn't factor into the hash key.

        // name and clearcolor don't factor into the key
        //context.hasher.Update(&colorlayouts[i].loadop, sizeof(colorlayouts[i].loadop));
        //context.hasher.Update(&colorlayouts[i].storeop, sizeof(colorlayouts[i].storeop));
        context.hasher.Update(&colorlayouts[i].format, sizeof(colorlayouts[i].format));
        //context.hasher.Update(&colorlayouts[i].outlayout, sizeof(colorlayouts[i].outlayout));
        // i don't think shaderinput factors into the key
    }
    hashkey = context.hasher.Digest();
}

void FramebufferLayout::Shutdown(PlatformContext& context) {
}

}