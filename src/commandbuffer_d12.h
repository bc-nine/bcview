#pragma once

#include <wrl.h>
#include <d3d12.h>

namespace bc9 {

struct CommandBuffer {
    Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList2> d12obj;
};

}
