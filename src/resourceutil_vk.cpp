#include "resourceutil.h"
#include "resourceutil_vk.h"
#include "allocator_vk.h"
#include "DirectXTex.h"
#include <atlstr.h>
#include "util_vk.h"
#include "rendertarget.h"
#include "framebufferlayout.h"
#include "sharedstructs.h"
#include "assetcache.h"
#include "shaderresource_vk.h"
#include "drawablecache.h"
#include "bclib/mathutils.h"
#include "bclib/dxutils.h"
#include "commandbuffer_vk.h"
#include "rendertarget_vk.h"
#include "framebuffer_vk.h"
#include "xrheadset.h"
#include "surface.h"
#include "bclib/stdallocators.h"

namespace bc9 {

struct CopyVkBufferParams {
    VkDeviceSize size = 0;
};

struct Vertex {
    float pos[3];
};

Result CreateShader(PlatformContext& context, const ShaderAsset& asset, ShaderResource& shader) {
    Throw_(GetVkShaderStage(asset.type, shader.stage));
    shader.entrypoint = asset.entrypoint;

    VkShaderModuleCreateInfo createinfo = {};
    createinfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    createinfo.codeSize = asset.blob.size();
    createinfo.pCode = reinterpret_cast<const uint32*>(asset.blob.data());

    if (vkCreateShaderModule(context.device, &createinfo,
                             VkAllocators(), &shader.module) != VK_SUCCESS) {
        throw std::runtime_error("failed to create shader module");
    }

    NameVulkanObject(context.device, VK_OBJECT_TYPE_SHADER_MODULE, shader.module,
                     asset.name.str());

    return Success();
}

void DestroyShader(PlatformContext& context, ShaderResource& shader) {
    vkDestroyShaderModule(context.device, shader.module, VkAllocators());
}

void CopyBuffer(PlatformContext& context,
                BufferResource dst, uint64 dstoffset,
                BufferResource src, uint64 srcoffset,
                uint64 numbytes) {
    CommandBuffer cmdbuffer = context.BeginSingleTimeCommands();

    VkBufferCopy copyregion = {};
    copyregion.srcOffset = srcoffset;
    copyregion.dstOffset = dstoffset;
    copyregion.size = numbytes;
    vkCmdCopyBuffer(cmdbuffer.vkobj, src.vkobj, dst.vkobj, 1, &copyregion);

    context.EndSingleTimeCommands(cmdbuffer);
}

// all buffer creation *must* go through this routine, so e.g. name is set properly.
Result CreateBuffer(PlatformContext& context, const CreateVkBufferParams& params,
                    BufferResource& buffer) {
    VkBufferCreateInfo createinfo = params.createinfo;
    buffer = BufferResource(params.instances, params.instancesize);

    createinfo.size = buffer.GetSize();

    context.memallocator.CreateBuffer(context, params.properties, &createinfo,
                                      &buffer.vkobj, buffer.memory, params.name);

    if (params.initdata) {
        uint64 initdatasize = params.initdatasize ? params.initdatasize : buffer.GetInstanceSize();
        for(uint32 i = 0; i < params.instances; ++i) {
            if (buffer.memory.mappedmem) {
                void *mapped = buffer.Map(i);
                memcpy(mapped, params.initdata, initdatasize);
                if (!buffer.memory.coherent) {
                    VkMappedMemoryRange range {};
                    range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
                    range.memory = buffer.memory.memory;
                    range.offset = buffer.memory.offset;
                    range.size = buffer.GetSize();
                    vkFlushMappedMemoryRanges(context.device, 1, &range);
                }
            }
            else {
                CreateBufferParams stagingparams(context, eBufferResourceGeneral, initdatasize,
                                                 "staging buffer (initdata)");
                stagingparams.SetAccessMode(eBufferCPUAccessWrite);
                stagingparams.SetUsageCopySource(true);

                BufferResource staging;
                Return_(CreateBuffer(context, stagingparams, staging));

                void* data = staging.Map(0);
                unsigned char* ptr = (unsigned char*) data;
                memcpy(ptr, params.initdata, (size_t) initdatasize);

                CopyBuffer(context, buffer, i*buffer.GetInstanceSize(),
                           staging, 0, initdatasize);

                DestroyBuffer(context, staging);
            }
        }
    }

    if (createinfo.usage & VK_BUFFER_USAGE_STORAGE_BUFFER_BIT)
        context.AddBindlessStorageBuffer(buffer);

    //VkMemoryRequirements memreqs;
    //vkGetBufferMemoryRequirements(context.device, buffer.vkobj, &memreqs);
    // seems like only time this would be an issue is if i had an array of buffers?
    //Assert_(memreqs.alignment == 256);

    NameVulkanObject(context.device, VK_OBJECT_TYPE_BUFFER, buffer.vkobj, params.name);

    return Success();
}

Result CreateBufferView(PlatformContext& context, VkBufferViewCreateInfo& createinfo,
                        const char* name, VkBufferView& bufferview) {
    vkCreateBufferView(context.device, &createinfo, VkAllocators(), &bufferview);

    NameVulkanObject(context.device, VK_OBJECT_TYPE_BUFFER_VIEW, bufferview, name);

    return Success();
}

Result CreateBuffer(PlatformContext& context, const CreateBufferParams& params,
                    BufferResource& buffer) {
    CreateVkBufferParams vkparams{};
    Return_(params.Get(context, vkparams));
    Return_(CreateBuffer(context, vkparams, buffer));

    // texel buffers require the creation of a view
    VkBufferUsageFlags& flags = vkparams.createinfo.usage;
    bool istexelbuffer = (flags & VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT) ||
                         (flags & VK_BUFFER_USAGE_STORAGE_TEXEL_BUFFER_BIT);
    if (istexelbuffer) {
        Assert_(vkparams.viewcreateinfo.format != kInvalidUInt32);
        vkparams.viewcreateinfo.buffer = buffer.vkobj;
        vkparams.viewcreateinfo.offset = 0;
        vkparams.viewcreateinfo.range = buffer.GetSize();
        Return_(CreateBufferView(context, vkparams.viewcreateinfo, vkparams.name, buffer.view));
    }

    return Success();
}

Result CreateIndexBuffer(PlatformContext& context, const stdvector<GeoAsset>& geoassets,
                         stdvector<GeoData>& geodatas, BufferResource& indexbuffer) {
    VkDeviceSize offset = 0;

    uint64 totalindexcount = 0;

    Assert_(geodatas.size() == geoassets.size());

    for (uintn g = 0; g < geoassets.size(); ++g) {
        const GeoAsset& geo(geoassets[g]);
        GeoData& geodata = geodatas[g];
        geodata.drawdata.indexcount = geo.indexcount;
        geodata.drawdata.indexoffset = geo.indexcount ? uint32_(offset) : kInvalidUInt32;
        totalindexcount += geo.indexcount;
        offset += geo.indexcount;
    }

    VkDeviceSize buffersize = GeoAssetIndexUInt32 * totalindexcount;

    if (!buffersize) {
        // ray tracing mode wants to always bind the index buffer, even if
        // there aren't any indexed models, so we'll just create a stub
        // buffer here, capable of holding a single entry, to avoid validation
        // errors.
        CreateBufferParams bufparams(context, eBufferResourceIndex, uint64_(GeoAssetIndexUInt32),
                                     "index buffer (stub)");
        Return_(CreateBuffer(context, bufparams, indexbuffer));

        return Success();
    }

    CreateBufferParams bufparams(context, eBufferResourceIndex, totalindexcount,
                                 uint32_(GeoAssetIndexUInt32), "index buffer (engine)");
    bufparams.SetUsageCopyDestination(true);

    Return_(CreateBuffer(context, bufparams, indexbuffer));

    CreateMappedBufferParams stagingparams(context, eBufferResourceGeneral,
                                           totalindexcount, uint32_(GeoAssetIndexUInt32),
                                           "index buffer (engine staging)");
    stagingparams.SetUsageCopySource(true);

    BufferResource staging;
    Return_(CreateBuffer(context, stagingparams, staging));

    void* data = staging.Map(0);
    unsigned char* ptr = (unsigned char*) data;
    for (auto& geo : geoassets) {
        VkDeviceSize subbuffersize = geo.indexcount * uint32_(GeoAssetIndexUInt32);
        memcpy(ptr, geo.indices, (size_t) subbuffersize);
        ptr += subbuffersize;
    }
    Assert_(ptr == ((unsigned char*) data)+buffersize);

    CopyBuffer(context, indexbuffer, 0, staging, 0, buffersize);

    DestroyBuffer(context, staging);

    return Success();
}

void FillGeometry(const GeoAsset& asset, stdvector<Vertex>& vertices,
                  stdvector<uint32>& indices, uint32& maxvertexindex) {
    vertices.resize(asset.vertexcount);
    for (uintn i = 0; i < asset.vertexcount; ++i) {
        vertices[i].pos[0] = asset.pnts[8*i];
        vertices[i].pos[1] = asset.pnts[8*i+1];
        vertices[i].pos[2] = asset.pnts[8*i+2];
    }

    maxvertexindex = 0;
    indices.resize(asset.indexcount);
    for (uintn i = 0; i < asset.indexcount; ++i) {
        indices[i] = asset.indices[i];
        if (indices[i] > maxvertexindex)
            maxvertexindex = indices[i];
    }
}

void FillGeometry(const GeoAsset& asset, stdvector<Vertex>& vertices) {
    vertices.resize(asset.vertexcount);
    for (uintn i = 0; i < asset.vertexcount; ++i) {
        vertices[i].pos[0] = asset.pnts[8*i];
        vertices[i].pos[1] = asset.pnts[8*i+1];
        vertices[i].pos[2] = asset.pnts[8*i+2];
    }
}

Result CreateBLAS(PlatformContext& context,
                  const GeoAsset& asset, const CreateBLASParams& asparams,
                  BLASResource& blas) {
    StdStackAllocator stackalloc(GlobalStack());
    uint32 maxvertexindex = 0;
    stdvector<Vertex> vertices(stackalloc);
    stdvector<uint32> indices(stackalloc);
    // FIXME: Note that FillGeometry() is hardcoded with 8 elements (3 elem pos,
    //        3 elem normal, and 2 elem uv). This is bad, since we now support
    //        geometries with other attribute formats (e.g. multiple uv's).
    if (asset.indexcount)
        FillGeometry(asset, vertices, indices, maxvertexindex);
    else
        FillGeometry(asset, vertices);

    VkTransformMatrixKHR transformmatrix = {
        1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f
    };

    // if you're going to use CreateVkBufferParams, pay attn to instancesize! (see mar 6 2023)
    CreateVkBufferParams vkparams {};

    vkparams.instancesize = vertices.size() * sizeof(Vertex);
    vkparams.instances = 1;

    vkparams.createinfo.usage = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                              VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR;
    vkparams.properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
    vkparams.initdata = vertices.data();
    vkparams.name = "vertex buffer (blas)";
    Return_(CreateBuffer(context, vkparams, blas.vertexbuffer));

    const uint32 numindices  = asset.indexcount ? uint32_(indices.size()) : 3;

    vkparams.instancesize = numindices * sizeof(uint32);
    vkparams.createinfo.usage = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                          VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR;
    vkparams.properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
    vkparams.initdata = indices.data();
    vkparams.name = "index buffer (blas)";
    Return_(CreateBuffer(context, vkparams, blas.indexbuffer));

    vkparams.instancesize = sizeof(VkTransformMatrixKHR);
    vkparams.createinfo.usage = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                              VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR;
    vkparams.properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
    vkparams.initdata = &transformmatrix;
    vkparams.name = "transform buffer (blas)";
    Return_(CreateBuffer(context, vkparams, blas.transformbuffer));

    VkDeviceOrHostAddressConstKHR vertexbufferdeviceaddress {};
    VkDeviceOrHostAddressConstKHR indexbufferdeviceaddress {};
    VkDeviceOrHostAddressConstKHR transformbufferdeviceaddress {};

    vertexbufferdeviceaddress.deviceAddress = GetBufferDeviceAddress(context,
                                                                     blas.vertexbuffer.vkobj);
    indexbufferdeviceaddress.deviceAddress = GetBufferDeviceAddress(context,
                                                                    blas.indexbuffer.vkobj);
    transformbufferdeviceaddress.deviceAddress = GetBufferDeviceAddress(context,
                                                                   blas.transformbuffer.vkobj);

    VkAccelerationStructureGeometryKHR asgeometry {};
    asgeometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    asgeometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
    asgeometry.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
    asgeometry.geometry.triangles.sType =
        VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
    asgeometry.geometry.triangles.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
    asgeometry.geometry.triangles.vertexData = vertexbufferdeviceaddress;
    asgeometry.geometry.triangles.maxVertex = maxvertexindex;
    asgeometry.geometry.triangles.vertexStride = sizeof(Vertex);
    asgeometry.geometry.triangles.indexData = indexbufferdeviceaddress;
    if (asset.indexcount)
        asgeometry.geometry.triangles.indexType = VK_INDEX_TYPE_UINT32;
    else
        asgeometry.geometry.triangles.indexType = VK_INDEX_TYPE_NONE_KHR;
    asgeometry.geometry.triangles.transformData.deviceAddress = 0;
    asgeometry.geometry.triangles.transformData.hostAddress = nullptr;
    asgeometry.geometry.triangles.transformData = transformbufferdeviceaddress;

    VkAccelerationStructureBuildGeometryInfoKHR asbuildgeometryinfo {};
    asbuildgeometryinfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    asbuildgeometryinfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    asbuildgeometryinfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    asbuildgeometryinfo.geometryCount = 1;
    asbuildgeometryinfo.pGeometries = &asgeometry;

    const uint32 numtriangles = asset.indexcount ? ((uint32) (indices.size() / 3)) :
                                                     ((uint32) (vertices.size() / 3));
    VkAccelerationStructureBuildSizesInfoKHR asbuildsizesinfo {};
    asbuildsizesinfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    vkGetAccelerationStructureBuildSizesKHR(
        context.device,
        VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &asbuildgeometryinfo,
        &numtriangles,
        &asbuildsizesinfo);

    Return_(CreateAccelStructureBuffer(context, blas.buffer, asbuildsizesinfo));

    VkAccelerationStructureCreateInfoKHR ascreateinfo {};
    ascreateinfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    ascreateinfo.buffer = blas.buffer.vkobj;
    ascreateinfo.size = asbuildsizesinfo.accelerationStructureSize;
    ascreateinfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    VkCheck_(vkCreateAccelerationStructureKHR(context.device, &ascreateinfo,
                                              VkAllocators(), &blas.vkobj));

    RayTracingScratchBuffer scratchbuffer;
    Return_(CreateScratchBuffer(context, asbuildsizesinfo.buildScratchSize, scratchbuffer));

    asbuildgeometryinfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    asbuildgeometryinfo.dstAccelerationStructure = blas.vkobj;
    asbuildgeometryinfo.scratchData.deviceAddress = scratchbuffer.deviceaddress;

    VkAccelerationStructureBuildRangeInfoKHR asbuildrangeinfo {};
    asbuildrangeinfo.primitiveCount = numtriangles;
    asbuildrangeinfo.primitiveOffset = 0;
    asbuildrangeinfo.firstVertex = 0;
    asbuildrangeinfo.transformOffset = 0;
    stdvector<VkAccelerationStructureBuildRangeInfoKHR*> buildstructurerangeinfos = {
        &asbuildrangeinfo
    };

    if (context.asfeatures.accelerationStructureHostCommands) {
        vkBuildAccelerationStructuresKHR(
            context.device,
            VK_NULL_HANDLE,
            1,
            &asbuildgeometryinfo,
            buildstructurerangeinfos.data());
    }
    else {
        CommandBuffer cmdbuffer = context.BeginSingleTimeCommands();
        vkCmdBuildAccelerationStructuresKHR(
            cmdbuffer.vkobj,
            1,
            &asbuildgeometryinfo,
            buildstructurerangeinfos.data());
        context.EndSingleTimeCommands(cmdbuffer);
    }

    VkAccelerationStructureDeviceAddressInfoKHR deviceaddressinfo {};
    deviceaddressinfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    deviceaddressinfo.accelerationStructure = blas.vkobj;
    blas.deviceaddress = vkGetAccelerationStructureDeviceAddressKHR(context.device,
                                                                    &deviceaddressinfo);

    DeleteScratchBuffer(context, scratchbuffer);

    return Success();
}

void DestroyBLAS(PlatformContext& context, BLASResource& blas) {
    vkDestroyAccelerationStructureKHR(context.device, blas.vkobj, VkAllocators());
    DestroyBuffer(context, blas.buffer);
    DestroyBuffer(context, blas.vertexbuffer);
    DestroyBuffer(context, blas.indexbuffer);
    DestroyBuffer(context, blas.transformbuffer);
}

Result CreateTLAS(PlatformContext& context, const stdvector<NodeCache::BakedInstance>& instances,
                  const AssetCache& assetcache, const DrawableCache& drawablecache,
                  const CreateTLASParams& tlasparams, TLASResource& tlas) {
    StdStackAllocator stackalloc(GlobalStack());
    stdvector<VkAccelerationStructureInstanceKHR> vkinstances(stackalloc);

    uintn instancecount = uintn_(instances.size());

    vkinstances.resize(instancecount);
    for (uint32 i = 0; i < instancecount; ++i) {
        const NodeCache::BakedInstance& instance = instances[i];
        const Drawable* drawable(drawablecache.GetDrawable(instance.drawablekey));
        const BLASResource& bottom(assetcache.GetBLAS(drawable->geokey));

        VkAccelerationStructureInstanceKHR& vkinstance(vkinstances[i]);
        vkinstance.transform = GetTransformMatrix(instance.transform);
        vkinstance.instanceCustomIndex = instance.drawablekey.index;
        vkinstance.mask = 0xFF;
        vkinstance.instanceShaderBindingTableRecordOffset = i;
        vkinstance.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR;
        vkinstance.accelerationStructureReference = bottom.deviceaddress;
    }

    // if you're going to use CreateVkBufferParams, pay attn to instancesize! (see mar 6 2023)
    CreateVkBufferParams vkparams {};

    vkparams.instancesize = instancecount * sizeof(VkAccelerationStructureInstanceKHR);
    vkparams.instances = 1;

    vkparams.createinfo.usage = VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT |
                              VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR;
    vkparams.properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
    vkparams.initdata = vkinstances.data();
    vkparams.name = "instances buffer";
    BufferResource instancesbuffer;
    Return_(CreateBuffer(context, vkparams, instancesbuffer));

    VkDeviceOrHostAddressConstKHR instancedatadeviceaddress {};
    instancedatadeviceaddress.deviceAddress = GetBufferDeviceAddress(context,
                                                                     instancesbuffer.vkobj);

    VkAccelerationStructureGeometryKHR asgeometry {};
    asgeometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    asgeometry.geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    asgeometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
    asgeometry.geometry.instances.sType =
        VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
    asgeometry.geometry.instances.arrayOfPointers = VK_FALSE;
    asgeometry.geometry.instances.data = instancedatadeviceaddress;

    VkAccelerationStructureBuildGeometryInfoKHR asbuildgeometryinfo {};
    asbuildgeometryinfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    asbuildgeometryinfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    asbuildgeometryinfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_TRACE_BIT_KHR;
    asbuildgeometryinfo.geometryCount = 1;
    asbuildgeometryinfo.pGeometries = &asgeometry;

    uint32 primitivecount = instancecount;

    VkAccelerationStructureBuildSizesInfoKHR asbuildsizesinfo {};
    asbuildsizesinfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    vkGetAccelerationStructureBuildSizesKHR(
        context.device,
        VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &asbuildgeometryinfo,
        &primitivecount,
        &asbuildsizesinfo);

    Return_(CreateAccelStructureBuffer(context, tlas.buffer, asbuildsizesinfo));

    VkAccelerationStructureCreateInfoKHR ascreateinfo {};
    ascreateinfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
    ascreateinfo.buffer = tlas.buffer.vkobj;
    ascreateinfo.size = asbuildsizesinfo.accelerationStructureSize;
    ascreateinfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    VkCheck_(vkCreateAccelerationStructureKHR(context.device, &ascreateinfo,
                                            VkAllocators(), &tlas.vkobj));

    RayTracingScratchBuffer scratchbuffer;
    Return_(CreateScratchBuffer(context, asbuildsizesinfo.buildScratchSize, scratchbuffer));

    asbuildgeometryinfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    asbuildgeometryinfo.dstAccelerationStructure = tlas.vkobj;
    asbuildgeometryinfo.scratchData.deviceAddress = scratchbuffer.deviceaddress;

    VkAccelerationStructureBuildRangeInfoKHR asbuildrangeinfo {};
    asbuildrangeinfo.primitiveCount = primitivecount;
    asbuildrangeinfo.primitiveOffset = 0;
    asbuildrangeinfo.firstVertex = 0;
    asbuildrangeinfo.transformOffset = 0;
    stdvector<VkAccelerationStructureBuildRangeInfoKHR*> buildstructurerangeinfos = {
        &asbuildrangeinfo
    };

    if (context.asfeatures.accelerationStructureHostCommands) {
        VkCheck_(vkBuildAccelerationStructuresKHR(
            context.device,
            VK_NULL_HANDLE,
            1,
            &asbuildgeometryinfo,
            buildstructurerangeinfos.data()));
    }
    else
    {
        CommandBuffer cmdbuffer = context.BeginSingleTimeCommands();
        vkCmdBuildAccelerationStructuresKHR(
            cmdbuffer.vkobj,
            1,
            &asbuildgeometryinfo,
            buildstructurerangeinfos.data());
        context.EndSingleTimeCommands(cmdbuffer);
    }

    VkAccelerationStructureDeviceAddressInfoKHR deviceaddressinfo {};
    deviceaddressinfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    deviceaddressinfo.accelerationStructure = tlas.vkobj;
    tlas.deviceaddress = vkGetAccelerationStructureDeviceAddressKHR(context.device,
                                                                    &deviceaddressinfo);

    DeleteScratchBuffer(context, scratchbuffer);
    DestroyBuffer(context, instancesbuffer);

    context.AddBindlessAccelStructure(tlas);

    return Success();
}

void DestroyTLAS(PlatformContext& context, TLASResource& tlas) {
    vkDestroyAccelerationStructureKHR(context.device, tlas.vkobj, VkAllocators());
    DestroyBuffer(context, tlas.buffer);
}

Result CreateVertexBuffers(PlatformContext& context, const stdvector<GeoAsset>& geoassets,
                           stdvector<GeoData>& geodatas, BufferResource& pntbuffer,
                           BufferResource& vcolorbuffer, BufferResource& srgbcolorbuffer,
                           BufferResource& tex1coordbuffer, BufferResource& tangentbuffer) {
    uint32 pntvertexcount = 0;
    uint32 colorvertexcount = 0;
    uint32 srgbcolorvertexcount = 0;
    uint32 tex1coordvertexcount = 0;
    uint32 tangentvertexcount = 0;

    Assert_(geodatas.size() == geoassets.size());

    for (uint32 g = 0; g < geoassets.size(); ++g) {
        const GeoAsset& geo(geoassets[g]);
        GeoData& geodata = geodatas[g];
        geodata.drawdata.vertexcount = geo.vertexcount;

        geodata.pntoffset = pntvertexcount;
        pntvertexcount += geo.vertexcount;

        geodata.coloroffset = geo.colors ? colorvertexcount : kInvalidUInt32;
        if (geo.colors)
            colorvertexcount += geo.vertexcount;

        geodata.srgbcoloroffset = geo.srgbcolors ? srgbcolorvertexcount : kInvalidUInt32;
        if (geo.srgbcolors)
            srgbcolorvertexcount += geo.vertexcount;

        geodata.tex1coordoffset = geo.tex1coords ? tex1coordvertexcount : kInvalidUInt32;
        if (geo.tex1coords)
            tex1coordvertexcount += geo.vertexcount;

        geodata.tangentoffset = geo.tangents ? tangentvertexcount : kInvalidUInt32;
        if (geo.tangents)
            tangentvertexcount += geo.vertexcount;
    }

    uint32 pntbuffersize = pntvertexcount * GeoAsset::PNTSize;
    uint32 colorbuffersize = colorvertexcount * GeoAsset::ColorSize;
    uint32 srgbcolorbuffersize = srgbcolorvertexcount * GeoAsset::SRGBColorSize;
    uint32 tex1coordbuffersize = tex1coordvertexcount * GeoAsset::Tex1CoordSize;
    uint32 tangentbuffersize = tangentvertexcount * GeoAsset::TangentSize;

    // position/normal/texcoord0 (pnt) buffer
    Assert_(pntbuffersize);
    CreateBufferParams pntbufparams(context, eBufferResourceStructuredInput,
                                    pntvertexcount, GeoAsset::PNTSize, "pnt buffer (engine)");
    pntbufparams.SetUsageCopyDestination(true);
    Return_(CreateBuffer(context, pntbufparams, pntbuffer));

    CreateMappedBufferParams pntstagingparams(context, eBufferResourceStructuredInput,
                                              pntvertexcount, GeoAsset::PNTSize,
                                              "pnt buffer (engine staging)");
    pntstagingparams.SetUsageCopySource(true);

    BufferResource staging;
    Return_(CreateBuffer(context, pntstagingparams, staging));

    void* data = staging.memory.mappedmem;
    unsigned char* ptr = (unsigned char*) data;
    for (auto& geo : geoassets) {
        VkDeviceSize subbuffersize = geo.vertexcount * GeoAsset::PNTSize;
        memcpy(ptr, geo.pnts, (size_t) subbuffersize);
        ptr += subbuffersize;
    }
    Assert_(ptr == ((unsigned char*) data)+pntbuffersize);

    CopyBuffer(context, pntbuffer, 0, staging, 0, pntbuffersize);

    DestroyBuffer(context, staging);

    // vertex color buffer
    if (colorbuffersize) {
        CreateBufferParams vcolorbufparams(context, eBufferResourceStructuredInput,
                                           colorvertexcount, GeoAsset::ColorSize,
                                           "vertex color buffer (engine)");
        vcolorbufparams.SetUsageCopyDestination(true);
        Return_(CreateBuffer(context, vcolorbufparams, vcolorbuffer));

        CreateMappedBufferParams stagingparams(context, eBufferResourceStructuredInput,
                                               colorvertexcount, GeoAsset::ColorSize,
                                               "vertex color buffer (staging)");
        stagingparams.SetUsageCopySource(true);
        Return_(CreateBuffer(context, stagingparams, staging));

        void* data = staging.memory.mappedmem;
        unsigned char* ptr = (unsigned char*) data;
        for (auto& geo : geoassets) {
            if (geo.colors) {
                VkDeviceSize subbuffersize = staging.GetSize();
                memcpy(ptr, geo.colors, (size_t) subbuffersize);
                ptr += subbuffersize;
            }
        }
        Assert_(ptr == ((unsigned char*) data)+colorbuffersize);

        CopyBuffer(context, vcolorbuffer, 0, staging, 0, colorbuffersize);

        DestroyBuffer(context, staging);
    }

    // srgb vertex color buffer
    if (srgbcolorbuffersize) {
        CreateBufferParams srgbcolorbufparams(context, eBufferResourceStructuredInput,
                                              srgbcolorvertexcount, GeoAsset::SRGBColorSize,
                                              "srgb vertex color buffer (engine)");
        srgbcolorbufparams.SetUsageCopyDestination(true);
        Return_(CreateBuffer(context, srgbcolorbufparams, srgbcolorbuffer));

        CreateMappedBufferParams stagingparams(context, eBufferResourceStructuredInput,
                                               srgbcolorvertexcount, GeoAsset::SRGBColorSize,
                                               "srgb vertex color buffer (staging)");
        stagingparams.SetUsageCopySource(true);
        Return_(CreateBuffer(context, stagingparams, staging));

        void* data = staging.memory.mappedmem;
        unsigned char* ptr = (unsigned char*) data;
        for (auto& geo : geoassets) {
            if (geo.srgbcolors) {
                VkDeviceSize subbuffersize = geo.vertexcount * GeoAsset::SRGBColorSize;
                memcpy(ptr, geo.srgbcolors, (size_t) subbuffersize);
                ptr += subbuffersize;
            }
        }
        Assert_(ptr == ((unsigned char*) data)+srgbcolorbuffersize);

        CopyBuffer(context, srgbcolorbuffer, 0, staging, 0, srgbcolorbuffersize);

        DestroyBuffer(context, staging);
    }

    // tex coord 1 buffer
    if (tex1coordbuffersize) {
        CreateBufferParams tex1coordbufparams(context, eBufferResourceStructuredInput,
                                              tex1coordvertexcount, GeoAsset::Tex1CoordSize,
                                              "tex1coord buffer (engine)");
        tex1coordbufparams.SetUsageCopyDestination(true);
        Return_(CreateBuffer(context, tex1coordbufparams, tex1coordbuffer));

        CreateMappedBufferParams stagingparams(context, eBufferResourceStructuredInput,
                                               tex1coordvertexcount, GeoAsset::Tex1CoordSize,
                                               "tex1coord buffer (staging)");
        stagingparams.SetUsageCopySource(true);
        Return_(CreateBuffer(context, stagingparams, staging));

        void* data = staging.memory.mappedmem;
        unsigned char* ptr = (unsigned char*) data;
        for (auto& geo : geoassets) {
            if (geo.tex1coords) {
                VkDeviceSize subbuffersize = geo.vertexcount * GeoAsset::Tex1CoordSize;
                memcpy(ptr, geo.tex1coords, (size_t) subbuffersize);
                ptr += subbuffersize;
            }
        }
        Assert_(ptr == ((unsigned char*) data)+tex1coordbuffersize);

        CopyBuffer(context, tex1coordbuffer, 0, staging, 0, tex1coordbuffersize);

        DestroyBuffer(context, staging);
    }

    // tangent buffer
    if (tangentbuffersize) {
        CreateBufferParams tangentbufparams(context, eBufferResourceStructuredInput,
                                            tangentvertexcount, GeoAsset::TangentSize,
                                            "tangent buffer (engine)");
        tangentbufparams.SetUsageCopyDestination(true);
        Return_(CreateBuffer(context, tangentbufparams, tangentbuffer));

        CreateMappedBufferParams stagingparams(context, eBufferResourceStructuredInput,
                                               tangentvertexcount, GeoAsset::TangentSize,
                                               "tangent buffer (staging)");
        stagingparams.SetUsageCopySource(true);
        Return_(CreateBuffer(context, stagingparams, staging));

        void* data = staging.memory.mappedmem;
        unsigned char* ptr = (unsigned char*) data;
        for (auto& geo : geoassets) {
            if (geo.tangents) {
                VkDeviceSize subbuffersize = geo.vertexcount * GeoAsset::TangentSize;
                memcpy(ptr, geo.tangents, (size_t) subbuffersize);
                ptr += subbuffersize;
            }
        }
        Assert_(ptr == ((unsigned char*) data)+tangentbuffersize);

        CopyBuffer(context, tangentbuffer, 0, staging, 0, tangentbuffersize);

        DestroyBuffer(context, staging);
    }

    return Success();
}

void DestroyBuffer(PlatformContext& context, BufferResource& buffer) {
    if (buffer.view != VK_NULL_HANDLE)
        vkDestroyBufferView(context.device, buffer.view, VkAllocators());;
    if (buffer.vkobj)
        context.memallocator.DestroyBuffer(context, buffer.vkobj, buffer.memory);
}

Result CreateImage(PlatformContext& context, CommandBuffer* usecmdbuffer,
                   const CreateImageParams& params, ImageResource& image) {
    CreateVkImageParams vkparams{};
    Return_(params.Get(context, vkparams));
    Return_(CreateImage(context, vkparams, image));

    if (vkparams.initiallayout != VK_IMAGE_LAYOUT_UNDEFINED) {
        TransitionVkImageParams params { .computemasks = true };
        params.usecmdbuffer = usecmdbuffer;
        params.barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        params.barrier.newLayout = vkparams.initiallayout;
        params.barrier.image = image.vkobj;
        params.barrier.subresourceRange = vkparams.viewcreateinfo.subresourceRange;
        params.srcstagemask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
        params.dststagemask = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
        TransitionImage(context, params);
    }

    return Success();
}

Result CreateImage(PlatformContext& context, CreateVkImageParams& params, ImageResource& image) {
    image = ImageResource();

    Assert_(params.createinfo.usage);
    Assert_(params.createinfo.extent.width || params.createinfo.extent.height);
    context.memallocator.CreateImage(context, params.properties, &params.createinfo,
                                     &image.vkobj, image.memory, params.name);

    if (params.createview) {
        params.viewcreateinfo.image = image.vkobj;
        Return_(CreateImageView(context, params.viewcreateinfo, params.name, image.view.vkobj));

        if (params.createinfo.usage & VK_IMAGE_USAGE_SAMPLED_BIT)
            context.AddBindlessSampledImage(image.view);
        if (params.createinfo.usage & VK_IMAGE_USAGE_STORAGE_BIT) {
            Return_(CreateImageView(context, params.viewcreateinfo, params.name,
                                    image.rwview.vkobj));
            context.AddBindlessStorageImage(image.rwview);
        }
    }

    NameVulkanObject(context.device, VK_OBJECT_TYPE_IMAGE, image.vkobj, params.name);

    return Success();
}

static void CopyBufferToImage(PlatformContext& context, CommandBuffer* usecmdbuffer,
                              VkBuffer buffer, VkImage image, uint32 width, uint32 height) {
    CommandBuffer cmdbuffer = usecmdbuffer ? *usecmdbuffer : context.BeginSingleTimeCommands();

    VkBufferImageCopy region = {};
    region.bufferOffset = 0;
    region.bufferRowLength = 0;
    region.bufferImageHeight = 0;

    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;

    region.imageOffset = {0, 0, 0};
    region.imageExtent = {
        width,
        height,
        1
    };

    vkCmdCopyBufferToImage(
        cmdbuffer.vkobj,
        buffer,
        image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region
    );

    if (!usecmdbuffer)
        context.EndSingleTimeCommands(cmdbuffer);
}

static void CopyBufferToImage(PlatformContext& context, CommandBuffer* usecmdbuffer,
                              VkBuffer buffer, VkImage image,
                              stdvector<VkBufferImageCopy>& regions) {
    CommandBuffer cmdbuffer = usecmdbuffer ? *usecmdbuffer : context.BeginSingleTimeCommands();

    vkCmdCopyBufferToImage(
        cmdbuffer.vkobj,
        buffer,
        image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        uint32_(regions.size()),
        regions.data()
    );

    if (!usecmdbuffer)
        context.EndSingleTimeCommands(cmdbuffer);
}

void TransitionImage(PlatformContext& context, TransitionVkImageParams& params) {
    Assert_(params.computemasks || params.barrier.srcAccessMask != kInvalidUInt32);
    Assert_(params.computemasks || params.barrier.dstAccessMask != kInvalidUInt32);
    Assert_(params.srcstagemask != kInvalidUInt32);
    Assert_(params.dststagemask != kInvalidUInt32);
    CommandBuffer cmdbuffer = params.usecmdbuffer ? *params.usecmdbuffer :
                                                    context.BeginSingleTimeCommands();

    if (params.computemasks) {
        params.barrier.srcAccessMask = 0x0;
        params.barrier.dstAccessMask = 0x0;
        switch (params.barrier.oldLayout) {
            case VK_IMAGE_LAYOUT_UNDEFINED:
            params.barrier.srcAccessMask = 0;
            break;

            case VK_IMAGE_LAYOUT_PREINITIALIZED:
            params.barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
            break;

            case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
            params.barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            break;

            case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
            params.barrier.srcAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
            break;

            case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
            params.barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            break;

            case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
            params.barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            break;

            case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
            params.barrier.srcAccessMask = VK_ACCESS_SHADER_READ_BIT;
            break;

            default:
            params.barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
            break;
        }
        if (params.srcstagemask == VK_PIPELINE_STAGE_ALL_COMMANDS_BIT)
            params.barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;

        switch (params.barrier.newLayout)
        {
            case VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL:
            params.barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
            break;

            case VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL:
            params.barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
            break;

            case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
            params.barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            break;

            case VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL:
            params.barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
            break;

            case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
            params.barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            break;

            // see https://github.com/KhronosGroup/Vulkan-LoaderAndValidationLayers/issues/1717
            case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:
            params.barrier.dstAccessMask = 0;
            Assert_(params.dststagemask == VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT);
            break;

            default:
            params.barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
            break;
        }
        if (params.dststagemask == VK_PIPELINE_STAGE_ALL_COMMANDS_BIT)
            params.barrier.dstAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
    }

    vkCmdPipelineBarrier(
        cmdbuffer.vkobj,
        params.srcstagemask, params.dststagemask,
        0,
        0, nullptr,
        0, nullptr,
        1, &params.barrier
    );

    if (!params.usecmdbuffer)
        context.EndSingleTimeCommands(cmdbuffer);
}

Result CreateImageView(PlatformContext& context, const VkImageViewCreateInfo& createinfo,
                       const char* name, VkImageView& imageview) {
    VkCheck_(vkCreateImageView(context.device, &createinfo, VkAllocators(), &imageview));

    NameVulkanObject(context.device, VK_OBJECT_TYPE_IMAGE_VIEW, imageview, name);

    return Success();
}

Result CreateImage(PlatformContext& context, ImageAsset& asset, ImageResource& image) {
    DirectX::ScratchImage imagedata;
    DxThrow_(DirectX::LoadFromDDSFile(CStringW(asset.datapath.str()),
                                      DirectX::DDS_FLAGS_NONE, nullptr, imagedata));

    VkDeviceSize buffersize = (VkDeviceSize) imagedata.GetPixelsSize();

    const DirectX::TexMetadata& metadata = imagedata.GetMetadata();
    asset.width = (uintn) metadata.width;
    asset.height = (uintn) metadata.height;
    asset.depth = (uintn) metadata.depth;
    asset.arraysize = (uintn) metadata.arraySize;
    asset.miplevels = (uintn) metadata.mipLevels;
    asset.pmalpha = metadata.IsPMAlpha();

    bool redonly = false;
    Assert_(asset.colorspace != eColorSpaceTypeCount);
    Assert_(asset.colorspace == eColorSpaceLinear || asset.colorspace == eColorSpaceSRGB);
    switch (metadata.format) {
        case DXGI_FORMAT_R8_UNORM:
        redonly = true;
        if (asset.colorspace == eColorSpaceLinear)
            asset.format = eResourceFormatR8_UNORM;
        else if (asset.colorspace == eColorSpaceSRGB)
            asset.format = eResourceFormatR8_SRGB;
        break;

        case DXGI_FORMAT_R8G8B8A8_UNORM:
        case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
        if (asset.colorspace == eColorSpaceLinear)
            asset.format = eResourceFormatR8G8B8A8_UNORM;
        else if (asset.colorspace == eColorSpaceSRGB)
            asset.format = eResourceFormatR8G8B8A8_SRGB;
        break;

        case DXGI_FORMAT_B8G8R8A8_UNORM:
        case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
        if (asset.colorspace == eColorSpaceLinear)
            asset.format = eResourceFormatB8G8R8A8_UNORM;
        else if (asset.colorspace == eColorSpaceSRGB)
            asset.format = eResourceFormatB8G8R8A8_SRGB;
        break;

        default:
            return Error("%s: unsupported image format", asset.name.str());
        break;
    }

    VkFormat format;
    Ignore_(GetVkFormat(asset.format, format)); // already validated above

    // if you're going to use CreateVkBufferParams, pay attn to instancesize! (see mar 6 2023)
    CreateVkBufferParams params = {};
    params.instances = 1;
    params.instancesize = buffersize;
    params.createinfo.size = buffersize;
    params.createinfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    params.properties = VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT;
    FString stagingname("%s %s", asset.name.str(), "(staging)");
    params.name = stagingname.sid().str();
    BufferResource staging;
    Return_(CreateBuffer(context, params, staging));

    size_t offset = 0;
    stdvector<VkBufferImageCopy> buffercopyregions;

    char *cdata = static_cast<char*>(staging.memory.mappedmem);

    for (uint32 mip = 0; mip < asset.miplevels; ++mip) {
        VkBufferImageCopy buffercopyregion = {};
        buffercopyregion.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        buffercopyregion.imageSubresource.mipLevel = mip;
        buffercopyregion.imageSubresource.baseArrayLayer = 0;
        buffercopyregion.imageSubresource.layerCount = asset.arraysize;
        buffercopyregion.imageExtent.width = Max(1u,  uint32_(asset.width >> mip));
        buffercopyregion.imageExtent.height = Max(1u, uint32_(asset.height >> mip));
        Assert_(buffercopyregion.imageExtent.width);
        Assert_(buffercopyregion.imageExtent.height);
        buffercopyregion.imageExtent.depth = 1;
        buffercopyregion.bufferOffset = offset;

        buffercopyregions.push_back(buffercopyregion);

        for (uint32 slice = 0; slice < asset.arraysize; ++slice) {
            const DirectX::Image* subimage = imagedata.GetImage(mip, slice, 0);
            size_t pitch = subimage->rowPitch;
            const UINT8* sourcesubresmem = subimage->pixels;
            for (UINT64 height = 0; height < subimage->height; ++height)
            {
                memcpy(cdata, sourcesubresmem, pitch);
                cdata += pitch;
                sourcesubresmem += pitch;
                offset += pitch;
            }
        }
    }

    CreateVkImageParams vkparams { .createview = true };
    if (asset.type == eImageAsset1D)
        vkparams.createinfo.imageType = VK_IMAGE_TYPE_1D;
    else if (asset.type == eImageAsset1DArray)
        vkparams.createinfo.imageType = VK_IMAGE_TYPE_1D;
    else if (asset.type == eImageAsset2D)
        vkparams.createinfo.imageType = VK_IMAGE_TYPE_2D;
    else if (asset.type == eImageAsset2DArray)
        vkparams.createinfo.imageType = VK_IMAGE_TYPE_2D;
    else if (asset.type == eImageAsset3D)
        vkparams.createinfo.imageType = VK_IMAGE_TYPE_3D;
    else if (asset.type == eImageAssetCube) {
        vkparams.createinfo.imageType = VK_IMAGE_TYPE_2D;
        vkparams.createinfo.flags |= VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
    }
    else if (asset.type == eImageAssetCubeArray) {
        vkparams.createinfo.imageType = VK_IMAGE_TYPE_2D;
        vkparams.createinfo.flags |= VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
    }
    else
        return Error("invalid image type");

    vkparams.createinfo.extent.width = asset.width;
    vkparams.createinfo.extent.height = asset.height;
    vkparams.createinfo.extent.depth = 1;
    vkparams.createinfo.mipLevels = asset.miplevels;
    vkparams.createinfo.arrayLayers = asset.arraysize;
    vkparams.createinfo.format = format;
    vkparams.createinfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    vkparams.createinfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    vkparams.createinfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    vkparams.createinfo.samples = VK_SAMPLE_COUNT_1_BIT;
    vkparams.properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;
    vkparams.name = asset.name.str();

    // image view
    vkparams.createview = true;
    if (asset.type == eImageAsset1D)
        vkparams.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_1D;
    else if (asset.type == eImageAsset1DArray)
        vkparams.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_1D_ARRAY;
    else if (asset.type == eImageAsset2D)
        vkparams.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    else if (asset.type == eImageAsset2DArray)
        vkparams.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY;
    else if (asset.type == eImageAsset3D)
        vkparams.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_3D;
    else if (asset.type == eImageAssetCube)
        vkparams.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE;
    else if (asset.type == eImageAssetCubeArray)
        vkparams.viewcreateinfo.viewType = VK_IMAGE_VIEW_TYPE_CUBE_ARRAY;
    else
        Throw_(Error("invalid image type"));

    vkparams.viewcreateinfo.format = format;

    VkComponentSwizzle r = VK_COMPONENT_SWIZZLE_R;
    VkComponentSwizzle g = redonly ? VK_COMPONENT_SWIZZLE_R : VK_COMPONENT_SWIZZLE_G;
    VkComponentSwizzle b = redonly ? VK_COMPONENT_SWIZZLE_R : VK_COMPONENT_SWIZZLE_B;
    vkparams.viewcreateinfo.components = { r, g, b, VK_COMPONENT_SWIZZLE_A };
    vkparams.viewcreateinfo.subresourceRange = {};
    vkparams.viewcreateinfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    vkparams.viewcreateinfo.subresourceRange.baseMipLevel = 0;
    vkparams.viewcreateinfo.subresourceRange.levelCount = asset.miplevels;
    vkparams.viewcreateinfo.subresourceRange.baseArrayLayer = 0;
    vkparams.viewcreateinfo.subresourceRange.layerCount = asset.arraysize;

    Return_(CreateImage(context, vkparams, image));

    VkImageSubresourceRange range = {};
    range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    range.baseMipLevel = 0;
    range.levelCount = asset.miplevels;
    range.baseArrayLayer = 0;
    range.layerCount = asset.arraysize;

    CommandBuffer cmdbuffer = context.BeginSingleTimeCommands();

    TransitionVkImageParams tparams { .computemasks = true };
    tparams.usecmdbuffer = &cmdbuffer;
    tparams.barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    tparams.barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    tparams.barrier.image = image.vkobj;
    tparams.barrier.subresourceRange = vkparams.viewcreateinfo.subresourceRange;
    tparams.srcstagemask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    tparams.dststagemask = VK_PIPELINE_STAGE_TRANSFER_BIT;

    TransitionImage(context, tparams);

    CopyBufferToImage(context, &cmdbuffer, staging.vkobj, image.vkobj, buffercopyregions);

    tparams.barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    tparams.barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    tparams.srcstagemask = VK_PIPELINE_STAGE_TRANSFER_BIT;
    tparams.dststagemask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;

    TransitionImage(context, tparams);

    context.EndSingleTimeCommands(cmdbuffer);

    DestroyBuffer(context, staging);

    return Success();
}

Result CreateColorTarget(PlatformContext& context, FramebufferLayout& layout,
                         CreateColorTargetParams& targetparams, ColorTarget& color) {
    uintn samples = layout.GetSampleCount();

    ColorLayout colorlayout;
    layout.GetColorLayout(targetparams.name, colorlayout);
    Assert_(colorlayout.Valid());

    bool resolve = !colorlayout.NoResolve();
    bool shaderreadable = colorlayout.ShaderReadable();

    CreateImageParams params(colorlayout.format, targetparams.width, targetparams.height,
                             1, 1, targetparams.layers, 1, false, targetparams.name.str());
    CreateImageParams msaaparams(colorlayout.format, targetparams.width, targetparams.height,
                                 1, 1, targetparams.layers, samples, false,
                                 targetparams.name.str());

    if (samples > 1) {
        if (!resolve) {
            if (shaderreadable) {
                msaaparams.SetInitialLayout(eImageLayoutShaderReadOnly);
            }
            else {
                msaaparams.SetInitialLayout(eImageLayoutColorOutput);
            }
        }
        else {
            if (shaderreadable) {
                params.SetInitialLayout(eImageLayoutShaderReadOnly);
                msaaparams.SetInitialLayout(eImageLayoutColorOutput);
            }
            else {
                params.SetInitialLayout(eImageLayoutColorOutput);
                msaaparams.SetInitialLayout(eImageLayoutColorOutput);
            }
        }
    }
    else {
        if (shaderreadable) {
            params.SetInitialLayout(eImageLayoutShaderReadOnly);
        }
        else {
            params.SetInitialLayout(eImageLayoutColorOutput);
        }
    }

    params.SetUsageColorTarget();
    msaaparams.SetUsageColorTarget();

    // msaa target
    if (samples > 1) {
        FString64 targetname("%s%s", targetparams.name.str(), "(msaa)");
        msaaparams.SetName(targetname.sid().str());
        Return_(CreateImage(context, nullptr, msaaparams, color.msaatarget));
    }

    if (!colorlayout.Presenting()) {
        // resolve target (or non-msaa target)
        if (colorlayout.flags & eColorLayoutShaderReadable)
            params.SetUsageSampled();

        Return_(CreateImage(context, nullptr, params, color.target));
    }

    return Success();
}

void DestroyColorTarget(PlatformContext& context, ColorTarget& color) {
    if (color.msaatarget.vkobj)
        DestroyImage(context, color.msaatarget);
    if (color.target.vkobj)
        DestroyImage(context, color.target);
}

Result CreateDepthStencilTarget(PlatformContext& context, FramebufferLayout& layout,
                                CreateDepthStencilTargetParams& params,
                                DepthStencilTarget& dstarget) {
    // at some point this function may need to support creating separate resources
    // for depth and stencil images.
    uintn samples = layout.GetSampleCount();

    DepthStencilLayout dslayout;
    layout.GetDepthStencilLayout(dslayout);
    Assert_(dslayout.Valid());
    CreateImageParams imgparams(dslayout.format, params.width, params.height, 1,
                                1, params.layers, samples, false, "dstarget");

    if (dslayout.DepthEnabled())
        imgparams.SetUsageDepthTarget();

    if (dslayout.StencilEnabled())
        imgparams.SetUsageStencilTarget();

    // TODO: deal with setting up depth targets for reading from shaders

    Return_(CreateImage(context, nullptr, imgparams, dstarget.target));

    return Success();
}

void DestroyDepthStencilTarget(PlatformContext& context, DepthStencilTarget& dstarget) {
    DestroyImage(context, dstarget.target);
}

void DestroyImage(PlatformContext& context, ImageResource& image) {
    if (image.view.vkobj != VK_NULL_HANDLE)
        vkDestroyImageView(context.device, image.view.vkobj, VkAllocators());
    if (image.rwview.vkobj != VK_NULL_HANDLE)
        vkDestroyImageView(context.device, image.rwview.vkobj, VkAllocators());
    context.memallocator.DestroyImage(context, image.vkobj, image.memory);
}

Result CreateSwapchainFramebufferLayout(PlatformContext& context, const StringID& name,
                                        FramebufferLayout& fblayout) {
    fblayout = FramebufferLayout(1, 1);

    ColorLayout clayout(name.str(), context.surface.GetSwapchainFormat(),
                        eColorLayoutPresenting | eColorLayoutStoreOpStore);
    fblayout.AddColorLayout(context, clayout);

    return Success();
}

Result CreateXRSwapchainFramebufferLayout(PlatformContext& context, const StringID& name,
                                          FramebufferLayout& fblayout) {
    fblayout = FramebufferLayout(1, 1, eFramebufferLayoutFlagsNone, eMultiview2Layer);

    ColorLayout clayout(name.str(), context.headset.GetSwapchainFormat(), eColorLayoutStoreOpStore);
    fblayout.AddColorLayout(context, clayout);

    return Success();
}

Result CreateSwapchainFramebuffer(PlatformContext& context, const FramebufferLayout& fblayout,
                                  Framebuffer& framebuffer) {
    SwapchainTarget swapchaintarget{};
    uintn imagecount = context.surface.GetSwapchainImageCount();
    Assert_(imagecount < kMaxSwapchainImageCount);
    for (uintn i = 0; i < imagecount; ++i)
        swapchaintarget.targets[i] = context.surface.GetSwapchainImage(i);

    uintn w, h;
    context.surface.GetViewSize(w, h);
    framebuffer.Initialize(context, &fblayout, swapchaintarget, w, h, imagecount);

    return Success();
}

Result CreateXRSwapchainFramebuffer(PlatformContext& context, const FramebufferLayout& fblayout,
                                    Framebuffer& framebuffer) {
    SwapchainTarget swapchaintarget{};
    uintn imagecount = context.headset.GetSwapchainImageCount();
    Assert_(imagecount < kMaxSwapchainImageCount);
    for (uintn i = 0; i < imagecount; ++i)
        swapchaintarget.targets[i] = context.headset.GetSwapchainImage(i);

    uintn w, h;
    context.headset.GetViewSize(w, h);
    framebuffer.Initialize(context, &fblayout, swapchaintarget, w, h, imagecount);

    return Success();
}

Result CreateImageViewsFromArray(PlatformContext& context, ColorTarget& color,
                                 ColorLayout& layout, ImageView* imageviews, uintn viewcount) {
    VkImageViewCreateInfo createinfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = color.target.vkobj,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .components = {
            .r = VK_COMPONENT_SWIZZLE_R,
            .g = VK_COMPONENT_SWIZZLE_G,
            .b = VK_COMPONENT_SWIZZLE_B,
            .a = VK_COMPONENT_SWIZZLE_A
        },
        .subresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
        }
    };

    Return_(GetVkFormat(layout.format, createinfo.format));

    for (uintn vidx = 0; vidx < viewcount; ++vidx) {
        ImageView& imageview = imageviews[vidx];
        FString name("%d_%s", vidx, layout.name.str());
        createinfo.subresourceRange.baseArrayLayer = vidx;

        Return_(CreateImageView(context, createinfo, name.str(), imageview.vkobj));
        context.AddBindlessSampledImage(imageview);
    }

    return Success();
}

}
