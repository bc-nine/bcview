#include "pipelinestate.h"
#include "platformcontext.h"

namespace bc9 {

GraphicsPipelineBase::GraphicsPipelineBase(PlatformContext& context,
                                           ShaderKey vertexkey,
                                           ShaderKey pixelkey) :
vertexkey(vertexkey),
pixelkey(pixelkey) {
    context.hasher.Reset();
    context.hasher.Update(&vertexkey, sizeof(ShaderKey));
    context.hasher.Update(&pixelkey, sizeof(ShaderKey));
    context.hasher.Update(&topology, sizeof(PrimitiveTopology));
    hash = context.hasher.Digest();
}

uint64 GraphicsPipelineBase::GetHash() const {
    return hash;
}

uint64 GraphicsPipelineState::GetHash(const PlatformContext& context) const {
    context.hasher.Reset();
    context.hasher.Update(&blendingenabled, sizeof(blendingenabled));
    context.hasher.Update(&msaasamples, sizeof(msaasamples));
    context.hasher.Update(&polygonmode, sizeof(PolygonMode));
    context.hasher.Update(&depthcompareop, sizeof(depthcompareop));
    context.hasher.Update(&depthtest, sizeof(depthtest));
    context.hasher.Update(&depthwrite, sizeof(depthwrite));
    context.hasher.Update(&stenciltest, sizeof(stenciltest));
    context.hasher.Update(&cullmode, sizeof(cullmode));

    context.hasher.Update(&comparemask, sizeof(comparemask));
    context.hasher.Update(&writemask, sizeof(writemask));

    context.hasher.Update(&frontfailop, sizeof(frontfailop));
    context.hasher.Update(&frontpassop, sizeof(frontpassop));
    context.hasher.Update(&frontdepthfailop, sizeof(frontdepthfailop));
    context.hasher.Update(&frontcompareop, sizeof(frontcompareop));

    context.hasher.Update(&backfailop, sizeof(backfailop));
    context.hasher.Update(&backpassop, sizeof(backpassop));
    context.hasher.Update(&backdepthfailop, sizeof(backdepthfailop));
    context.hasher.Update(&backcompareop, sizeof(backcompareop));

    return context.hasher.Digest();
}

uint64 GraphicsPipelineData::GetPipelineHash(const PlatformContext& context,
                                             uint64 basehash) const {
    uint64 statehash = state.GetHash(context);

    context.hasher.Reset();
    context.hasher.Update(&basehash, sizeof(basehash));
    context.hasher.Update(&statehash, sizeof(statehash));
    context.hasher.Update(&fblayoutdata, sizeof(fblayoutdata));

    return context.hasher.Digest();
}

ComputePipelineBase::ComputePipelineBase(PlatformContext& context, ShaderKey computekey) :
computekey(computekey) {
    context.hasher.Reset();
    context.hasher.Update(&computekey, sizeof(ShaderKey));
    hash = context.hasher.Digest();
}

uint64 ComputePipelineBase::GetHash() const {
    return hash;
}

}