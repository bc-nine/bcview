#pragma once

#if winvk_
#include "imageview_vk.h"
#elif wind12_
#include "imageview_d12.h"
#endif