#pragma once

#include "bclib/stringid.h"
#include "bclib/stdvector.h"
#include "bclib/stdarray.h"
#include "drawdata.h"
#include "rhitypes.h"

namespace bc9 {

class SessionContext;
struct RenderState;
struct PerFrameConstants;

class RenderPass {
public:
    RenderPass(const StringID& name);

    virtual void Initialize(SessionContext& scontext);
    virtual void Shutdown(SessionContext& scontext);

    virtual void PostInitialize(SessionContext& scontext);

    virtual void PreRender(SessionContext& scontext);
    virtual void Render(SessionContext& scontext);
    virtual void PostRender(SessionContext& scontext);
    virtual void Update(SessionContext& scontext, float dt);

    virtual void UpdateConstants(SessionContext& scontext, PerFrameConstants& pf) {}

    virtual void Begin(SessionContext& scontext);
    virtual void End(SessionContext& scontext);

    virtual void OverlayUpdateMenuBar(SessionContext& scontext);
    virtual void OverlayUpdate(SessionContext& scontext);

    virtual void ViewResized(SessionContext& scontext, uintn width, uintn height);

    virtual void CaptureRenderState(RenderState& state);

    virtual ~RenderPass() {}

protected:
    StringID passname;
};

}