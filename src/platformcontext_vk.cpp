#include "platformcontext_vk.h"
#include "assetcache.h"
#include "allocator_vk.h"
#include "util_vk.h"
#include "externalallocators.h"
#include "imgui.h"
#include "imgui_impl_sdl2.h"
#include "imgui_impl_vulkan.h"
#include "resourceutil_vk.h"
#include "sessioncontext.h"
#include "framebufferlayout.h"
#include "constants.h"
#include "world.h"
#include "bclib/stdset.h"
#include "bclib/mathutils.h"
#include "sharedstructs.h"
#include "surface.h"
#include "xrheadset.h"
#include "bclib/stdallocators.h"

#include <stdexcept>

// FIXME remove this
#define SDL_MAIN_HANDLED
#include "SDL.h"
#include "SDL_vulkan.h"

namespace bc9 {

static constexpr uintn kMaxBindlessSampledImages   = 1024;
static constexpr uintn kMaxBindlessStorageImages   = 1024;
static constexpr uintn kMaxBindlessAccelStructures = 1024;
static constexpr uintn kMaxBindlessStorageBuffers  = 256;

// NOTE: these slots *must* stay in sync with global.hlsli!
static constexpr uintn kBindlessStorageBufferSlot  = 0;
static constexpr uintn kBindlessSampledImageSlot   = 1;
static constexpr uintn kBindlessStorageImageSlot   = 2;
static constexpr uintn kBindlessAccelStructureSlot = 4;

static void CheckVkResult(VkResult err)
{
    VkCheck_(err);
}

Result FindAsyncComputeQueueFamily(VkPhysicalDevice device, uint32_t& asynccomputeidx) {
    uint32_t queuefamilycount = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queuefamilycount, nullptr);

    stdvector<VkQueueFamilyProperties> queuefamilies(queuefamilycount);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &queuefamilycount, queuefamilies.data());

    for (uint32_t i = 0; i < queuefamilies.size(); ++i) {
        auto& queuefamily(queuefamilies[i]);
        if ((queuefamily.queueFlags & VK_QUEUE_COMPUTE_BIT) &&
            !(queuefamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
            asynccomputeidx = i;
            return Success();
        }
    }

    return Error("no suitable family found for async compute");
}

static void CreateTextureSamplers(PlatformContext& context) {
    uintn samplercount = context.assetcache.GetSamplerCount();
    context.samplers.resize(samplercount);

    VkFilter minfilter, magfilter;
    VkSamplerMipmapMode mipmapmode;
    VkSamplerAddressMode addrmodeu, addrmodev, addrmodew;
    VkSamplerCreateInfo samplerinfo = { VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO };
    for (uintn sidx = 0; sidx < samplercount; ++sidx) {
        VkSampler& vksampler = context.samplers[sidx];
        SamplerAsset sampler = context.assetcache.GetSamplerAsset(SamplerKey(sidx));
        Throw_(GetVkSamplerMode(sampler.addressu, addrmodeu));
        Throw_(GetVkSamplerMode(sampler.addressv, addrmodev));
        Throw_(GetVkSamplerMode(sampler.addressw, addrmodew));
        Throw_(GetVkSamplerFilter(sampler.minfilter, minfilter));
        Throw_(GetVkSamplerFilter(sampler.magfilter, magfilter));
        Throw_(GetVkSamplerMipmapFilter(sampler.mipmapfilter, mipmapmode));
        samplerinfo.magFilter = magfilter;
        samplerinfo.minFilter = minfilter;
        samplerinfo.mipmapMode = mipmapmode;
        samplerinfo.addressModeU = addrmodeu;
        samplerinfo.addressModeV = addrmodev;
        samplerinfo.addressModeW = addrmodew;
        samplerinfo.mipLodBias = 0.0f;
        samplerinfo.anisotropyEnable = VK_TRUE;
        samplerinfo.maxAnisotropy = context.deviceproperties.limits.maxSamplerAnisotropy;
        samplerinfo.compareEnable = VK_FALSE;
        samplerinfo.compareOp = VK_COMPARE_OP_ALWAYS;
        samplerinfo.minLod = 0.0f;
        samplerinfo.maxLod = VK_LOD_CLAMP_NONE;
        samplerinfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
        samplerinfo.unnormalizedCoordinates = VK_FALSE;
        Throw_(CreateSampler(context, samplerinfo, vksampler, sampler.name));
    }
}

static void CreateBindlessDescriptorSets(PlatformContext& context) {
    for (size_t i = 0; i < kFrameRenderBufferCount; ++i) {
        VkDescriptorSetAllocateInfo allocinfo = {};
        allocinfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
        allocinfo.descriptorPool = context.descriptorpool;
        allocinfo.descriptorSetCount = 1;
        allocinfo.pSetLayouts = &context.setlayouts[1];

        if (vkAllocateDescriptorSets(context.device, &allocinfo,
                                     &context.bindlesssets[i]) != VK_SUCCESS) {
          Throw_(Error("failed to allocate descriptor sets"));
        }
        NameVulkanObject(context.device, VK_OBJECT_TYPE_DESCRIPTOR_SET, context.bindlesssets[i],
                         "bindless-set");
    }

    context.current.bindlessset = context.bindlesssets[context.bufferidx];
}

static VkSampleCountFlagBits GetSampleCount(VkPhysicalDevice physicaldevice, uintn samples) {
    // Ensure that requested sample count is a power of two.
    Assert_( (samples != 0) && ((samples & (samples - 1)) == 0) );

    VkPhysicalDeviceProperties physicaldeviceproperties;
    vkGetPhysicalDeviceProperties(physicaldevice, &physicaldeviceproperties);

    VkSampleCountFlags counts = physicaldeviceproperties.limits.framebufferColorSampleCounts &
                                physicaldeviceproperties.limits.framebufferDepthSampleCounts;

    while (1) {
        switch (samples) {
            case 64:
            if (counts & VK_SAMPLE_COUNT_64_BIT) { return VK_SAMPLE_COUNT_64_BIT; }
            break;

            case 32:
            if (counts & VK_SAMPLE_COUNT_32_BIT) { return VK_SAMPLE_COUNT_32_BIT; }
            break;

            case 16:
            if (counts & VK_SAMPLE_COUNT_16_BIT) { return VK_SAMPLE_COUNT_16_BIT; }
            break;

            case 8:
            if (counts & VK_SAMPLE_COUNT_8_BIT) { return VK_SAMPLE_COUNT_8_BIT; }
            break;

            case 4:
            if (counts & VK_SAMPLE_COUNT_4_BIT) { return VK_SAMPLE_COUNT_4_BIT; }
            break;

            case 2:
            if (counts & VK_SAMPLE_COUNT_2_BIT) { return VK_SAMPLE_COUNT_2_BIT; }
            break;

            case 1:
            return VK_SAMPLE_COUNT_1_BIT;
            break;
        }
        samples >>= 1;
    }

    Assert_(0);
    return VK_SAMPLE_COUNT_1_BIT;
}

static const char* ValidationExclusions[] = {
    "(null)", // can't have arrays of size 0
};

static VKAPI_ATTR VkBool32 VKAPI_CALL
DebugUtilsCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageseverity,
                   VkDebugUtilsMessageTypeFlagsEXT messagetype,
                   const VkDebugUtilsMessengerCallbackDataEXT* pcallbackdata,
                   void* puserdata) {
    bool exclude = false;
    for (int i = 0; i < ArraySize_(ValidationExclusions); ++i)
        if (strstr(pcallbackdata->pMessage, ValidationExclusions[i]))
            exclude = true;

    if (!exclude)
        printf("\nvulkan validation: %s\n", pcallbackdata->pMessage);

    if (!exclude && messageseverity == VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
        __debugbreak();

    return VK_FALSE;
}

VkBool32 DebugReportCallback(VkDebugReportFlagsEXT flags,
                             VkDebugReportObjectTypeEXT objecttype,
                             uint64 object,
                             sizen location,
                             int32 messagecode,
                             const char* playerprefix,
                             const char* pmessage,
                             void* userdata) {
    bool exclude = false;
    for (int i = 0; i < ArraySize_(ValidationExclusions); ++i)
        if (strstr(pmessage, ValidationExclusions[i]))
            exclude = true;

    if (!exclude)
        printf("\nvulkan validation: %s\n", pmessage);

    if (!exclude && (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT))
        __debugbreak();

    return VK_FALSE;
}

static void PopulateDebugReportCreateInfo(VkDebugReportCallbackCreateInfoEXT& createinfo) {
    createinfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    createinfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;
    createinfo.flags |= VK_DEBUG_REPORT_INFORMATION_BIT_EXT |
                        VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT |
                        VK_DEBUG_REPORT_DEBUG_BIT_EXT;
    createinfo.pfnCallback = DebugReportCallback;
    createinfo.pUserData = nullptr;
}

static void PopulateDebugUtilsCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createinfo) {
    // the INFO bit below is needed for debug printf messages to be visible.
    createinfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    createinfo.messageSeverity = //VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                 VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    createinfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                             VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT; //|
                             //VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    createinfo.pfnUserCallback = DebugUtilsCallback;
    createinfo.pUserData = nullptr;
}

static void SetupDebugUtilsCallback(VkInstance instance, VkDebugUtilsMessengerEXT& callback) {
    VkDebugUtilsMessengerCreateInfoEXT createinfo = {};
    PopulateDebugUtilsCreateInfo(createinfo);

    VkCheck_(vkCreateDebugUtilsMessengerEXT(instance, &createinfo, VkAllocators(), &callback));
}

static void SetupDebugReportCallback(VkInstance instance, VkDebugReportCallbackEXT& callback) {
    VkDebugReportCallbackCreateInfoEXT createinfo = {};
    PopulateDebugReportCreateInfo(createinfo);

    VkCheck_(vkCreateDebugReportCallbackEXT(instance, &createinfo, VkAllocators(), &callback));
}

static bool CheckValidationLayerSupport(const stdvector<const char *> &layers) {
    uint32_t layercount;
    vkEnumerateInstanceLayerProperties(&layercount, nullptr);

    stdvector<VkLayerProperties> availablelayers(layercount);
    vkEnumerateInstanceLayerProperties(&layercount, availablelayers.data());

    for (const char* layername : layers) {
        bool layerfound = false;

        for (const auto& layerproperties : availablelayers) {
            if (strcmp(layername, layerproperties.layerName) == 0) {
                layerfound = true;
                break;
            }
        }

        if (!layerfound)
            return false;
    }

    return true;
}

void PlatformContext::BindBindlessInputSet(PipelineType pipelinetype) {
    if (pipelinetype == ePipelineGraphics && assetcache.GetGeoTotalIndexCount()) {
        vkCmdBindIndexBuffer(current.cmdbuffer.vkobj,
                             assetcache.GetGeoIndexBuffer().vkobj, 0,
                             VK_INDEX_TYPE_UINT32);
    }
    VkPipelineBindPoint bindpoint;
    Ignore_(GetPipelineBindPoint(pipelinetype, bindpoint));
    vkCmdBindDescriptorSets(current.cmdbuffer.vkobj,
                            bindpoint, pipelinelayout, 1, 1,
                            &current.bindlessset, 0, nullptr);
}

void PlatformContext::BuildVkDeviceCreateInfo(stdvector<const char*>& deviceextensions,
                                              VkDeviceCreateInfo& createinfo) {
    static stdvector<VkDeviceQueueCreateInfo> queuecreateinfos;
    queuecreateinfos.clear(); // on session reset, clear this
    stdset<uint32_t> uniquequeuefamilies = {
        indices.graphicsfamily,
        indices.presentfamily,
        indices.asynccomputefamily
    };

    static float queuepriority = 1.0f;
    for (uint32_t queuefamily : uniquequeuefamilies) {
        if (queuefamily != kInvalidUInt32) {
            VkDeviceQueueCreateInfo queuecreateinfo = {};
            queuecreateinfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queuecreateinfo.queueFamilyIndex = queuefamily;
            queuecreateinfo.queueCount = 1;
            queuecreateinfo.pQueuePriorities = &queuepriority;
            queuecreateinfos.push_back(queuecreateinfo);
        }
    }

    static VkPhysicalDeviceVulkan13Features vulkan13features{};
    vulkan13features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_3_FEATURES;
    vulkan13features.pNext = deviceextensionchain;
    vulkan13features.synchronization2 = VK_TRUE;

    static VkPhysicalDeviceVulkan12Features vulkan12features{};
    vulkan12features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
    vulkan12features.pNext = &vulkan13features;
    vulkan12features.samplerMirrorClampToEdge = VK_TRUE;
    vulkan12features.bufferDeviceAddress = VK_TRUE;
    vulkan12features.descriptorIndexing = VK_TRUE;
    vulkan12features.shaderSampledImageArrayNonUniformIndexing = VK_TRUE;
    vulkan12features.descriptorBindingSampledImageUpdateAfterBind = VK_TRUE;
    vulkan12features.descriptorBindingStorageImageUpdateAfterBind = VK_TRUE;
    vulkan12features.descriptorBindingUpdateUnusedWhilePending = VK_TRUE;
    vulkan12features.descriptorBindingUniformBufferUpdateAfterBind = VK_TRUE;
    vulkan12features.descriptorBindingStorageBufferUpdateAfterBind = VK_TRUE;
    vulkan12features.descriptorBindingPartiallyBound = VK_TRUE;
    vulkan12features.runtimeDescriptorArray = VK_TRUE;

    static VkPhysicalDeviceVulkan11Features multiviewfeatures{};
    multiviewfeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES;
    multiviewfeatures.pNext = &vulkan12features;
    multiviewfeatures.multiview = VK_TRUE;

    static VkPhysicalDeviceFeatures2 devicefeatures = {};
    devicefeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    devicefeatures.pNext = &multiviewfeatures;
    devicefeatures.features.samplerAnisotropy = VK_TRUE;
    devicefeatures.features.fillModeNonSolid = VK_TRUE;
    devicefeatures.features.imageCubeArray = VK_TRUE;
    devicefeatures.features.multiViewport = VK_TRUE;
    // quiet an oculus runtime validation error
    devicefeatures.features.shaderStorageImageMultisample = VK_TRUE;

    createinfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    createinfo.pNext = &devicefeatures;

    createinfo.queueCreateInfoCount = static_cast<uint32_t>(queuecreateinfos.size());
    createinfo.pQueueCreateInfos = queuecreateinfos.data();

    //createinfo.pEnabledFeatures = &devicefeatures;

    printf("\nloading device extensions:\n");
    for (const auto& extension : deviceextensions)
        printf("\t%s\n", extension);

    createinfo.enabledExtensionCount = static_cast<uint32_t>(deviceextensions.size());
    createinfo.ppEnabledExtensionNames = deviceextensions.data();

    // device layers are deprecated
    createinfo.enabledLayerCount = 0;
    createinfo.ppEnabledLayerNames = nullptr;
}

void PlatformContext::BuildVkInstanceCreateInfo(stdvector<const char*>& instanceextensions,
                                                VkInstanceCreateInfo& createinfo,
                                                bool usedebugutils) {
    uint32_t availableextensioncount = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &availableextensioncount, nullptr);
    stdvector<VkExtensionProperties> availableextensions(availableextensioncount);
    vkEnumerateInstanceExtensionProperties(nullptr, &availableextensioncount,
                                           availableextensions.data());
    printf("\navailable instance extensions:\n");
    for (const auto& extension : availableextensions)
        printf("\t, %s\n", extension.extensionName);

#if ENABLE_VALIDATION
    validationlayers.push_back("VK_LAYER_KHRONOS_validation");

    if (!CheckValidationLayerSupport(validationlayers))
        Throw_(Error("some or all validation layers requested are unavailable"));
#endif

    static VkApplicationInfo appinfo = {};
    appinfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appinfo.pApplicationName = "bcview";
    appinfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    appinfo.pEngineName = "bcview";
    appinfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
    appinfo.apiVersion = VK_API_VERSION_1_3;

    createinfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    createinfo.pApplicationInfo = &appinfo;
    createinfo.enabledExtensionCount = static_cast<uint32_t>(instanceextensions.size());
    createinfo.ppEnabledExtensionNames = instanceextensions.data();

#if ENABLE_VALIDATION && ENABLE_GPU_VALIDATION && ENABLE_PRINTF_VALIDATION
    Assert_(0, "ENABLE_GPU_VALIDATION and ENABLE_PRINTF_VALIDATION are mutually exclusive.");
#endif

#if ENABLE_VALIDATION
    // note that you can't enable debug printf and gpu assisted validation
    // simultaneously. apparently, if you do, printf will be disabled
    // automatically, but just don't do it.
    static VkValidationFeatureEnableEXT enables[] = {
#if ENABLE_BEST_PRACTICES_VALIDATION
        VK_VALIDATION_FEATURE_ENABLE_BEST_PRACTICES_EXT,
#endif
#if ENABLE_GPU_VALIDATION
        VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_EXT,
        VK_VALIDATION_FEATURE_ENABLE_GPU_ASSISTED_RESERVE_BINDING_SLOT_EXT,
#elif ENABLE_PRINTF_VALIDATION
        VK_VALIDATION_FEATURE_ENABLE_DEBUG_PRINTF_EXT,
#endif
#if ENABLE_SYNCHRONIZATION_VALIDATION
        VK_VALIDATION_FEATURE_ENABLE_SYNCHRONIZATION_VALIDATION_EXT,
#endif
    };
    static VkValidationFeaturesEXT validationfeatures = {};
    validationfeatures.sType = VK_STRUCTURE_TYPE_VALIDATION_FEATURES_EXT;
    validationfeatures.pNext = instancepnextchain;
    validationfeatures.enabledValidationFeatureCount = ArraySize_(enables);
    validationfeatures.pEnabledValidationFeatures = enables;

    createinfo.enabledLayerCount = static_cast<uint32_t>(validationlayers.size());
    createinfo.ppEnabledLayerNames = validationlayers.data();

    if (usedebugutils) {
        static VkDebugUtilsMessengerCreateInfoEXT debugcreateinfo {};
        PopulateDebugUtilsCreateInfo(debugcreateinfo);
        debugcreateinfo.pNext = &validationfeatures;
        createinfo.pNext = &debugcreateinfo;
    }
    else {
        static VkDebugReportCallbackCreateInfoEXT debugcreateinfo {};
        PopulateDebugReportCreateInfo(debugcreateinfo);
        debugcreateinfo.pNext = &validationfeatures;
        createinfo.pNext = &debugcreateinfo;
    }
#else
    createinfo.pNext = instancepnextchain;
    createinfo.enabledLayerCount = 0;
#endif
}

static bool CreateInstance(PlatformContext& context) {
    VkInstanceCreateInfo createinfo{};
    context.BuildVkInstanceCreateInfo(context.instanceextensions, createinfo, true);

    if (vkCreateInstance(&createinfo, VkAllocators(), &context.instance) != VK_SUCCESS) {
        Throw_(Error("failed to create instance!"));
    }

    volkLoadInstance(context.instance);

    return true;
}

static void CreateIMGuiDescriptorPool(VkDevice device, VkDescriptorPool& imguipool) {
    VkDescriptorPoolSize poolsizes[] =
    {
        { VK_DESCRIPTOR_TYPE_SAMPLER, 1000 },
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 1000 },
        { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, 1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, 1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, 1000 },
        { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 1000 }
    };
    VkDescriptorPoolCreateInfo poolinfo = {};
    poolinfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolinfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    poolinfo.maxSets = 1000 * IM_ARRAYSIZE(poolsizes);
    poolinfo.poolSizeCount = (uint32_t)IM_ARRAYSIZE(poolsizes);
    poolinfo.pPoolSizes = poolsizes;
    if (vkCreateDescriptorPool(device, &poolinfo, VkAllocators(), &imguipool) != VK_SUCCESS)
        Throw_(Error("failed to create imgui descriptor pool"));
}

static void CreateCommandPool(PlatformContext& context) {
    VkCommandPoolCreateInfo poolinfo = {};
    poolinfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    poolinfo.queueFamilyIndex = context.indices.graphicsfamily;
    poolinfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    VkCheck_(vkCreateCommandPool(context.device, &poolinfo, VkAllocators(), &context.commandpool));
}

static void CreateCommandBuffers(PlatformContext& context) {
    context.commandbuffers.resize(kFrameRenderBufferCount);

    VkCommandBufferAllocateInfo allocinfo = {};
    allocinfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocinfo.commandPool = context.commandpool;
    allocinfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocinfo.commandBufferCount = uint32_(context.commandbuffers.size());

    if (vkAllocateCommandBuffers(context.device, &allocinfo,
                                 context.commandbuffers.data()) != VK_SUCCESS) {
        Throw_(Error("failed to allocate command buffers"));
    }

    context.current.cmdbuffer.vkobj = context.commandbuffers[context.bufferidx];
}

static void
CreateSyncObjects(PlatformContext& context) {
    VkSemaphoreCreateInfo semaphoreinfo = {};
    semaphoreinfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkFenceCreateInfo fenceinfo = {};
    fenceinfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceinfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    for (size_t i = 0; i < kFrameRenderBufferCount; i++) {
        if (vkCreateSemaphore(context.device, &semaphoreinfo, VkAllocators(),
                              &context.imageavailablesemaphores[i]) != VK_SUCCESS ||
            vkCreateSemaphore(context.device, &semaphoreinfo, VkAllocators(),
                              &context.renderfinishedsemaphores[i]) != VK_SUCCESS ||
            vkCreateFence(context.device, &fenceinfo, VkAllocators(),
                          &context.inflightfences[i]) != VK_SUCCESS) {
            Throw_(Error("failed to create synchronization objects for a frame"));
        }
    }

    context.current.imageavailablesemaphore = context.imageavailablesemaphores[context.bufferidx];
    context.current.renderfinishedsemaphore = context.renderfinishedsemaphores[context.bufferidx];
    context.current.inflightfence = context.inflightfences[context.bufferidx];
}

static PFN_vkVoidFunction ImGuiFuncLoader(const char* functionname, void* userdata) {
    VkInstance* instance = (VkInstance*) userdata;
    return vkGetInstanceProcAddr(*instance, functionname);
}

static void InitImGui(SessionContext& scontext, VkDescriptorPool& imguidescriptorpool) {
    ImGui::StyleColorsDark();

    FramebufferLayout flayout;
    scontext.passcache.GetDisplayFramebufferLayout(flayout);

    ImGui_ImplVulkan_LoadFunctions(ImGuiFuncLoader, &scontext.pc.instance);
    ImGui_ImplSDL2_InitForVulkan(scontext.pc.sdlwin);
    ImGui_ImplVulkan_InitInfo initinfo = {};
    initinfo.Instance = scontext.pc.instance;
    initinfo.PhysicalDevice = scontext.pc.physicaldevice;
    initinfo.Device = scontext.pc.device;
    initinfo.QueueFamily = scontext.pc.indices.graphicsfamily;
    initinfo.Queue = scontext.pc.graphicsqueue;
    initinfo.PipelineCache = VK_NULL_HANDLE;
    initinfo.DescriptorPool = imguidescriptorpool;
    initinfo.Allocator = VkAllocators();
    initinfo.MinImageCount = 2;
    initinfo.ImageCount = kFrameRenderBufferCount;
    initinfo.CheckVkResultFn = CheckVkResult;
    Ignore_(GetVkSampleCount(flayout.layoutdata.msaasamples, initinfo.MSAASamples));

    ImGui_ImplVulkan_Init(&initinfo, flayout.layoutdata.renderpass);

    {
        CommandBuffer cmdbuffer = scontext.pc.BeginSingleTimeCommands();

        ImGui_ImplVulkan_CreateFontsTexture(cmdbuffer.vkobj);

        scontext.pc.EndSingleTimeCommands(cmdbuffer);

        ImGui_ImplVulkan_DestroyFontUploadObjects();
    }
}

void InitializePools(PlatformContext& context, bool enableraytracing) {
    // Descriptor Pool
    uintn rtignores = enableraytracing ? 0 : 1; // update this to match below
    VkDescriptorPoolSize poolsizes[] =
    {
        { VK_DESCRIPTOR_TYPE_SAMPLER, 512 },
        { VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, 512 },
        { VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE, kFrameRenderBufferCount*kMaxBindlessSampledImages },
        { VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, kFrameRenderBufferCount*kMaxBindlessStorageImages },
        { VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER, 512 },
        { VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER, 512 },
        { VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, 512 },
        { VK_DESCRIPTOR_TYPE_STORAGE_BUFFER, kFrameRenderBufferCount*kMaxBindlessStorageBuffers},
        { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 512 },
        { VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, 512 },
        // Ray tracing stuff should always be at the end so we can exclude rtignores entries!
        { VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR,
          kFrameRenderBufferCount*kMaxBindlessAccelStructures }
    };

    VkDescriptorPoolCreateInfo poolinfo = {};
    poolinfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    poolinfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT |
                     VK_DESCRIPTOR_POOL_CREATE_UPDATE_AFTER_BIND_BIT;
    poolinfo.maxSets = 2 * (ArraySize_(poolsizes) - rtignores);
    poolinfo.poolSizeCount = uint32_t(ArraySize_(poolsizes)) - rtignores;
    poolinfo.pPoolSizes = poolsizes;
    VkCheck_(vkCreateDescriptorPool(context.device, &poolinfo, VkAllocators(),
                                    &context.descriptorpool));

    // Temporary Command Pool
    VkCommandPoolCreateInfo cmdpoolinfo = {};
    cmdpoolinfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    cmdpoolinfo.queueFamilyIndex = context.indices.graphicsfamily;
    cmdpoolinfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    VkCheck_(vkCreateCommandPool(context.device, &cmdpoolinfo, VkAllocators(),
                                 &context.tmpcmdpool));
}

static void CreateBindlessLayouts(PlatformContext& context, bool enableraytracing) {
    // Any changes here must be synced with register definitions on HLSL side!

    // Note that this is all a little fragile, since the bindings all have to
    // match up exactly. See notes in global.hlsli for further information.

    // push descriptor set
    stdvector<VkDescriptorSetLayoutBinding> pushbindings;
    VkDescriptorSetLayoutBinding pushbinding;

    // constant buffers
    for (uintn b = 0; b < kMaxConstantBuffers; ++b) {
        pushbinding = {};
        pushbinding.binding = uint32_(pushbindings.size());
        pushbinding.descriptorCount = 1;
        pushbinding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
        pushbinding.pImmutableSamplers = nullptr;
        pushbinding.stageFlags = VK_SHADER_STAGE_ALL;
        pushbindings.push_back(pushbinding);
    }

    VkDescriptorSetLayoutCreateInfo pushlayoutinfo = {};
    pushlayoutinfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    pushlayoutinfo.bindingCount = static_cast<uint32>(pushbindings.size());
    pushlayoutinfo.pBindings = pushbindings.data();
    pushlayoutinfo.flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR;

    Throw_(CreateDescriptorSetLayout(context, pushlayoutinfo, context.setlayouts[0],
                                     "bindless-push-set-layout"));

    // bindless descriptor set
    stdvector<VkPushConstantRange> pcranges;

    VkPushConstantRange pcrange = {};
    pcrange.stageFlags = VK_SHADER_STAGE_ALL;
    pcrange.offset = 0;
    pcrange.size = 2*sizeof(uint32);
    pcranges.push_back(pcrange);

    stdvector<VkDescriptorSetLayoutBinding> bindings;
    VkDescriptorSetLayoutBinding binding;

    // storage buffers
    binding = {};
    binding.binding = uint32_(bindings.size());
    binding.descriptorCount = kMaxBindlessStorageBuffers;
    binding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    binding.pImmutableSamplers = nullptr;
    binding.stageFlags = VK_SHADER_STAGE_ALL;
    bindings.push_back(binding);

    // sampled images
    binding = {};
    binding.binding = uint32_(bindings.size());
    binding.descriptorCount = kMaxBindlessSampledImages;
    binding.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    binding.pImmutableSamplers = nullptr;
    binding.stageFlags = VK_SHADER_STAGE_ALL;
    bindings.push_back(binding);

    // storage images
    binding = {};
    binding.binding = uint32_(bindings.size());
    binding.descriptorCount = kMaxBindlessStorageImages;
    binding.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    binding.pImmutableSamplers = nullptr;
    binding.stageFlags = VK_SHADER_STAGE_ALL;
    bindings.push_back(binding);

    // samplers
    binding = {};
    binding.binding = uint32_(bindings.size());
    binding.descriptorCount = uint32_(context.samplers.size());
    binding.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLER;
    binding.pImmutableSamplers = context.samplers.data();
    binding.stageFlags = VK_SHADER_STAGE_ALL;
    bindings.push_back(binding);

    // acceleration structures
    binding = {};
    binding.binding = uint32_(bindings.size());
    binding.descriptorCount = kMaxBindlessAccelStructures;
    binding.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    binding.pImmutableSamplers = nullptr;
    binding.stageFlags = VK_SHADER_STAGE_ALL;
    bindings.push_back(binding);

    // now create the layouts
    VkDescriptorBindingFlags flags = VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT |
                                     VK_DESCRIPTOR_BINDING_UPDATE_AFTER_BIND_BIT |
                                     VK_DESCRIPTOR_BINDING_UPDATE_UNUSED_WHILE_PENDING_BIT;
    VkDescriptorBindingFlags bindlessflags[] = {
        flags, // storage buffers
        flags, // sampled images
        flags, // storage images
        0x0,   // samplers
        flags  // accel structures
    };

    Assert_(ArraySize_(bindlessflags) == bindings.size());

    uintn rtignores = enableraytracing ? 0 : 1;

    VkDescriptorSetLayoutCreateInfo layoutinfo = {};
    layoutinfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    layoutinfo.bindingCount = static_cast<uint32>(bindings.size()) - rtignores;
    layoutinfo.pBindings = bindings.data();
    layoutinfo.flags = VK_DESCRIPTOR_SET_LAYOUT_CREATE_UPDATE_AFTER_BIND_POOL_BIT;

    VkDescriptorSetLayoutBindingFlagsCreateInfo extendedinfo {
        VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO,
        nullptr
    };
    extendedinfo.bindingCount = ArraySize_(bindlessflags) - rtignores;
    extendedinfo.pBindingFlags = bindlessflags;
    layoutinfo.pNext = &extendedinfo;

    Throw_(CreateDescriptorSetLayout(context, layoutinfo, context.setlayouts[1],
                                     "bindless-set-layout"));

    VkPipelineLayoutCreateInfo playoutinfo = {};
    playoutinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    playoutinfo.setLayoutCount = ArraySize_(context.setlayouts);
    playoutinfo.pSetLayouts = context.setlayouts;
    playoutinfo.pushConstantRangeCount = uint32(pcranges.size());
    playoutinfo.pPushConstantRanges = pcranges.data();

    Throw_(CreatePipelineLayout(context, playoutinfo, context.pipelinelayout,
                                "bindless-pipeline-layout"));
}

PlatformContext::PlatformContext(const World& world,
                                 const AssetCache& assetcache,
                                 Surface& surface,
                                 XRHeadset& headset,
                                 uintn xrmode) :
xrmode(xrmode),
assetcache(assetcache),
surface(surface),
headset(headset) {
}

void PlatformContext::BeginFrame() {
    vkWaitForFences(device, 1, &current.inflightfence, VK_TRUE,
                    std::numeric_limits<uint64>::max());

    if (xrmode == 0) {
        VkResult result = vkAcquireNextImageKHR(device, surface.swapchain,
                                                std::numeric_limits<uint64>::max(),
                                                current.imageavailablesemaphore,
                                                VK_NULL_HANDLE, &surface.swapidx);

        if (result == VK_ERROR_OUT_OF_DATE_KHR)
            Throw_(Error("received VK_ERROR_OUT_OF_DATE_KHR"));
        else if (result != VK_SUCCESS && result != VK_SUBOPTIMAL_KHR)
            Throw_(Error("failed to acquire swap chain image"));
    }

    // vkBeginCommandBuffer implicitly does command buffer reset...
}

void PlatformContext::EndFrame() {
    VkSubmitInfo submitinfo = { VK_STRUCTURE_TYPE_SUBMIT_INFO };
    submitinfo.commandBufferCount = 1;
    submitinfo.pCommandBuffers = &current.cmdbuffer.vkobj;

    VkSemaphore waitsemaphores[] = { current.imageavailablesemaphore };
    VkPipelineStageFlags waitstages[] = { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    VkSemaphore signalsemaphores[] = { current.renderfinishedsemaphore };
    if (xrmode == 0) {
        submitinfo.waitSemaphoreCount = 1;
        submitinfo.pWaitSemaphores = waitsemaphores;
        submitinfo.pWaitDstStageMask = waitstages;
        submitinfo.signalSemaphoreCount = 1;
        submitinfo.pSignalSemaphores = signalsemaphores;
    }

    vkResetFences(device, 1, &current.inflightfence);

    if (vkQueueSubmit(graphicsqueue, 1, &submitinfo, current.inflightfence) != VK_SUCCESS) {
        throw std::runtime_error("failed to submit draw command buffer");
    }

    if (xrmode == 0) {
        VkSwapchainKHR swapchains[] = { surface.swapchain };
        VkPresentInfoKHR presentinfo = { VK_STRUCTURE_TYPE_PRESENT_INFO_KHR };

        presentinfo.waitSemaphoreCount = 1;
        presentinfo.pWaitSemaphores = signalsemaphores;
        presentinfo.swapchainCount = 1;
        presentinfo.pSwapchains = swapchains;
        presentinfo.pImageIndices = &surface.swapidx;

        VkResult result = vkQueuePresentKHR(presentqueue, &presentinfo);

        if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
            throw std::runtime_error("received VK_ERROR_OUT_OF_DATE_KHR or VK_SUBOPTIMAL_KHR");
        else if (result != VK_SUCCESS)
            throw std::runtime_error("failed to present swap chain image");
    }
}

void PlatformContext::Initialize(Window& window, OutputDevice outdevice, const World& world) {
    sdlwin = window.sdlobj;
    outputdevice = outdevice;
    Assert_(outputdevice != eOutputDeviceCount);

    enableraytracing = world.RayTracingEnabled();

    // OpenXR handles all this if it is enabled. if not, we do it here.
    if (outputdevice == eOutputSystem) {
        RequireBaseInstanceExtensions(instanceextensions, true);
        Surface::GetInstanceExtensions(window, instanceextensions);

        if (!CreateInstance(*this))
            Throw_(Error("failed to create instance"));

#if ENABLE_VALIDATION
        bc9::SetupDebugUtilsCallback(instance, debugmessenger);
#endif

        surface.Initialize(*this, window);

        if (!ChoosePhysicalDevice(instance, surface.vkobj, deviceextensions, physicaldevice))
            Throw_(Error("failed to create physical device"));
        Throw_(FindQueueFamilies(physicaldevice, surface.vkobj, indices));

        surface.GetDeviceExtensions(deviceextensions);
        RequireDeviceExtensions(deviceextensions);

        VkDeviceCreateInfo createinfo{};
        BuildVkDeviceCreateInfo(deviceextensions, createinfo);

        if (!CreateLogicalDevice(createinfo))
            Throw_(Error("failed to create logical device"));

        CreateDeviceQueues();
    }
    else {
        Throw_(headset.Initialize(*this, surface, window));
    }

    msaasamples = GetSampleCount(physicaldevice, world.GetMSAASamples());

    ImGui::SetAllocatorFunctions(imguialloc, imguifree, nullptr);
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    memallocator.Initialize(*this);

    InitializePools(*this, enableraytracing);

    vkGetPhysicalDeviceProperties(physicaldevice, &deviceproperties);

    surface.CreateSwapchain(*this, &window);

    CreateIMGuiDescriptorPool(device, imguidescriptorpool);

    CreateCommandPool(*this);
    CreateCommandBuffers(*this);
    CreateSyncObjects(*this);

    CreateTextureSamplers(*this);

    CreateBindlessLayouts(*this, enableraytracing);

    CreateBindlessDescriptorSets();

    CreateConstantBufferParams params(*this, sizeof(PerFrameConstants), "perframe");
    Throw_(CreateBuffer(*this, params, pfconstantsbuf));
}

void PlatformContext::RequireBaseInstanceExtensions(stdvector<const char *> &extensions,
                                                    bool usedebugutils) {
#if ENABLE_VALIDATION
    if (usedebugutils)
        extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    else
        extensions.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    extensions.push_back(VK_EXT_VALIDATION_FEATURES_EXTENSION_NAME);
#endif
}

bool PlatformContext::CreateLogicalDevice(VkDeviceCreateInfo& createinfo) {
    VkCheck_(vkCreateDevice(physicaldevice, &createinfo, VkAllocators(), &device));

    return true;
}

void PlatformContext::CreateDeviceQueues() {
    // this is a convenient place to load volk, since every path
    // comes through here.
    volkLoadDevice(device);

    vkGetDeviceQueue(device, indices.presentfamily, 0, &presentqueue);
    vkGetDeviceQueue(device, indices.graphicsfamily, 0, &graphicsqueue);
    if (indices.asynccomputefamily != kInvalidUInt32) {
        vkGetDeviceQueue(device, indices.asynccomputefamily, 0, &asynccomputequeue);
    }
}

void PlatformContext::RequireDeviceExtensions(stdvector<const char*>& deviceextensions) {
    // note that there's a difference between extensions and features (see may 17 2022)
    // it doesn't make much sense to do both pushing onto deviceextensions *and* declaring
    // a feature struct with the feature turned on. main weird part is that something
    // can be a core feature but not be supported by a particular device.
    deviceextensions.push_back(VK_EXT_MEMORY_BUDGET_EXTENSION_NAME);
    deviceextensions.push_back(VK_KHR_PUSH_DESCRIPTOR_EXTENSION_NAME);
    deviceextensions.push_back(VK_KHR_MULTIVIEW_EXTENSION_NAME);

    if (enableraytracing) {
        // if we need some of these outside of ray tracing, may need to factor out.
        deviceextensions.push_back(VK_KHR_ACCELERATION_STRUCTURE_EXTENSION_NAME);
        deviceextensions.push_back(VK_KHR_RAY_TRACING_PIPELINE_EXTENSION_NAME);
        deviceextensions.push_back(VK_KHR_DEFERRED_HOST_OPERATIONS_EXTENSION_NAME);

        deviceextensions.push_back(VK_KHR_RAY_QUERY_EXTENSION_NAME);
        deviceextensions.push_back(VK_KHR_SPIRV_1_4_EXTENSION_NAME);
        deviceextensions.push_back(VK_KHR_SHADER_FLOAT_CONTROLS_EXTENSION_NAME);

        rtpipelinefeatures.sType =
            VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_TRACING_PIPELINE_FEATURES_KHR;
        rtpipelinefeatures.rayTracingPipeline = VK_TRUE;
        rtpipelinefeatures.pNext = nullptr;

        asfeatures.sType =
            VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
        asfeatures.accelerationStructure = VK_TRUE;
        asfeatures.descriptorBindingAccelerationStructureUpdateAfterBind = VK_TRUE;
        asfeatures.pNext = &rtpipelinefeatures;

        rqfeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
        rqfeatures.rayQuery = VK_TRUE;
        rqfeatures.pNext = &asfeatures;

        deviceextensionchain = &rqfeatures;
    }
    else {
        deviceextensionchain = nullptr;
    }
}

void PlatformContext::CreateBindlessDescriptorSets() {
    bc9::CreateBindlessDescriptorSets(*this);
}

void PlatformContext::AddBindlessStorageBuffer(BufferResource& buffer) {
    Assert_(nextstoragebufferidx < kMaxBindlessStorageBuffers);

    uint32 instances = buffer.GetInstances();
    VkWriteDescriptorSet writes[kMaxBufferInstances*kFrameRenderBufferCount] = {};
    VkDescriptorBufferInfo bufferinfos[kMaxBufferInstances*kFrameRenderBufferCount] = {};

    buffer.SetDescriptorIndex(nextstoragebufferidx);

    uintn writecount = 0;
    for (uintn i = 0; i < kFrameRenderBufferCount; ++i) {
        VkDescriptorSet& descset = bindlesssets[i];
        uintn descriptoridx = nextstoragebufferidx;
        for (uintn j = 0; j < instances; ++j) {
            uintn idx = instances * i + j;
            bufferinfos[idx].buffer = buffer.vkobj;
            bufferinfos[idx].offset = j * buffer.GetInstanceSize();
            bufferinfos[idx].range = buffer.GetInstanceSize();

            VkWriteDescriptorSet& write = writes[idx];
            write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
            write.dstSet = descset;
            write.dstBinding = kBindlessStorageBufferSlot;
            write.dstArrayElement = descriptoridx++;
            write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
            write.descriptorCount = 1;
            write.pBufferInfo = &bufferinfos[idx];
            ++writecount;
        }
    }
    vkUpdateDescriptorSets(device, writecount, writes, 0, nullptr);

    nextstoragebufferidx += instances;
}

void PlatformContext::AddBindlessAccelStructure(TLASResource& tlas) {
    Assert_(nextaccelstructureidx < kMaxBindlessAccelStructures);

    VkWriteDescriptorSet writes[kFrameRenderBufferCount] = {};
    VkWriteDescriptorSetAccelerationStructureKHR accelinfo = {
        VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR
    };
    accelinfo.accelerationStructureCount = 1;
    accelinfo.pAccelerationStructures = &tlas.vkobj;

    tlas.SetDescriptorIndex(nextaccelstructureidx);

    uintn writecount = 0;
    for (uintn i = 0; i < kFrameRenderBufferCount; ++i) {
        VkDescriptorSet& descset = bindlesssets[i];

        VkWriteDescriptorSet& write = writes[i];
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.dstSet = descset;
        write.dstBinding = kBindlessAccelStructureSlot;
        write.dstArrayElement = nextaccelstructureidx;
        write.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
        write.descriptorCount = 1;
        write.pNext = &accelinfo;
        ++writecount;
    }
    vkUpdateDescriptorSets(device, writecount, writes, 0, nullptr);

    ++nextaccelstructureidx;
}

void PlatformContext::SwapBuffers() {
    bufferidx = (bufferidx + 1) % kFrameRenderBufferCount;
    current.cmdbuffer.vkobj = commandbuffers[bufferidx];
    current.bindlessset = bindlesssets[bufferidx];
}

void PlatformContext::AddBindlessSampledImage(ImageView& imageview) {
    Assert_(nextsampledimageidx < kMaxBindlessSampledImages);

    VkWriteDescriptorSet writes[kFrameRenderBufferCount] = {};
    VkDescriptorImageInfo imageinfo = {};
    imageinfo.imageView = imageview.vkobj;
    imageinfo.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    imageview.SetDescriptorIndex(nextsampledimageidx);

    uintn writecount = 0;
    for (uintn i = 0; i < kFrameRenderBufferCount; ++i) {
        VkDescriptorSet& descset = bindlesssets[i];

        VkWriteDescriptorSet& write = writes[i];
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.dstSet = descset;
        write.dstBinding = kBindlessSampledImageSlot;
        write.dstArrayElement = nextsampledimageidx;
        write.descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
        write.descriptorCount = 1;
        write.pImageInfo = &imageinfo;
        ++writecount;
    }
    vkUpdateDescriptorSets(device, writecount, writes, 0, nullptr);

    ++nextsampledimageidx;
}

uintn PlatformContext::GetSwapIndex() const {
    return surface.swapidx;
}

void PlatformContext::AddBindlessStorageImage(ImageView& imageview) {
    Assert_(nextstorageimageidx < kMaxBindlessStorageImages);

    VkWriteDescriptorSet writes[kFrameRenderBufferCount] = {};
    VkDescriptorImageInfo imageinfo = {};
    imageinfo.imageView = imageview.vkobj;
    imageinfo.imageLayout = VK_IMAGE_LAYOUT_GENERAL;

    imageview.SetDescriptorIndex(nextstorageimageidx);

    uintn writecount = 0;
    for (uintn i = 0; i < kFrameRenderBufferCount; ++i) {
        VkDescriptorSet& descset = bindlesssets[i];

        VkWriteDescriptorSet& write = writes[i];
        write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        write.dstSet = descset;
        write.dstBinding = kBindlessStorageImageSlot;
        write.dstArrayElement = nextstorageimageidx;
        write.descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
        write.descriptorCount = 1;
        write.pImageInfo = &imageinfo;
        ++writecount;
    }
    vkUpdateDescriptorSets(device, writecount, writes, 0, nullptr);

    ++nextstorageimageidx;
}

void PlatformContext::PostInitialize(SessionContext& scontext) {
    InitImGui(scontext, imguidescriptorpool);
}

void PlatformContext::DeviceWaitIdle() {
    vkDeviceWaitIdle(device);
}

CommandBuffer PlatformContext::BeginSingleTimeCommands() {
    VkCommandBuffer vkcmdbuffer = nullptr;
    VkCommandBufferAllocateInfo allocinfo = {};
    allocinfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocinfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocinfo.commandPool = tmpcmdpool;
    allocinfo.commandBufferCount = 1;

    vkAllocateCommandBuffers(device, &allocinfo, &vkcmdbuffer);

    VkCommandBufferBeginInfo begininfo = {};
    begininfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begininfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    VkCheck_(vkBeginCommandBuffer(vkcmdbuffer, &begininfo));

    return { .vkobj = vkcmdbuffer };
}

void PlatformContext::EndSingleTimeCommands(CommandBuffer& cmdbuffer) {
    vkEndCommandBuffer(cmdbuffer.vkobj);

    VkSubmitInfo submitinfo = {};
    submitinfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitinfo.commandBufferCount = 1;
    submitinfo.pCommandBuffers = &cmdbuffer.vkobj;

    vkQueueSubmit(graphicsqueue, 1, &submitinfo, VK_NULL_HANDLE);
    vkQueueWaitIdle(graphicsqueue);

    vkFreeCommandBuffers(device, tmpcmdpool, 1, &cmdbuffer.vkobj);
    cmdbuffer.vkobj = nullptr;
}

void PlatformContext::SetupDebugReportCallback() {
#if ENABLE_VALIDATION
    bc9::SetupDebugReportCallback(instance, dbgreportcallback);
#endif
}

void PlatformContext::SetupDebugUtilsCallback() {
#if ENABLE_VALIDATION
    bc9::SetupDebugUtilsCallback(instance, debugmessenger);
#endif
}

void PlatformContext::Shutdown() {
    DestroyBuffer(*this, pfconstantsbuf);

    for (uintn l = 0; l < ArraySize_(setlayouts); ++l)
        vkDestroyDescriptorSetLayout(device, setlayouts[l], VkAllocators());
    vkDestroyPipelineLayout(device, pipelinelayout, VkAllocators());

    for (size_t i = 0; i < samplers.size(); ++i)
        vkDestroySampler(device, samplers[i], VkAllocators());

    ImGui_ImplVulkan_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    for (size_t i = 0; i < kFrameRenderBufferCount; i++) {
        vkDestroySemaphore(device, renderfinishedsemaphores[i], VkAllocators());
        vkDestroySemaphore(device, imageavailablesemaphores[i], VkAllocators());
        vkDestroyFence(device, inflightfences[i], VkAllocators());
    }

    vkFreeCommandBuffers(device, commandpool,
                         static_cast<uint32_t>(commandbuffers.size()), commandbuffers.data());

    vkDestroyCommandPool(device, commandpool, VkAllocators());

    vkDestroyDescriptorPool(device, imguidescriptorpool, VkAllocators());

    vkDestroyDescriptorPool(device, descriptorpool, VkAllocators());
    vkDestroyCommandPool(device, tmpcmdpool, VkAllocators());

    surface.Shutdown(*this);

    memallocator.Shutdown(*this);

    if (outputdevice > eOutputSystem)
        Throw_(headset.Shutdown(*this));

    vkDestroyDevice(device, VkAllocators());

#if ENABLE_VALIDATION
    if (debugmessenger)
        vkDestroyDebugUtilsMessengerEXT(instance, debugmessenger, VkAllocators());

    if (dbgreportcallback) {
        vkDestroyDebugReportCallbackEXT(instance, dbgreportcallback, VkAllocators());
    }
#endif

    vkDestroyInstance(instance, VkAllocators());
}

}