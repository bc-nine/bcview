#pragma once

#include "bclib/errorutils.h"
#include "openxr/openxr.h"
#include "rhitypes.h"
#include "glm/glm.hpp"
#include "bclib/stdvector.h"
#include "xrgraphics.h"

namespace bc9 {

class Window;
class Surface;
class Framebuffer;
class PlatformContext;

class XRHeadset {
public:
    XRHeadset();

    // surface and window are for a mirrored display, and we also currently
    // rely on the window for receiving keyboard/mouse input.
    Result Initialize(PlatformContext& context, Surface& surface, Window& window);

    Result Shutdown(PlatformContext& context);

    void ProcessEvents(PlatformContext& context, bool& requestrenderloop, bool& requestrestart);

    bool SessionRunning();
    bool ShouldRender();

    bool BeginFrame(PlatformContext& context, glm::mat4* viewmats, glm::mat4* invviewmats,
                    glm::mat4* projmats, glm::mat4* invprojmats, uintn matcount);
    void EndFrame(PlatformContext& context);

    void BeginRenderPass(PlatformContext& context, Framebuffer& framebuffer);
    void EndRenderPass(PlatformContext& context, Framebuffer& framebuffer);

    void GetViewSize(uintn& width, uintn& height) const;
    uintn GetViewCount() const;

    ResourceFormat GetSwapchainFormat() const;

    uintn GetSwapchainImageCount() const;

    ImageResource GetSwapchainImage(uintn idx) const;

private:
    void CreateSwapchain(PlatformContext& context);

    bool sessionrunning = false;

    XrDebugUtilsMessengerEXT xrdebug = {};

    XrFormFactor appconfigform = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

    XrViewConfigurationType appconfigview = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;

    XrEnvironmentBlendMode xrblend = {};

    XRGraphics xrgraphics;

    stdvector<XrSpace> visualizedspaces;

    XrSpace appspace { XR_NULL_HANDLE };

    struct Swapchain {
        XrSwapchain handle;
        int32 width;
        int32 height;
    };

    bool shouldrender = false;

    Swapchain swapchain;

    stdvector<XrView> views;
    stdvector<XrViewConfigurationView> configviews;
    stdvector<XrCompositionLayerProjectionView> projlayerviews;

    int64 colorswapchainformat = kInvalidUInt64;

    XrDebugUtilsMessengerEXT debugmessenger;

    XrEventDataBuffer eventdatabuffer;

    XrSessionState sessionstate { XR_SESSION_STATE_UNKNOWN };

    XrCompositionLayerProjection layer { XR_TYPE_COMPOSITION_LAYER_PROJECTION };

    XrFrameState framestate { XR_TYPE_FRAME_STATE };
    XrFrameWaitInfo framewaitinfo { XR_TYPE_FRAME_WAIT_INFO };

    XrSession xrsession = {};

    const XrEventDataBaseHeader* TryReadNextEvent(PlatformContext& context);

    void LogActionSourceName(PlatformContext& context, XrAction action,
                             const StringID& actionname) const;

    void HandleSessionStateChangedEvent(PlatformContext& context,
                                        const XrEventDataSessionStateChanged& statechangedevent,
                                        bool& exitrenderloop, bool& requestrestart);

private:
    uintn InitViewConfigurations();
};

}