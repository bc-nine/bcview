#include "framebuffer_d12.h"
#include "platformcontext.h"
#include "framebufferlayout.h"
#include "resourceutil.h"
#include "util_d12.h"
#include "rendertarget.h"
#include "bclib/stdallocators.h"

namespace bc9 {

Framebuffer::Framebuffer() {
}

void Framebuffer::PreTransitions(PlatformContext& context) {
    context.BeginResourceBarriers();
    for (auto& colortarget : colortargets)
        if (!layout->Multisampled() && !colortarget->presenting)
            context.AddResourceBarrier(colortarget->target, D3D12_RESOURCE_STATE_RENDER_TARGET);
    context.EndResourceBarriers(context.commandlist);
}

void Framebuffer::SetStencilReference(uint32 ref) {
    stencilref = ref;
}

void Framebuffer::Bind(PlatformContext& context) {
    UINT numrtvs = 0;
    D3D12_CPU_DESCRIPTOR_HANDLE* rtvhandleptr = nullptr;
    D3D12_CPU_DESCRIPTOR_HANDLE* dsvhandleptr = nullptr;

    RTVHandle rtvh(rtvhandle);
    DSVHandle dsvh(dsvhandle);

    if (dstarget)
        dsvhandleptr = &dsvh.cpuhandle;
    if (colortargets.size()) {
        numrtvs = 1;
        rtvhandleptr = &rtvh.cpuhandle;
    }

    // The only time we store the swapchain images in the rtvheap is when we're presenting,
    // but not doing an MSAA resolve (because either we're not using MSAA at all, or because
    // MSAA was resolved in a different render pass).
    if (layout->presenting && !layout->resolving)
        rtvh.Offset(context, context.swapidx);
    context.commandlist->OMSetRenderTargets(numrtvs, rtvhandleptr, FALSE, dsvhandleptr);

    if (dstarget)
        context.commandlist->OMSetStencilRef(stencilref);
}

void Framebuffer::Clear(PlatformContext& context) {
    RTVHandle rtvh(rtvhandle);
    DSVHandle dsvh(dsvhandle);

    for (unsigned int i = 0; i < colortargets.size(); ++i) {
        ColorLayout colorlayout;
        layout->GetColorLayout(i, colorlayout);
        if (colorlayout.LoadOpClear()) {
            if (layout->presenting && !layout->resolving)
                rtvh.Offset(context, context.swapidx);
            context.commandlist->ClearRenderTargetView(rtvh.cpuhandle,
                                                       colortargets[i]->clearcolor, 0, nullptr);
        }
    }

    if (dstarget) {
        D3D12_CLEAR_FLAGS flags = {};
        DepthStencilLayout dslayout;
        layout->GetDepthStencilLayout(dslayout);
        if (dslayout.DepthLoadOpClear())
            flags |= D3D12_CLEAR_FLAG_DEPTH;
        if (dslayout.StencilLoadOpClear())
            flags |= D3D12_CLEAR_FLAG_STENCIL;
        if (flags) {
            context.commandlist->ClearDepthStencilView(dsvhandle.cpuhandle, flags,
                dstarget->cleardepth, dstarget->clearstencil, 0, nullptr);
        }
    }
}

void Framebuffer::Initialize(PlatformContext& context, const FramebufferLayout* _layout,
                             ColorTarget* _colortargets, unsigned int numcolortargets,
                             DepthStencilTarget* _dstarget, unsigned int width,
                             unsigned int height, unsigned int layers) {
    layout = _layout;

    // rtv heap
    if (numcolortargets) {
        // Multiple render targets is complicated enough that I'll punt on that until
        // I actually need it. Some code may look like it's MRT ready (e.g. looping
        // over all color targets), but that will definitely need some additional work.
        assert(numcolortargets == 1);

        colortargets.resize(numcolortargets);
        for (unsigned int i = 0; i < numcolortargets; ++i)
            colortargets[i] = &_colortargets[i];

        rtvhandle = context.heaps.AllocateRTV(context,
                                           layout->presenting ? context.swapchainimagecount : 1);

        D3D12_RENDER_TARGET_VIEW_DESC colordesc = {};
        layout->GetColorDescription(0, colordesc);

        // See notes in Bind() above regarding swapchain images. If we are presenting *and*
        // resolving, then we must be rendering to an MSAA target, so we need to install
        // the MSAA image onto the rtv heap here. If we're presenting but not resolving,
        // then we're rendering directly to the swapchain image so we need to install that
        // instead. Finally, if we're not presenting, then it just comes down to whether or
        // not we're rendering to an MSAA target.
        RTVHandle rtvh(rtvhandle);
        if (layout->presenting && !layout->resolving) {
            for (unsigned int i = 0; i < context.swapchainimagecount; ++i) {
                context.device->CreateRenderTargetView(context.swapchainimages[i].Get(),
                                                       &colordesc, rtvh.cpuhandle);
                rtvh.Offset(context);
            }
        }
        else if (layout->msaasamples == 1) {
            context.device->CreateRenderTargetView(colortargets[0]->target.resource.Get(),
                                                   &colordesc, rtvh.cpuhandle);
        }
        else {
            context.device->CreateRenderTargetView(colortargets[0]->msaatarget.resource.Get(),
                                                   &colordesc, rtvh.cpuhandle);
        }
    }

    // dsv heap
    if (_dstarget) {
        dstarget = _dstarget;

        dsvhandle = context.heaps.AllocateDSV(context, 1);

        D3D12_DEPTH_STENCIL_VIEW_DESC depthdesc = {};
        layout->GetDepthDescription(depthdesc);

        context.device->CreateDepthStencilView((*dstarget).target.resource.Get(),
                                               &depthdesc, dsvhandle.cpuhandle);
    }
}

void Framebuffer::PostTransitions(PlatformContext& context) {
    StdStackAllocator stackalloc(GlobalStack());
    stdvector<D3D12_RESOURCE_STATES> prevstates(stackalloc);
    prevstates.resize(colortargets.size());

    for (unsigned int i = 0; i < colortargets.size(); ++i) {
        auto& colortarget(colortargets[i]);
        if (!colortarget->resolving)
            continue;

        context.BeginResourceBarriers();
        if (colortarget->presenting) {
            context.AddSwapchainResourceBarrier(context.swapidx,
                                                D3D12_RESOURCE_STATE_RESOLVE_DEST);
        }
        else {
            context.AddResourceBarrier(colortarget->target, D3D12_RESOURCE_STATE_RESOLVE_DEST);
        }

        prevstates[i] = colortarget->msaatarget.state;
        context.AddResourceBarrier(colortarget->msaatarget, D3D12_RESOURCE_STATE_RESOLVE_SOURCE);
        context.EndResourceBarriers(context.commandlist);

        if (colortarget->presenting) {
            context.commandlist->ResolveSubresource(context.swapchainimages[context.swapidx].Get(), 0,
                                                    colortarget->msaatarget.resource.Get(), 0,
                                                    context.d12swapchainformat);
        }
        else {
            context.commandlist->ResolveSubresource(colortarget->target.resource.Get(), 0,
                                                    colortarget->msaatarget.resource.Get(), 0,
                                                    colortarget->target.srv.Format);
        }
    }

    // Batch up the barriers into a single call for efficiency.
    context.BeginResourceBarriers();
    for (unsigned int i = 0; i < colortargets.size(); ++i) {
        auto& colortarget(colortargets[i]);

        if (colortarget->shaderreadable) {
            context.AddResourceBarrier(colortarget->target,
                                       D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
        }
        else if (colortarget->presenting) {
            // prepare for imgui render
            context.AddSwapchainResourceBarrier(context.swapidx,
                                                D3D12_RESOURCE_STATE_RENDER_TARGET);
        }

        if (colortarget->resolving)
            context.AddResourceBarrier(colortarget->msaatarget, prevstates[i]);
    }
    context.EndResourceBarriers(context.commandlist);
}

void Framebuffer::SetColorClear(const StringID& name, const glm::vec4 clear) {
    unsigned int index = -1;
    layout->GetColorIndex(name, index);
    colortargets[index]->clearcolor[0] = clear.x;
    colortargets[index]->clearcolor[1] = clear.y;
    colortargets[index]->clearcolor[2] = clear.z;
    colortargets[index]->clearcolor[3] = clear.w;
}

void Framebuffer::SetDepthClear(float depthclear) {
    Assert_(dstarget);
    dstarget->cleardepth = depthclear;
}

void Framebuffer::SetStencilClear(unsigned int stencilclear) {
    Assert_(dstarget);
    dstarget->clearstencil = stencilclear;
}

void Framebuffer::Shutdown(PlatformContext& context) {
}

}