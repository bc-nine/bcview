#pragma once

#include "camera.h"
#include "interpolator.h"
#include "rhitypes.h"
#include "bclib/stdjson.h"
#include "cachekeys.h"

namespace bc9 {

class World;
class NodeCache;
class SessionContext;

struct ButtonEvent {
    glm::ivec2 position;
};

struct MotionEvent {
    glm::ivec2 position;
};

struct KeyEvent {
    bool shift;
    KeyboardKey key;
};

class CameraController {
public:
    virtual void LeftButtonDown(SessionContext& scontext, const ButtonEvent& event) = 0;
    virtual void LeftButtonMotion(SessionContext& scontext, const MotionEvent& event) = 0;
    virtual void KeyUp(SessionContext& scontext, const KeyEvent& event) = 0;
    virtual void KeyDown(SessionContext& scontext, const KeyEvent& event) = 0;

    virtual void Update(SessionContext& scontext, float dt) = 0;

protected:
    Camera* camera = nullptr;
};

class TiltTwirlControls : public CameraController {
public:
    TiltTwirlControls();

    void LeftButtonDown(SessionContext& scontext, const ButtonEvent& event);
    void LeftButtonMotion(SessionContext& scontext, const MotionEvent& event);
    void KeyUp(SessionContext& scontext, const KeyEvent& event);
    void KeyDown(SessionContext& scontext, const KeyEvent& event);

    void Update(SessionContext& scontext, float dt);

    void SetSpeed(SessionContext& scontext, const glm::vec2& speed);
    void SetCameraNode(SessionContext& scontext, uintn cameranodeidx);
    void SetWorldNode(SessionContext& scontext, uintn worldnodeidx);

    void UseObjectSpace(SessionContext& scontext, bool mode);
    void ToggleObjectSpace(SessionContext& scontext);


private:
    uintn cameranodeidx, worldnodeidx;

    bool objectspace = false;
    glm::ivec2 prevposition;
    glm::quat startorient;
    float tilt = 0.f, twirl = 0.f;

    Interpolator fwdinterp;
};

class FlyControls : public CameraController {
public:
    FlyControls();

    void LeftButtonDown(SessionContext& scontext, const ButtonEvent& event);
    void LeftButtonMotion(SessionContext& scontext, const MotionEvent& event);
    void KeyUp(SessionContext& scontext, const KeyEvent& event);
    void KeyDown(SessionContext& scontext, const KeyEvent& event);

    void Update(SessionContext& scontext, float dt);

    void SetSpeed(SessionContext& scontext, const glm::vec2& speed);
    void SetCameraNode(SessionContext& scontext, uintn cameranodeidx);

private:
    uintn cameranodeidx;

    glm::ivec2 prevposition;

    float yaw, pitch;

    Interpolator fwdinterp;
    Interpolator rightinterp;
    Interpolator upinterp;
    Interpolator yawinterp;
    Interpolator pitchinterp;
};

class CameraControls {
public:
    CameraControls(const stdjson& bundlejson);

    void Initialize(SessionContext& scontext);
    void Shutdown(SessionContext& scontext);

    void LeftButtonDown(SessionContext& scontext, const ButtonEvent& event);
    void LeftButtonMotion(SessionContext& scontext, const MotionEvent& event);
    void KeyUp(SessionContext& scontext, const KeyEvent& event);
    void KeyDown(SessionContext& scontext, const KeyEvent& event);

    void Update(SessionContext& scontext, float dt);

private:
    TiltTwirlControls ttzcontrols;
    FlyControls flycontrols;

    glm::vec2 speed = glm::vec2(0.5, 0.05);
    CameraController* cameracontroller;

    StringID activecameranode;
};

}