#include "computecommands_vk.h"
#include "platformcontext_vk.h"
#include "commandbuffer_vk.h"

namespace bc9 {

namespace Compute {

void PushConstantBuffers(PlatformContext& context, const CBufferSet& cbufferset) {
    Shader::PushConstantBuffers(context, cbufferset, ePipelineCompute);
}

void PushConstantBuffers(PlatformContext& context, CommandBuffer& cmdbuffer,
                         const CBufferSet& cbufferset) {
    Shader::PushConstantBuffers(context, cmdbuffer, cbufferset, ePipelineCompute);
}

void Compute(PlatformContext& context, Pipeline& pipeline,
             const CBufferSet& cbufferset,
             const DispatchParams& params) {
    Compute(context, context.current.cmdbuffer, pipeline, cbufferset, params);
}

void Compute(PlatformContext& context, CommandBuffer& cmdbuffer, Pipeline& pipeline,
             const CBufferSet& cbufferset, const DispatchParams& params) {
    Shader::PushConstantBuffers(context, cmdbuffer, cbufferset, ePipelineCompute);

    Shader::BindPipeline(context, cmdbuffer, pipeline);

    vkCmdDispatch(cmdbuffer.vkobj, params.xgroups, params.ygroups, params.zgroups);
}

}

}
