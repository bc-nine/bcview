#pragma once

#include <wrl.h>
#include <d3d12.h>

namespace bc9 {

struct ImageResource {
    // i'm not sure if this is right, maybe there should be more here? let's start with this...
    D3D12_SHADER_RESOURCE_VIEW_DESC srv;

    Microsoft::WRL::ComPtr<ID3D12Resource> resource;

    D3D12_RESOURCE_STATES state = D3D12_RESOURCE_STATE_GENERIC_READ;
};

}
