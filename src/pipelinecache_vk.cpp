#include "pipelinecache.h"
#include "assetcache.h"
#include "util_vk.h"
#include "sessioncontext.h"
#include <filesystem>
#include "allocator_vk.h"
#include "bclib/stdvector.h"
#include "bclib/geoasset.h"
#include "bclib/stdallocators.h"
#include "bclib/fileutils.h"
#include "bclib/timeutils.h"
#include "bclib/stdutility.h"

// TODO: check cached pipeline uuid with current uuid so we can invalidate cache if need be
//       (or whatever has to happen in this regard...)

namespace bc9 {

float
PipelineCache::CreateGraphicsPipeline(const PlatformContext& context, VkPipeline& pipeline) {
    VkGraphicsPipelineCreateInfo pipelineinfo = {};
    pipelineinfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;

    // TODO: fix this so it's not so brittle. currently it only expects to get either
    // a vertex shader/pixel shader combo, or just a vertex shader (e.g. depth prepass).
    pipelineinfo.stageCount = shaderstages[1].module ? 2 : 1;
    pipelineinfo.pStages = shaderstages;
    pipelineinfo.pVertexInputState = &vertexinputinfo;
    pipelineinfo.pInputAssemblyState = &inputassembly;
    pipelineinfo.pTessellationState = nullptr;
    pipelineinfo.pViewportState = &viewportstate;
    pipelineinfo.pRasterizationState = &rasterizer;
    pipelineinfo.pMultisampleState = &multisampling;
    pipelineinfo.pDepthStencilState = &depthstencil;
    pipelineinfo.pColorBlendState = &colorblending;
    pipelineinfo.pDynamicState = &dynamicinfo;
    pipelineinfo.layout = pipelinelayout;
    pipelineinfo.renderPass = renderpass;
    pipelineinfo.subpass = 0;
    pipelineinfo.basePipelineHandle = VK_NULL_HANDLE;
    pipelineinfo.basePipelineIndex = -1;

    Assert_(renderpass);
    auto begintime = BeginTime();
    if (vkCreateGraphicsPipelines(context.device, vkpcache, 1, &pipelineinfo,
                                  VkAllocators(), &pipeline) != VK_SUCCESS) {
        throw std::runtime_error("failed to create graphics pipeline");
    }
    return EndTime(begintime);
}

Pipeline
PipelineCache::GetComputePipeline(const PlatformContext &context,
                                  const ComputePipelineBase &base) {
    Pipeline pipeline = {};
    pipeline.type = ePipelineCompute;

    uint64 psohash = base.GetHash();
    auto iter(computepipelines.find(psohash));
    if (iter == computepipelines.end()) {
        pipeline.vkobj = CreateComputePipeline(context, base);
        computepipelines[psohash] = pipeline.vkobj;
    }
    else {
        pipeline.vkobj = (*iter).second;
    }

    return pipeline;
}

Pipeline PipelineCache::GetGraphicsPipeline(const PlatformContext &context,
                                            const GraphicsPipelineBase& psobase,
                                            const GraphicsPipelineData& psodata) {
    Pipeline pipeline = {};
    pipeline.type = ePipelineGraphics;

    uint64 psohash = psodata.GetPipelineHash(context, psobase.GetHash());
    auto iter(graphicspipelines.find(psohash));
    if (iter == graphicspipelines.end()) {
        pipeline.vkobj = CreateGraphicsPipeline(context, psobase, psodata);
        graphicspipelines[psohash] = pipeline.vkobj;
    }
    else {
        pipeline.vkobj = (*iter).second;
    }

    return pipeline;
}

void PipelineCache::Initialize(const PlatformContext& context) {
    // disabling the pipeline cache for now, as it never really seemed to give much
    // improvement, and i no longer have access to a dataroot, since ideally runtime
    // wouldn't have knowledge of that. if you want to re-enable, you'll need a new
    // solution for where to store the cache file.
    #if ENABLE_PIPELINE_CACHE
        vkpcachepath = FString("%s%s", context.dataroot.str(), "cache/vk/pipelinecache.data");

        VkPipelineCacheCreateInfo createinfo = { VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO };
        createinfo.pNext = nullptr;
        createinfo.flags = 0;
        createinfo.initialDataSize = 0;
        createinfo.pInitialData = nullptr;

        if (std::filesystem::exists(vkpcachepath.str())) {
            stdvector<char> data;
            Throw_(ReadFile(vkpcachepath.str(), data));
            createinfo.initialDataSize = data.size();
            createinfo.pInitialData = data.data();
            VkCheck_(vkCreatePipelineCache(context.device, &createinfo, VkAllocators(),
                                           &vkpcache));
        }
        else {
            VkCheck_(vkCreatePipelineCache(context.device, &createinfo, VkAllocators(),
                                           &vkpcache));
        }
    #endif
}

VkPipeline PipelineCache::CreateComputePipeline(const PlatformContext &context,
                                                const ComputePipelineBase &base) {
    VkPipeline pipeline = {};

    const ShaderResource cresource = context.assetcache.GetShader(base.computekey);
    VkPipelineShaderStageCreateInfo stageinfo = {};
    stageinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    stageinfo.stage = cresource.stage;
    stageinfo.module = cresource.module;
    stageinfo.pName = cresource.entrypoint.str();

    VkComputePipelineCreateInfo pipelineinfo = {};
    pipelineinfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    pipelineinfo.pNext = nullptr;
    pipelineinfo.stage = stageinfo;
    pipelineinfo.layout = context.pipelinelayout;
    pipelineinfo.basePipelineHandle = VK_NULL_HANDLE;
    pipelineinfo.basePipelineIndex = -1;

    auto begintime = BeginTime();
    if (vkCreateComputePipelines(context.device, vkpcache, 1, &pipelineinfo,
                                 VkAllocators(), &pipeline)) {
        throw std::runtime_error("failed to create compute pipeline");
    }
    float time = EndTime(begintime);
    printf("pipeline creation time: %0.6f\n", time);

    return pipeline;
}

VkPipeline PipelineCache::CreateGraphicsPipeline(const PlatformContext &context,
                                                 const GraphicsPipelineBase &psobase,
                                                 const GraphicsPipelineData &psodata) {
    VkPipeline pipeline = {};

    const ShaderResource vresource = context.assetcache.GetShader(psobase.vertexkey);
    const ShaderResource fresource = context.assetcache.GetShader(psobase.pixelkey);

    // PSO Shader Stage
    shaderstages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderstages[0].stage = vresource.stage;
    shaderstages[0].module = vresource.module;
    shaderstages[0].pName = vresource.entrypoint.str();
    shaderstages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderstages[1].stage = fresource.stage;
    shaderstages[1].module = fresource.module;
    shaderstages[1].pName = fresource.entrypoint.str();

    // PSO Vertex Input State
    vertexinputinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexinputinfo.vertexBindingDescriptionCount = 0;
    vertexinputinfo.vertexAttributeDescriptionCount = 0;
    vertexinputinfo.pVertexBindingDescriptions = nullptr;
    vertexinputinfo.pVertexAttributeDescriptions = nullptr;

    // PSO Input Assembly State
    inputassembly.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    VkPrimitiveTopology vktopology;
    Throw_(GetVkPrimitiveTopology(psobase.topology, vktopology));
    inputassembly.topology = vktopology;
    inputassembly.primitiveRestartEnable = VK_FALSE;

    // PSO Viewport State (dynamic now)
    viewportstate.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportstate.viewportCount = 1;
    viewportstate.pViewports = nullptr;
    viewportstate.scissorCount = 1;
    viewportstate.pScissors = nullptr;

    // PSO Rasterization State
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    VkPolygonMode vkpolygonmode;
    Throw_(GetVkPolygonMode(psodata.state.polygonmode, vkpolygonmode));
    rasterizer.polygonMode = vkpolygonmode;
    rasterizer.lineWidth = 1.0f;

    VkCullModeFlags vkcullmode;
    Throw_(GetVkCullMode(psodata.state.cullmode, vkcullmode));
    rasterizer.cullMode = vkcullmode;
    rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0f; // Optional
    rasterizer.depthBiasClamp = 0.0f; // Optional
    rasterizer.depthBiasSlopeFactor = 0.0f; // Optional

    // PSO Multisample State
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = VK_FALSE;
    Throw_(GetVkSampleCount(psodata.fblayoutdata.msaasamples, multisampling.rasterizationSamples));
    multisampling.minSampleShading = 1.0f; // Optional
    multisampling.pSampleMask = nullptr; // Optional
    multisampling.alphaToCoverageEnable = VK_FALSE; // Optional
    multisampling.alphaToOneEnable = VK_FALSE; // Optional

    // PSO Depth/Stencil State
    depthstencil.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthstencil.depthTestEnable = psodata.state.depthtest ? VK_TRUE : VK_FALSE;
    depthstencil.depthWriteEnable = psodata.state.depthwrite ? VK_TRUE : VK_FALSE;
    VkCompareOp vkdepthcompareop;
    Throw_(GetVkCompareOp(psodata.state.depthcompareop, vkdepthcompareop));
    depthstencil.depthCompareOp = vkdepthcompareop;
    depthstencil.depthBoundsTestEnable = VK_FALSE;
    depthstencil.stencilTestEnable = psodata.state.stenciltest ? VK_TRUE : VK_FALSE;

    // stencil front
    VkStencilOp vkfrontfailop, vkfrontpassop, vkfrontdepthfailop;
    Throw_(GetVkStencilOp(psodata.state.frontfailop, vkfrontfailop));
    Throw_(GetVkStencilOp(psodata.state.frontpassop, vkfrontpassop));
    Throw_(GetVkStencilOp(psodata.state.frontdepthfailop, vkfrontdepthfailop));
    depthstencil.front.failOp = vkfrontfailop;
    depthstencil.front.passOp = vkfrontpassop;
    depthstencil.front.depthFailOp = vkfrontdepthfailop;

    VkCompareOp vkfrontcompareop;
    Throw_(GetVkCompareOp(psodata.state.frontcompareop, vkfrontcompareop));
    depthstencil.front.compareOp = vkfrontcompareop;
    depthstencil.front.compareMask = psodata.state.comparemask;
    depthstencil.front.writeMask = psodata.state.writemask;

    // stencil back
    VkStencilOp vkbackfailop, vkbackpassop, vkbackdepthfailop;
    Throw_(GetVkStencilOp(psodata.state.backfailop, vkbackfailop));
    Throw_(GetVkStencilOp(psodata.state.backpassop, vkbackpassop));
    Throw_(GetVkStencilOp(psodata.state.backdepthfailop, vkbackdepthfailop));
    depthstencil.back.failOp = vkbackfailop;
    depthstencil.back.passOp = vkbackpassop;
    depthstencil.back.depthFailOp = vkbackdepthfailop;

    VkCompareOp vkbackcompareop;
    Throw_(GetVkCompareOp(psodata.state.backcompareop, vkbackcompareop));
    depthstencil.back.compareOp = vkbackcompareop;
    depthstencil.back.compareMask = psodata.state.comparemask;
    depthstencil.back.writeMask = psodata.state.writemask;

    // PSO Blend State
    colorblendattachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT |
                                          VK_COLOR_COMPONENT_G_BIT |
                                          VK_COLOR_COMPONENT_B_BIT |
                                          VK_COLOR_COMPONENT_A_BIT;
    colorblendattachment.blendEnable = psodata.state.blendingenabled ? VK_TRUE : VK_FALSE;
    colorblendattachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    colorblendattachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorblendattachment.colorBlendOp = VK_BLEND_OP_ADD; // Optional
    colorblendattachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE; // Optional
    colorblendattachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO; // Optional
    colorblendattachment.alphaBlendOp = VK_BLEND_OP_ADD; // Optional

    colorblending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorblending.logicOpEnable = VK_FALSE;
    colorblending.logicOp = VK_LOGIC_OP_COPY;
    colorblending.attachmentCount = 1;
    colorblending.pAttachments = &colorblendattachment;
    colorblending.blendConstants[0] = 0.0f;
    colorblending.blendConstants[1] = 0.0f;
    colorblending.blendConstants[2] = 0.0f;
    colorblending.blendConstants[3] = 0.0f;

    // PSO Dynamic State
    dynamicinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicinfo.pNext = NULL;
    dynamicinfo.flags = 0;
    dynamicinfo.dynamicStateCount = ArraySize_(dynstates);
    dynamicinfo.pDynamicStates = dynstates;

    // PSO Pipeline Layout
    pipelinelayout = context.pipelinelayout;

    // PSO Render Pass
    renderpass = psodata.fblayoutdata.renderpass;

    float time = CreateGraphicsPipeline(context, pipeline);
    printf("pipeline creation time: %0.6f\n", time);

    return pipeline;
}

void PipelineCache::Shutdown(const PlatformContext& context) {
    for (auto &pipeline : graphicspipelines)
        vkDestroyPipeline(context.device, pipeline.second, VkAllocators());
    for (auto &pipeline : computepipelines)
        vkDestroyPipeline(context.device, pipeline.second, VkAllocators());

    graphicspipelines.clear();
    computepipelines.clear();

    #if ENABLE_PIPELINE_CACHE
        size_t datasize = 0, readsize = 0;
        vkGetPipelineCacheData(context.device, vkpcache, &datasize, nullptr);
        readsize = datasize;
        // FIXME: use the stack allocator here (then no need to deallocate, which
        //        does nothing anyway)
        unsigned char *data = (unsigned char *)SessionLinear().Allocate(datasize);
        vkGetPipelineCacheData(context.device, vkpcache, &readsize, data);
        assert(readsize == datasize);
        Throw_(WriteFile(vkpcachepath.str(), data, readsize));
        SessionLinear().Deallocate(data);
        vkDestroyPipelineCache(context.device, vkpcache, VkAllocators());
    #endif
}

}
