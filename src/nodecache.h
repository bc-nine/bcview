#pragma once

#include "bclib/stringid.h"
#include "bclib/errorutils.h"
#include "bclib/stdjson.h"
#include "node.h"
#include "model.h"
#include "camera.h"
#include "drawdata.h"
#include "sharedstructs.h"

namespace bc9 {

class RenderBins;
class ModelCache;
class SessionContext;

class NodeCache {
public:
    struct BakedInstance {
        DrawableKey drawablekey;
        glm::mat4x3 transform;
    };

    NodeCache(const stdjson& nodejson);

    void Initialize(SessionContext &scontext);
    void Shutdown(SessionContext &scontext);

    uintn Update(SessionContext& scontext, RenderBins& renderbins);

    void Bake(SessionContext& scontext, stdvector<BakedInstance>& instances);

    void SetNodeTranslation(uintn nodeidx, const glm::vec3& t);
    void SetNodeRotation(uintn nodeidx, const glm::quat& r);
    void SetNodeScale(uintn nodeidx, const glm::vec3& s);

    glm::vec3 NodeTranslation(uintn nodeidx) const;
    glm::quat NodeRotation(uintn nodeidx) const;
    glm::vec3 NodeScale(uintn nodeidx) const;

    glm::mat4 NodeTransform(uintn nodeidx);
    glm::mat4 NodeInverseTransform(uintn nodeidx);

    glm::mat4 ViewTransform(uintn nodeidx);
    glm::mat4 ViewInverseTransform(uintn nodeidx);

    uintn FindNode(const StringID& name) const;

    const Camera* NodeCamera(uintn nodeidx) const;
    uintn CameraNodeIndex(const StringID& name) const;
    StringID CameraNodeName(uintn zindex) const;
    uintn CameraNodeCount() const;

    uintn DrawableInstanceCount() const;

private:
    void TraverseModel(SessionContext& scontext, RenderBins& renderbins,
                       const Model& model, uint32 modelnodeidx);
    void TraverseNode(SessionContext& scontext, RenderBins& renderbins,
                      Node& node, const glm::mat4x3& xform, const glm::mat4x3& invxform);

    void TraverseNode(SessionContext& scontext, stdvector<BakedInstance>& instances,
                      uintn& instanceidx, Node& node, const glm::mat4x3& xform);

    stdvector<Node> nodes;

    uint32 nextxformidx = 0;
    stdvector<ModelNodeData> modeldatas;

    stdvector<uintn> cameranodes;

    stdvector<Model> models;
    stdvector<Camera> cameras;

    BufferResource modelnodebuf;

    uintn drawableinstancecount = 0;
};

}