#pragma once

#include "volk.h"
#include "bclib/fstring.h"

namespace bc9 {

struct ShaderResource {
    VkShaderModule module = nullptr;
    VkShaderStageFlagBits stage;
    FString128 entrypoint;
};

}
