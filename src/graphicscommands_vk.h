#pragma once

#include "pipelines.h"
#include "drawdata.h"
#include "shadercommands.h"

namespace bc9 {

class PlatformContext;
class Framebuffer;

namespace Graphics {

struct DrawParams {
    uintn instancecount = 1;
    uintn firstinstance = 0;

    uintn vertexcount = 0;
    uintn firstvertex = 0;
};

struct DrawIndexedParams {
    uintn instancecount = 1;
    uintn firstinstance = 0;

    uintn indexcount = 0;
    uintn firstindex = 0;
    uintn vertexoffset = 0;
};

// multi-call interface
void PushConstantBuffers(PlatformContext& context, const CBufferSet& cbufferset);

void BindPipeline(PlatformContext& context, Pipeline& pipeline);

void Draw(PlatformContext& context, const DrawParams& params);
void Draw(PlatformContext& context, const DrawIndexedParams& params);
void Draw(PlatformContext& context, const DrawData& dd);

// single-call interface
void Render(PlatformContext& context,
            Pipeline& pipeline,
            const CBufferSet& cbufferset,
            const DrawParams& params);
void Render(PlatformContext& context,
            Pipeline& pipeline,
            const CBufferSet& cbufferset,
            const DrawIndexedParams& params);
void Render(PlatformContext& context, Pipeline& pipeline, const DrawData& dd);

// other commands
void BeginRenderPass(PlatformContext& context, Framebuffer& framebuffer);
void EndRenderPass(PlatformContext& context, Framebuffer& framebuffer);

}

}