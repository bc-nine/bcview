#pragma once

#include "bclib/stringid.h"
#include "bclib/stdvector.h"
#include "cachekeys.h"

namespace bc9 {

struct Drawable;

struct Model {
    StringID name = "null";
    stdvector<DrawableKey> drawables;
};

}