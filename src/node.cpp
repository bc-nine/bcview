#include "node.h"

using namespace std;

namespace bc9 {

uint32 Node::GetChildCount() const {
    return uint32_(children.size());
}

}