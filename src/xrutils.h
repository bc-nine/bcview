#pragma once

#define DECL_XR_FUNC(xrgraphics, func) \
    PFN_ ## func func = nullptr; \
    XrCheck_(xrGetInstanceProcAddr((xrgraphics).xrinstance, #func, (PFN_xrVoidFunction *)(&func)))

inline void XrCheck(XrResult xrr) {
    __debugbreak();

    throw std::runtime_error("openxr error");
}

#define XrCheck_(R) do { auto r = R; if (r != XR_SUCCESS) XrCheck(r); } while(0)