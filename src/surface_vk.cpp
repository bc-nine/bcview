#include "surface_vk.h"
#include "window.h"
#include "bclib/errorutils.h"
#include "util_vk.h"
#include "bclib/stdset.h"
#include "bclib/stdallocators.h"
#include "platformcontext_vk.h"
#include "allocator_vk.h"
#include "bclib/mathutils.h"
#include "resourceutil_vk.h"
#include "imgui_impl_vulkan.h"
#include "renderstate.h"

#define SDL_MAIN_HANDLED
#include "SDL.h"
#include "SDL_vulkan.h"

namespace bc9 {

static VkSurfaceFormatKHR
ChooseSwapSurfaceFormat(const stdvector<VkSurfaceFormatKHR>& availableformats,
                        ColorSpaceType colorspace) {
    if (colorspace == eColorSpaceSRGB) {
        for (const auto& vksurfaceformat : availableformats) {
            // this probably needs to be addressed at some point, as different
            // cards will have different "preferred" formats. here bgra is
            // used, because nvidia uses it. but an amd card might want rgba.
            if (vksurfaceformat.format == VK_FORMAT_B8G8R8A8_SRGB &&
                vksurfaceformat.colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR) {
                return vksurfaceformat;
            }
        }
    }
    else {
        for (const auto& vksurfaceformat : availableformats) {
            if (vksurfaceformat.format == VK_FORMAT_B8G8R8A8_UNORM)
                return vksurfaceformat;
        }
    }

    Throw_(Error("couldn't find suitable sRGB vksurface format"));

    return VkSurfaceFormatKHR();
}

static void ChooseSwapPresentMode(const stdvector<VkPresentModeKHR> &modes,
                                  bool vsync, VkPresentModeKHR& presentmode) {
    bool immediateavailable = false;
    for (const auto& availablepresentmode : modes) {
        if (availablepresentmode == VK_PRESENT_MODE_IMMEDIATE_KHR) {
            immediateavailable = true;
            break;
        }
    }

    if (vsync) {
        // fifo is always guaranteed to be available
        presentmode = VK_PRESENT_MODE_FIFO_KHR;
        return;
    }
    else if (!vsync && immediateavailable) {
        presentmode = VK_PRESENT_MODE_IMMEDIATE_KHR;
        return;
    }

    Throw_(Error("vsync disabled but immediate present mode not available"));
}

static VkExtent2D ChooseSwapExtent(SDL_Window* win, const VkSurfaceCapabilitiesKHR& capabilities) {
    if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
        return capabilities.currentExtent;
    }
    else {
        int width, height;
        SDL_Vulkan_GetDrawableSize(win, &width, &height);

        VkExtent2D actualextent = {
            static_cast<uint32_t>(width),
            static_cast<uint32_t>(height)
        };

        actualextent.width = Max(capabilities.minImageExtent.width,
                                 Min(capabilities.maxImageExtent.width,
                                     actualextent.width));
        actualextent.height = Max(capabilities.minImageExtent.height,
                                  Min(capabilities.maxImageExtent.height,
                                      actualextent.height));

        return actualextent;
    }
}

Surface::Surface(bool raytracing, ColorSpaceType colorspace, bool vsync) :
raytracing(raytracing),
colorspace(colorspace),
vsync(vsync) {
}

bool Surface::ResizeView(PlatformContext& context, uintn width, uintn height) {
    if (swapchaindim.x != width || swapchaindim.y != height) {
        vkQueueWaitIdle(context.graphicsqueue);
        RecreateSwapchain(context, width, height);

        return true;
    }

     return false;
}

void Surface::CleanupSwapchain(PlatformContext& context) {
    for (uintn i = 0; i < swapchainimagecount; i++)
        vkDestroyImageView(context.device, swapchainimages[i].view.vkobj, VkAllocators());

    vkDestroySwapchainKHR(context.device, swapchain, VkAllocators());

    if (readbackimg.vkobj) {
        DestroyImage(context, readbackimg);
        DestroyBuffer(context, readbackbuf);
    }
}


void Surface::RecreateSwapchain(PlatformContext& context, uintn width, uintn height) {
    ImGui_ImplVulkan_SetMinImageCount(2);
    swapchaindim.x = width;
    swapchaindim.y = height;

    context.DeviceWaitIdle();

    CleanupSwapchain(context);

    CreateSwapchain(context, nullptr, width, height);
}

void Surface::GetDeviceExtensions(stdvector<const char*>& deviceextensions) {
    deviceextensions.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);
}

static bool CreateSurface(SDL_Window *window, VkInstance instance, VkSurfaceKHR& vksurface) {
    if (!SDL_Vulkan_CreateSurface(window, instance, &vksurface)) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                     "SDL_Vulkan_CreateSurface(): %s\n",
                     SDL_GetError());
        return false;
    }

    return true;
}

void Surface::Initialize(PlatformContext& context, Window& window) {
    if (!CreateSurface(window.sdlobj, context.instance, vkobj))
        Throw_(Error("failed to create vksurface"));
}

void Surface::Shutdown(PlatformContext& context) {
    CleanupSwapchain(context);
    // note that SDL_Vulkan_CreateSurface() is broken in the sense that it provides
    // no way to pass in a VkAllocationCallbacks struct. In fact, it simply passes
    // null in for the parameter internally. vulkan requires that the allocator
    // used for create matches the allocator used for destroy, so that leaves us no
    // choice but to pass in null here. it's actually not a problem because at least
    // the alloc/free is paired... it just means we can't track it. a bug report
    // has been filed, so if/when it is resolved, we can uncomment the allocator
    // call below.
    vkDestroySurfaceKHR(context.instance, vkobj, nullptr/*VkAllocators()*/);

}

void Surface::CreateSwapchain(PlatformContext& context, Window* window, uintn width, uintn height){
    VkImageUsageFlags swapchainusageflags = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    // this is only necessary for image read-back, so look into disabling
    // it unless that's required.
    swapchainusageflags |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

    if (raytracing) {
        swapchainusageflags |= VK_IMAGE_USAGE_TRANSFER_DST_BIT;

        //VkPhysicalDeviceFeatures2 devicefeatures2 {};
        //devicefeatures2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
        //devicefeatures2.pNext = &asfeatures;
        //asfeatures.pNext = &indexingfeatures;
        //indexingfeatures.pNext = nullptr;
        //vkGetPhysicalDeviceFeatures2(physicaldevice, &devicefeatures2);
    }

    SwapchainSupportDetails swapchainsupport = QuerySwapchainSupport(context.physicaldevice,
                                                                     vkobj);

    VkSurfaceFormatKHR vksurfaceformat = ChooseSwapSurfaceFormat(swapchainsupport.formats,
                                                                 colorspace);

    VkPresentModeKHR presentmode;
    ChooseSwapPresentMode(swapchainsupport.presentmodes, vsync, presentmode);

    VkExtent2D extent;
    if (window) {
        extent = ChooseSwapExtent(window->sdlobj, swapchainsupport.capabilities);
        swapchaindim.x = extent.width;
        swapchaindim.y = extent.height;
    }
    else {
        extent.width = width;
        extent.height = height;
        Assert_(width);
        Assert_(height);
        swapchaindim.x = width;
        swapchaindim.y = height;
    }

    vkswapchainformat = vksurfaceformat.format;
    // recall ResourceFormat is a direct Vulkan mapping
    swapchainformat = (ResourceFormat) vkswapchainformat;

    uint32_t imagecount = swapchainsupport.capabilities.minImageCount + 1;
    if (swapchainsupport.capabilities.maxImageCount > 0 &&
        imagecount > swapchainsupport.capabilities.maxImageCount) {
        imagecount = swapchainsupport.capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR createinfo = {};
    createinfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    createinfo.surface = vkobj;

    createinfo.minImageCount = imagecount;
    createinfo.imageFormat = vksurfaceformat.format;
    createinfo.imageColorSpace = vksurfaceformat.colorSpace;
    createinfo.imageExtent = extent;
    createinfo.imageArrayLayers = 1;
    createinfo.imageUsage = swapchainusageflags;

    uint32_t queuefamilyindices[] = {(uint32_t) context.indices.graphicsfamily,
                                     (uint32_t) context.indices.presentfamily};

    if (context.indices.graphicsfamily != context.indices.presentfamily) {
        createinfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        createinfo.queueFamilyIndexCount = 2;
        createinfo.pQueueFamilyIndices = queuefamilyindices;
    }
    else {
        createinfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        createinfo.queueFamilyIndexCount = 0; // Optional
        createinfo.pQueueFamilyIndices = nullptr; // Optional
    }

    createinfo.preTransform = swapchainsupport.capabilities.currentTransform;
    createinfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

    createinfo.presentMode = presentmode;
    createinfo.clipped = VK_TRUE;

    createinfo.oldSwapchain = VK_NULL_HANDLE;

    if (vkCreateSwapchainKHR(context.device, &createinfo, VkAllocators(),
                             &swapchain) != VK_SUCCESS) {
        Throw_(Error("failed to create swap chain"));
    }

    StdStackAllocator alloc(GlobalStack());
    vkGetSwapchainImagesKHR(context.device, swapchain, &swapchainimagecount,
                            nullptr);
    swapchainimages.resize(swapchainimagecount);
    stdvector<VkImage> vkswapchainimages(swapchainimagecount, alloc);
    vkGetSwapchainImagesKHR(context.device, swapchain, &imagecount,
                            vkswapchainimages.data());
    for (uintn i = 0; i < swapchainimagecount; ++i)
        swapchainimages[i].vkobj = vkswapchainimages[i];

    VkImageSubresourceRange range = { .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                      .baseMipLevel = 0,
                                      .levelCount = 1,
                                      .baseArrayLayer = 0,
                                      .layerCount = 1 };

    TransitionVkImageParams params { .computemasks = true };
    params.barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    params.barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    params.barrier.subresourceRange = range;
    // see (mar 12 2023)
    params.srcstagemask = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    params.dststagemask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;

    for (uintn i = 0; i < swapchainimagecount; ++i) {
        params.barrier.image = swapchainimages[i].vkobj;
        TransitionImage(context, params);

        VkImageViewCreateInfo createinfo = {};
        createinfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        createinfo.image = swapchainimages[i].vkobj;
        createinfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
        createinfo.format = vkswapchainformat;
        createinfo.subresourceRange = range;
        VkImageView& imageview = swapchainimages[i].view.vkobj;
        Throw_(CreateImageView(context, createinfo, "swapchain", imageview));
    }

    ResizeReadbackImage(context, extent.width, extent.height);
}

void Surface::GetInstanceExtensions(Window& window, stdvector<const char*>& instanceextensions) {
    uintn count = 0;
        if (!SDL_Vulkan_GetInstanceExtensions(window.sdlobj, &count, nullptr)) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                     "SDL_Vulkan_GetInstanceExtensions(): %s\n",
                     SDL_GetError());
        Throw_(Error("error getting SDL instance extensions"));
    }

    stdvector<const char*> sdlextensions(count);

    if (!SDL_Vulkan_GetInstanceExtensions(window.sdlobj, &count, sdlextensions.data())) {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION,
                     "SDL_Vulkan_GetInstanceExtensions(): %s\n",
                     SDL_GetError());
        Throw_(Error("error getting SDL instance extensions"));
    }

    for (auto& sdlextension : sdlextensions)
        instanceextensions.push_back(sdlextension);
}

void Surface::ProcessCapture(PlatformContext& context,
                             std::function<void(RenderState&, uchar*, uintn, uintn,
                                                uintn, uintn)> cb,
                             RenderState& state) {
    // reading from write-combined memory is realllly slow, especially when image
    // libraries read the same areas repeatedly, so do it all in one go here.

    StdStackAllocator stackalloc(GlobalStack());
    unsigned char* data = (unsigned char*) stackalloc.Allocate(readbackbuf.memory.size);
    memcpy(data, readbackbuf.memory.mappedmem, readbackbuf.memory.size);

    uintn width = swapchaindim[0];
    uintn height = swapchaindim[1];

    cb(state, data, width * readbacktexelsize, width, height, vkreadbackfmt);
    stackalloc.Deallocate(data);
}

void Surface::CaptureImage(PlatformContext& context) {
    VkImageSubresourceRange range{};
    range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    range.baseMipLevel = 0;
    range.levelCount = 1;
    range.baseArrayLayer = 0;
    range.layerCount = 1;

    TransitionVkImageParams params{};
    params.usecmdbuffer = &context.current.cmdbuffer;
    params.barrier.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
    params.barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    params.barrier.oldLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    params.barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    params.barrier.image = swapchainimages[swapidx].vkobj;
    params.barrier.subresourceRange = range;
    params.srcstagemask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
    params.dststagemask = VK_PIPELINE_STAGE_TRANSFER_BIT;
    TransitionImage(context, params);

    VkImageBlit blitinfo{};
    blitinfo.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    blitinfo.srcSubresource.mipLevel = 0;
    blitinfo.srcSubresource.baseArrayLayer = 0;
    blitinfo.srcSubresource.layerCount = 1;
    blitinfo.srcOffsets[1].x = swapchaindim[0];
    blitinfo.srcOffsets[1].y = swapchaindim[1];
    blitinfo.srcOffsets[1].z = 1;
    blitinfo.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    blitinfo.dstSubresource.mipLevel = 0;
    blitinfo.dstSubresource.baseArrayLayer = 0;
    blitinfo.dstSubresource.layerCount = 1;
    blitinfo.dstOffsets[1].x = swapchaindim[0];
    blitinfo.dstOffsets[1].y = swapchaindim[1];
    blitinfo.dstOffsets[1].z = 1;

    vkCmdBlitImage(context.current.cmdbuffer.vkobj,
                   swapchainimages[swapidx].vkobj,
                   VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                   readbackimg.vkobj,
                   VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                   1,
                   &blitinfo,
                   VK_FILTER_NEAREST);

    params.barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    params.barrier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    params.barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    params.barrier.dstAccessMask = 0;
    params.srcstagemask = VK_PIPELINE_STAGE_TRANSFER_BIT;
    params.dststagemask = VK_PIPELINE_STAGE_NONE;
    TransitionImage(context, params);

    params.barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    params.barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    params.barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    params.barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    params.srcstagemask = VK_PIPELINE_STAGE_TRANSFER_BIT;
    params.dststagemask = VK_PIPELINE_STAGE_TRANSFER_BIT;
    params.barrier.image = readbackimg.vkobj;
    TransitionImage(context, params);

    VkBufferImageCopy region{};
    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;
    region.imageOffset.x = 0;
    region.imageOffset.y = 0;
    region.imageOffset.z = 0;
    region.imageExtent.width = swapchaindim[0];
    region.imageExtent.height = swapchaindim[1];
    region.imageExtent.depth = 1;
    vkCmdCopyImageToBuffer(context.current.cmdbuffer.vkobj,
                           readbackimg.vkobj,
                           VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
                           readbackbuf.vkobj,
                           1,
                           &region);

    params.barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    params.barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    params.barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
    params.barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    params.srcstagemask = VK_PIPELINE_STAGE_TRANSFER_BIT;
    params.dststagemask = VK_PIPELINE_STAGE_TRANSFER_BIT;
    TransitionImage(context, params);
}

void Surface::ResizeReadbackImage(PlatformContext& context, uintn width, uintn height) {
    // we'll likely need to add to this list as we encounter different graphics cards...
    // note that we're only interested in whether it's linear or has an srgb transfer
    // function applied. in all cases we want destination to be in rgba order so it
    // can be written directly to a disk file without having to reorder channels. blit
    // handles that reordering.
    ResourceFormat readbackfmt;
    VkFormat vkformat = vkswapchainformat;
    switch (vkformat) {
        case VK_FORMAT_B8G8R8A8_SRGB:
        readbackfmt = eResourceFormatR8G8B8A8_SRGB;
        vkreadbackfmt = VK_FORMAT_R8G8B8A8_SRGB;
        break;

        case VK_FORMAT_B8G8R8A8_UNORM:
        readbackfmt = eResourceFormatR8G8B8A8_UNORM;
        vkreadbackfmt = VK_FORMAT_R8G8B8A8_UNORM;
        break;

        default:
        Assert_(0, "unknown format for readback image");
        break;
    }

    CreateImageParams iparams(readbackfmt, width, height, 1, "readbackimg");
    iparams.SetInitialLayout(eImageLayoutTransferDestination);
    iparams.SetUsageCopySource();
    iparams.SetUsageCopyDestination();
    Throw_(CreateImage(context, nullptr, iparams, readbackimg));

    Throw_(GetResourceFormatProperties(readbackfmt, readbackchannels, readbacktexelsize));
    uint64 bsize = width * height * readbacktexelsize;
    CreateBufferParams bparams(context, eBufferResourceGeneral, bsize, "readbackbuf");
    bparams.SetAccessMode(eBufferCPUAccessRead);
    bparams.SetUsageCopyDestination();
    Throw_(CreateBuffer(context, bparams, readbackbuf));
}

void Surface::GetViewSize(uintn& width, uintn& height) const {
    width = swapchaindim.x;
    height = swapchaindim.y;
}

ResourceFormat Surface::GetSwapchainFormat() const {
    return swapchainformat;
}

uintn Surface::GetSwapchainImageCount() const {
    return swapchainimagecount;
}

ImageResource Surface::GetSwapchainImage(uintn idx) const {
    return swapchainimages[idx];
}

}