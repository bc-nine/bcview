#include "world.h"
#include "imgui.h"
#include "assetcache.h"
#include "sessioncontext.h"
#include "nodecache.h"
#include "surface.h"
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/quaternion.hpp"
#include "glm/gtc/matrix_access.hpp"

using namespace glm;

namespace bc9 {

Result GetColorSpace(const StringID& strcolorspace, ColorSpaceType& colorspace) {
    switch (strcolorspace.Hash()) {
        case "srgb"_sid:
        colorspace = eColorSpaceSRGB;
        break;

        case "linear"_sid:
        colorspace = eColorSpaceLinear;
        break;

        default:
        return Error("%s: image color space not found", strcolorspace.str());
        break;
    }

    return Success();
}

uint64 World::Update() {
    uint64 updates = update;
    update = 0;

    return updates;
}

World::World(const stdjson& sjson, AssetCache &assetcache) :
assetcache(assetcache) {
    bool found = false;
    StringID sid;

    if (sjson.contains("render-settings")) {
        Throw_(OptUInt(sjson["render-settings"], "msaa-samples", msaasamples, found));
        StringID renderer;
        Throw_(OptString(sjson["render-settings"], "renderer", renderer, found));
        if (found) {
            if (renderer == "raster")
                renderermode = eRendererRaster;
            else if (renderer == "raytrace")
                renderermode = eRendererRayTrace;
            else
                Throw_(Error("%s: invalid renderer mode", renderer.str()));
        }

        StringID strsurfacespace;
        Throw_(OptString(sjson["render-settings"], "surface-color-space", strsurfacespace, found));
        if (found)
            Throw_(GetColorSpace(strsurfacespace, surfacespace));
        Throw_(OptBool(sjson["render-settings"], "vsync", vsync, found));
        Throw_(OptUInt(sjson["render-settings"], "raytrace-max-accum", raytracemaxaccum, found));
        Throw_(OptUInt(sjson["render-settings"], "raytrace-bounces", raytracebounces, found));
    }

    Throw_(ReqString(sjson["on-start"], "camera-node", activecameranode));

    StringID activebackground;
    Throw_(ReqString(sjson["on-start"], "background", activebackground));

    Throw_(OptQuat(sjson["on-start"], "camera-rotation", startcamrot, hasstartcamrot));
    Throw_(OptFloat3(sjson["on-start"], "camera-translation", startcampos, hasstartcampos));

    backgrounds.resize(sjson["backgrounds"].size());
    uintn bgi = 0;
    stdjson::const_iterator it;
    for (it = sjson["backgrounds"].begin(); it != sjson["backgrounds"].end(); ++it, ++bgi) {
        const stdjson& bgjson = *it;
        Background& background = backgrounds[bgi];
        background = Background(bgjson, assetcache);
        if (background.Name() == activebackground)
            activebackgroundidx = bgi;
    }
    if (activebackground != "" && activebackgroundidx == kInvalidUInt) {
        Throw_(Error("%s: background not found", activebackground.str()));
    }

#if winvk_ && 0
    if (sjson.contains("miss-shaders")) {
        for (it = sjson["miss-shaders"].begin(); it != sjson["miss-shaders"].end(); ++it) {
            const char* name(it.key().c_str());
            stdjson node = sjson["miss-shaders"][name];
            if (node.contains("shader")) {
                uint32 key;
                Throw_(ReqUInt(node, "shader", key));
                missshaders[name] = MissShaderKey(key);
            }
        }
    }
    if (sjson.contains("closest-hit-shaders")) {
        for (it = sjson["closest-hit-shaders"].begin();
             it != sjson["closest-hit-shaders"].end(); ++it) {
            const char* name(it.key().c_str());
            stdjson node = sjson["closest-hit-shaders"][name];
            if (node.contains("shader")) {
                uint32 key;
                Throw_(ReqUInt(node, "shader", key));
                closesthitshaders[name] = ClosestHitShaderKey(key);
            }
        }
    }
    if (sjson.contains("ray-shaders")) {
        for (it = sjson["ray-shaders"].begin();
             it != sjson["ray-shaders"].end(); ++it) {
            const char* name(it.key().c_str());
            stdjson node = sjson["ray-shaders"][name];
            if (node.contains("shader")) {
                uint32 key;
                Throw_(ReqUInt(node, "shader", key));
                rayshaders[name] = RayShaderKey(key);
            }
        }
    }
#endif

#if 0
    if (renderermode == eRendererRayTrace) {
        if (hasactive && sjson["active"].contains("miss-shader")) {
                Throw_(ReqString(sjson["active"], "miss-shader", sid));
                auto it(missshaders.find(sid));
                if (it == missshaders.end())
                    Throw_(Error("%s: miss shader not found", sid.str()));

                activemissshader = sid;
        }
        else {
            auto it = missshaders.find("default");
            if (it != missshaders.end())
                activemissshader = "default";
        }

        if (hasactive && sjson["active"].contains("closest-hit-shader")) {
                Throw_(ReqString(sjson["active"], "closest-hit-shader", sid));
                auto it(closesthitshaders.find(sid));
                if (it == closesthitshaders.end())
                    Throw_(Error("%s: closest hit shader not found", sid.str()));

                activeclosesthitshader = sid;
        }
        else {
            auto it = closesthitshaders.find("default");
            if (it != closesthitshaders.end())
                activeclosesthitshader = "default";
        }

        if (hasactive && sjson["active"].contains("ray-shader")) {
                Throw_(ReqString(sjson["active"], "ray-shader", sid));
                auto it(rayshaders.find(sid));
                if (it == rayshaders.end())
                    Throw_(Error("%s: ray shader not found", sid.str()));

                activerayshader = sid;
        }
        else {
            auto it = rayshaders.find("default");
            if (it != rayshaders.end())
                activerayshader = "default";
        }
    }
#endif

    update = eWorldBackground;
}

bool World::RayTracingEnabled() const {
    return renderermode == eRendererRayTrace;
}

uintn World::GetMSAASamples() const {
    return msaasamples;
}

ColorSpaceType World::GetSurfaceColorSpace() const {
    return surfacespace;
}

bool World::GetVSync() const {
    return vsync;
}

vec4 World::GetClearColor() const {
    Assert_(activebackgroundidx < backgrounds.size());
    const Background& background(backgrounds[activebackgroundidx]);

    return glm::vec4(background.Color1());
}

uintn World::ActiveCameraNode() const {
    return activecamnodeidx;
}

void World::SetActiveCameraNode(uintn activecameranode) {
    activecamnodeidx = activecameranode;
}

const Background& World::GetActiveBackground() const {
    return backgrounds[activebackgroundidx];
}

void World::OverlayUpdateMenuBar(SessionContext& scontext) {
    if (ImGui::BeginMenu("World")) {
        ImGui::Checkbox("Transforms", &transformsview);
        ImGui::Checkbox("Inverse Transforms", &invtransformsview);
        ImGui::EndMenu();
    }

    if (ImGui::BeginMenu("Background")) {
        for (uintn bgi = 0; bgi < uintn_(backgrounds.size()); ++bgi) {
            Background& background = backgrounds[bgi];
            if (ImGui::MenuItem(background.Name().str(), NULL, activebackgroundidx == bgi)) {
                SetActiveBackground(bgi);
                break;
            }
        }
        ImGui::EndMenu();
    }

    if (ImGui::BeginMenu("Camera")) {
        uintn camnodecount = scontext.nodecache.CameraNodeCount();
        for (uintn cidx = 0; cidx < camnodecount; ++cidx) {
            StringID nodename = scontext.nodecache.CameraNodeName(cidx);
            uintn nodeidx = scontext.nodecache.CameraNodeIndex(nodename);
            if (ImGui::MenuItem(nodename.str(), NULL, activecamnodeidx == nodeidx)) {
                SetActiveCameraNode(nodeidx);
                break;
            }
        }
        ImGui::EndMenu();
    }
}

static void Display4x4Matrix(const glm::mat4 &mat, const char* name) {
    if (ImGui::BeginTable(name, 4))
    {
        char buf[64];
        ImGui::TableNextRow();
        for (int i = 0; i < 4; i++)
        {
            glm::vec4 row = glm::row(mat, i);
            for (int j = 0; j < 4; ++j) {
                ImGui::TableNextColumn();
                snprintf(buf, 64, "% 0.4f", row[j]);
                ImGui::Text(buf);
            }
        }
        ImGui::EndTable();
    }
}

void World::OverlayUpdate(SessionContext& scontext) {
    uintn width, height;
    scontext.pc.surface.GetViewSize(width, height);
    float aspect = float(width)/height;

    if (transformsview) {
        ImGui::Begin("Transforms");

        ImGui::Text("View");
        glm::mat4 view = scontext.nodecache.NodeTransform(activecamnodeidx);
        Display4x4Matrix(view, "view");
        ImGui::Text("");

        ImGui::Text("Projection");
        const Camera* camera = scontext.nodecache.NodeCamera(activecamnodeidx);
        glm::mat4 proj = camera->ProjectionTransform(aspect);
        Display4x4Matrix(proj, "projection");
        ImGui::Text("");

        glm::mat4 viewproj = proj * view;
        ImGui::Text("View Projection");
        Display4x4Matrix(viewproj, "view projection");

        ImGui::End();
    }

    if (invtransformsview) {
        ImGui::Begin("Inverse Transforms");

        ImGui::Text("View");
        glm::mat4 view = scontext.nodecache.NodeTransform(activecamnodeidx);
        glm::mat4 invview = scontext.nodecache.NodeInverseTransform(activecamnodeidx);
        Display4x4Matrix(invview, "inverse view");
        ImGui::Text("");

        ImGui::Text("Projection");
        const Camera* camera = scontext.nodecache.NodeCamera(activecamnodeidx);
        glm::mat4 proj = camera->ProjectionTransform(aspect);
        Display4x4Matrix(inverse(proj), "inverse projection");
        ImGui::Text("");

        glm::mat4 viewproj = proj * view;
        ImGui::Text("View Projection");
        Display4x4Matrix(inverse(viewproj), "inverse view projection");

        ImGui::End();
    }
}

void World::SetActiveBackground(uintn activebgidx) {
    Assert_(activebgidx < backgrounds.size());
    activebackgroundidx = activebgidx;
    update |= eWorldBackground;
}

uintn World::GetRayTraceMaxAccum() const {
    return raytracemaxaccum;
}

uintn World::GetRayTraceBounces() const {
    return raytracebounces;
}

RendererMode World::GetRendererMode() const {
    return renderermode;
}

void World::Initialize(SessionContext &scontext) {
    activecamnodeidx = scontext.nodecache.CameraNodeIndex(activecameranode);

    if (hasstartcampos)
        scontext.nodecache.SetNodeTranslation(activecamnodeidx, startcampos);
    if (hasstartcamrot)
        scontext.nodecache.SetNodeRotation(activecamnodeidx, startcamrot);
}

void World::Shutdown(SessionContext &scontext) {
}

}