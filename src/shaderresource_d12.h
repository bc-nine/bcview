#pragma once

#include <wrl.h>
#include <d3d12.h>

namespace bc9 {

struct ShaderResource {
    Microsoft::WRL::ComPtr<ID3DBlob> blob;
};

}
