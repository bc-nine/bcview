#pragma once

#include "volk.h"
#include "memallocator_vk.h"
#include "bclib/errorutils.h"

namespace bc9 {

constexpr uintn kMaxBufferInstances = 2;

struct BufferResource {
    // platform-independent interface
    BufferResource() {}
    // NOTE: clients should not be creating a BufferResource directly -
    //       that is done via CreateBuffer() with appropriate params.
    // "instances" refers to the number of copies of data in the buffer, so
    // e.g. a double-buffered setup would specify 2, tripled-buffered 3, etc.
    // set to 1 if only a single copy of the data is required (e.g. device
    // local).
    BufferResource(uintn instances, uint64 instancesize) : instancesize(instancesize),
                                                           instances(instances) {
        Assert_(instances);
        Assert_(instancesize);
        Assert_(instances <= kMaxBufferInstances);
    }

    void* Map(uint8 instance) {
        Assert_(memory.mappedmem);
        return (char*)(memory.mappedmem) + instance * instancesize;
    }

    uint32 UpdateAndSwap(void* data) {
        Assert_(instances > 1);
        size_t subbufsize = instancesize;
        unsigned char* back = (unsigned char*)Map(backidx);
        memcpy(back, data, subbufsize);

        backidx = 1 - backidx;

        return DescriptorIndex();
    }

    uint64 GetSize() const { return instances * instancesize; }
    uint64 GetInstanceSize() const { return instancesize; }
    uint32 GetInstances() const { return instances; }

    void SetDescriptorIndex(uintn index) { basedescriptoridx = index; }
    uint32 DescriptorIndex() const {
        return (instances > 1) ? basedescriptoridx + (1 - backidx) : basedescriptoridx;
    }

    uint64 GetOffset() const {
        return (instances > 1) ? (1 - backidx) * instancesize : 0;
    }

    operator bool() { return instancesize != 0; }

    // platform-dependent interface
    Allocation memory;
    VkBuffer vkobj = 0;
    VkBufferView view = VK_NULL_HANDLE; // for texel buffers

private:
    uint64 instancesize = 0;
    uint32 instances = 0;
    uint32 backidx = 0;
    uint32 basedescriptoridx = kInvalidUInt32;
};

}
