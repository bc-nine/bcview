#pragma once

#include "bclib/podtypes.h"
#include "bclib/stdlist.h"
#include "bclib/stdvector.h"
#include <assert.h>
#include <stdexcept>
#include "constants.h"

#define NOMINMAX
#define VMA_RECORDING_ENABLED 0
#define VMA_STATIC_VULKAN_FUNCTIONS 1
#define VMA_DYNAMIC_VULKAN_FUNCTIONS 0
#include "vk_mem_alloc.h"

namespace bc9 {

class PlatformContext;

struct Allocation {
    VkDeviceMemory memory = VK_NULL_HANDLE;
    VkDeviceSize size = 0;
    VkDeviceSize offset = 0;
    void* mappedmem = nullptr;
    flag8 coherent = false;

    VmaAllocation vma;
};

class MemAllocatorVk {
public:
    void CreateBuffer(PlatformContext& context, VkMemoryPropertyFlags properties,
                      const VkBufferCreateInfo* bufferinfo, VkBuffer* buffer,
                      Allocation& alloc, const char* name);
    void CreateImage(PlatformContext& context, VkMemoryPropertyFlags properties,
                     const VkImageCreateInfo* imageinfo, VkImage* image, Allocation& alloc,
                     const char* name);

    void DestroyBuffer(PlatformContext& context, VkBuffer buffer, Allocation& alloc);
    void DestroyImage(PlatformContext& context, VkImage image, Allocation& alloc);

    void Initialize(PlatformContext& context);
    void Shutdown(PlatformContext& context);

    VmaAllocator allocator;
};

}
