#pragma once

#include "bufferresource.h"
#include "pipelinestate.h"
#include "geodata.h"
#include "shadercommands.h"

namespace bc9 {

// TODO: i believe this can be platform-independent now...

struct DrawData {
    DrawData() {}

    GraphicsPipelineBase pipelinebase;

    // drawableidx is the index into the global list of drawables.
    uint32 drawableidx = kInvalidUInt32;
    uint32 modelnodeidx = kInvalidUInt32;

    uint32 instances = 1;

    GeoDrawData geodrawdata;

    CBufferSet* cbufferset = nullptr;
};

}
