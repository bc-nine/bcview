// this is the custom config.h for eastl, customized for bcview so we don't taint EASTL source.

#define EASTL_USER_DEFINED_ALLOCATOR
#define EASTLAllocatorType bc9::StdAllocator
#define EASTLAllocatorDefault bc9::DefaultAllocator

#include "stdallocatorbase.h"