#include "interpolator.h"
#include "bclib/mathutils.h"

namespace bc9 {

Interpolator::Interpolator(float peaktime) {
    // tmin is left at the default for this particular use case.
    assert(peaktime >= 0.0f);
    peaktime = Max(peaktime, kEpsilon);

    tmax[0] = tmax[1] = peaktime;
}

Interpolator::Interpolator(float peaktime, float maxvalue, float minvalue) {
    // tmin is left at the default for this particular use case.
    assert(peaktime >= 0.0f);
    peaktime = Max(peaktime, kEpsilon);

    tmax[0] = tmax[1] = peaktime;
    SetMinMax(maxvalue, minvalue);
}

void Interpolator::SetMinMax(float maxabsvalue, float minabsvalue) {
    bias[0] = bias[1] = minabsvalue;
    scale[0] = scale[1] = (maxabsvalue-minabsvalue);
}

void Interpolator::Start(bool positive) {
    if (status)
        return;

    status = active = 1;
    t = tmin[status];
    dir = positive ? 1 : -1;
}

void Interpolator::Stop() {
    status = 0;
    t = Min(t, tmax[status]);
}

bool Interpolator::Active() const {
    return active;
}

float Interpolator::Update(float dt) {
    if (status || t >= tmin[0]) {
        t += status ? dt : -dt;
        value = dir * (bias[status] + scale[status]*SmootherStep(tmin[status], tmax[status], t));
    }
    else {
        active = 0;
    }

    return value;
}

}