#include "externalallocators.h"

void* jsonalloc(size_t size) {
    return bc9::Heaps().Linear(bc9::eHeapJSON).Allocate(size);
}

void jsonfree(void *ptr) {
    return bc9::Heaps().Linear(bc9::eHeapJSON).Deallocate(ptr);
}

void* xxhashalloc(size_t size) {
    return bc9::Heaps().Linear(bc9::eHeapXXHash).Allocate(size);
}

void xxhashfree(void *ptr) {
    return bc9::Heaps().Linear(bc9::eHeapXXHash).Deallocate(ptr);
}

void* imguialloc(size_t size, void*) {
    return bc9::Heaps().Linear(bc9::eHeapIMGui).Allocate(size);
}

void imguifree(void* ptr, void*) {
    return bc9::Heaps().Linear(bc9::eHeapIMGui).Deallocate(ptr);
}