#pragma once

#include <d3d12.h>
#include "glm/glm.hpp"
#include "bclib/podtypes.h"
#include "rhitypes.h"

namespace bc9 {

class PlatformContext;

class CreateBufferParams {
public:
    CreateBufferParams();
    // single instance, no stride
    CreateBufferParams(PlatformContext& context, uint64 size, BufferResourceType type,
                       const char* name);
    // single instance, stride
    CreateBufferParams(PlatformContext& context, uint64 size, BufferResourceType type,
                       uint32 stride, const char* name);
    // multiple instances, no stride
    CreateBufferParams(PlatformContext& context, uint64 instancesize, uint32 instances,
                       BufferResourceType type, const char* name);
    // multiple instances, stride
    CreateBufferParams(PlatformContext& context, uint64 instancesize, uint32 instances,
                       BufferResourceType type, uint32 stride, const char* name);

    // names can be changed, e.g. when building multiple buffers that only differ by name.
    void SetName(const char* name);

    void SetAccessMode(BufferCPUAccessMode accessmode);

    void SetVisibilityVertex(bool visible);
    void SetVisibilityPixel(bool visible);

    void SetUsageCopySource(bool source);
    void SetUsageCopyDestination(bool dest);
    bool GetUsageCopySource() const;
    bool GetUsageCopyDestination() const;

    void SetInitialData(void* data, uint64 size);

    const uint64 instancesize = 0;
    const uint32 instances = 1;
    const BufferResourceType type = eBufferResourceTypeCount;
    const uint32 stride = 0;
    const char* name = nullptr;

    BufferCPUAccessMode accessmode = eBufferCPUAccessNone;

    void* initdata = nullptr;
    uint64 initdatasize = 0;

private:
    bool vertexvisible = false;
    bool pixelvisible = false;
    bool copysource = false;
    bool copydest = false;
};

class CreateImageParams {
public:
    // public interface (platform independent)
    CreateImageParams();
    // simple 2d interface
    CreateImageParams(PlatformContext& context, ResourceFormat format,
                      unsigned int width, unsigned int height, unsigned int samples,
                      const char* name);
    // general interface (arraylayers of 0 = scalar 2D image, 1 = image array of size 1)
    // let's say 1D textures would have height = 0 (hasn't been implemented yet though)
    CreateImageParams(PlatformContext& context, ResourceFormat format,
                      unsigned int width, unsigned int height, unsigned int depth,
                      unsigned int miplevels, unsigned int arraylayers, unsigned int samples,
                      bool cube, const char* name);

    // names can be changed, e.g. when building multiple buffers that only differ by name.
    void SetName(const char* name);

    void SetInitialLayout(ImageLayout layout);

    void SetAccessMode(ImageCPUAccessMode accessmode);

    void SetUsageCopySource();
    void SetUsageCopyDestination();
    void SetUsageSampled();
    void SetUsageStorage();
    void SetUsageColorTarget(const glm::vec4 clearcolor = { 0.0f, 0.0f, 0.0f, 1.0f });
    void SetUsageDepthTarget(float cleardepth = 1.0f);
    void SetUsageStencilTarget(uint32_t clearstencil = 0);

    // this is required for using an image as either a cube or cube array
    void SetCubeCompatible();

    void SetQueueUsage(PlatformContext& context, bool graphics, bool compute);

    // non-public section (don't access from platform independent code)
    bool cube = false;
    bool arrayimage = false;
    bool depthenabled = false;
    bool stencilenabled = false;
    const char* name = nullptr;

    D3D12_RESOURCE_DESC imageinfo = {};
    D3D12_RESOURCE_STATES initial = {};
    D3D12_CLEAR_VALUE* clear = nullptr;
    D3D12_CLEAR_VALUE clearstruct = {};

    ImageCPUAccessMode accessmode = eImageCPUAccessNone;
private:
    void PopulateRemaining();
};

}
