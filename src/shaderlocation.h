#pragma once

#include <climits>
#include "bclib/stdjson.h"

#if winvk_
#include "volk.h"
#endif

namespace bc9 {

class ShaderLocation {
public:
    ShaderLocation(const stdjson &mjson);
    ShaderLocation();

    //stdstring name;
#if winvk_
    unsigned int location = UINT_MAX;
    VkFormat format = VK_FORMAT_UNDEFINED;
#endif
};

}
