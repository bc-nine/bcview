#pragma once

#include "bclib/geoasset.h"
#include "imageasset.h"
#include "shaderasset.h"

namespace bc9 {

struct GeoKey {
    GeoKey() {}
    GeoKey(uintn index) : index(index) {}

    bool operator==(const GeoKey& rhs) { return index == rhs.index; }
    bool Valid() const { return index != kInvalidUInt; }
    uintn index = kInvalidUInt;
};

struct ImageKey {
    ImageKey() {} ImageKey(uintn index) : index(index) {}

    bool Valid() const { return index != kInvalidUInt; }
    uintn index = kInvalidUInt;

    bool operator==(const ImageKey& rhs) {
        return index == rhs.index;
    }
};

struct SamplerKey {
    SamplerKey() {} SamplerKey(uintn index) : index(index) {}

    bool Valid() const { return index != kInvalidUInt; }
    uintn index = kInvalidUInt;

    bool operator==(const SamplerKey& rhs) {
        return index == rhs.index;
    }
};

struct TextureKey {
    TextureKey() {} TextureKey(uintn index) : index(index) {}

    bool Valid() const { return index != kInvalidUInt; }
    uintn index = kInvalidUInt;

    bool operator==(const TextureKey& rhs) {
        return index == rhs.index;
    }
};

struct ShaderKey {
    ShaderKey() {}
    ShaderKey(uintn index) : index(index) {}

    bool Valid() const { return index != kInvalidUInt; }
    uintn index = kInvalidUInt;

    bool operator==(const ShaderKey& rhs) { return index == rhs.index; }
};

struct MaterialKey {
    // we ensure in material cache that there's always a default material at index 0.
    MaterialKey() {}
    explicit MaterialKey(uintn index) : index(index) {}

    bool Valid() const { return index != kInvalidUInt; }
    uintn index = 0;

    bool operator==(const MaterialKey& rhs) { return index == rhs.index; }
};

struct DrawableKey {
    DrawableKey() {}
    explicit DrawableKey(uint32 index) : index(index) {}

    bool Valid() const { return index != kInvalidUInt32; }
    uint32 index = kInvalidUInt32;

    bool operator==(const DrawableKey& rhs) { return index == rhs.index; }
};

struct NodeKey {
    NodeKey() {}
    NodeKey(uint32 index) : index(index) {}

    bool Valid() const { return index != kInvalidUInt32; }
    uint32 index = kInvalidUInt;

    bool operator==(const NodeKey& rhs) { return index == rhs.index; }
};

}