#pragma once

#if winvk_
#include "graphicscommands_vk.h"
#elif wind12_
#include "graphicscommands_d12.h"
#endif