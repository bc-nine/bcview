#pragma once

#include "bclib/podtypes.h"
#include "bclib/errorutils.h"

namespace bc9 {

struct RenderState {
    uint32 frame = kInvalidUInt32;

    // ray tracing state
    uint32 rt_pathlength = 0;
    uint32 rt_maxaccum = 0;
};

bool operator==(const RenderState& lhs, const RenderState& rhs);

Result ReadRenderState(const char* filename, RenderState& renderstate);

Result WriteRenderState(const char* filename, const RenderState& renderstate);

}