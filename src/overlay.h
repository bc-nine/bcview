#pragma once

#define SDL_MAIN_HANDLED

#include "window.h"
#include "SDL.h"
#include "SDL_vulkan.h"
#include "allocator_vk.h"

namespace bc9 {

class SessionContext;

class Overlay {
public:
    void Display(SessionContext& scontext);
    void UpdateMenuBar(SessionContext& scontext);

    static void BeginImGuiFrame(SessionContext& scontext);
    static void EndImGuiFrame(SessionContext& scontext);
    static bool ProcessWindowEvent(SessionContext& scontext, WindowEvent event);

private:
    void DisplayMemoryUsage();
    void DisplayDemo(SessionContext& scontext);

    bool memoryview = false;
    bool demoview = false;
};

}