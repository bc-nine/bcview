#pragma once

#if winvk_
#include "framebuffer_vk.h"
#elif wind12_
#include "framebuffer_d12.h"
#endif