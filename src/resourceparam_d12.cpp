#include "resourceparam_d12.h"
#include "platformcontext_d12.h"
#include "util_d12.h"

namespace bc9 {

static uint64 ComputeAlignedSize(PlatformContext& context, BufferResourceType type, uint64 size) {
    // FIXME! handle alignments here...
    return size;
}

CreateBufferParams::CreateBufferParams() {
}

CreateBufferParams::CreateBufferParams(PlatformContext& context, uint64 size,
                                       BufferResourceType type, const char* name) :
instancesize(ComputeAlignedSize(context, type, size)),
instances(1),
type(type),
stride(kInvalidUInt32),
name(name) {
}

CreateBufferParams::CreateBufferParams(PlatformContext& context, uint64 size,
                                       BufferResourceType type, uint32 stride, const char* name) :
instancesize(ComputeAlignedSize(context, type, size)),
instances(1),
type(type),
stride(stride),
name(name) {
}

CreateBufferParams::CreateBufferParams(PlatformContext& context, uint64 instancesize,
                                       uint32 instances, BufferResourceType type,
                                       const char* name) :
instancesize(ComputeAlignedSize(context, type, instancesize)),
instances(instances),
type(type),
stride(kInvalidUInt32),
name(name) {
}

CreateBufferParams::CreateBufferParams(PlatformContext& context, uint64 instancesize,
                                       uint32 instances, BufferResourceType type, uint32 stride,
                                       const char* name) :
instancesize(ComputeAlignedSize(context, type, instancesize)),
instances(instances),
type(type),
stride(stride),
name(name) {
}

void CreateBufferParams::SetName(const char* buffername) {
    name = buffername;
}

void CreateBufferParams::SetVisibilityVertex(bool visible) {
    vertexvisible = visible;
}

void CreateBufferParams::SetVisibilityPixel(bool visible) {
    pixelvisible = visible;
}

void CreateBufferParams::SetAccessMode(BufferCPUAccessMode mode) {
    accessmode = mode;
}

void CreateBufferParams::SetUsageCopySource(bool source) {
    copysource = source;
}
void CreateBufferParams::SetUsageCopyDestination(bool dest) {
    copydest = dest;
}
bool CreateBufferParams::GetUsageCopySource() const {
    return copysource;
}
bool CreateBufferParams::GetUsageCopyDestination() const {
    return copydest;
}

void CreateBufferParams::SetInitialData(void* data, uint64 size) {
    initdata = data;
    initdatasize = size;
}

CreateImageParams::CreateImageParams() {
}

CreateImageParams::CreateImageParams(PlatformContext& context, ResourceFormat format,
                                     unsigned int width, unsigned int height, unsigned int samples,
                                     const char* name) :
name(name) {
    assert(width >= 1 && height >= 1);
    // we can infer that we're really only dealing with a dimension of 2 here,
    // since we require a width *and* a height, but no depth, and it must be an
    // image. that only leaves D3D12_RESOURCE_DIMENSION_TEXTURE2D.

    imageinfo.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    imageinfo.Alignment = D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT;
    imageinfo.Width = width;
    imageinfo.Height = height;
    imageinfo.DepthOrArraySize = 1;
    imageinfo.MipLevels = 1;
    Throw_(GetDXGIFormat(context, format, imageinfo.Format));
    imageinfo.SampleDesc.Count = samples;
    imageinfo.SampleDesc.Quality = 0; // might need to allow changing this at some point
    imageinfo.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    imageinfo.Flags = D3D12_RESOURCE_FLAG_NONE; // more options below

    PopulateRemaining();
}

CreateImageParams::CreateImageParams(PlatformContext& context, ResourceFormat format,
                                     unsigned int width, unsigned int height, unsigned int depth,
                                     unsigned int miplevels, unsigned int arraylayers,
                                     unsigned int samples, bool cube, const char* name) :
cube(cube),
arrayimage(arraylayers),
name(name) {
    assert(depth && (depth == 1 || arraylayers == 0));
    assert(height >= 1 || depth == 1 || depth == 0);
    assert(!cube || depth == 1);
    assert(!cube || (arraylayers % 6 == 0));

    if (height == 0) {
        imageinfo.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE1D;
    }
    else if (depth > 1) {
        imageinfo.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE3D;
    }
    else {
        imageinfo.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    }
    //                                       D3D12_SRV_DIMENSION_TEXTURE1D;
    //}
    //else if (cube) {
    //    imageinfo.Dimension = (arraylayers / 6 > 1) ? D3D12_SRV_DIMENSION_TEXTURECUBEARRAY :
    //                                                  D3D12_SRV_DIMENSION_TEXTURECUBE;
    //}
    //else if (depth > 1) {
    //    imageinfo.Dimension = D3D12_SRV_DIMENSION_TEXTURE3D;
    //}
    //else {
    //    if (arrayimage) {
    //        imageinfo.Dimension = samples > 1 ? D3D12_SRV_DIMENSION_TEXTURE2DMSARRAY :
    //                                            D3D12_SRV_DIMENSION_TEXTURE2DARRAY;
    //    }
    //    else {
    //        imageinfo.Dimension = samples > 1 ? D3D12_SRV_DIMENSION_TEXTURE2DMS :
    //                                            D3D12_SRV_DIMENSION_TEXTURE2D;
    //    }
    //}

    if (arrayimage)
        imageinfo.DepthOrArraySize = (arraylayers == 0) ? 1 : arraylayers;
    else
        imageinfo.DepthOrArraySize = depth;

    imageinfo.Alignment = D3D12_DEFAULT_RESOURCE_PLACEMENT_ALIGNMENT;
    imageinfo.Width = width;
    imageinfo.Height = height;
    imageinfo.MipLevels = miplevels;
    Throw_(GetDXGIFormat(context, format, imageinfo.Format));
    imageinfo.SampleDesc.Count = samples;
    imageinfo.SampleDesc.Quality = 0; // might need to allow changing this at some point
    imageinfo.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    imageinfo.Flags = D3D12_RESOURCE_FLAG_NONE; // more options below

    PopulateRemaining();
}

void CreateImageParams::SetInitialLayout(ImageLayout layout) {
    Throw_(GetResourceState(layout, initial));
}

void CreateImageParams::SetAccessMode(ImageCPUAccessMode mode) {
    accessmode = mode;
}

void CreateImageParams::PopulateRemaining() {
}

void CreateImageParams::SetUsageCopySource() {
}

void CreateImageParams::SetUsageCopyDestination() {
}

void CreateImageParams::SetUsageSampled() {
}

void CreateImageParams::SetUsageStorage() {
}

void CreateImageParams::SetUsageColorTarget(const glm::vec4 clearcolor) {
    imageinfo.Flags |= D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;

    initial = D3D12_RESOURCE_STATE_RENDER_TARGET;

    clearstruct.Format = imageinfo.Format;
    clearstruct.Color[0] = clearcolor.x;
    clearstruct.Color[1] = clearcolor.y;
    clearstruct.Color[2] = clearcolor.z;
    clearstruct.Color[3] = clearcolor.w;
    clear = &clearstruct;
}

void CreateImageParams::SetUsageStencilTarget(uint32_t clearstencil) {
    stencilenabled = true;
    imageinfo.Flags |= D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
    imageinfo.Flags |= D3D12_RESOURCE_FLAG_DENY_SHADER_RESOURCE;

    initial = D3D12_RESOURCE_STATE_DEPTH_WRITE;

    clearstruct.Format = imageinfo.Format;
    clearstruct.DepthStencil.Stencil = clearstencil;
    clear = &clearstruct;
}

void CreateImageParams::SetUsageDepthTarget(float cleardepth) {
    depthenabled = true;
    imageinfo.Flags |= D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
    imageinfo.Flags |= D3D12_RESOURCE_FLAG_DENY_SHADER_RESOURCE;

    initial = D3D12_RESOURCE_STATE_DEPTH_WRITE;

    clearstruct.Format = imageinfo.Format;
    clearstruct.DepthStencil.Depth = cleardepth;
    clear = &clearstruct;
}

void CreateImageParams::SetName(const char* imagename) {
    name = imagename;
}

void CreateImageParams::SetCubeCompatible() {
}

void CreateImageParams::SetQueueUsage(PlatformContext& context, bool graphics, bool compute) {
    // TODO
}

}