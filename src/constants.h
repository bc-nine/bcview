#pragma once

#include "bclib/podtypes.h"

namespace bc9 {

constexpr uintn kMaxViews = 8;

constexpr uintn kMaxSamplers = 256;

constexpr uintn kFrameRenderBufferCount = 2;

constexpr uintn kMaxArrayTextureSlices = 1024;
constexpr uintn kMaxArrayMipLevels = 14; // support 2^(14-1) = 8192x8192 max tex dim

constexpr uintn kMaxRenderTargets = 8;

// keep this in sync with the equivalent in global.hlsli!
constexpr uintn kMaxMultiviewLayers = 4;

constexpr uintn kMaxEngineConstantBuffers = 4;
constexpr uintn kMaxCustomConstantBuffers = 4;

constexpr uintn kMaxConstantBuffers = kMaxEngineConstantBuffers + kMaxCustomConstantBuffers;

constexpr uintn kMaxSwapchainImageCount = 4;

}