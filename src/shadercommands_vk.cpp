#include "shadercommands.h"
#include "platformcontext_vk.h"
#include "util_vk.h"
#include "pipelines_vk.h"

namespace bc9 {

namespace Shader {

static void PushConstantBuffer(BufferResource& cbuffer,
                               VkWriteDescriptorSet* writes,
                               VkDescriptorBufferInfo* cbufinfos,
                               uintn& slot, uintn offset) {
    VkDescriptorBufferInfo& cbufinfo = cbufinfos[slot];
    cbufinfo = {};
    cbufinfo.buffer = cbuffer.vkobj;
    cbufinfo.offset = cbuffer.GetOffset();
    cbufinfo.range = cbuffer.GetInstanceSize();

    VkWriteDescriptorSet& write = writes[slot];
    write = {};
    write.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write.dstSet = 0; // ignored
    write.dstBinding = offset + slot;
    write.dstArrayElement = 0;
    write.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write.descriptorCount = 1;
    write.pBufferInfo = &cbufinfo;

    ++slot;
}

void PushConstantBuffers(PlatformContext& context, const CBufferSet& cbufferset,
                         PipelineType ptype) {
    PushConstantBuffers(context, context.current.cmdbuffer, cbufferset, ptype);
}

void PushConstantBuffers(PlatformContext& context, CommandBuffer& cmdbuffer,
                         const CBufferSet& cbufferset, PipelineType ptype) {
    VkWriteDescriptorSet writes[kMaxConstantBuffers];
    VkDescriptorBufferInfo cbufinfos[kMaxConstantBuffers];

    uintn numwrites = 0;
    static constexpr uintn customoffset = kMaxEngineConstantBuffers;
    if (cbufferset.engine0)
        PushConstantBuffer(*cbufferset.engine0, writes, cbufinfos, numwrites, 0);
    if (cbufferset.engine1)
        PushConstantBuffer(*cbufferset.engine1, writes, cbufinfos, numwrites, 0);
    if (cbufferset.custom0)
        PushConstantBuffer(*cbufferset.custom0, writes, cbufinfos, numwrites, customoffset);
    if (cbufferset.custom1)
        PushConstantBuffer(*cbufferset.custom1, writes, cbufinfos, numwrites, customoffset);

    if (numwrites) {
        VkPipelineBindPoint bindpoint;
        Throw_(GetPipelineBindPoint(ptype, bindpoint));
        vkCmdPushDescriptorSetKHR(cmdbuffer.vkobj, bindpoint, context.pipelinelayout,
                                  0, numwrites, writes);
    }
}

void BindPipeline(PlatformContext& context, Pipeline& pipeline) {
    BindPipeline(context, context.current.cmdbuffer, pipeline);
}

void BindPipeline(PlatformContext& context, CommandBuffer& cmdbuffer, Pipeline& pipeline) {
    VkPipelineBindPoint bindpoint;
    Throw_(GetPipelineBindPoint(pipeline.type, bindpoint));
    vkCmdBindPipeline(cmdbuffer.vkobj, bindpoint, pipeline.vkobj);
}

}

}