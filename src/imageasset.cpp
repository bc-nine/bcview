#include "imageasset.h"

namespace bc9 {

Result GetImageAssetType(const StringID& strtype, ImageAssetType& type) {
    switch (strtype.Hash()) {
        case "1d"_sid:
        type = eImageAsset1D;
        break;

        case "1d-array"_sid:
        type = eImageAsset1DArray;
        break;

        case "2d"_sid:
        type = eImageAsset2D;
        break;

        case "2d-array"_sid:
        type = eImageAsset2DArray;
        break;

        case "3d"_sid:
        type = eImageAsset3D;
        break;

        case "cube"_sid:
        type = eImageAssetCube;
        break;

        case "cube-array"_sid:
        type = eImageAssetCubeArray;
        break;

        default:
            return Error("%s: image type not found", strtype.str());
        break;
    }

    return Success();
}

Result GetImageAssetColorSpace(const StringID& strcolorspace, ColorSpaceType& colorspace) {
    switch (strcolorspace.Hash()) {
        case "srgb"_sid:
        colorspace = eColorSpaceSRGB;
        break;

        case "linear"_sid:
        colorspace = eColorSpaceLinear;
        break;

        default:
        return Error("%s: image color space not found", strcolorspace.str());
        break;
    }

    return Success();
}

}
