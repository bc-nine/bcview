#pragma once

#include "bufferresource.h"
#include "pipelines.h"
#include "pipelinekeys.h"
#include "bclib/stringid.h"

namespace bc9 {

struct DrawData {
    DrawData() {}
    DrawData(unsigned int vertices) : vertices(vertices), instances(1) {}

    //Pipeline pipeline;
    PipelineBaseKey pipelinebasekey;

    uint32_t instanceidx = 0;

    // only one of indices/vertices will be filled.
    unsigned int indices = 0;
    unsigned int vertices = 0;
    uint32_t instances = 1;

    // these are the locations within the single vertex/index buffer holding all
    // geometry for the world.
    uint32_t vtxbufoffset = 0;
    uint32_t idxbufoffset = 0;
};

}
