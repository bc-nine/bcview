#pragma once

#include "bclib/stdvector.h"
#include "bclib/stdset.h"
#include "rhitypes.h"
#include "bclib/stdjson.h"
#include "cachekeys.h"
#include "property.h"
#include "bufferresource.h"
#include "sharedstructs.h"

namespace bc9 {

class World;
class AssetCache;
class SessionContext;

// NOTE: order here MUST match the propertydata order in materialcache.cpp!
enum MaterialPropertyID {
    eMaterialPropertyBaseColor,
    eMaterialPropertyBaseColorMap,
    eMaterialPropertyBaseColorMapSet,
    eMaterialPropertyMetalness,
    eMaterialPropertyRoughness,
    eMaterialPropertyMetalnessRoughnessMap,
    eMaterialPropertyNormalMap,
    eMaterialPropertyNormalMapSet,
    eMaterialPropertyEmissive,
    eMaterialPropertyEmissiveMap,
    eMaterialPropertyEmissiveMapSet,
    eMaterialPropertyBSDF,
    eMaterialPropertyAlphaCutoff,
    eMaterialPropertyAlphaMode,
    eMaterialPropertyRefractionIndex,
    eMaterialPropertyCount
};

typedef stdmap<MaterialPropertyID, Property> MaterialProperties;

struct MaterialType {
    StringID name = "null";
    MaterialGroup group;
    ShaderKey vertexkey;
    ShaderKey pixelkey;
};

struct Material {
    StringID name = "null";
    uint32 typeidx = 0;
    MaterialProperties properties;
};

struct MaterialProperty {
    uint64 name;
    PropertyType type;
    Property property;
};

class MaterialCache {
public:

    MaterialCache(const stdjson& graphjson, const World& world, const AssetCache& assetcache);

    void Initialize(SessionContext &scontext);
    void PostInitialize(SessionContext& scontext);
    void Shutdown(SessionContext &scontext);

    uintn Update(SessionContext& scontext);

    MaterialKey FindMaterial(const StringID& mtlname) const;
    const Material& GetMaterial(const MaterialKey& key) const;
    const MaterialType& GetMaterialType(const MaterialKey& key) const;

    Property GetProperty(const MaterialKey& key, MaterialPropertyID id) const;

    void SetProperty(const MaterialKey& key, MaterialPropertyID id, const Property& property);

    uint32 GetMaterialCount() const;

private:
    Property GetProperty(const Material& material, MaterialPropertyID id) const;

    void SetProperty(MaterialProperties& properties, MaterialPropertyID id,
                     const MaterialProperty& matprop, const StringID& matname);
    void SetProperties(MaterialProperties& properties, const stdjson& jsonprops,
                       const StringID& name);

    MaterialProperty GetMaterialProperty(const StringID& propname, MaterialPropertyID& id);
    MaterialProperty GetMaterialProperty(const StringID& propname, const Property& property,
                                         MaterialPropertyID& id);

    void UpdateData(SessionContext& scontext, Material& material, MaterialData& data);

    Result ValidateProperty(const MaterialProperty& matprop, const StringID& matname) const;

    Material& GetMaterial(const MaterialKey& key);

    const World& world;

    stdvector<MaterialType> materialtypes;

    stdvector<Material> materials;

    stdvector<MaterialData> materialdata;

    stdmap<StringID, uint32> keys;

    BufferResource materialbuf;
    uint32 updatematerials = false;

    typedef stdmap<uint64, MaterialPropertyID> KnownPropertyMap;
    KnownPropertyMap knownproperties;
};

}