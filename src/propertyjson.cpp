#include "propertyjson.h"

namespace bc9 {

using namespace glm;

Result CreateProperty(const stdjson &propjson, PropertyType ptype, Property& prop) {
    // we want to allow types like float vectors of size 3, bools, etc from json, but
    // we don't want to let them propagate into the system, so we must check here
    // explicitly for these cases, map them appropriately into their larger, aligned
    // types, and record their original types (when we want to serialize back out).
    try {
        switch (ptype) {
            case ePropertyTypeStringHash:
            // accept only string encoded as 64 bit hash
            {
                StringID str;
                Throw_(GetString(propjson, str));
                prop = Property(str.Hash());
            }
            break;

            case ePropertyTypeUInt:
            // accept boolean and unsigned integer
            if (propjson.type() == nlohmann::json::value_t::number_unsigned) {
                prop = Property(propjson.get<unsigned int>());
            }
            else if (propjson.type() == nlohmann::json::value_t::boolean) {
                prop = Property((glm::uint)(propjson.get<bool>()));
            }
            break;

            case ePropertyTypeInt:
            prop = Property(propjson.get<int>());
            break;

            case ePropertyTypeFloat:
            prop = Property(propjson.get<float>());
            break;

            case ePropertyTypeFloat2:
            {
                float x = propjson[0].get<float>();
                float y = propjson[1].get<float>();
                prop = Property(vec2(x, y));
            }
            break;

            case ePropertyTypeFloat3:
            {
                float x = propjson[0].get<float>();
                float y = propjson[1].get<float>();
                float z = propjson[2].get<float>();
                prop = Property(vec3(x, y, z));
            }
            break;

            case ePropertyTypeFloat4:
            {
                float x = propjson[0].get<float>();
                float y = propjson[1].get<float>();
                float z = propjson[2].get<float>();
                float w = propjson[3].get<float>();
                prop = Property(vec4(x, y, z, w));
            }
            break;

            default:
            return Error("unknown property type");
            break;
        }
    }
    catch (const stdjson__parse_error& error) {
        return Error(error.what());
    }
    catch (const stdjson__invalid_iterator& error) {
        return Error(error.what());
    }
    catch (const stdjson__type_error& error) {
        return Error(error.what());
    }
    catch (const stdjson__out_of_range& error) {
        return Error(error.what());
    }
    catch (const stdjson__other_error& error) {
        return Error(error.what());
    }

    return Success();
}

}
