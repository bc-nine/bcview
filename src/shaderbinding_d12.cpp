#include "shaderbinding.h"
#include "util_d12.h"

namespace bc9 {

ShaderBinding::ShaderBinding(const stdjson &bjson, ShaderStage stage) : stages(stage) {
    bindpoint = bjson["register"].get<unsigned int>();
    space = bjson["space"].get<unsigned int>();
    count = bjson["count"].get<unsigned int>();
    StringID strtype, strdim;
    Throw_(ReqString(bjson, "type", strtype));
    Throw_(ReqString(bjson, "dimension", strdim));
    Throw_(GetShaderInputType(strtype, type));
    Throw_(GetSRVDimension(strdim, dimension));
}

ShaderBinding::ShaderBinding() {
}

bool ShaderBinding::Valid() const {
    return stages != eShaderStageInvalid;
}

bool
ShaderBinding::operator==(const ShaderBinding& rhs) {
    // leave off stages comparison, as that isn't relevant to equality.
    return bindpoint == rhs.bindpoint &&
           space == rhs.space &&
           count == rhs.count &&
           type  == rhs.type &&
           dimension == rhs.dimension;
}

}
