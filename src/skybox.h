#pragma once

#include "drawdata.h"
#include "bclib/podtypes.h"
#include "bufferresource.h"
#include "cachekeys.h"

namespace bc9 {

struct FramebufferLayoutData;

struct SkyboxConstants {
    uintn pntoffset;
    uintn sampleridx;
    uintn imageidx;
    uintn pad;
};

class SessionContext;

class Skybox {
public:
    Skybox(SessionContext& scontext);

    void Update(SessionContext& scontext);

    void Initialize(SessionContext& scontext, FramebufferLayoutData& layoutdata);
    void Shutdown(SessionContext& scontext);

    bool Active() const;

    DrawData drawdata;
    CBufferSet cbufferset;

private:
    BufferResource cbuffer;
    SkyboxConstants constants;

    GeoKey geokey;
    ShaderKey vertexkey, pixelkey;

    bool hasmap = false;
    bool enabled = false;
};

}