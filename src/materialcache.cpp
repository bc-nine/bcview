#include "materialcache.h"
#include "propertyjson.h"
#include "resourceutil.h"
#include "world.h"
#include "sessioncontext.h"
#include "framebufferlayout.h"
#include "assetcache.h"
#include "bclib/errorutils.h"
#include "bclib/fileutils.h"

using namespace glm;
using namespace std;

namespace bc9 {

Result GetMaterialGroup(const StringID &strtype, MaterialGroup& mtlgroup) {
    switch (strtype.Hash()) {
        case "opaque"_sid:
        mtlgroup = eMaterialOpaque;
        break;

        case "transparent"_sid:
        mtlgroup = eMaterialTransparent;
        break;

        default:
        return Error("%s: material group not found", strtype.str());
        break;
    }

    return Success();
}

// NOTE: order here MUST match the order in MaterialPropertyID!
static const MaterialProperty propertydata[eMaterialPropertyCount] = {
    { "base-color"_sid,              ePropertyTypeFloat4,     glm::vec4(1) },
    { "base-color-map"_sid,          ePropertyTypeUInt,       0u },
    { "base-color-map-set"_sid,      ePropertyTypeUInt,       0u },
    { "metalness"_sid,               ePropertyTypeFloat,      0.0f },
    { "roughness"_sid,               ePropertyTypeFloat,      1.0f },
    { "metalness-roughness-map"_sid, ePropertyTypeUInt,       0u },
    { "normal-map"_sid,              ePropertyTypeUInt,       0u },
    { "normal-map-set"_sid,          ePropertyTypeUInt,       0u },
    { "emissive"_sid,                ePropertyTypeFloat3,     glm::vec3(0) },
    { "emissive-map"_sid,            ePropertyTypeUInt,       0u },
    { "emissive-map-set"_sid,        ePropertyTypeUInt,       0u },
    { "bsdf"_sid,                    ePropertyTypeStringHash, "pbr-default"_sid },
    { "alpha-cutoff"_sid,            ePropertyTypeFloat,      0.5f },
    { "alpha-mode"_sid,              ePropertyTypeUInt,       0u },
    { "refraction-index"_sid,        ePropertyTypeFloat,      1.0f },
};

MaterialProperty MaterialCache::GetMaterialProperty(const StringID& propname,
                                                    MaterialPropertyID& id) {
    const auto &it(knownproperties.find(propname.Hash()));
    Assert_(it != knownproperties.end(), "%s: %s", propname.str(), "unknown property");

    id = (*it).second;
    PropertyType proptype(propertydata[id].type);
    Property property(propertydata[id].property);

    return { propname.Hash(), proptype, property };
}

MaterialProperty MaterialCache::GetMaterialProperty(const StringID& propname,
                                                    const Property& property,
                                                    MaterialPropertyID& id) {
    const auto &it(knownproperties.find(propname.Hash()));
    Assert_(it != knownproperties.end(), "%s: %s", propname.str(), "unknown property");

    id = (*it).second;
    PropertyType proptype(propertydata[id].type);

    return { propname.Hash(), proptype, property };
}

const MaterialType& MaterialCache::GetMaterialType(const MaterialKey& key) const {
    const Material& material = GetMaterial(key);
    Assert_(material.typeidx < materialtypes.size(), "key index %u too large", material.typeidx);

    return materialtypes[material.typeidx];
}

MaterialCache::MaterialCache(const stdjson& graphjson, const World& world,
                             const AssetCache& assetcache) :
world(world) {
    bool found = false;

    const stdjson& mtljson = graphjson["materials"];
    const stdjson& mtltypesjson = graphjson["material-types"];
    for (uint32 i = 0; i < eMaterialPropertyCount; ++i)
        knownproperties[propertydata[i].name] = (MaterialPropertyID) i;

    materials.resize(mtljson.size());
    materialdata.resize(mtljson.size());
    materialtypes.resize(mtltypesjson.size());

    uint32 midx = 0;
    for (stdjson::const_iterator it = mtljson.begin(); it != mtljson.end(); ++it, ++midx) {
        auto& mjson(*it);
        if (mjson.contains("name")) {
            StringID name;
            Throw_(ReqString(mjson, "name", name));
            keys[name] = midx;
        }
    }

    uint32 i = 0;
    for (stdjson::const_iterator it = mtltypesjson.begin(); it != mtltypesjson.end(); ++it, ++i) {
        auto& mtypejson(*it);
        MaterialType& mtype(materialtypes[i]);
        Throw_(OptString(mtypejson, "name", mtype.name, found));
        StringID group;
        Throw_(ReqString(mtypejson, "group", group));
        Throw_(GetMaterialGroup(group, mtype.group));
        uint32 key;
        Assert_(mtypejson.contains("shadersets"));
        Throw_(ReqUInt(mtypejson["shadersets"][0], "vertex", key));
        mtype.vertexkey = ShaderKey(key);
        Throw_(ReqUInt(mtypejson["shadersets"][0], "pixel", key));
        mtype.pixelkey = ShaderKey(key);
    }

    i = 0;
    for (stdjson::const_iterator it = mtljson.begin(); it != mtljson.end(); ++it, ++i) {
        auto& mjson(*it);
        Material& material(materials[i]);
        Throw_(OptString(mjson, "name", material.name, found));
        uint32 typeidx;
        Throw_(ReqUInt(mjson, "type", typeidx));
        material.typeidx = typeidx;
        SetProperties(material.properties, mjson["properties"], material.name);
    }
}

void MaterialCache::SetProperties(MaterialProperties& properties, const stdjson& jsonprops,
                                  const StringID& materialname) {
    for (stdjson::const_iterator pit = jsonprops.begin(); pit != jsonprops.end(); ++pit) {
        StringID propname(pit.key().c_str());
        MaterialPropertyID id;
        MaterialProperty matprop(GetMaterialProperty(propname, id));
        Result res = CreateProperty(*pit, matprop.type, matprop.property);
        if (!res)
            Throw_(Error("%s.%s: %s", materialname.str(), propname.str(), res.Message()));

        SetProperty(properties, id, matprop, materialname);
    }
}

void MaterialCache::SetProperty(MaterialProperties& properties,
                                MaterialPropertyID id,
                                const MaterialProperty& materialprop,
                                const StringID& materialname) {
    Result res = ValidateProperty(materialprop, materialname);
    if (!res)
        Throw_(Error("%s.%s: %s", materialname.str(), StringID::LookupStr(materialprop.name), res.Message()));

    properties[id] = materialprop.property;
}

uintn MaterialCache::Update(SessionContext& scontext) {
    uintn descriptoridx = materialbuf.DescriptorIndex();

    if (updatematerials) {
        updatematerials = false;

        uintn materialcount = uintn_(materials.size());
        for (uintn i = 0; i < materials.size(); ++i)
            UpdateData(scontext, materials[i], materialdata[i]);

        descriptoridx = materialbuf.UpdateAndSwap(materialdata.data());
    }

    return descriptoridx;
}

static void BSDFToUInt(uint64 mtype, uint32& u) {
    if (mtype == "pbr-default"_sid)
        u = 0;
    else if (mtype == "pbr-diffuse"_sid)
        u = 1;
    else if (mtype == "pbr-dielectric"_sid)
        u = 2;
    else
        Throw_(Error("%s: invalid material type", StringID::LookupStr(mtype)));
}

Result MaterialCache::ValidateProperty(const MaterialProperty& matprop,
                                       const StringID& matname) const {
    return Success();
}

uint32 MaterialCache::GetMaterialCount() const {
    return uint32_(materials.size());
}

void MaterialCache::Initialize(SessionContext &scontext) {
    CreateInstancedBufferParams params(scontext.pc, eBufferResourceStructuredInput,
                                       materialdata.size(), sizeof(MaterialData),
                                       "materialcache");
    Throw_(CreateBuffer(scontext.pc, params, materialbuf));

    updatematerials = true;
}

void MaterialCache::PostInitialize(SessionContext& scontext) {
}

Material& MaterialCache::GetMaterial(const MaterialKey& key) {
    Assert_(key.index < materials.size(), "key index %u too large", key.index);

    return materials[key.index];
}

const Material& MaterialCache::GetMaterial(const MaterialKey& key) const {
    Assert_(key.index < materials.size(), "key index %u too large", key.index);

    return materials[key.index];
}

MaterialKey MaterialCache::FindMaterial(const StringID& mtlname) const {
    auto iter = keys.find(mtlname);

    return (iter == keys.end()) ? MaterialKey() : MaterialKey((*iter).second);

}

Property MaterialCache::GetProperty(const MaterialKey& key, MaterialPropertyID id) const {
    return GetProperty(GetMaterial(key), id);
}

Property MaterialCache::GetProperty(const Material& material, MaterialPropertyID id) const {
    auto iter = material.properties.find(id);
    if (iter != material.properties.end())
        return iter->second;

    return propertydata[id].property;
}

void MaterialCache::SetProperty(const MaterialKey& key, MaterialPropertyID id,
                                const Property& property) {
    Material& material(GetMaterial(key));
    MaterialProperty matprop = propertydata[id];
    matprop.property = property;
    SetProperty(material.properties, id, matprop, material.name);
}

void MaterialCache::UpdateData(SessionContext& scontext,
                               Material& material, MaterialData& data) {
    Property basecolormap = GetProperty(material, eMaterialPropertyBaseColorMap);
    uintn basecolormapset = GetProperty(material, eMaterialPropertyBaseColorMapSet).UInt();
    const TextureAsset& basecolorasset = scontext.assetcache.GetTextureAsset(basecolormap.UInt());
    data.basecolormap = (basecolorasset.EncodedIndex() << 2) | basecolormapset;

    Property emissivemap = GetProperty(material, eMaterialPropertyEmissiveMap);
    uintn emissivemapset = GetProperty(material, eMaterialPropertyEmissiveMapSet).UInt();
    const TextureAsset& emissiveasset = scontext.assetcache.GetTextureAsset(emissivemap.UInt());
    data.emissivemap = (emissiveasset.EncodedIndex() << 2) | emissivemapset;

    Property normalmap = GetProperty(material, eMaterialPropertyNormalMap);
    uintn normalmapset = GetProperty(material, eMaterialPropertyNormalMapSet).UInt();
    const TextureAsset& normalasset = scontext.assetcache.GetTextureAsset(normalmap.UInt());
    data.normalmap = (normalasset.EncodedIndex() << 2) | normalmapset;

    Property basecolor = GetProperty(material, eMaterialPropertyBaseColor);
    data.basecolor = basecolor.Float4();

    Property emissive = GetProperty(material, eMaterialPropertyEmissive);
    data.emissive = emissive.Float3();

    Property bsdf = GetProperty(material, eMaterialPropertyBSDF);
    BSDFToUInt(bsdf.StringHash(), data.bsdf);

    Property metalness = GetProperty(material, eMaterialPropertyMetalness);
    data.metalness = metalness.Float();
    Property roughness = GetProperty(material, eMaterialPropertyRoughness);
    data.roughness = roughness.Float();

    Property alphacutoff = GetProperty(material, eMaterialPropertyAlphaCutoff);
    data.alphacutoff = alphacutoff.Float();

    Property alphamode = GetProperty(material, eMaterialPropertyAlphaMode);
    data.alphamode = alphamode.UInt();

    Property refractionidx = GetProperty(material, eMaterialPropertyRefractionIndex);
    data.refractionidx = refractionidx.Float();
}

void MaterialCache::Shutdown(SessionContext &scontext) {
    DestroyBuffer(scontext.pc, materialbuf);
}

}