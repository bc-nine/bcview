#include "xrgraphics_vk.h"
#include "xrutils.h"
#include "surface_vk.h"
#include "window_vk.h"
#include "platformcontext_vk.h"
#include "util_vk.h"
#include "allocator_vk.h"
#include "bclib/stdalgorithm.h"
#include "bclib/stdallocators.h"
#include "resourceutil_vk.h"
#include "framebuffer.h"
#include "framebufferlayout.h"
#include "graphicscommands.h"

namespace bc9 {

static  stdvector<const char*> ParseExtensionString(char* names) {
    stdvector<const char*> list;
    while (*names != 0) {
        list.push_back(names);
        while (*(++names) != 0) {
            if (*names == ' ') {
                *names++ = '\0';
                break;
            }
        }
    }
    return list;
}

void XRGraphics::Initialize(PlatformContext& context, Surface& surface, Window& window) {
    DECL_XR_FUNC(*this, xrGetVulkanInstanceExtensionsKHR);
    DECL_XR_FUNC(*this, xrCreateVulkanInstanceKHR);

    XrGraphicsRequirementsVulkan2KHR reqs { XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN2_KHR };

    XrCheck_(GetVulkanGraphicsRequirements2KHR(context, &reqs));

    context.instanceextensions.push_back("VK_KHR_win32_surface");

    context.RequireBaseInstanceExtensions(context.instanceextensions, true);
    Surface::GetInstanceExtensions(window, context.instanceextensions);

    VkInstanceCreateInfo instancecreateinfo{};
    context.BuildVkInstanceCreateInfo(context.instanceextensions, instancecreateinfo, true);

    XrVulkanInstanceCreateInfoKHR createinfo { XR_TYPE_VULKAN_INSTANCE_CREATE_INFO_KHR };
    createinfo.systemId = xrsystemid;
    createinfo.pfnGetInstanceProcAddr = vkGetInstanceProcAddr;
    createinfo.vulkanCreateInfo = &instancecreateinfo;
    createinfo.vulkanAllocator = VkAllocators();
    XrCheck_(CreateVulkanInstanceKHR(context, &createinfo, &context.instance));

    context.SetupDebugUtilsCallback();

    surface.Initialize(context, window);

    XrVulkanGraphicsDeviceGetInfoKHR devicegetinfo { XR_TYPE_VULKAN_GRAPHICS_DEVICE_GET_INFO_KHR };
    devicegetinfo.systemId = xrsystemid;
    devicegetinfo.vulkanInstance = context.instance;
    XrCheck_(GetVulkanGraphicsDevice2KHR(context, &devicegetinfo, &context.physicaldevice));

    // openxr runtime picks the video card, so it's not as challenging for us to
    // "pick a device" in this case, whereas for non-vr, picking a device involves
    // creating a surface, and ensuring that the device can "present" on that surface.
    Throw_(FindQueueFamilies(context.physicaldevice, surface.vkobj, context.indices));

    surface.GetDeviceExtensions(context.deviceextensions);
    context.RequireDeviceExtensions(context.deviceextensions);

    VkDeviceCreateInfo devicecreateinfo{};
    context.BuildVkDeviceCreateInfo(context.deviceextensions, devicecreateinfo);

    XrVulkanDeviceCreateInfoKHR xrdevicecreateinfo { XR_TYPE_VULKAN_DEVICE_CREATE_INFO_KHR };
    xrdevicecreateinfo.systemId = xrsystemid;
    xrdevicecreateinfo.pfnGetInstanceProcAddr = vkGetInstanceProcAddr;
    xrdevicecreateinfo.vulkanCreateInfo = &devicecreateinfo;
    xrdevicecreateinfo.vulkanPhysicalDevice = context.physicaldevice;
    xrdevicecreateinfo.vulkanAllocator = VkAllocators();
    XrCheck_(CreateVulkanDeviceKHR(context, &xrdevicecreateinfo));

    context.CreateDeviceQueues();

    graphicsbinding.type = GetGraphicsBindingType();

    graphicsbinding.instance = context.instance;
    graphicsbinding.physicalDevice = context.physicaldevice;
    graphicsbinding.device = context.device;
    graphicsbinding.queueFamilyIndex = context.indices.graphicsfamily;
    graphicsbinding.queueIndex = 0;
}

void XRGraphics::BeginRenderPass(PlatformContext& context, Framebuffer& framebuffer){
    VkRect2D rect{};
    rect.offset = { 0, 0 };
    framebuffer.GetDimensions(context, rect.extent.width, rect.extent.height);

    VkRenderPassBeginInfo renderpassinfo = {};
    renderpassinfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderpassinfo.renderPass = framebuffer.layout->layoutdata.renderpass;
    renderpassinfo.framebuffer = framebuffer.vkobjs[xrswapidx];
    renderpassinfo.renderArea.offset = {0, 0};
    renderpassinfo.renderArea.extent = rect.extent;

    renderpassinfo.clearValueCount = framebuffer.layout->hasclearattachment ?
                                        static_cast<uint32>(framebuffer.clearvalues.size()) : 0;
    renderpassinfo.pClearValues = framebuffer.clearvalues.data();

    vkCmdBeginRenderPass(context.current.cmdbuffer.vkobj, &renderpassinfo, VK_SUBPASS_CONTENTS_INLINE);

    framebuffer.Bind(context);

    context.BindBindlessInputSet(ePipelineGraphics);

    Graphics::PushConstantBuffers(context, { .engine0 = &context.pfconstantsbuf });
}

void XRGraphics::EndRenderPass(PlatformContext& context) {
    vkCmdEndRenderPass(context.current.cmdbuffer.vkobj);
}

int64 XRGraphics::SetColorSwapchainFormat(const stdvector<int64>& runtimeformats) {
    // TODO: actually pick an srgb one here? looks like we get a unorm otherwise...
    constexpr int64 supportedformats[] = {
        VK_FORMAT_B8G8R8A8_SRGB, VK_FORMAT_R8G8B8A8_SRGB,
        VK_FORMAT_B8G8R8A8_UNORM, VK_FORMAT_R8G8B8A8_UNORM
    };

    //auto iter = std::find_first_of(runtimeformats.begin(), runtimeformats.end(),
    //                               std::begin(supportedformats),
    //                               std::end(supportedformats));
    auto iter = std::find_first_of(std::begin(supportedformats), std::end(supportedformats),
                                   runtimeformats.begin(), runtimeformats.end());
    if (iter == runtimeformats.end()) {
        Throw_(Error("no runtime swapchain format supported for color swapchain"));
    }

    VkFormat vkformat = (VkFormat) *iter;
    Throw_(GetResourceFormat(vkformat, xrswapchainformat));
    xrvkswapchainformat = vkformat;

    return *iter;
}

uint32 XRGraphics::GetSupportedSwapchainSampleCount(const XrViewConfigurationView&) const {
    return VK_SAMPLE_COUNT_1_BIT;
}

XrStructureType XRGraphics::GetGraphicsBindingType() const {
    return XR_TYPE_GRAPHICS_BINDING_VULKAN2_KHR;
}

void XRGraphics::Shutdown(PlatformContext& context) {
    for (uintn i = 0; i < xrswapchainimagecount; i++)
        vkDestroyImageView(context.device, xrswapchainimages[i].view.vkobj,VkAllocators());
}

XrResult
XRGraphics::CreateVulkanInstanceKHR(PlatformContext& context,
                                      const XrVulkanInstanceCreateInfoKHR* createinfo,
                                      VkInstance* vkinstance) {
    DECL_XR_FUNC(*this, xrCreateVulkanInstanceKHR);

    VkResult vkresult;
    XrResult result = xrCreateVulkanInstanceKHR(xrinstance, createinfo, vkinstance,
                                                &vkresult);
    VkCheck_(vkresult);

    volkLoadInstance(context.instance);

    return result;
}

XrResult
XRGraphics::CreateVulkanDeviceKHR(PlatformContext& context,
                                    const XrVulkanDeviceCreateInfoKHR* createinfo) {
    DECL_XR_FUNC(*this, xrCreateVulkanDeviceKHR);

    VkResult vkresult;
    XrResult result = xrCreateVulkanDeviceKHR(xrinstance, createinfo, &context.device,
                                              &vkresult);
    VkCheck_(vkresult);

    return result;
}

XrResult XRGraphics::LoadSwapImages(PlatformContext& context, XrSwapchain swapchain,
                                    uintn imagecount) {
    xrswapchainimages.resize(imagecount);

    StdStackAllocator alloc(GlobalStack());

    stdvector<XrSwapchainImageVulkan2KHR> swapimages(imagecount, alloc);
    stdvector<XrSwapchainImageBaseHeader*> bhdata(imagecount, alloc);
    for (uintn i = 0; i < imagecount; ++i) {
        swapimages[i] = { .type = XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR };
        bhdata[i] = reinterpret_cast<XrSwapchainImageBaseHeader*>(&swapimages[i]);
    }
    XrCheck_(xrEnumerateSwapchainImages(swapchain, imagecount, &imagecount, bhdata[0]));

    VkImageSubresourceRange range = { .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                      .baseMipLevel = 0,
                                      .levelCount = 1,
                                      .baseArrayLayer = 0,
                                      .layerCount = xrviewcount };

    for (uintn imageidx = 0; imageidx < imagecount; ++imageidx) {
        VkImage image = swapimages[imageidx].image;
        VkImageViewCreateInfo createinfo = {
            .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
            .image = image,
            .viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY,
            .format = xrvkswapchainformat,
            .subresourceRange = range
        };

        VkImageView imageview;
        FString imgviewname("xrswapchainimgview-%d", imageidx);
        Throw_(CreateImageView(context, createinfo, imgviewname.str(), imageview));

        ImageResource& resource = xrswapchainimages[imageidx];
        resource.vkobj = image;
        resource.view.vkobj = imageview;
    }

    return XR_SUCCESS;
}

XrResult XRGraphics::GetVulkanGraphicsRequirements2KHR(PlatformContext& context,
                                                         XrGraphicsRequirementsVulkan2KHR* reqs) {
    DECL_XR_FUNC(*this, xrGetVulkanGraphicsRequirements2KHR);

    return xrGetVulkanGraphicsRequirements2KHR(xrinstance, xrsystemid, reqs);
}

XrResult
XRGraphics::GetVulkanGraphicsDevice2KHR(PlatformContext& context,
                                                const XrVulkanGraphicsDeviceGetInfoKHR* getinfo,
                                                VkPhysicalDevice* vkphysicaldevice) {
    DECL_XR_FUNC(*this, xrGetVulkanGraphicsDevice2KHR);

    return xrGetVulkanGraphicsDevice2KHR(xrinstance, getinfo, vkphysicaldevice);
}

const XrBaseInStructure* XRGraphics::GetGraphicsBinding() const {
    return reinterpret_cast<const XrBaseInStructure*>(&graphicsbinding);
}

}