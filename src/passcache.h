#pragma once

#include "bclib/stdmap.h"
#include "bclib/stringid.h"
#include "framebufferlayout.h"

namespace bc9 {

struct ImageView;
class SessionContext;

class PassCache {
public:
    PassCache();

    void Initialize(SessionContext &scontext);
    void Shutdown(SessionContext &scontext);

    void RegisterPass(const StringID& passname);
    void ResetPass(const StringID& passname);

    void UpdateFramebufferLayout(const StringID& passname, FramebufferLayout& layout);
    void GetFramebufferLayout(const StringID& passname, FramebufferLayout& layout);

    void RegisterTarget(const StringID& passname, const StringID& targetname);
    void UpdateTarget(const StringID& passname, const StringID& targetname, ImageView* imageview);
    void GetTarget(const StringID& passname, const StringID& targetname, ImageView** imageview);

    // if no "display" target exists, "main" target is used, since all configurations
    // require at least a "main" target.
    void GetDisplayTarget(const StringID& targetname, ImageView** imageview);
    void GetDisplayFramebufferLayout(FramebufferLayout& layout);

private:
    typedef stdmap<StringID, ImageView*> ImageViewMap;
    stdmap<StringID, ImageViewMap> passimageviewmaps;
    stdmap<StringID, FramebufferLayout> framebufferlayouts;
};

}