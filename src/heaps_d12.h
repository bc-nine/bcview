#pragma once

#include <wrl.h>
#include <d3d12.h>
#include "types_d12.h"

namespace bc9 {

class PlatformContext;

class D12Heaps {
public:
    D12Heaps();

    void Initialize(Microsoft::WRL::ComPtr<ID3D12Device2> device);
    void Shutdown();

    SRVHandle AllocateSRV(const PlatformContext& context, unsigned int descriptorcount);
    RTVHandle AllocateRTV(const PlatformContext& context, unsigned int descriptorcount);
    DSVHandle AllocateDSV(const PlatformContext& context, unsigned int descriptorcount);
    SamplerHandle AllocateSampler(const PlatformContext& context, unsigned int descriptorcount);

    void ResetSRVHeap();
    void ResetRTVHeap();
    void ResetDSVHeap();
    void ResetSamplerHeap();

    bool RTVHeapReset() const;

    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> srvheap;
    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> rtvheap;
    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> dsvheap;
    Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> samplerheap;

    SRVHandle startsrv, nextsrv;
    RTVHandle startrtv, nextrtv;
    DSVHandle startdsv, nextdsv;
    SamplerHandle startsampler, nextsampler;
};

}
