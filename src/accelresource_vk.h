#pragma once

#include "volk.h"
#include "bufferresource_vk.h"

namespace bc9 {

struct BLASResource {
    uint64_t deviceaddress = 0;
    BufferResource buffer, vertexbuffer, indexbuffer, transformbuffer;
    VkAccelerationStructureKHR vkobj;
};

struct TLASResource {
    uint64_t deviceaddress = 0;
    BufferResource buffer;
    VkAccelerationStructureKHR vkobj;

    void SetDescriptorIndex(uintn index) { descriptoridx = index; }
    uint32 DescriptorIndex() const { return descriptoridx; }

private:
    uint32 descriptoridx = kInvalidUInt32;
};

}
