#pragma once

#include "bclib/stdvector.h"
#include "bclib/stringid.h"
#include "glm/vec3.hpp"
#include "glm/gtc/quaternion.hpp"

namespace bc9 {

struct Model;
class Camera;

struct Node {
    uint32 GetChildCount() const;

    StringID name = "null";

    glm::vec3 translation = { 0., 0., 0. };
    glm::quat rotation = { 1., 0., 0., 0. };
    glm::vec3 scale = { 1., 1., 1. };

    // cumulatives
    glm::mat4x4 transform;
    glm::mat4x4 invtransform;

    const Model* model = nullptr;
    uint32 modeldataidx = kInvalidUInt32;

    const Camera* camera = nullptr;

    uint32 parent;
    stdvector<uint32> children;
};

}