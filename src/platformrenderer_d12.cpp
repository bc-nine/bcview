#include "platformrenderer_d12.h"
#include "util_d12.h"
#include "resourceutil.h"
#include "sharedstructs.h"
#include "drawablecache.h"
#include "materialcache.h"
#include "sessioncontext.h"
#include "inputset.h"
#include "imgui_impl_dx12.h"
#include "imgui_impl_win32.h"
#include "renderer.h"
#include "world.h"
#include "bclib/timeutils.h"
#include "bclib/dxutils.h"

using namespace Microsoft::WRL;

namespace bc9 {

static void UpdateSwapChainImages(PlatformContext& context,
                                  stdarray<RTVHandle, kFrameRenderBufferCount>& swaphandles,
                                  const World& world) {

    D3D12_RENDER_TARGET_VIEW_DESC desc = {};
    // DX12 has this weird thing where the swap chain image format itself doesn't
    // typically support sRGB, but *views* on it do.
    Throw_(GetDXGIFormat(context, world.GetSurfaceColorSpace(), desc.Format));
    desc.ViewDimension = D3D12_RTV_DIMENSION_TEXTURE2D;
    desc.Texture2D.MipSlice = 0;
    desc.Texture2D.PlaneSlice = 0;

    assert(context.heaps.RTVHeapReset());

    for (int i = 0; i < kFrameRenderBufferCount; ++i) {
        swaphandles[i] = context.heaps.AllocateRTV(context, 1);
        ComPtr<ID3D12Resource> backbuffer;
        DxThrow_(context.swapchain->GetBuffer(i, IID_PPV_ARGS(&backbuffer)));

        context.swapchainimages[i] = backbuffer;

        context.device->CreateRenderTargetView(context.swapchainimages[i].Get(),
                                               &desc, swaphandles[i].cpuhandle);
    }
}

static ComPtr<IDXGISwapChain4> CreateSwapchain(PlatformContext& context,
                                               unsigned int width,
                                               unsigned int height) {
    ComPtr<IDXGISwapChain4> dxgiswapchain4;
    ComPtr<IDXGIFactory4> dxgifactory4;
    UINT createfactoryflags = 0;
#if defined(_DEBUG)
    createfactoryflags = DXGI_CREATE_FACTORY_DEBUG;
#endif

    DxThrow_(CreateDXGIFactory2(createfactoryflags, IID_PPV_ARGS(&dxgifactory4)));
    DXGI_SWAP_CHAIN_DESC1 swapchaindesc = {};
    swapchaindesc.Width = width;
    swapchaindesc.Height = height;
    swapchaindesc.Format = context.d12swapchainformat;
    swapchaindesc.Stereo = FALSE;
    swapchaindesc.SampleDesc = { 1, 0 };
    swapchaindesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapchaindesc.BufferCount = context.swapchainimagecount;
    swapchaindesc.Scaling = DXGI_SCALING_STRETCH;
    swapchaindesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
    swapchaindesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
    // It is recommended to always allow tearing if tearing support is available.
    swapchaindesc.Flags = context.tearingsupported ? DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING : 0;
    ComPtr<IDXGISwapChain1> swapchain1;
    DxThrow_(dxgifactory4->CreateSwapChainForHwnd(
        context.graphicsqueue.Get(),
        context.mswin,
        &swapchaindesc,
        nullptr,
        nullptr,
        &swapchain1));

    // Disable the Alt+Enter fullscreen toggle feature. Switching to fullscreen
    // will be handled manually.
    DxThrow_(dxgifactory4->MakeWindowAssociation(context.mswin, DXGI_MWA_NO_ALT_ENTER));

    DxThrow_(swapchain1.As(&dxgiswapchain4));

    return dxgiswapchain4;
}

static ComPtr<ID3D12CommandAllocator> CreateCommandAllocator(ComPtr<ID3D12Device2> device,
                                                             D3D12_COMMAND_LIST_TYPE type) {
    ComPtr<ID3D12CommandAllocator> commandallocator;
    DxThrow_(device->CreateCommandAllocator(type, IID_PPV_ARGS(&commandallocator)));

    return commandallocator;
}

static ComPtr<ID3D12GraphicsCommandList2>
CreateCommandList(ComPtr<ID3D12Device2> device,
                  ComPtr<ID3D12CommandAllocator> commandallocator,
                  D3D12_COMMAND_LIST_TYPE type) {
    ComPtr<ID3D12GraphicsCommandList2> commandlist;
    DxThrow_(device->CreateCommandList(0, type, commandallocator.Get(), nullptr,
                                     IID_PPV_ARGS(&commandlist)));

    DxThrow_(commandlist->Close());

    return commandlist;
}

static uint64_t Signal(ComPtr<ID3D12CommandQueue> commandqueue, ComPtr<ID3D12Fence> fence,
                       uint64_t& fencevalue) {
    uint64_t fencevalueforsignal = ++fencevalue;
    DxThrow_(commandqueue->Signal(fence.Get(), fencevalueforsignal));

    return fencevalueforsignal;
}

static void WaitForFenceValue(ComPtr<ID3D12Fence> fence, uint64_t fencevalue, HANDLE fenceevent,
                              std::chrono::milliseconds duration=std::chrono::milliseconds::max()){
    if (fence->GetCompletedValue() < fencevalue) {
        DxThrow_(fence->SetEventOnCompletion(fencevalue, fenceevent));
        ::WaitForSingleObject(fenceevent, static_cast<DWORD>(duration.count()));
    }
}

PlatformRenderer::PlatformRenderer(Renderer& renderer) :
renderer(renderer) {
}

void PlatformRenderer::PostInitialize(SessionContext& scontext) {
}

void PlatformRenderer::Initialize(SessionContext& scontext) {
    RECT rect;
    GetClientRect(scontext.pc.mswin, &rect);
    unsigned int width = rect.right - rect.left;
    unsigned int height = rect.bottom - rect.top;
    scontext.pc.swapchaindim.x = width;
    scontext.pc.swapchaindim.y = height;
    viewport = CD3DX12_VIEWPORT(0.0f, 0.0f,
                                static_cast<float>(width),
                                static_cast<float>(height));
    scissorrect = CD3DX12_RECT(0, 0, LONG_MAX, LONG_MAX);

    // FIXME! we don't do it this way anymore...
    for (unsigned int i = 0; i < ePipelineTypeCount; ++i) {
        PipelineType ptype((PipelineType)i);
        CreateBindlessInputLayout(scontext, scontext.pc.bindlesslayouts[ptype], ptype);
        scontext.pc.bindlesslayouts[ptype].Initialize(scontext);
    }

    scontext.pc.swapchain = CreateSwapchain(scontext.pc, width, height);

    scontext.pc.swapidx = scontext.pc.swapchain->GetCurrentBackBufferIndex();

    UpdateSwapChainImages(scontext.pc, swaphandles, scontext.world);

    for (int i = 0; i < kFrameRenderBufferCount; ++i) {
        commandallocators[i] = CreateCommandAllocator(scontext.pc.device,
                                                      D3D12_COMMAND_LIST_TYPE_DIRECT);
    }

    scontext.pc.commandlist = CreateCommandList(scontext.pc.device,
                                                commandallocators[scontext.pc.swapidx],
                                                D3D12_COMMAND_LIST_TYPE_DIRECT);

    // per-frame constants
    CreateBufferParams params(scontext.pc, sizeof(PerFrameConstants), kFrameRenderBufferCount,
                              eBufferResourceConstant, "perframe");
    params.SetAccessMode(eBufferCPUAccessWrite);
    pfconstantsbuf = CreateBuffer(scontext.pc, params);

    initialized = true;
}

void PlatformRenderer::Shutdown(SessionContext& scontext) {
}

void PlatformRenderer::BeginGraphicsCommands(SessionContext& scontext) {
}

void PlatformRenderer::EndGraphicsCommands(SessionContext& scontext) {
}

void PlatformRenderer::BeginFrame(SessionContext& scontext) {
    assert(initialized);

    auto commandallocator = commandallocators[scontext.pc.swapidx];
    auto backbuffer = scontext.pc.swapchainimages[scontext.pc.swapidx];

    commandallocator->Reset();
    scontext.pc.commandlist->Reset(commandallocator.Get(), nullptr);

    ID3D12DescriptorHeap* heaps[] = { scontext.pc.heaps.srvheap.Get(),
                                      scontext.pc.heaps.samplerheap.Get() };
    scontext.pc.commandlist->SetDescriptorHeaps(2, heaps);

    scontext.pc.BeginResourceBarriers();
    scontext.pc.AddSwapchainResourceBarrier(scontext.pc.swapidx,
                                            D3D12_RESOURCE_STATE_RENDER_TARGET);
    scontext.pc.EndResourceBarriers(scontext.pc.commandlist);
}

void PlatformRenderer::BeginRenderPass(SessionContext& scontext, Framebuffer& framebuffer) {
    framebuffer.PreTransitions(scontext.pc);
    framebuffer.Bind(scontext.pc);
    framebuffer.Clear(scontext.pc);

    if (scontext.EnableOverlay() && framebuffer.layout->presenting) {
        BeginImGuiFrame(scontext);
        renderer.DisplayOverlay(scontext);
    }
}

void PlatformRenderer::EndFrame(SessionContext& scontext) {
    if (scontext.pc.swapchainstates[scontext.pc.swapidx] != D3D12_RESOURCE_STATE_PRESENT) {
        scontext.pc.BeginResourceBarriers();
        scontext.pc.AddSwapchainResourceBarrier(scontext.pc.swapidx,
                                                D3D12_RESOURCE_STATE_PRESENT);
        scontext.pc.EndResourceBarriers(scontext.pc.commandlist);
    }

    DxThrow_(scontext.pc.commandlist->Close());

    ID3D12CommandList* const commandlists[] = { scontext.pc.commandlist.Get() };
    scontext.pc.graphicsqueue->ExecuteCommandLists(_countof(commandlists), commandlists);
    bool vsync = scontext.pc.vsync;
    UINT syncinterval = vsync ? 1 : 0;
    UINT presentflags = scontext.pc.tearingsupported && !vsync ? DXGI_PRESENT_ALLOW_TEARING : 0;
    DxThrow_(scontext.pc.swapchain->Present(syncinterval, presentflags));

    scontext.pc.framefencevalues[scontext.pc.swapidx] = Signal(scontext.pc.graphicsqueue,
                                                       scontext.pc.fence, scontext.pc.fencevalue);
    scontext.pc.swapidx = scontext.pc.swapchain->GetCurrentBackBufferIndex();

    WaitForFenceValue(scontext.pc.fence, scontext.pc.framefencevalues[scontext.pc.swapidx],
                      scontext.pc.fenceevent);
}

void PlatformRenderer::CreateBindlessInputLayout(SessionContext& scontext, InputLayout& layout,
                                                 PipelineType ptype) {
    layout.bindlessrootparamcount = 13;
    layout.bindlessrootparams.resize(layout.bindlessrootparamcount);
    auto& rootparams(layout.bindlessrootparams);

    size_t texcount1d = scontext.assetcache.GetTextureCount(eTextureAssetType1D);
    size_t texcount1darray = scontext.assetcache.GetTextureCount(eTextureAssetType1DArray);
    size_t texcount2d = scontext.assetcache.GetTextureCount(eTextureAssetType2D);
    size_t texcount2darray = scontext.assetcache.GetTextureCount(eTextureAssetType2DArray);
    size_t texcount3d = scontext.assetcache.GetTextureCount(eTextureAssetType3D);
    size_t texcountcube = scontext.assetcache.GetTextureCount(eTextureAssetTypeCube);
    size_t texcountcubearray = scontext.assetcache.GetTextureCount(eTextureAssetTypeCubeArray);

    UINT t0 = 0, s0 = 0, b0 = 0;

    // constants & perframe
    rootparams[0].InitAsConstants(2, b0, 100); // 2 constants
    rootparams[1].InitAsConstantBufferView(b0, 101);

    // 1d & 1d array textures
    layout.bindless1dtable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, (UINT)texcount1d, t0, 100);
    rootparams[2].InitAsDescriptorTable(1, &layout.bindless1dtable,
                                        D3D12_SHADER_VISIBILITY_PIXEL);
    layout.bindless1darraytable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, (UINT)texcount1darray,
                                     t0, 101);
    rootparams[3].InitAsDescriptorTable(1, &layout.bindless1darraytable,
                                        D3D12_SHADER_VISIBILITY_PIXEL);

    // 2d & 2d array textures
    layout.bindless2dtable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, (UINT)texcount2d, t0, 102);
    rootparams[4].InitAsDescriptorTable(1, &layout.bindless2dtable,
                                        D3D12_SHADER_VISIBILITY_PIXEL);
    layout.bindless2darraytable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, (UINT)texcount2darray,
                                     t0, 103);
    rootparams[5].InitAsDescriptorTable(1, &layout.bindless2darraytable,
                                        D3D12_SHADER_VISIBILITY_PIXEL);

    // 3d textures
    layout.bindless3dtable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, (UINT)texcount3d,
                                t0, 104);
    rootparams[6].InitAsDescriptorTable(1, &layout.bindless3dtable,
                                        D3D12_SHADER_VISIBILITY_PIXEL);

    // cube & cube array textures
    layout.bindlesscubetable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, (UINT)texcountcube,
                                     t0, 105);
    rootparams[7].InitAsDescriptorTable(1, &layout.bindlesscubetable,
                                        D3D12_SHADER_VISIBILITY_PIXEL);
    layout.bindlesscubearraytable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, (UINT)texcountcubearray,
                                       t0, 106);
    rootparams[8].InitAsDescriptorTable(1, &layout.bindlesscubearraytable,
                                        D3D12_SHADER_VISIBILITY_PIXEL);

    // vertices
    layout.bindlessvtxtable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, t0, 107);
    rootparams[9].InitAsDescriptorTable(1, &layout.bindlessvtxtable);

    // drawables
    layout.bindlessdrwtable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, t0, 108);
    rootparams[10].InitAsDescriptorTable(1, &layout.bindlessdrwtable);

    // materials
    layout.bindlessmtltable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, t0, 109);
    rootparams[11].InitAsDescriptorTable(1, &layout.bindlessmtltable);

    // samplers
    layout.bindlesssamptable.Init(D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 2, s0, 100);
    rootparams[12].InitAsDescriptorTable(1, &layout.bindlesssamptable);
}

void PlatformRenderer::CreateBindlessInputSet(SessionContext& scontext, InputLayout& layout,
                                              InputSet& inputset) {
    size_t texcount1d = scontext.assetcache.GetTextureCount(eTextureAssetType1D);
    size_t texcount1darray = scontext.assetcache.GetTextureCount(eTextureAssetType1DArray);
    size_t texcount2d = scontext.assetcache.GetTextureCount(eTextureAssetType2D);
    size_t texcount2darray = scontext.assetcache.GetTextureCount(eTextureAssetType2DArray);
    size_t texcount3d = scontext.assetcache.GetTextureCount(eTextureAssetType3D);
    size_t texcountcube = scontext.assetcache.GetTextureCount(eTextureAssetTypeCube);
    size_t texcountcubearray = scontext.assetcache.GetTextureCount(eTextureAssetTypeCubeArray);
    // srv heap - texcounts, plus one for (structured buffer) vertex buffer,
    // two for drawables buffer, and two for materials buffer plus layout descriptors.
    size_t totaldesccount = kFrameRenderBufferCount * layout.GetTotalDescriptorCount() + 5 +
                            texcount1d + texcount1darray + texcount2d + texcount2darray +
                            texcount3d + texcountcube + texcountcubearray;


    inputset.samplerhandle = scontext.pc.heaps.AllocateSampler(scontext.pc, 2);
    inputset.srvhandle = scontext.pc.heaps.AllocateSRV(scontext.pc,
                                                       (unsigned int) totaldesccount);

    SamplerHandle samplerhandle = inputset.samplerhandle;

    // per-frame buffer is created in Initialize().

    // sampler
    D3D12_SAMPLER_DESC samplerdesc = {};
    samplerdesc.Filter = D3D12_FILTER_ANISOTROPIC;
    samplerdesc.AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
    samplerdesc.AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
    samplerdesc.AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
    samplerdesc.MinLOD = 0.0f;
    samplerdesc.MaxLOD = D3D12_FLOAT32_MAX;
    samplerdesc.MipLODBias = 0;
    samplerdesc.MaxAnisotropy = 0;
    samplerdesc.ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
    scontext.pc.device->CreateSampler(&samplerdesc, samplerhandle.cpuhandle);
    samplerhandle.Offset(scontext.pc);

    samplerdesc.Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
    scontext.pc.device->CreateSampler(&samplerdesc, samplerhandle.cpuhandle);
    samplerhandle.Offset(scontext.pc);

    // array2d descriptors
    SRVHandle srvhandle = inputset.srvhandle;

    // 1d descriptors
    inputset.handle1d = srvhandle.gpuhandle;
    for (unsigned int i = 0; i < texcount1d; ++i) {
        TextureKey texkey(i, eTextureAssetType1D);
        TextureAsset texasset = scontext.assetcache.GetTextureAsset(texkey);
        ImageResource texresource = scontext.assetcache.GetTexture(texkey);

        auto resource = texresource.resource.Get();
        scontext.pc.device->CreateShaderResourceView(resource, &texresource.srv,
                                                     srvhandle.cpuhandle);
        srvhandle.Offset(scontext.pc);
    }

    // 1d array descriptors
    inputset.handle1darray = srvhandle.gpuhandle;
    for (unsigned int i = 0; i < texcount1darray; ++i) {
        TextureKey texkey(i, eTextureAssetType1DArray);
        TextureAsset texasset = scontext.assetcache.GetTextureAsset(texkey);
        ImageResource texresource = scontext.assetcache.GetTexture(texkey);

        auto resource = texresource.resource.Get();
        scontext.pc.device->CreateShaderResourceView(resource, &texresource.srv,
                                                     srvhandle.cpuhandle);
        srvhandle.Offset(scontext.pc);
    }

    // 2d descriptors
    inputset.handle2d = srvhandle.gpuhandle;
    for (unsigned int i = 0; i < texcount2d; ++i) {
        TextureKey texkey(i, eTextureAssetType2D);
        TextureAsset texasset = scontext.assetcache.GetTextureAsset(texkey);
        ImageResource texresource = scontext.assetcache.GetTexture(texkey);

        auto resource = texresource.resource.Get();
        scontext.pc.device->CreateShaderResourceView(resource, &texresource.srv,
                                                     srvhandle.cpuhandle);
        srvhandle.Offset(scontext.pc);
    }

    // 2d array descriptors
    inputset.handle2darray = srvhandle.gpuhandle;
    for (unsigned int i = 0; i < texcount2darray; ++i) {
        TextureKey texkey(i, eTextureAssetType2DArray);
        TextureAsset texasset = scontext.assetcache.GetTextureAsset(texkey);
        ImageResource texresource = scontext.assetcache.GetTexture(texkey);

        auto resource = texresource.resource.Get();
        scontext.pc.device->CreateShaderResourceView(resource, &texresource.srv,
                                                     srvhandle.cpuhandle);
        srvhandle.Offset(scontext.pc);
    }

    // 3d descriptors
    inputset.handle3d = srvhandle.gpuhandle;
    for (unsigned int i = 0; i < texcount3d; ++i) {
        TextureKey texkey(i, eTextureAssetType3D);
        TextureAsset texasset = scontext.assetcache.GetTextureAsset(texkey);
        ImageResource texresource = scontext.assetcache.GetTexture(texkey);

        auto resource = texresource.resource.Get();
        scontext.pc.device->CreateShaderResourceView(resource, &texresource.srv,
                                                     srvhandle.cpuhandle);
        srvhandle.Offset(scontext.pc);
    }

    // cube descriptors
    inputset.handlecube = srvhandle.gpuhandle;
    for (unsigned int i = 0; i < texcountcube; ++i) {
        TextureKey texkey(i, eTextureAssetTypeCube);
        TextureAsset texasset = scontext.assetcache.GetTextureAsset(texkey);
        ImageResource texresource = scontext.assetcache.GetTexture(texkey);

        auto resource = texresource.resource.Get();
        scontext.pc.device->CreateShaderResourceView(resource, &texresource.srv,
                                                     srvhandle.cpuhandle);
        srvhandle.Offset(scontext.pc);
    }

    // cube array descriptors
    inputset.handlecubearray = srvhandle.gpuhandle;
    for (unsigned int i = 0; i < texcountcubearray; ++i) {
        TextureKey texkey(i, eTextureAssetTypeCubeArray);
        TextureAsset texasset = scontext.assetcache.GetTextureAsset(texkey);
        ImageResource texresource = scontext.assetcache.GetTexture(texkey);

        auto resource = texresource.resource.Get();
        scontext.pc.device->CreateShaderResourceView(resource, &texresource.srv,
                                                     srvhandle.cpuhandle);
        srvhandle.Offset(scontext.pc);
    }

    // vertex buffer
    {
        inputset.handlevertices = srvhandle.gpuhandle;
        BufferResource vertexbuffer(scontext.assetcache.GetGeoVertexBuffer());

        auto resource = vertexbuffer.resource.Get();
        scontext.pc.device->CreateShaderResourceView(resource, &vertexbuffer.srv,
                                                     srvhandle.cpuhandle);
        srvhandle.Offset(scontext.pc);
    }

    // drawables buffers (2, since it's double buffered)
    {
        BufferResource drawables(scontext.drawablecache.GetBuffer());
        D3D12_SHADER_RESOURCE_VIEW_DESC drawablesdesc = drawables.srv;

        auto resource = drawables.resource.Get();
        scontext.pc.device->CreateShaderResourceView(resource, &drawablesdesc,
                                                     srvhandle.cpuhandle);
        inputset.drawablehandles[0] = srvhandle.gpuhandle;
        srvhandle.Offset(scontext.pc);

        drawablesdesc.Buffer.FirstElement = scontext.drawablecache.GetInstanceCount();
        scontext.pc.device->CreateShaderResourceView(resource, &drawablesdesc,
                                                     srvhandle.cpuhandle);
        inputset.drawablehandles[1] = srvhandle.gpuhandle;
        srvhandle.Offset(scontext.pc);
    }

    // materials buffers (2, since it's double buffered)
    {
        BufferResource materials(scontext.materialcache.GetBuffer());
        D3D12_SHADER_RESOURCE_VIEW_DESC materialsdesc = materials.srv;

        auto resource = materials.resource.Get();
        scontext.pc.device->CreateShaderResourceView(resource, &materialsdesc,
                                                     srvhandle.cpuhandle);
        inputset.materialhandles[0] = srvhandle.gpuhandle;
        srvhandle.Offset(scontext.pc);

        materialsdesc.Buffer.FirstElement = scontext.materialcache.GetMaterialCount();
        scontext.pc.device->CreateShaderResourceView(resource, &materialsdesc,
                                                     srvhandle.cpuhandle);
        inputset.materialhandles[1] = srvhandle.gpuhandle;
        srvhandle.Offset(scontext.pc);
    }

    inputset.verttable = srvhandle;
    srvhandle.Offset(scontext.pc, (unsigned int) kFrameRenderBufferCount *
                                  layout.GetVertexShaderDescriptorCount());
    inputset.fragtable = srvhandle;
    srvhandle.Offset(scontext.pc, (unsigned int) kFrameRenderBufferCount *
                                  layout.GetPixelShaderDescriptorCount());
    inputset.multitable = srvhandle;
}

void PlatformRenderer::BindBindlessInputSet(SessionContext& scontext, PipelineType pipelinetype) {
}

void PlatformRenderer::BindIndexBuffer(SessionContext& scontext, BufferResource& indexbuffer,
                                       uint64_t offset) {
    D3D12_INDEX_BUFFER_VIEW view(indexbuffer.ibv);
    view.BufferLocation = indexbuffer.resource->GetGPUVirtualAddress() + offset;
    scontext.pc.commandlist->IASetIndexBuffer((const D3D12_INDEX_BUFFER_VIEW *)&indexbuffer.ibv);
}

void PlatformRenderer::BindInputSet(SessionContext& scontext, PipelineType pipelinetype,
                                    InputSet& inputset) {
    scontext.pc.commandlist->SetGraphicsRootSignature(inputset.layout->rootsignature.Get());

    // constants get set per-draw in SetConstants()

    // perframe
    uint64 pfoffset = scontext.pc.bufferidx * pfconstantsbuf.GetInstanceSize();
    scontext.pc.commandlist->SetGraphicsRootConstantBufferView(1,
        pfconstantsbuf.resource->GetGPUVirtualAddress() + pfoffset);

    // 1d & 1d array textures
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(2, inputset.handle1d);
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(3, inputset.handle1darray);

    // 2d & 2d array textures
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(4, inputset.handle2d);
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(5, inputset.handle2darray);

    // 3d textures
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(6, inputset.handle3d);

    // cube & cube array textures
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(7, inputset.handlecube);
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(8, inputset.handlecubearray);

    // vertices
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(9, inputset.handlevertices);

    // drawables
    unsigned int drawablebufidx = scontext.drawablecache.GetFrontBufferIndex();
    CD3DX12_GPU_DESCRIPTOR_HANDLE h(inputset.drawablehandles[drawablebufidx]);
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(10, h);

    // materials
    unsigned int materialbufidx = scontext.materialcache.GetFrontBufferIndex();
    h = inputset.materialhandles[materialbufidx];
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(11, h);

    // sampler
    scontext.pc.commandlist->SetGraphicsRootDescriptorTable(12, inputset.samplerhandle.gpuhandle);

    inputset.Bind(scontext);

    BufferResource vertexbuffer(scontext.assetcache.GetGeoVertexBuffer());
    BufferResource indexbuffer(scontext.assetcache.GetGeoIndexBuffer());
    scontext.pc.commandlist->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    if (indexbuffer)
        scontext.pc.commandlist->IASetIndexBuffer((const D3D12_INDEX_BUFFER_VIEW *)
                                                  &indexbuffer.ibv);

    scontext.pc.commandlist->RSSetViewports(1, &viewport);
    scontext.pc.commandlist->RSSetScissorRects(1, &scissorrect);
}

void PlatformRenderer::Render(SessionContext& scontext) {
}

void PlatformRenderer::EndRender(SessionContext& scontext) {
}

void PlatformRenderer::Update(SessionContext& scontext, PerFrameConstants& pfconstants) {
    // FIXME: use new-style UpdateAndSwap() here!
    void* data = pfconstantsbuf.Map(scontext.pc.bufferidx);
    memcpy(data, &pfconstants, sizeof(pfconstants));
}

bool PlatformRenderer::ViewportResized(SessionContext& scontext,
                                       unsigned int width, unsigned int height) {
    if (scontext.pc.swapchaindim.x != width || scontext.pc.swapchaindim.y != height) {
        assert(width > 0 && height > 0);
        scontext.pc.swapchaindim.x = width;
        scontext.pc.swapchaindim.y = height;
        viewport = CD3DX12_VIEWPORT(0.0f, 0.0f,
                                    static_cast<float>(width),
                                    static_cast<float>(height));

        // Flush the GPU queue to make sure the swap chain's back buffers
        // are not being referenced by an in-flight command list.
        scontext.pc.WaitForIdle();

        scontext.pc.heaps.ResetRTVHeap();
        scontext.pc.heaps.ResetDSVHeap();

        for (int i = 0; i < kFrameRenderBufferCount; ++i) {
            // Any references to the back buffers must be released before the swap
            // chain can be resized.
            scontext.pc.swapchainimages[i].Reset();
            scontext.pc.framefencevalues[i] = scontext.pc.framefencevalues[scontext.pc.swapidx];
        }
        DXGI_SWAP_CHAIN_DESC swapchaindesc = {};
        DxThrow_(scontext.pc.swapchain->GetDesc(&swapchaindesc));
        DxThrow_(scontext.pc.swapchain->ResizeBuffers(kFrameRenderBufferCount, width, height,
                                        swapchaindesc.BufferDesc.Format, swapchaindesc.Flags));

        scontext.pc.swapidx = scontext.pc.swapchain->GetCurrentBackBufferIndex();

        UpdateSwapChainImages(scontext.pc, swaphandles, scontext.world);

        return true;
    }

    return false;
}

void PlatformRenderer::SingleDraw(SessionContext& scontext, const Pipeline& pipeline,
                                  const DrawData& dd) {
    SetConstants(scontext, pipeline, dd);

    if (dd.indices) {
        assert(dd.idxbufoffset != ~(uint32_t)(0));
        scontext.pc.commandlist->DrawIndexedInstanced(dd.indices, dd.instances,
                                                      dd.idxbufoffset, dd.vtxbufoffset, 0);
    }
    else {
        assert(dd.vtxbufoffset != ~(uint32_t)(0));
        scontext.pc.commandlist->DrawInstanced(dd.vertices, dd.instances, dd.vtxbufoffset, 0);
    }
}

void PlatformRenderer::BindPipeline(SessionContext& scontext, Pipeline& pipeline) {
    scontext.pc.commandlist->SetPipelineState(pipeline.obj.Get());
}

void PlatformRenderer::SetConstants(SessionContext& scontext, const Pipeline& pipeline,
                                    const DrawData& dd) {
    unsigned int constants[2] = {
        dd.vtxbufoffset,
        dd.instanceidx
    };

    scontext.pc.commandlist->SetGraphicsRoot32BitConstants(0, 2, constants, 0);
}

void PlatformRenderer::EndRenderPass(SessionContext& scontext, Framebuffer& framebuffer) {
    framebuffer.PostTransitions(scontext.pc);

    if (scontext.EnableOverlay() && framebuffer.layout->presenting)
        EndImGuiFrame(scontext);
}

void PlatformRenderer::BeginImGuiFrame(SessionContext& scontext) {
    ImGui_ImplDX12_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();
}

void PlatformRenderer::EndImGuiFrame(SessionContext& scontext) {
    scontext.pc.commandlist->OMSetRenderTargets(1, &swaphandles[scontext.pc.swapidx].cpuhandle,
                                                FALSE, nullptr);
    ImGui::Render();
    ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), scontext.pc.commandlist.Get());
}

void PlatformRenderer::DeviceWaitIdle(SessionContext& scontext) {
    scontext.pc.WaitForIdle();
}

bool PlatformRenderer::ProcessEvent(SessionContext& scontext, WindowEvent& event) {
    if (scontext.EnableOverlay()) {
        ImGuiIO& io = ImGui::GetIO();
        return (io.WantCaptureMouse || io.WantCaptureKeyboard);
    }

    return false;
}

}