#pragma once

#include "drawdata.h"
#include "bclib/podtypes.h"
#include "bclib/stdvector.h"
#include "rhitypes.h"

namespace bc9 {

struct DrawData;

class RenderBins {
public:
    void Clear();

    void AddDrawData(DrawData& dd, RenderBinType bin);

    RenderBinType GetBin(MaterialGroup mtlgroup) const;

    stdvector<DrawData> opaques;

    stdvector<DrawData> transparents;

    stdvector<DrawData> environment;
};

}