#pragma once

#if winvk_
#include "platformcontext_vk.h"
#elif wind12_
#include "platformcontext_d12.h"
#endif
