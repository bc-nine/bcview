#include "session.h"

namespace bc9 {

Session::Session(const stdjson& bundlejson, uintn xrmode) :
assetcache(bundlejson["graph"]),
world(bundlejson["world"], assetcache),
surface(world.RayTracingEnabled(), world.GetSurfaceColorSpace(), world.GetVSync()),
materialcache(bundlejson["graph"], world, assetcache),
drawablecache(bundlejson["graph"]["drawables"], assetcache, materialcache),
nodecache(bundlejson["graph"]),
renderer(surface),
imdraw(renderer),
cameracontrols(bundlejson),
context(world,
        imdraw,
        assetcache,
        surface,
        headset,
        materialcache,
        drawablecache,
        nodecache,
        renderer,
        xrmode) {
}

void Session::Initialize(Window& window, OutputDevice device) {
    context.Initialize(window, device);
    cameracontrols.Initialize(context);
}

void Session::Shutdown() {
    cameracontrols.Shutdown(context);
    context.Shutdown();
}

}