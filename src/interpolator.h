#pragma once

#include "bclib/stdarray.h"

namespace bc9 {

class Interpolator {
public:
    Interpolator() {}

    // peaktime is the time in seconds (from when Start() is called) at
    // which maxabsvalue is returned, and is in the [0,1] range (values
    // greater than 1 are fine though). the absolute value is used
    // because the interpolator can be started in either positive or
    // negative mode.
    Interpolator(float peaktime);
    Interpolator(float peaktime, float maxabsvalue, float minabsvalue = 0.0f);

    void SetMinMax(float maxabsvalue, float minabsvalue = 0.0f);

    void Start(bool positive);
    void Stop();

    float Update(float dt);

    bool Active() const;

private:
    // there is a difference between active and status (the interpolator
    // can still be active for a short while after it's stopped).
    int active = 0;
    float t = 0.0f, value = 0.0f;

    int dir = 0;    // -1 = negative, 0 = none, 1 = positive
    int status = 0; // 0 = stopped, 1 = started

    stdarray<float, 2> tmin  = { 0.0f, 0.0f };
    stdarray<float, 2> tmax  = { 1.0f, 1.0f };
    stdarray<float, 2> scale = { 1.0f, 1.0f };
    stdarray<float, 2> bias  = { 0.0f, 0.0f };
};

}