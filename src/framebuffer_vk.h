#pragma once

#include "volk.h"
#include "glm/glm.hpp"
#include "bclib/stdvector.h"
#include "bclib/errorutils.h"
#include "bclib/stringid.h"

namespace bc9 {

class ColorTarget;
class SwapchainTarget;
class DepthStencilTarget;
class PlatformContext;
class FramebufferLayout;

struct VkRect;

class Framebuffer {
public:
    Framebuffer();

    // swapchain version
    void Initialize(PlatformContext& context, const FramebufferLayout* _layout,
                    SwapchainTarget& swaptarget, uintn w, uintn h, uintn imagecount);
    // general version
    void Initialize(PlatformContext& context, const FramebufferLayout* layout,
                    ColorTarget* colortargets, uintn numcolortargets,
                    DepthStencilTarget* depthtarget, uintn width, uintn height);
    void Shutdown(PlatformContext& context);

    void SetClearColor(const StringID& name, const glm::vec4& clear);
    void SetClearDepth(float depth);
    void SetClearStencil(uint32 stencil);
    void SetStencilReference(uint32 ref);

    void Bind(PlatformContext& context);

    void GetDimensions(PlatformContext& context, uintn& width, uintn& height);

    const FramebufferLayout* layout = nullptr;

    // platform-dependent values
    stdvector<VkFramebuffer> vkobjs;

    stdvector<VkClearValue> clearvalues;

    uint32 stencilref = 0;

private:
    uintn width = 0;
    uintn height = 0;
};

}