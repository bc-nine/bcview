#pragma once

#include "shaderbinding.h"
#include "shaderlocation.h"

#include "bclib/stdmap.h"
#include "bclib/stringid.h"
#include "bclib/fstring.h"
#include "bclib/stdvector.h"

namespace bc9 {

struct ShaderAsset {
    Result GetBinding(const StringID &name, ShaderBinding& binding) const {
        auto iter(bindingmap.find(name));
        if (iter == bindingmap.end())
            return Error("%s: couldn't find binding", name.str());

        binding = (*iter).second;
        return Success();
    }

    StringID name;
    stdvector<char> blob;
    ShaderAssetType type = eShaderAssetTypeCount;
    FString128 entrypoint;

    stdmap<StringID, ShaderBinding> bindingmap;
};

Result GetShaderAssetType(const StringID& strtype, ShaderAssetType& type);

}
