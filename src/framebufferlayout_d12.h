#pragma once

#include <d3d12.h>
#include "bclib/stringid.h"
#include "bclib/stdvector.h"
#include "bclib/errorutils.h"
#include "rhitypes.h"

namespace bc9 {

class ColorTarget;
class PlatformContext;

// it's important to note that FramebufferLayout should not be mutable once Initialize()'d,
// as a hash key is built then that could become invalidated if anything were to change.

class FramebufferLayout {
public:
    // platform independent interface

    // non-MSAA constructors
    FramebufferLayout();
    FramebufferLayout(unsigned int colortargets, unsigned int msaasamples,
                      FramebufferLayoutFlags flags = eFramebufferLayoutFlagsNone);

    void AddColorLayout(PlatformContext& context, ColorLayout& layout);
    void AddDepthStencilLayout(PlatformContext& context, DepthStencilLayout& layout);

    void Initialize(PlatformContext& context, const StringID& name);
    void Shutdown(PlatformContext& context);

    unsigned int GetDepthStencilLayoutCount() const;

    unsigned int GetColorLayoutCount() const;
    void GetColorIndex(const StringID& name, unsigned int& index) const;

    void GetColorLayout(unsigned int idx, ColorLayout& layout) const;
    void GetColorLayout(const StringID& name, ColorLayout& layout) const;
    void GetDepthStencilLayout(DepthStencilLayout& layout) const;

    bool Multisampled() const;
    unsigned int GetSampleCount() const;

    // platform specific interface
    void GetColorDescription(unsigned int idx, D3D12_RENDER_TARGET_VIEW_DESC& desc) const;
    void GetDepthDescription(D3D12_DEPTH_STENCIL_VIEW_DESC& desc) const;

    uint64_t GetHash() const;

    unsigned int msaasamples = 1;

    unsigned int colorcount = 0;
    unsigned int depthcount = 0;

    ImageLayout colorresolvelayout = eImageLayoutUndefined;
    ImageLayout dsresolvelayout = eImageLayoutUndefined;

    bool presenting = false;
    bool resolving = false;

    stdvector<ColorLayout> colorlayouts;
    stdvector<D3D12_RENDER_TARGET_VIEW_DESC> colordescs;

    DepthStencilLayout dslayout;
    D3D12_DEPTH_STENCIL_VIEW_DESC depthdesc;

    uint64_t hashkey = 0;

private:
    FramebufferLayoutFlags flags;
};

}