#pragma once

#if winvk_
#include "rendertarget_vk.h"
#elif wind12_
#include "rendertarget_d12.h"
#endif