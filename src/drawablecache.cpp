#include "drawablecache.h"
#include "materialcache.h"
#include "sessioncontext.h"
#include "renderbins.h"
#include "renderer.h"
#include "assetcache.h"

using namespace glm;
using namespace std;

namespace bc9 {

const Drawable* DrawableCache::GetDrawable(const DrawableKey& key) const {
    Assert_(key.index < drawables.size(), "key index %u too large", key.index);

    return &drawables[key.index];
}

void DrawableCache::TraverseDrawable(SessionContext& scontext, RenderBins& renderbins,
                                     DrawableKey dkey, uint32 modelnodeidx) {
    Drawable& drawable = drawables[dkey.index];
    const MaterialType& mtype = materialcache.GetMaterialType(MaterialKey(drawable.materialidx));

    DrawData dd{};
    dd.drawableidx = dkey.index;
    dd.modelnodeidx = modelnodeidx;
    dd.pipelinebase.vertexkey = mtype.vertexkey;
    dd.pipelinebase.pixelkey = mtype.pixelkey;
    dd.pipelinebase.topology = ePrimitiveTriangleList; // FIXME (get from drawable)

    scontext.assetcache.GetGeoDrawData(drawable.geokey, dd.geodrawdata);

    RenderBinType bin = renderbins.GetBin(mtype.group);
    renderbins.AddDrawData(dd, bin);
}

DrawableCache::DrawableCache(const stdjson& drawablejson,
                             const AssetCache& assetcache,
                             const MaterialCache& materialcache) :
materialcache(materialcache) {
    bool found = false;

    size_t drawablecount = drawablejson.size();
    drawables.resize(drawablecount);
    drawabledata.resize(drawablecount);

    uint32 didx = 0;
    for (stdjson::const_iterator it = drawablejson.begin(); it != drawablejson.end();++it,++didx) {
        auto& djson(*it);
        Drawable& drawable(drawables[didx]);
        Throw_(OptString(djson, "name", drawable.name, found));

        uint32 geokey;
        Throw_(ReqUInt(djson, "geo", geokey));
        drawable.geokey = geokey;

        Throw_(OptInt(djson, "normal-sense", drawable.normalsense, found));
        Throw_(OptUInt(djson, "material", drawable.materialidx, found));
    }
}

void DrawableCache::UpdateData(SessionContext& scontext, Drawable& drawable, DrawableData& data) {
    data.materialidx = drawable.materialidx;
    data.normalsense = drawable.normalsense;

    GeoData geodata;
    scontext.assetcache.GetGeoData(drawable.geokey, geodata);
    data.indexoffset = geodata.drawdata.indexoffset;
    data.pntoffset = geodata.pntoffset;
    data.coloroffset = geodata.coloroffset;
    data.srgbcoloroffset = geodata.srgbcoloroffset;
    data.tex1coordoffset = geodata.tex1coordoffset;
    data.tangentoffset = geodata.tangentoffset;
}

uintn DrawableCache::Update(SessionContext& scontext) {
    uintn descriptoridx = drawablebuf.DescriptorIndex();

    if (updatedrawables) {
        updatedrawables = false;

        uint32 drawablecount = uint32_(drawables.size());
        for (uint32 i = 0; i < drawablecount; ++i)
            UpdateData(scontext, drawables[i], drawabledata[i]);

        descriptoridx = drawablebuf.UpdateAndSwap(drawabledata.data());
    }

    return descriptoridx;
}

void DrawableCache::Initialize(SessionContext& scontext) {
    CreateInstancedBufferParams params(scontext.pc, eBufferResourceStructuredInput,
                                       drawables.size(), sizeof(DrawableData), "drawablecache");
    Throw_(CreateBuffer(scontext.pc, params, drawablebuf));

    // for ray tracing, we need everything to be initialized properly
    updatedrawables = true;
    Update(scontext);
}

void DrawableCache::Shutdown(SessionContext& scontext) {
    DestroyBuffer(scontext.pc, drawablebuf);
}

}