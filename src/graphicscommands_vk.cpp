#include "graphicscommands_vk.h"
#include "platformcontext_vk.h"
#include "bclib/miscutils.h"
#include "framebuffer_vk.h"
namespace bc9 {

namespace Graphics {

void PushConstantBuffers(PlatformContext& context, const CBufferSet& cbufferset) {
    Shader::PushConstantBuffers(context, cbufferset, ePipelineGraphics);
}

void BindPipeline(PlatformContext& context, Pipeline& pipeline) {
    Shader::BindPipeline(context, pipeline);
}

void Draw(PlatformContext& context, const DrawParams& params) {
    vkCmdDraw(context.current.cmdbuffer.vkobj, params.vertexcount, params.instancecount,
              params.firstvertex, params.firstinstance);
}

void Draw(PlatformContext& context, const DrawIndexedParams& params) {
    Assert_(params.firstindex != kInvalidUInt32);
    vkCmdDrawIndexed(context.current.cmdbuffer.vkobj, params.indexcount, params.instancecount,
                     params.firstindex, params.vertexoffset, params.firstinstance);
}

void Draw(PlatformContext& context, const DrawData& dd) {
    uint32 pcs[2] = { dd.modelnodeidx, dd.drawableidx };
    vkCmdPushConstants(context.current.cmdbuffer.vkobj,
                       context.pipelinelayout, VK_SHADER_STAGE_ALL, 0,
                       sizeof(uint32)*ArraySize_(pcs), &pcs);

    if (dd.geodrawdata.indexcount) {
        Graphics::DrawIndexedParams params {
            .indexcount = dd.geodrawdata.indexcount,
            .firstindex = dd.geodrawdata.indexoffset
        };
        Draw(context, params);
    }
    else {
        Graphics::DrawParams params {
            .instancecount = dd.instances,
            .vertexcount = dd.geodrawdata.vertexcount
        };
        Draw(context, params);
    }
}

void Render(PlatformContext& context, Pipeline& pipeline,
            const CBufferSet& cbufferset, const DrawParams& params) {
    Shader::PushConstantBuffers(context, cbufferset, ePipelineGraphics);

    Shader::BindPipeline(context, pipeline);

    Draw(context, params);
}

void Render(PlatformContext& context, Pipeline& pipeline,
            const CBufferSet& cbufferset, const DrawIndexedParams& params) {
    Shader::PushConstantBuffers(context, cbufferset, ePipelineGraphics);

    Shader::BindPipeline(context, pipeline);

    Draw(context, params);
}

void Render(PlatformContext& context, Pipeline& pipeline, const DrawData& dd) {
    if (dd.cbufferset)
        PushConstantBuffers(context, *dd.cbufferset);

    Shader::BindPipeline(context, pipeline);

    Draw(context, dd);
}

void Graphics::BeginRenderPass(PlatformContext& context, Framebuffer& framebuffer) {
    VkRect2D rect{};
    rect.offset = { 0, 0 };
    framebuffer.GetDimensions(context, rect.extent.width, rect.extent.height);

    VkRenderPassBeginInfo renderpassinfo = {};
    renderpassinfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    renderpassinfo.renderPass = framebuffer.layout->layoutdata.renderpass;
    if (framebuffer.layout->presenting)
        renderpassinfo.framebuffer = framebuffer.vkobjs[context.GetSwapIndex()];
    else
        renderpassinfo.framebuffer = framebuffer.vkobjs[0];
    renderpassinfo.renderArea.offset = {0, 0};
    renderpassinfo.renderArea.extent = rect.extent;

    renderpassinfo.clearValueCount = framebuffer.layout->hasclearattachment ?
                                        static_cast<uint32>(framebuffer.clearvalues.size()) : 0;
    renderpassinfo.pClearValues = framebuffer.clearvalues.data();

    vkCmdBeginRenderPass(context.current.cmdbuffer.vkobj, &renderpassinfo,
                         VK_SUBPASS_CONTENTS_INLINE);

    framebuffer.Bind(context);

    context.BindBindlessInputSet(ePipelineGraphics);
    Graphics::PushConstantBuffers(context, { .engine0 = &context.pfconstantsbuf });
}

void Graphics::EndRenderPass(PlatformContext& context, Framebuffer& framebuffer) {
    vkCmdEndRenderPass(context.current.cmdbuffer.vkobj);
}

}

}