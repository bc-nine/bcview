#pragma once

#include "bclib/errorutils.h"

namespace bc9 {

enum SamplerMode {
    eSamplerRepeat,
    eSamplerMirroredRepeat,
    eSamplerClampToEdge,
    eSamplerClampToBorder,
    eSamplerMirrorClampToEdge,
    eSamplerModeCount
};

enum SamplerFilter {
    eSamplerNearest,
    eSamplerLinear,
    eSamplerFilterCount
};

enum SamplerMipmapFilter {
    eSamplerMipmapNearest,
    eSamplerMipmapLinear,
    eSamplerMipmapFilterCount
};

struct SamplerAsset {
    StringID name;
    SamplerMode addressu = eSamplerClampToEdge;
    SamplerMode addressv = eSamplerClampToEdge;
    SamplerMode addressw = eSamplerClampToEdge;
    SamplerFilter minfilter = eSamplerLinear;
    SamplerFilter magfilter = eSamplerLinear;
    SamplerMipmapFilter mipmapfilter = eSamplerMipmapLinear;
};

Result GetSamplerMode(const StringID& strtype, SamplerMode& mode);
Result GetSamplerFilter(const StringID& strfilter, SamplerFilter& filter);
Result GetSamplerMipmapFilter(const StringID& strfilter, SamplerMipmapFilter& filter);

}