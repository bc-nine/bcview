#pragma once

#include "cachekeys.h"
#include "drawdata.h"
#include "sharedstructs.h"
#include "drawable.h"

namespace bc9 {

class AssetCache;
class RenderBins;
class MaterialCache;
class SessionContext;

class DrawableCache {
public:
    DrawableCache(const stdjson& drawablejson,
                  const AssetCache& assetcache,
                  const MaterialCache& materialcache);

    void Initialize(SessionContext &scontext);
    void Shutdown(SessionContext &scontext);

    uintn Update(SessionContext& scontext);

    const Drawable* GetDrawable(const DrawableKey& key) const;

    void TraverseDrawable(SessionContext& scontext, RenderBins& renderbins,
                          DrawableKey dkey, uint32 modelnodeidx);

private:
    void UpdateData(SessionContext& scontext, Drawable& drawable, DrawableData& data);

    const MaterialCache& materialcache;

    stdvector<Drawable> drawables;

    stdvector<DrawableData> drawabledata;

    BufferResource drawablebuf;
    uint32_t updatedrawables = false;
};

}
