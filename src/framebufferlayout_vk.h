#pragma once

#include "volk.h"
#include "rhitypes.h"
#include "bclib/stdvector.h"
#include "bclib/stdset.h"
#include "bclib/stdmap.h"
#include "bclib/stringid.h"
#include "bclib/errorutils.h"

namespace bc9 {

class ColorTarget;
class DepthStencilTarget;
class PlatformContext;

enum SubpassDependency {
    eSubpassDependencyNone,
    eSubpassDependencyToColorAttach,
    eSubpassDependencyFromColorAttach,
    eSubpassDependencyColorAttachToShaderRead
};

struct FramebufferLayoutData {
    FramebufferLayoutData() {}
    FramebufferLayoutData(uintn msaasamples) : msaasamples(msaasamples) {}

    uintn msaasamples = 1;
    VkRenderPass renderpass = nullptr;
};

class FramebufferLayout {
public:
    // platform independent interface
    FramebufferLayout();
    FramebufferLayout(uintn colortargets, uintn msaasamples,
                      FramebufferLayoutFlags flags = eFramebufferLayoutFlagsNone,
                      MultiviewMode multiview = eMultiviewDisabled);

    void AddColorLayout(PlatformContext& context, ColorLayout& layout);
    void AddDepthStencilLayout(PlatformContext& context, DepthStencilLayout& layout);

    void Initialize(PlatformContext& context, const StringID& name);
    void Shutdown(PlatformContext& context);

    uintn GetColorTargetCount() const;
    void GetColorIndex(const StringID& name, uintn& index) const;

    uintn GetMultiviewLayerCount() const;
    bool MultiviewEnabled() const;

    void GetColorLayout(uintn idx, ColorLayout& color) const;
    void GetColorLayout(const StringID& name, ColorLayout& color) const;
    void GetDepthStencilLayout(DepthStencilLayout& depthstencil) const;

    bool UseDepthStencilTarget() const;

    uintn GetSampleCount() const;

    // platform specific interface
    stdvector<ColorLayout> colorlayouts;

    DepthStencilLayout dslayout;

    stdset<SubpassDependency> dependencies;

    stdvector<VkAttachmentDescription> attachments;

    stdvector<VkAttachmentReference> colorattachmentrefs;
    stdvector<VkAttachmentReference> colorresolveattachmentrefs;
    VkAttachmentReference depthattachmentref;

    ImageLayout dsresolvelayout = eImageLayoutUndefined;

    uintn colortargets = 0;

    stdmap<StringID, uintn> attachmentindices, attachmentindicesmsaa;

    bool presenting = false;

    bool hasclearattachment = false;

    uint32 GetColorAttachmentIndex(const StringID& name, bool msaaindex) const;
    // color clear would be set on either msaa target (if msaa is enabled)
    // *or* the regular non-msaa target, but calling code shouldn't have
    // to know or care about this, hence the separate API call here.
    uint32 GetColorClearAttachmentIndex(const StringID& name) const;

    uint32 GetDepthIndex() const;
    uint32 GetDepthClearIndex() const;

    FramebufferLayoutData layoutdata;

private:
    void AddColorDependency(VkImageLayout layout);

    FramebufferLayoutFlags flags;

    uintn layers = 1;

    bool multiviewenabled = false;
};

}