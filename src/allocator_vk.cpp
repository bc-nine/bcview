#include "allocator_vk.h"

namespace bc9 {

VkAllocationCallbacks *VkAllocators() {
    static VkAllocationCallbacks callbacks;
    callbacks.pUserData = nullptr;
    callbacks.pfnAllocation = vkalloc;
    callbacks.pfnReallocation = vkrealloc;
    callbacks.pfnFree = vkfree;
    callbacks.pfnInternalAllocation = vkinternalalloc;
    callbacks.pfnInternalFree = vkinternalfree;

    return &callbacks;
};

}