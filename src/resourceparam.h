#pragma once

namespace bc9 {

class CreateColorTargetParams {
public:
    CreateColorTargetParams() {}
    // arraylayers of 0 = scalar 2D image, 1 = image array of size 1, etc.
    CreateColorTargetParams(const StringID& name, uintn width, uintn height,
                            uintn arraylayers = 0) :
    name(name),
    width(width),
    height(height),
    layers(arraylayers) {
    }

    StringID name;
    uintn width = 0;
    uintn height = 0;
    uintn layers = 0;
};

class CreateDepthStencilTargetParams {
public:
    CreateDepthStencilTargetParams() {}
    // arraylayers of 0 = scalar 2D image, 1 = image array of size 1, etc.
    CreateDepthStencilTargetParams(uintn width, uintn height, uintn arraylayers = 0) :
    width(width),
    height(height),
    layers(arraylayers) {
    }

    uintn width = 0;
    uintn height = 0;
    uintn layers = 0;
};

}

#if winvk_
#include "resourceparam_vk.h"
#elif wind12_
#include "resourceparam_d12.h"
#endif