#pragma once

#if winvk_
#include "commandbuffer_vk.h"
#elif wind12_
#include "commandbuffer_d12.h"
#endif