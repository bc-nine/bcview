#pragma once

#include "bclib/stringid.h"

namespace bc9 {

struct TextureAsset {
    TextureAsset() {}
    TextureAsset(uint32 imageidx, uint32 sampleridx) { encodedidx = (imageidx << 7) | sampleridx; }
    uint32 SamplerIndex() const { return encodedidx & 0x7F; }
    uint32 ImageIndex() const { return encodedidx >> 7; }
    uint32 EncodedIndex() const { return encodedidx; }

    StringID name = "null";

private:
    uint32 encodedidx = kInvalidUInt32;
};

}
