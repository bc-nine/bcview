#pragma once

#include <wrl.h>
#include <d3d12.h>
#include "heaps_d12.h"
#include <cstdint>
#include <dxgi1_6.h>
#include "constants.h"
#include "glm/glm.hpp"
#include "inputlayout.h"
#include "commandbuffer_d12.h"
#include "bclib/hashutils.h"

namespace bc9 {

class World;
class Window;
struct ImageResource;

class PlatformContext {
public:
    PlatformContext(const World& world);

    CommandBuffer BeginSingleTimeCommands();
    void EndSingleTimeCommands(CommandBuffer &cmdlist);

    void Initialize(Window& window, const World& world);
    void Shutdown();

    void PostInitialize(SessionContext& scontext);

    void WaitForIdle();

    // platform dependent
    void BeginResourceBarriers();
    void AddSwapchainResourceBarrier(unsigned int swapidx, D3D12_RESOURCE_STATES state);
    void AddResourceBarrier(ImageResource& image, D3D12_RESOURCE_STATES state);
    void EndResourceBarriers(SessionContext& scontext);
    void EndResourceBarriers(CommandBuffer& cmdlist);

    stdvector<D3D12_RESOURCE_BARRIER> resourcebarriers;

    HWND mswin;

    Hasher hasher;

    const bool vsync;
    bool tearingsupported = false;

    bool usewarp = false;

    Microsoft::WRL::ComPtr<ID3D12Device2> device;

    Microsoft::WRL::ComPtr<ID3D12CommandQueue> graphicsqueue;

    UINT srvdescsize;
    UINT rtvdescsize;
    UINT dsvdescsize;
    UINT samplerdescsize;

    Microsoft::WRL::ComPtr<ID3D12CommandAllocator> tmpcmdallocator;

    Microsoft::WRL::ComPtr<ID3D12Fence> fence;
    uint64_t fencevalue = 0;
    uint64_t framefencevalues[kFrameRenderBufferCount] = {};
    HANDLE fenceevent;

    unsigned int msaasamples = 1;

    unsigned int bufferidx = 0;

    // swapchaindim is the source-of-truth for the current render target dimensions
    UINT swapidx;
    glm::uvec2 swapchaindim;
    static constexpr uint32_t swapchainimagecount = kFrameRenderBufferCount;
    // probably will need to revisit/clean up how these are determined at some point...
    ResourceFormat swapchainformat = eResourceFormatR8G8B8A8_UNORM
    DXGI_FORMAT d12swapchainformat = DXGI_FORMAT_R8G8B8A8_UNORM;
    DXGI_FORMAT depthformat = DXGI_FORMAT_D32_FLOAT;

    // these are managed by the platform renderer, but live here for visibility
    InputLayout bindlesslayouts[ePipelineTypeCount];

    CommandBuffer commandlist;

    Microsoft::WRL::ComPtr<IDXGISwapChain4> swapchain;

    Microsoft::WRL::ComPtr<ID3D12Resource> swapchainimages[PlatformContext::swapchainimagecount];

    D3D12_RESOURCE_STATES swapchainstates[PlatformContext::swapchainimagecount];

    D12Heaps heaps;

    uint64 currentcpuframe = 1;
};

}
