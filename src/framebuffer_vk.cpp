#include "framebuffer_vk.h"
#include "platformcontext.h"
#include "framebufferlayout.h"
#include "util_vk.h"
#include "resourceutil.h"
#include "allocator_vk.h"

namespace bc9 {

Framebuffer::Framebuffer() {
}

void Framebuffer::Initialize(PlatformContext& context, const FramebufferLayout* _layout,
                             SwapchainTarget& swaptarget, uintn w, uintn h, uintn imagecount) {
    layout = _layout;

    width = w;
    height = h;

    vkobjs.resize(imagecount);

    clearvalues.resize(1);
    Assert_(layout->attachments.size() == 1);

    for (uintn idx = 0; idx < imagecount; ++idx){
        VkImageView imageview { swaptarget.targets[idx].view.vkobj };
        VkFramebufferCreateInfo createinfo = { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO };
        createinfo.renderPass = layout->layoutdata.renderpass;
        createinfo.attachmentCount = 1;
        createinfo.pAttachments = &imageview;
        createinfo.width = width;
        createinfo.height = height;
        createinfo.layers = 1; // (see mar 11 2023)

        VkCheck_(vkCreateFramebuffer(context.device, &createinfo, VkAllocators(), &vkobjs[idx]));
    }
}

void Framebuffer::Initialize(PlatformContext& context, const FramebufferLayout* _layout,
                             ColorTarget* colortargets, uintn numcolortargets,
                             DepthStencilTarget* dstarget, uintn inwidth, uintn inheight) {
    layout = _layout;

    width = inwidth;
    height = inheight;

    if ((layout->colortargets != numcolortargets) ||
        (bool(dstarget) != layout->UseDepthStencilTarget())) {
        Throw_(Error("layout count vs target count mismatch"));
    }

    vkobjs.resize(1);

    size_t attachmentcount = layout->attachments.size();
    clearvalues.resize(attachmentcount);

    stdvector<VkImageView> imageviews(layout->attachments.size());

    for (uintn i = 0; i < numcolortargets; ++i) {
        ColorLayout colorlayout;
        layout->GetColorLayout(i, colorlayout);
        ColorTarget& color(colortargets[i]);

        if (colorlayout.LoadOpClear()) {
            SetClearColor(colorlayout.name, { 0.0, 0.0, 0.0, 1.0 });
        }

        uint32 colorattachmentidx = layout->GetColorAttachmentIndex(colorlayout.name, false);
        Assert_(colorattachmentidx != -1);
        if (layout->layoutdata.msaasamples == 1 || !colorlayout.NoResolve()) {
            Assert_(color.target.view.vkobj);
            imageviews[colorattachmentidx] = color.target.view.vkobj;
        }
        else {
            // multisampling enabled w/o a resolve buffer
        }

        uint32 colormsaaattachmentidx = layout->GetColorAttachmentIndex(colorlayout.name, true);
        if (colormsaaattachmentidx != -1) {
            Assert_(color.msaatarget.view.vkobj);
            imageviews[colormsaaattachmentidx] = color.msaatarget.view.vkobj;
        }
    }

    if (dstarget) {
        DepthStencilTarget& dst(*dstarget);
        uint32 dstattachmentidx = layout->GetDepthIndex();
        Assert_(dstattachmentidx != -1);
        imageviews[dstattachmentidx] = dst.target.view.vkobj;

        DepthStencilLayout dslayout;
        layout->GetDepthStencilLayout(dslayout);
        if (dslayout.DepthLoadOpClear()) {
            SetClearDepth(0.0f); // recall we do reverse-z
        }
        if (dslayout.StencilLoadOpClear()) {
            SetClearStencil(0);
        }
    }

    VkFramebufferCreateInfo createinfo = { VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO };
    createinfo.renderPass = layout->layoutdata.renderpass;
    createinfo.attachmentCount = (uint32_t)imageviews.size();
    createinfo.pAttachments = imageviews.data();
    createinfo.width = width;
    createinfo.height = height;
    createinfo.layers = 1; // (see mar 11 2023)

    VkCheck_(vkCreateFramebuffer(context.device, &createinfo, VkAllocators(), &vkobjs[0]));
}

void Framebuffer::GetDimensions(PlatformContext& context, uintn& outwidth, uintn& outheight) {
    outwidth = width;
    outheight = height;
}

void Framebuffer::Bind(PlatformContext& context) {
    // note that we don't need to use the negative viewport hack anymore,
    // as we derive our projection matrix from first priniciples assuming
    // Vulkan's y downward coordinate system. see (mar 1 2023)
    VkViewport viewports[kMaxMultiviewLayers] = {};
    VkRect2D rects[kMaxMultiviewLayers] = {};

    uintn multiviewlayercount = layout->GetMultiviewLayerCount();
    for (uintn cti = 0; cti < multiviewlayercount; ++ cti) {
        VkViewport& vp = viewports[cti];
        vp.x = 0;
        vp.y = 0;
        vp.width = float(width);
        vp.height = float(height);
        vp.minDepth = 0.0f;
        vp.maxDepth = 1.0f;

        VkRect2D& rect = rects[cti];
        rect.offset = { 0, 0 };
        rect.extent.width = width;
        rect.extent.height = height;
    }

    vkCmdSetScissor(context.current.cmdbuffer.vkobj, 0, multiviewlayercount, rects);
    vkCmdSetViewport(context.current.cmdbuffer.vkobj, 0, multiviewlayercount, viewports);

    vkCmdSetStencilReference(context.current.cmdbuffer.vkobj,
                             VK_STENCIL_FACE_FRONT_AND_BACK, stencilref);
}

void Framebuffer::SetStencilReference(uint32 ref) {
    stencilref = ref;
}

void Framebuffer::SetClearColor(const StringID& name, const glm::vec4& clear) {
    uintn idx = layout->GetColorClearAttachmentIndex(name);
    Assert_(idx < clearvalues.size());
    clearvalues[idx].color = { clear.x, clear.y, clear.z, clear.w };
}

void Framebuffer::SetClearDepth(float depth) {
    uintn idx = layout->GetDepthClearIndex();
    Assert_(idx < clearvalues.size());
    clearvalues[idx].depthStencil.depth = depth;
}

void Framebuffer::SetClearStencil(uintn stencil) {
    uintn idx = layout->GetDepthClearIndex();
    Assert_(idx < clearvalues.size());
    clearvalues[idx].depthStencil.stencil = (uint32_t) stencil;
}

void Framebuffer::Shutdown(PlatformContext& context) {
    for (auto& object : vkobjs)
        vkDestroyFramebuffer(context.device, object, VkAllocators());
}

}