#pragma once

#include "bclib/stdvector.h"
#include "bclib/mathutils.h"
#include "drawdata.h"
#include "overlay.h"

#include "renderstate.h"
#include "resourceutil.h"
#include "renderpassfactory.h"

#include "renderbins.h"

namespace bc9 {

class Surface;
class DisplayPass;
class WindowEvent;
class SessionContext;
struct ModelInstance;
struct RenderBinDrawData;
struct PerFrameConstants;

enum CaptureState {
    eCaptureStateNotCapturing,
    eCaptureStateCaptureImage,
    eCaptureStateImageCaptured
};

class Renderer {
public:
    Renderer (Surface& surface);

    bool Initialize(SessionContext& scontext);

    void ViewResized(SessionContext& scontext, uintn width, uintn height);

    void Shutdown(SessionContext& scontext);

    bool ProcessEvent(SessionContext& scontext, WindowEvent& event);

    void CaptureImage(SessionContext& scontext,
                      std::function<void(RenderState& state, uchar* data, uintn stride,
                                         uintn width, uintn height, uintn format)> cb);

    void RenderBin(SessionContext& scontext, RenderBinType bin,
                   const GraphicsPipelineData& psodata);
    void RenderFrame(SessionContext& scontext, float dt);
    void RenderImage(SessionContext& scontext, ImageResource& image);

    void DisplayOverlay(SessionContext& scontext);

    void CaptureRenderState(SessionContext& scontext, RenderState& state);

    void RenderImGui(SessionContext& scontext);

    void AddDrawData(DrawData& dd, RenderBinType bin);

    void ToggleRayTraceAccumulation();
    void SetRayTraceAccumulation(bool accum);
    bool RayTraceAccumulationMode() const;

private:

    void TraverseModelInstance(const ModelInstance& modelinstance,
                               stdvector<DrawData>& drawdatas,
                               const glm::mat4x3& xform);

    void Update(SessionContext& scontext, float dt);
    void UpdateConstants(SessionContext& scontext);
    void UpdateConstants(SessionContext& scontext, PerFrameConstants& pf, uintn viewcount);

    void Render(SessionContext& scontext);
    void RenderDisplay(SessionContext& scontext);

    Surface& surface;

    Overlay overlay;

    RenderPassFactory passfactory;

    stdvector<RenderPass*> renderpasses;

    DisplayPass* displaypass = nullptr;

    bool initialized = false;

    RenderState capturecbstate;
    std::function<void(RenderState&, uchar*, uintn, uintn, uintn, uintn)> capturecb;

    stdvector<DrawData> drawdatas;

    CaptureState capturestate = eCaptureStateNotCapturing;

    RenderBins renderbins;

    bool raytraceaccum = false;
};

}