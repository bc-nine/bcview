#pragma once

#include "property.h"
#include "bclib/stdjson.h"

namespace bc9 {

Result CreateProperty(const stdjson &propjson, PropertyType ptype, Property& property);

}