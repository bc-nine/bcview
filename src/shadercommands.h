#pragma once

#include "rhitypes.h"

namespace bc9 {

struct Pipeline;
struct CommandBuffer;
class PlatformContext;

namespace Shader {

void PushConstantBuffers(PlatformContext& context,
                         const CBufferSet& cbufferset,
                         PipelineType ptype);
void PushConstantBuffers(PlatformContext& context, CommandBuffer& cmdbuffer,
                         const CBufferSet& cbufferset, PipelineType ptype);

void BindPipeline(PlatformContext& context, Pipeline& pipeline);
void BindPipeline(PlatformContext& context, CommandBuffer& cmdbuffer, Pipeline& pipeline);

}

}