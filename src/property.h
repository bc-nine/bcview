#pragma once

#include "bclib/stdvariant.h"
#include "bclib/stringid.h"
#include "bclib/stdmap.h"
#include "bclib/stdvector.h"
#include "bclib/stringid.h"
#include "glm/glm.hpp"
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/quaternion.hpp"

namespace bc9 {

enum PropertyType {
    ePropertyTypeStringHash,
    ePropertyTypeUInt,
    ePropertyTypeUInt2,
    ePropertyTypeUInt3,
    ePropertyTypeUInt4,
    ePropertyTypeInt,
    ePropertyTypeInt2,
    ePropertyTypeInt3,
    ePropertyTypeInt4,
    ePropertyTypeFloat,
    ePropertyTypeFloat2,
    ePropertyTypeFloat3,
    ePropertyTypeFloat4,
    ePropertyTypeFloat2x2,
    ePropertyTypeFloat3x3,
    ePropertyTypeFloat4x3,
    ePropertyTypeFloat4x4,
    ePropertyTypeQuat,
    ePropertyTypeCount
};

struct Property {
    Property () : type(ePropertyTypeCount) {}
    Property (const Property& p) : type(p.type), value(p.value) {}
    Property& operator= (const Property& rhs) {
        if (this != &rhs) {
            type = rhs.type;
            value = rhs.value;
        }
        return *this;
    }

    Property(uint64 value) :
    type(ePropertyTypeStringHash),
    value(value) {}

    Property(glm::uint value) :
    type(ePropertyTypeUInt),
    value(value) {}

    Property(glm::uvec2 value) :
    type(ePropertyTypeUInt2),
    value(value) {}

    Property(glm::uvec4 value) :
    type(ePropertyTypeUInt4),
    value(value) {}

    Property(int value) :
    type(ePropertyTypeInt),
    value(value) {}

    Property(glm::ivec2 value) :
    type(ePropertyTypeInt2),
    value(value) {}

    Property(glm::ivec4 value) :
    type(ePropertyTypeInt4),
    value(value) {}

    Property(const float value) :
    type(ePropertyTypeFloat),
    value(value) {}

    Property(const glm::vec2& value) :
    type(ePropertyTypeFloat2),
    value(value) {}

    Property(const glm::vec3& value) :
    type(ePropertyTypeFloat3),
    value(value) {}

    Property(const glm::vec4& value) :
    type(ePropertyTypeFloat4),
    value(value) {}

    Property(const glm::mat4x3& value) :
    type(ePropertyTypeFloat4x3),
    value(value) {}

    Property(const glm::quat& value) :
    type(ePropertyTypeQuat),
    value(value) {}

    PropertyType Type() const { return type; }

    uint64 StringHash() const {
        assert(type == ePropertyTypeStringHash);
        assert(std__holds_alternative<uint64>(value));
        return std__get<uint64>(value);
    }

    glm::uint Int() const {
        assert(type == ePropertyTypeInt);
        assert(std__holds_alternative<int>(value));
        return std__get<int>(value);
    }

    glm::uint UInt() const {
        assert(type == ePropertyTypeUInt);
        assert(std__holds_alternative<glm::uint>(value));
        return std__get<glm::uint>(value);
    }

    float Float() const {
        assert(type == ePropertyTypeFloat);
        assert(std__holds_alternative<float>(value));
        return std__get<float>(value);
    }

    glm::vec3 Float3() const {
        assert(type == ePropertyTypeFloat3);
        assert(std__holds_alternative<glm::vec3>(value));
        return std__get<glm::vec3>(value);
    }

    glm::vec4 Float4() const {
        assert(type == ePropertyTypeFloat4);
        assert(std__holds_alternative<glm::vec4>(value));
        return std__get<glm::vec4>(value);
    }

    glm::mat4x3 Float4x3() const {
        assert(type == ePropertyTypeFloat4x3);
        assert(std__holds_alternative<glm::mat4x3>(value));
        return std__get<glm::mat4x3>(value);
    }

    glm::quat Quat() const {
        assert(type == ePropertyTypeQuat);
        assert(std__holds_alternative<glm::quat>(value));
        return std__get<glm::quat>(value);
    }

private:
    PropertyType type = ePropertyTypeCount;

    stdvariant<uint64,
               glm::quat, glm::uint, glm::uvec2, glm::uvec3, glm::uvec4,
               int, glm::ivec2, glm::ivec3, glm::ivec4,
               float, glm::vec2, glm::vec3, glm::vec4,
               glm::mat2, glm::mat3, glm::mat4, glm::mat4x3> value;
};

extern void PrintProperty(const StringID& name, const Property& property);

}