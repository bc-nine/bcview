#pragma once

#include <cstdint>
#include <assert.h>
#include "bclib/errorutils.h"

namespace bc9 {

enum KeyboardKey {
    e1Key,
    e2Key,
    e3Key,
    e4Key,
    e5Key,
    e6Key,
    e7Key,
    e8Key,
    e9Key,
    e0Key,
    eWKey,
    eAKey,
    eSKey,
    eDKey,
    eEKey,
    eCKey,
    eRKey,
    eMKey,
    eOKey,
    eFKey,
    eTKey,
    eUpKey,
    eDownKey,
    eLeftKey,
    eRightKey,
    eSpaceKey
};

// note that material types are generally a subset of render bin types, but
// the correlation is, let's say, "weak". it's fine for these to not line
// up 1-1 here, as there's a function that maps a material type to a render
// bin. (see feb 19 2023)
enum MaterialGroup {
    eMaterialOpaque,
    eMaterialTransparent,
    eMaterialGroupCount
};

enum RenderBinType {
    eRenderBinOpaque,
    eRenderBinTransparent,
    eRenderBinEnvironment,
    eRenderBinTypeCount
};

enum CameraType {
    eCameraTypePerspective,
    eCameraTypeOrthographic,
    eCameraTypeCount
};

enum ColorSpaceType {
  eColorSpaceSRGB,
  eColorSpaceLinear,
  eColorSpaceTypeCount
};

// the integer values and suffixes come directly from Vulkan
enum ResourceFormat {
    eResourceFormatUNDEFINED = 0,

    eResourceFormatR8_UNORM = 9,
    eResourceFormatR8_UINT = 13,
    eResourceFormatR8_SRGB = 15,

    eResourceFormatR8G8B8_SRGB = 29,

    eResourceFormatR8G8B8A8_UNORM = 37,
    eResourceFormatR8G8B8A8_SRGB = 43,

    eResourceFormatB8G8R8A8_UNORM = 44,
    eResourceFormatB8G8R8A8_SRGB = 50,

    eResourceFormatR32_UINT = 98,
    eResourceFormatR32_SFLOAT = 100,

    eResourceFormatR32G32B32A32_SFLOAT = 109,

    eResourceFormatD16_UNORM = 124,
    eResourceFormatX8_D24_UNORM_PACK32 = 125,
    eResourceFormatD32_SFLOAT = 126,
    eResourceFormatS8_UINT = 127,
    eResourceFormatD16_UNORM_S8_UINT = 128,
    eResourceFormatD24_UNORM_S8_UINT = 129,
    eResourceFormatD32_SFLOAT_S8_UINT = 130,

    eResourceFormatInvalid = 0xffffffff
};

enum ImageDim {
    eImage1D,
    eImage2D,
    eImage3D
};

enum ImageLayout {
    eImageLayoutUndefined,
    eImageLayoutPresent,
    eImageLayoutColorOutput,
    eImageLayoutShaderReadOnly,
    eImageLayoutShaderReadWrite,
    eImageLayoutDepthStencilOutput,
    eImageLayoutDepthStencilReadOnly,
    eImageLayoutTransferSource,
    eImageLayoutTransferDestination,
    eImageLayoutCount
};

enum ShaderStage : uint64_t {
    eShaderStageVertex     = 0x1,
    eShaderStagePixel      = 0x2,
    eShaderStageCompute    = 0x4,
    eShaderStageRay        = 0x8,
    eShaderStageMiss       = 0x10,
    eShaderStageClosestHit = 0x20,
    eShaderStageAnyHit     = 0x40,
    eShaderStageInvalid    = -2,
    eShaderStageAllBitsOn  = -1
};

enum ShaderAssetType {
    eShaderAssetVertex,
    eShaderAssetPixel,
    eShaderAssetCompute,
    eShaderAssetRay,
    eShaderAssetMiss,
    eShaderAssetClosestHit,
    eShaderAssetTypeCount
};

typedef uint64_t ColorLayoutFlags;

enum ColorLayoutFlagBits : uint64_t {
    eColorLayoutFlagsNone      = 0x0,
    eColorLayoutShaderReadable = 0x1,
    eColorLayoutPresenting     = 0x2,
    eColorLayoutNoResolve      = 0x4,
    eColorLayoutLoadOpClear    = 0x8,
    eColorLayoutLoadOpPreserve = 0x10,
    eColorLayoutStoreOpStore   = 0x20
};

enum CullMode : uint64_t {
  eCullNone,
  eCullFront,
  eCullBack,
  eCullModeCount
};

enum CompareOp : uint64_t {
  eCompareNever,
  eCompareLess,
  eCompareEqual,
  eCompareLessOrEqual,
  eCompareGreater,
  eCompareNotEqual,
  eCompareGreaterOrEqual,
  eCompareAlways,
  eCompareOpCount
};

enum StencilOp : uint64_t {
  eStencilKeep,
  eStencilZero,
  eStencilReplace,
  eStencilIncrementAndClamp,
  eStencilDecrementAndClamp,
  eStencilInvert,
  eStencilIncrementAndWrap,
  eStencilDecrementAndWrap,
  eStencilOpCount
};

typedef uint64_t DepthStencilLayoutFlags;

enum DepthStencilLayoutFlagBits : uint64_t {
    eDepthStencilLayoutFlagsNone             = 0x0,
    eDepthStencilLayoutDepthShaderReadable   = 0x1,
    eDepthStencilLayoutStencilShaderReadable = 0x2,
    eDepthStencilLayoutDepthDisabled         = 0x4,
    eDepthStencilLayoutStencilEnabled        = 0x8,
    eDepthStencilLayoutDepthStencilSeparate  = 0x10,
    eDepthStencilLayoutDepthLoadOpClear      = 0x20,
    eDepthStencilLayoutDepthLoadOpPreserve   = 0x40,
    eDepthStencilLayoutDepthStoreOpStore     = 0x80,
    eDepthStencilLayoutStencilLoadOpClear    = 0x100,
    eDepthStencilLayoutStencilLoadOpPreserve = 0x200,
    eDepthStencilLayoutStencilStoreOpStore   = 0x400,
    // add these even though we don't yet support depth/stencil resolve (eventually we will?)
    eDepthStencilLayoutDepthNoResolve        = 0x800,
    eDepthStencilLayoutStencilNoResolve      = 0x1000
};

typedef uint64_t FramebufferLayoutFlags;

enum FramebufferLayoutFlagBits : uint64_t {
    eFramebufferLayoutFlagsNone             = 0x0,
    eFramebufferLayoutUseDepthStencilTarget = 0x1
};

typedef uint64_t ShaderStages;

enum PipelineType {
    ePipelineGraphics,
    ePipelineCompute,
    ePipelineRayTracing,
    ePipelineTypeCount
};

enum PolygonMode {
    ePolygonModeFill,
    ePolygonModeLine,
    ePolygonModePoint,
    ePolygonModeCount
};

enum PrimitiveTopology {
    ePrimitiveTriangleList,
    ePrimitiveLineList,
    ePrimitivePointList,
    ePrimitiveTopologyCount
};

enum BufferResourceType {
  // general
  eBufferResourceGeneral,                 // general purpose buffer not used in shaders,
                                          // useful for e.g. copying to/from images
  // index
  eBufferResourceIndex,                   // no descriptor type mapping for this one
  // constant
  eBufferResourceConstant,                // (CBV) ConstantBuffer
  eBufferResourceTexture,                 // (CBV) TextureBuffer
  // input
  eBufferResourceFormattedInput,          // (SRV) Buffer
  eBufferResourceStructuredInput,         // (SRV) StructuredBuffer
  eBufferResourceByteAddressInput,        // (SRV) ByteAddressBuffer
  // output
  eBufferResourceFormattedOutput,         // (UAV) RWBuffer
  eBufferResourceStructuredOutput,        // (UAV) RWStructuredBuffer
  eBufferResourceByteAddressOutput,       // (UAV) RWByteAddressBuffer
  eBufferResourceStructuredAppendOutput,  // (UAV) AppendStructuredBuffer
  eBufferResourceStructuredConsumeOutput, // (UAV) ConsumeStructuredBuffer
  eBufferResourceTypeCount
};

enum BufferCPUAccessMode {
  // technically you can do cpu reads *and* writes in some cases, but you'd
  // likely be reading from write-combined memory, which you wouldn't want
  // to be doing, so we'll ignore that possibility.
  eBufferCPUAccessRead,
  eBufferCPUAccessWrite,
  eBufferCPUAccessNone,
  eBufferCPUAccessModeCount
};

enum ImageResourceType {
    eImageResourceInput,    // (SRV) Texture*
    eImageResourceOutput,   // (UAV) RWTexture*
    eImageResourceTypeCount
};

enum ImageCPUAccessMode {
  eImageCPUAccessRead,
  eImageCPUAccessWrite,
  eImageCPUAccessNone,
  eImageCPUAccessModeCount
};

enum MultiviewMode {
  eMultiviewDisabled = 0,
  eMultiview1Layer,
  eMultiview2Layer,
  eMultiview3Layer,
  eMultiview4Layer
};

struct BufferResource;

struct CBufferSet {
  BufferResource* custom0 = nullptr;
  BufferResource* custom1 = nullptr;
  BufferResource* engine0 = nullptr;
  BufferResource* engine1 = nullptr;
};

enum OutputDevice {
    eOutputSystem,
    eOutputQuest2,
    eOutputDeviceCount
};

Result GetShaderStage(ShaderAssetType type, ShaderStage& stage);
Result GetResourceFormatProperties(ResourceFormat format, uintn& channels, uintn& texelsize);

}