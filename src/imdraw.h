#pragma once

#include "drawdata.h"
#include "pipelinestate.h"
#include "pipelinestate.h"
#include "bufferresource.h"

#define IMDRAW_DEBUG_RAYTRACE 0

namespace bc9 {

class Renderer;
class SessionContext;

struct IMConstants {
    float opacity = 1.0f;
};

class IMDraw {
public:
    IMDraw(Renderer& renderer);
    void Initialize(SessionContext &scontext);
    void Shutdown(SessionContext &scontext);

    void PreRender(SessionContext& scontext);
    void Render(SessionContext& scontext);

    void DrawTriangle(SessionContext& scontext);
    void DrawLines(SessionContext& scontext);

    BufferResource& GPUVertexBuffer();

private:
    uint32_t GetVertexBufferOffset();

    uint32_t GetIndexBufferOffset();

    Renderer& renderer;
    ShaderKey vertexkey;
    ShaderKey pixelkey;

    GraphicsPipelineState pipelinestate;

    BufferResource constantsbuf;
    BufferResource indexbuf;
    BufferResource vertexbuf;

    BufferResource gpuvertexbuf;

    IMConstants constants;

    stdvector<DrawData> drawdatas;

    uint32_t backidx = 0;
};

}