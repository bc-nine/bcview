#pragma once

#include "volk.h"
#include "rhitypes.h"
#include "bclib/podtypes.h"
#include "glm/glm.hpp"

namespace bc9 {

class PlatformContext;

struct CreateVkBufferParams {
    // NOTE: this struct should *only* contain data actually needed
    //       for construction of a buffer resource.
    VkBufferCreateInfo createinfo { .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
                                    .sharingMode = VK_SHARING_MODE_EXCLUSIVE };
    VkMemoryPropertyFlags properties = 0;

    // texel buffers need a view
    VkBufferViewCreateInfo viewcreateinfo { .sType = VK_STRUCTURE_TYPE_BUFFER_VIEW_CREATE_INFO };

    uintn instances = 0;
    uint64 instancesize = 0;

    void* initdata = nullptr;
    uint64 initdatasize = 0;
    const char* name = nullptr;
};

class CreateBufferParams {
public:
    CreateBufferParams(PlatformContext& context, BufferResourceType type,
                       uint64 size, const char* name);
    CreateBufferParams(PlatformContext& context, BufferResourceType type,
                       uint64 elements, uint64 elementsize, const char* name);

    // names can be changed, e.g. when building multiple buffers that only differ by name.
    void SetName(const char* name);

    // only texel buffers need a format
    Result SetFormat(ResourceFormat format);

    void SetAccessMode(BufferCPUAccessMode accessmode);

    void SetVisibilityVertex(bool visible);
    void SetVisibilityPixel(bool visible);

    void SetUsageCopySource(bool cs = true);
    void SetUsageCopyDestination(bool cd = true);

    void SetInitialData(void* data, uint64 size = 0);

    uint64 GetElements() const;
    uint64 GetStride() const;

    // platform-dependent interface
    Result Get(PlatformContext& context, CreateVkBufferParams& params) const;

protected:
    CreateBufferParams(BufferResourceType type, const char* name);

    CreateVkBufferParams vkparams;

    uint64 elements = 0;
    uint64 stride = kInvalidUInt64;
    BufferResourceType type = eBufferResourceTypeCount;
    BufferCPUAccessMode accessmode = eBufferCPUAccessNone;
};

class CreateConstantBufferParams : public CreateBufferParams {
public:
    CreateConstantBufferParams(PlatformContext& context, uint64 size, const char* name);
};

// mapped and instanced buffers are both cpu-accessible, but instanced
// version supports multiple instances for e.g. double/triple-buffering.
class CreateMappedBufferParams : public CreateBufferParams {
public:
    CreateMappedBufferParams(PlatformContext& context, BufferResourceType type,
                             uint64 size, const char* name);
    CreateMappedBufferParams(PlatformContext& context, BufferResourceType type,
                             uint64 elements, uint64 elementsize, const char* name);
};

class CreateInstancedBufferParams : public CreateBufferParams {
public:
    CreateInstancedBufferParams(PlatformContext& context, BufferResourceType type,
                                uint64 size, const char* name);
    CreateInstancedBufferParams(PlatformContext& context, BufferResourceType type,
                                uint64 elements, uint64 elementsize, const char* name);
    // if you ever need a more general CreateInstancedBufferParams, e.g. you
    // need an instances value other than kFrameRenderBufferCount, add here.
};

struct CreateVkImageParams {
    VkImageCreateInfo createinfo { .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
                                   .sharingMode = VK_SHARING_MODE_EXCLUSIVE };
    VkMemoryPropertyFlags properties = VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT;

    VkImageViewCreateInfo viewcreateinfo { .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO };

    bool createview = false;
    VkImageLayout initiallayout = VK_IMAGE_LAYOUT_UNDEFINED;

    const char* name = nullptr;
};

class CreateImageParams {
public:
    CreateImageParams();
    // simple 2d interface
    CreateImageParams(ResourceFormat format, uintn width, uintn height, uintn samples,
                      const char* name);
    // general interface (arraylayers of 0 = scalar 2D image, 1 = image array of size 1)
    // let's say 1D textures would have height = 0 (hasn't been implemented yet though)
    CreateImageParams(ResourceFormat format, uintn width, uintn height, uintn depth,
                      uintn miplevels, uintn arraylayers, uintn samples, bool cube,
                      const char* name);

    // names can be changed, e.g. when building multiple images that only differ by name.
    void SetName(const char* name);

    void SetAccessMode(ImageCPUAccessMode accessmode);

    void SetInitialLayout(ImageLayout layout);

    void SetUsageCopySource();
    void SetUsageCopyDestination();
    void SetUsageSampled();
    void SetUsageStorage();
    void SetUsageColorTarget();
    void SetUsageDepthTarget();
    void SetUsageStencilTarget();

    void SetQueueUsage(PlatformContext& context, bool graphics, bool compute);

    // platform-dependent interface
    Result Get(PlatformContext& context, CreateVkImageParams& params) const;

    VkImageAspectFlags aspect = VK_IMAGE_ASPECT_COLOR_BIT;

private:
    CreateVkImageParams vkparams;

    bool arrayimage = false;

    ImageCPUAccessMode accessmode = eImageCPUAccessNone;

    void Populate(ResourceFormat format, uintn samples, const char* name);
};

}