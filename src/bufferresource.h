#pragma once

#if winvk_
#include "bufferresource_vk.h"
#elif wind12_
#include "bufferresource_d12.h"
#endif
