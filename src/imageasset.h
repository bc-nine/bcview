#pragma once

#include "bclib/stringid.h"
#include "bclib/fstring.h"
#include "bclib/errorutils.h"
#include "rhitypes.h"

namespace bc9 {

enum ImageAssetType {
    eImageAsset1D,
    eImageAsset1DArray,
    eImageAsset2D,
    eImageAsset2DArray,
    eImageAsset3D,
    eImageAssetCube,
    eImageAssetCubeArray,
    eImageAssetTypeCount
};

struct ImageAsset {
    // TODO: put these guys behind a function, since they should be immutable
    StringID name;
    ImageAssetType type = eImageAssetTypeCount;
    ColorSpaceType colorspace = eColorSpaceTypeCount;
    FString datapath;

    // these are filled in when data is loaded
    bool pmalpha = false;
    unsigned int width = 0;
    unsigned int height = 0;
    unsigned int depth = 0;    // only applicable to 3D textures, so value should be 1 for 1D/2D
    unsigned int arraysize = 0;
    unsigned int miplevels = 0;
    ResourceFormat format = eResourceFormatInvalid;
};

Result GetImageAssetType(const StringID& strtype, ImageAssetType& type);
Result GetImageAssetColorSpace(const StringID& strcolorspace, ColorSpaceType& colorspace);

}
