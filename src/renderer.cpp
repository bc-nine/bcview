#include "renderer.h"
#include "world.h"
#include "nodecache.h"
#include "materialcache.h"
#include "drawablecache.h"
#include "sharedstructs.h"
#include "window.h"
#include "imgui.h"
#include "bclib/timeutils.h"
#include "assetcache.h"
#include "graphicscommands.h"
#include "sessioncontext.h"
#include "xrheadset.h"
#include "renderpasses/displaypass.h"
#include "overlay.h"
#include "surface.h"

namespace bc9 {

Renderer::Renderer(Surface& surface) :
surface(surface) {
}

bool Renderer::Initialize(SessionContext& scontext) {
    renderpasses.push_back(passfactory.CreateRenderPass(scontext, eRenderPassPrePass));

    if (scontext.world.GetRendererMode() == eRendererRayTrace) {
        renderpasses.push_back(passfactory.CreateRenderPass(scontext, eRenderPassRayTrace));
    }
    else {
        renderpasses.push_back(passfactory.CreateRenderPass(scontext, eRenderPassMainHDR));
    }

    displaypass = new DisplayPass(scontext);
    renderpasses.push_back(displaypass);

    for (auto& renderpass : renderpasses)
        renderpass->Initialize(scontext);

    scontext.PostInitialize();

    for (auto& renderpass : renderpasses)
        renderpass->PostInitialize(scontext);

    initialized = true;

    return initialized;
}

void Renderer::ToggleRayTraceAccumulation() {
    SetRayTraceAccumulation(!raytraceaccum);
}

void Renderer::SetRayTraceAccumulation(bool accum) {
    raytraceaccum = accum;
}

bool Renderer::RayTraceAccumulationMode() const {
    return raytraceaccum;
}

void Renderer::DisplayOverlay(SessionContext& scontext) {
    bool menubar = ImGui::BeginMainMenuBar();

    if (menubar) {
        overlay.UpdateMenuBar(scontext);
        if (ImGui::BeginMenu("Pass")) {
            for (auto& renderpass : renderpasses)
                renderpass->OverlayUpdateMenuBar(scontext);
            ImGui::EndMenu();
        }
    }

    if (menubar)
       ImGui::EndMainMenuBar();

    overlay.Display(scontext);

    for (auto& renderpass : renderpasses)
        renderpass->OverlayUpdate(scontext);
}

bool Renderer::ProcessEvent(SessionContext& scontext, WindowEvent& event) {
    if (initialized) {
        return Overlay::ProcessWindowEvent(scontext, event);
    }

    return false;
}

void Renderer::Update(SessionContext& scontext, float dt) {
    renderbins.Clear();

    for (auto& renderpass : renderpasses)
        renderpass->Update(scontext, dt);
}

void Renderer::UpdateConstants(SessionContext& scontext) {
    uintn cameranodeidx = scontext.world.ActiveCameraNode();
    const Camera* camera = scontext.nodecache.NodeCamera(cameranodeidx);

    PerFrameConstants pf = {};

    pf.view[0] = glm::mat4(1.0f);
    pf.invview[0] = glm::mat4(1.0f);

    uintn width, height;
    scontext.pc.surface.GetViewSize(width, height);
    float aspect = float(width)/height;
    pf.proj[0] = camera->ProjectionTransform(aspect);
    pf.invproj[0] = inverse(pf.proj[0]);

    UpdateConstants(scontext, pf, 1);
}

void Renderer::UpdateConstants(SessionContext& scontext, PerFrameConstants& pf, uintn viewcount) {
    uintn cameranodeidx = scontext.world.ActiveCameraNode();
    const Camera* camera = scontext.nodecache.NodeCamera(cameranodeidx);

    pf.focusdistance = camera->GetFocusDistance();
    pf.aperturesize = camera->GetApertureSize();

    glm::mat4 viewxform = scontext.nodecache.ViewTransform(cameranodeidx);
    glm::mat4 invviewxform = scontext.nodecache.ViewInverseTransform(cameranodeidx);

    for (uintn i = 0; i < viewcount; ++i) {
        pf.view[i] = pf.view[i] * viewxform;
        pf.invview[i] = invviewxform * pf.invview[i]; // FIXME: this may not be right for VR!
    }

    pf.drawablesidx = scontext.drawablecache.Update(scontext);
    pf.verticesidx = scontext.assetcache.GetGeoVertexBuffer().DescriptorIndex();
    pf.vertexcolorsidx = scontext.assetcache.GetGeoVertexColorBuffer().DescriptorIndex();
    pf.srgbvertexcolorsidx =scontext.assetcache.GetGeoSRGBVertexColorBuffer().DescriptorIndex();
    pf.tex1coordsidx = scontext.assetcache.GetGeoTex1CoordBuffer().DescriptorIndex();
    pf.tangentsidx = scontext.assetcache.GetGeoTangentBuffer().DescriptorIndex();
    pf.modelnodesidx = scontext.nodecache.Update(scontext, renderbins);
    pf.materialsidx = scontext.materialcache.Update(scontext);
    pf.indicesidx = scontext.assetcache.GetGeoIndexBuffer().DescriptorIndex();

    for (auto& renderpass : renderpasses)
        renderpass->UpdateConstants(scontext, pf);

    scontext.pc.pfconstantsbuf.UpdateAndSwap(&pf);
}

void Renderer::AddDrawData(DrawData& dd, RenderBinType bin) {
    renderbins.AddDrawData(dd, bin);
}

void Renderer::RenderBin(SessionContext& scontext, RenderBinType bin,
                         const GraphicsPipelineData& psodata) {
    PipelineCache& pcache = scontext.pipelinecache;
    if (bin == eRenderBinOpaque) {
        for (auto& dd : renderbins.opaques) {
            if (dd.cbufferset)
                Graphics::PushConstantBuffers(scontext.pc, *dd.cbufferset);
            Pipeline pipeline = pcache.GetGraphicsPipeline(scontext.pc, dd.pipelinebase, psodata);
            // at some point of course, we'd sort by pipeline, and have
            // multiple draws issued for a bound pipeline, but for now...
            Graphics::BindPipeline(scontext.pc, pipeline);
            Graphics::Draw(scontext.pc, dd);
        }
    }
    else if (bin == eRenderBinTransparent) {
    }
    else if (bin == eRenderBinEnvironment) {
        for (auto& dd : renderbins.environment) {
            Pipeline pipeline = pcache.GetGraphicsPipeline(scontext.pc, dd.pipelinebase, psodata);
            Graphics::Render(scontext.pc, pipeline, dd);
        }
    }
}

void Renderer::RenderFrame(SessionContext& scontext, float dt) {
    if (!initialized)
        return;

    Update(scontext, dt);

    scontext.pc.BeginFrame();

    bool xrbf = (scontext.pc.xrmode > 0) && scontext.pc.headset.SessionRunning();
    if (xrbf) {
        PerFrameConstants pf = {};
        scontext.pc.headset.BeginFrame(scontext.pc, pf.view, pf.invview,
                                       pf.proj, pf.invproj, kMaxMultiviewLayers);
        UpdateConstants(scontext, pf, scontext.pc.headset.GetViewCount());
    }
    else {
        UpdateConstants(scontext);
    }

    Render(scontext);

    scontext.pc.EndFrame();

    if (xrbf)
        scontext.pc.headset.EndFrame(scontext.pc);

    if (capturestate == eCaptureStateImageCaptured) {
        scontext.pc.DeviceWaitIdle();
        surface.ProcessCapture(scontext.pc, capturecb, capturecbstate);
        capturestate = eCaptureStateNotCapturing;
    }

    scontext.pc.SwapBuffers();
}

void Renderer::RenderImage(SessionContext& scontext, ImageResource& image) {
    if (!initialized)
        return;

    scontext.pc.DeviceWaitIdle();
    displaypass->OverrideImage(scontext, image.view);

    scontext.pc.BeginFrame();

    RenderDisplay(scontext);

    scontext.pc.EndFrame();

    displaypass->ClearImageOverride(scontext);

    scontext.pc.SwapBuffers();
}

void Renderer::ViewResized(SessionContext& scontext, uintn width, uintn height) {
    if (surface.ResizeView(scontext.pc, width, height))
        for (auto& renderpass : renderpasses)
            renderpass->ViewResized(scontext, width, height);
}

void Renderer::CaptureImage(SessionContext& scontext,
                            std::function<void(RenderState& state, uchar* data, uintn stride,
                                               uintn width, uintn height, uintn format)> cb) {
    capturecb = cb;
    capturestate = eCaptureStateCaptureImage;
    CaptureRenderState(scontext, capturecbstate);
}

void Renderer::Render(SessionContext& scontext) {
    scontext.pc.current.cmdbuffer.BeginGraphics(scontext);

    for (auto& renderpass : renderpasses)
        renderpass->PreRender(scontext);

    for (auto& renderpass : renderpasses)
        renderpass->Render(scontext);

    for (auto& renderpass : renderpasses)
        renderpass->PostRender(scontext);

    if (capturestate == eCaptureStateCaptureImage) {
        surface.CaptureImage(scontext.pc);
        capturestate = eCaptureStateImageCaptured;
    }

    scontext.pc.current.cmdbuffer.EndGraphics(scontext);

    ++scontext.pc.currentcpuframe;
}

void Renderer::RenderImGui(SessionContext& scontext) {
    if (scontext.EnableOverlay()) {
        Overlay::BeginImGuiFrame(scontext);
        DisplayOverlay(scontext);
        Overlay::EndImGuiFrame(scontext);
    }
}

void Renderer::RenderDisplay(SessionContext& scontext) {
    scontext.pc.current.cmdbuffer.BeginGraphics(scontext);

    displaypass->PreRender(scontext);
    displaypass->Render(scontext);
    displaypass->PostRender(scontext);

    scontext.pc.current.cmdbuffer.EndGraphics(scontext);
}

void Renderer::Shutdown(SessionContext& scontext) {
    if (!initialized)
        return;

    scontext.pc.DeviceWaitIdle();

    for (auto& renderpass : renderpasses) {
        renderpass->Shutdown(scontext);
        delete renderpass;
    }

    // this needs to be last
    initialized = false;
}

void Renderer::CaptureRenderState(SessionContext& scontext, RenderState& state) {
    state.frame = uint32_(scontext.pc.currentcpuframe);
    for (auto& renderpass : renderpasses)
        renderpass->CaptureRenderState(state);
}

}