#pragma once

#include "cachekeys.h"
#include "framebufferlayout.h"

namespace bc9 {

class PlatformContext;

// GraphicsPipelineState contains *just* the programmable elements of a pso.
class GraphicsPipelineState {
public:
    uint64 GetHash(const PlatformContext& context) const;

    bool blendingenabled = false;

    PolygonMode polygonmode = ePolygonModeFill;

    uintn msaasamples = 1;

    CullMode cullmode = eCullNone;

    CompareOp depthcompareop = eCompareGreaterOrEqual;

    bool depthtest = true;
    bool depthwrite = true;

    bool stenciltest = false;

    uint32 comparemask = 0xff;
    uint32 writemask = 0xff;

    StencilOp frontfailop = eStencilKeep;
    StencilOp frontpassop = eStencilKeep;
    StencilOp frontdepthfailop = eStencilKeep;
    CompareOp frontcompareop = eCompareNever;

    StencilOp backfailop = eStencilKeep;
    StencilOp backpassop = eStencilKeep;
    StencilOp backdepthfailop = eStencilKeep;
    CompareOp backcompareop = eCompareNever;
};

// GraphicsPipelineBase and GraphicsPipelineData contain all
// the state necessary to create a graphics pipeline state object.
class GraphicsPipelineBase {
public:
    GraphicsPipelineBase() {}
    GraphicsPipelineBase(PlatformContext& context, ShaderKey vertexkey, ShaderKey pixelkey);

    uint64 GetHash() const;

    ShaderKey vertexkey;
    ShaderKey pixelkey;
    PrimitiveTopology topology = ePrimitiveTriangleList;

private:
    uint64 hash = 0;
};

struct GraphicsPipelineData {
    GraphicsPipelineData() {}
    GraphicsPipelineData(const FramebufferLayoutData& fblayoutdata) :
    fblayoutdata(fblayoutdata) {}

    uint64 GetPipelineHash(const PlatformContext& context, uint64 basehash) const;

    GraphicsPipelineState state;
    FramebufferLayoutData fblayoutdata;
};

class ComputePipelineBase {
public:
    ComputePipelineBase(PlatformContext& context, ShaderKey computekey);

    uint64 GetHash() const;

    ShaderKey computekey;

private:
    uint64 hash = 0;
};

}