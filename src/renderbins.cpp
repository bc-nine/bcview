#include "renderbins.h"

namespace bc9 {

void RenderBins::Clear() {
    opaques.clear();
    transparents.clear();
    environment.clear();
}

RenderBinType RenderBins::GetBin(MaterialGroup mtlgroup) const {
    if (mtlgroup == eMaterialOpaque)
        return eRenderBinOpaque;
    else if (mtlgroup == eMaterialTransparent)
        return eRenderBinTransparent;

    return eRenderBinTypeCount;
}

void RenderBins::AddDrawData(DrawData& dd, RenderBinType bin) {
    if (bin == eRenderBinOpaque)
        opaques.push_back(dd);
    else if (bin == eRenderBinTransparent)
        transparents.push_back(dd);
    else if (bin == eRenderBinEnvironment)
        environment.push_back(dd);
}

}