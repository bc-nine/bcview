#pragma once

#include "volk.h"
#include "rhitypes.h"

namespace bc9 {

struct Pipeline {
    VkPipeline vkobj = nullptr;
    PipelineType type = ePipelineTypeCount;
};

}