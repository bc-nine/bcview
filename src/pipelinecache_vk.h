#pragma once

#include "pipelinestate.h"
#include "pipelines.h"
#include "bclib/fstring.h"
#include "bclib/stdutility.h"
#include "bclib/stdmap.h"

#include "volk.h"
#include "platform_vk.h"

namespace bc9 {

class PlatformContext;

class PipelineCache {
public:
    PipelineCache() {}

    void Initialize(const PlatformContext &context);
    void Shutdown(const PlatformContext &context);

    Pipeline GetGraphicsPipeline(const PlatformContext &context,
                                 const GraphicsPipelineBase& psobase,
                                 const GraphicsPipelineData& psodata);

    Pipeline GetComputePipeline(const PlatformContext &context, const ComputePipelineBase& base);

private:
    ShaderKey vshaderkey;
    ShaderKey pshaderkey;
    VkPipelineShaderStageCreateInfo shaderstages[2];
    VkPipelineVertexInputStateCreateInfo vertexinputinfo = {};
    VkPipelineInputAssemblyStateCreateInfo inputassembly = {};
    VkPipelineViewportStateCreateInfo viewportstate = {};
    VkPipelineRasterizationStateCreateInfo rasterizer = {};
    VkPipelineMultisampleStateCreateInfo multisampling = {};
    VkPipelineDepthStencilStateCreateInfo depthstencil = {};
    VkPipelineColorBlendAttachmentState colorblendattachment = {};
    VkPipelineColorBlendStateCreateInfo colorblending = {};
    VkDynamicState dynstates[3] = { VK_DYNAMIC_STATE_VIEWPORT,
                                    VK_DYNAMIC_STATE_SCISSOR,
                                    VK_DYNAMIC_STATE_STENCIL_REFERENCE };
    VkPipelineDynamicStateCreateInfo dynamicinfo = {};
    VkPipelineLayoutCreateInfo pipelinelayoutinfo = {};
    VkPipelineLayout pipelinelayout;
    VkRenderPass renderpass;

    stdumap<uint64, VkPipeline> computepipelines;
    stdumap<uint64, VkPipeline> graphicspipelines;

    VkPipelineCache vkpcache = VK_NULL_HANDLE;

    FString vkpcachepath;

    VkPipeline CreateGraphicsPipeline(const PlatformContext &context,
                                      const GraphicsPipelineBase &psobase,
                                      const GraphicsPipelineData &psodata);
    VkPipeline CreateComputePipeline(const PlatformContext &context,
                                     const ComputePipelineBase &base);

    float CreateGraphicsPipeline(const PlatformContext& context, VkPipeline& pipeline);
};

}
