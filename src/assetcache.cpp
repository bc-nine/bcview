#include "assetcache.h"
#include "resourceutil.h"
#include "bclib/stdallocators.h"
#include "bclib/fileutils.h"
#include "bclib/errorutils.h"
#include "sessioncontext.h"
#include "world.h"

#if wind12_
#include "DirectXTex.h"
#endif

namespace bc9 {

Result AssetCache::LoadAssets(const stdjson& gjson) {
    bool found = false;
    StringID value;

    const stdjson& jsongeos = gjson["geos"];
    uintn gi = 0;
    geoassets.resize(jsongeos.size());
    for (stdjson::const_iterator it = jsongeos.begin(); it != jsongeos.end(); ++it, ++gi) {
        const stdjson& geojson = *it;
        GeoAsset& geo = geoassets[gi];

        FString geopath;
        Throw_(ReqString(geojson, "data", geopath));
        Throw_(ReqString(geojson, "name", geo.name));

        Return_(DeserializeGeo(geo, geopath.str()));
    }

    uintn ti = 0;
    const stdjson& jsontextures = gjson["textures"];
    textureassets.resize(jsontextures.size());
    for (stdjson::const_iterator it = jsontextures.begin(); it != jsontextures.end(); ++it, ++ti) {
        const stdjson& texturejson = *it;
        TextureAsset& texture = textureassets[ti];
        uint32 imageidx, sampleridx;
        Throw_(ReqUInt(texturejson, "image", imageidx));
        Throw_(ReqUInt(texturejson, "sampler", sampleridx));
        Assert_(sampleridx < kMaxSamplers);
        texture = TextureAsset(imageidx, sampleridx);

        Throw_(OptString(texturejson, "name", texture.name, found));
    }

    uintn ii = 0;
    const stdjson& jsonimages = gjson["images"];
    imageassets.resize(jsonimages.size());
    for (stdjson::const_iterator it = jsonimages.begin(); it != jsonimages.end(); ++it, ++ii) {
        const stdjson& imagejson = *it;
        ImageAsset& image = imageassets[ii];
        image = {};

        StringID strcolorspace, strtype, name;
        Throw_(ReqString(imagejson, "data", image.datapath));
        Throw_(ReqString(imagejson, "name", image.name));
        Throw_(ReqString(imagejson, "type", strtype));
        Throw_(GetImageAssetType(strtype, image.type));
        Throw_(ReqString(imagejson, "color-space", strcolorspace));
        Throw_(GetImageAssetColorSpace(strcolorspace, image.colorspace));
    }

    uintn si = 0;
    const stdjson& jsonsamplers = gjson["samplers"];
    samplerassets.resize(jsonsamplers.size());
    for (stdjson::const_iterator it = jsonsamplers.begin(); it != jsonsamplers.end(); ++it, ++si) {
        const stdjson& samplerjson = *it;
        SamplerAsset& sampler = samplerassets[si];

        Throw_(OptString(samplerjson, "address-u", value, found));
        if (found)
            Throw_(GetSamplerMode(value, sampler.addressu));
        Throw_(OptString(samplerjson, "address-v", value, found));
        if (found)
            Throw_(GetSamplerMode(value, sampler.addressv));
        Throw_(OptString(samplerjson, "min-filter", value, found));
        if (found)
            Throw_(GetSamplerFilter(value, sampler.minfilter));
        Throw_(OptString(samplerjson, "mag-filter", value, found));
        if (found)
            Throw_(GetSamplerFilter(value, sampler.magfilter));
        Throw_(OptString(samplerjson, "mipmap-filter", value, found));
        if (found)
            Throw_(GetSamplerMipmapFilter(value, sampler.mipmapfilter));
        Throw_(OptString(samplerjson, "name", value, found));
        if (found)
            sampler.name = value;
    }

    const stdjson &jsonshaders = gjson["shaders"];
    si = 0;
    shaderassets.resize(jsonshaders.size());
    for (stdjson::const_iterator it = jsonshaders.begin(); it != jsonshaders.end(); ++it, ++si) {
        const stdjson& shaderjson = *it;
        ShaderAsset& shader = shaderassets[si];

        StringID strtype;
        Throw_(ReqString(shaderjson, "name", shader.name));
        Throw_(ReqString(shaderjson, "entry", shader.entrypoint));
        Throw_(ReqString(shaderjson, "type", strtype));
        Throw_(GetShaderAssetType(strtype, shader.type));

        FString spv;
        Throw_(ReqString(shaderjson, "data", spv));
        Throw_(ReadFile(spv.str(), shader.blob));

        stdjson::const_iterator lit, bit;
        for (bit = shaderjson["bindings"].begin(); bit != shaderjson["bindings"].end(); ++bit) {
            ShaderStage stage;
            Throw_(GetShaderStage(shader.type, stage));
            ShaderBinding sbinding(*bit, stage);
            StringID bname;
            Throw_(ReqString(*bit, "name", bname));
            shader.bindingmap[bname] = sbinding;
        }
    }

    return Success();
}

AssetCache::AssetCache(const stdjson& graphjson) {
    Throw_(LoadAssets(graphjson));
}

void AssetCache::Initialize(SessionContext &scontext) {
    size_t numimageassets = imageassets.size();
    size_t numshaderassets = shaderassets.size();

    imageresources.resize(numimageassets);
    shaderresources.resize(numshaderassets);

    geodatas.resize(geoassets.size());
    Throw_(CreateVertexBuffers(scontext.pc, geoassets, geodatas,
                               geovertexresource,
                               geovcolorresource,
                               geosrgbvcolorresource,
                               geotex1coordresource,
                               geotangentresource));
    Throw_(CreateIndexBuffer(scontext.pc, geoassets, geodatas,
                             geoindexresource));

    geototalindexcount = 0;
    for (auto& geo : geoassets)
        geototalindexcount += geo.indexcount;

    if (scontext.world.RayTracingEnabled())
        BuildBottomAccelStructures(scontext);

    for (uintn i = 0; i < numimageassets; ++i)
        Throw_(CreateImage(scontext.pc, imageassets[i], imageresources[i]));

    for (uintn i = 0; i < numshaderassets; ++i)
        Throw_(CreateShader(scontext.pc, shaderassets[i], shaderresources[i]));
}

void AssetCache::BuildBottomAccelStructures(SessionContext &scontext) {
    CreateBLASParams params{};
    if (blasresources.size() != geoassets.size())
        blasresources.resize(geoassets.size());
    for (uintn i = 0; i < geoassets.size(); ++i)
        Throw_(CreateBLAS(scontext.pc, geoassets[i], params, blasresources[i]));
}

uintn AssetCache::GetGeoTotalIndexCount() const {
    return geototalindexcount;
}

void AssetCache::GetGeoData(const GeoKey& key, GeoData& geodata) const {
    Assert_(key.index < geodatas.size());

    geodata = geodatas[key.index];
}

void AssetCache::GetGeoDrawData(const GeoKey& key, GeoDrawData& geodrawdata) const {
    Assert_(key.index < geodatas.size());

    geodrawdata = geodatas[key.index].drawdata;
}

BufferResource AssetCache::GetGeoVertexBuffer() const {
    return geovertexresource;
}

BufferResource AssetCache::GetGeoIndexBuffer() const {
    return geoindexresource;
}

BufferResource AssetCache::GetGeoVertexColorBuffer() const {
    return geovcolorresource;
}

BufferResource AssetCache::GetGeoSRGBVertexColorBuffer() const {
    return geosrgbvcolorresource;
}

BufferResource AssetCache::GetGeoTex1CoordBuffer() const {
    return geotex1coordresource;
}

BufferResource AssetCache::GetGeoTangentBuffer() const {
    return geotangentresource;
}

uintn AssetCache::GetImageCount() const {
    return uintn_(imageassets.size());
}

uintn AssetCache::GetSamplerCount() const {
    return uintn_(samplerassets.size());
}

ShaderResource AssetCache::GetShader(const ShaderKey& key) const {
    Assert_(key.index < shaderresources.size());

    return shaderresources[key.index];
}

const ShaderAsset *AssetCache::GetShaderAsset(const ShaderKey& key) const {
    Assert_(key.index < shaderassets.size());

    return &shaderassets[key.index];
}

GeoKey AssetCache::FindGeo(const StringID& name) const {
    for (uintn i = 0; i < geoassets.size(); ++i)
        if (geoassets[i].name == name)
            return i;

    return GeoKey();
}

ImageKey AssetCache::FindImage(const StringID& name) const {
    for (uintn i = 0; i < imageassets.size(); ++i)
        if (imageassets[i].name == name)
            return i;

    return ImageKey();
}

TextureKey AssetCache::FindTexture(const StringID& name) const {
    for (uintn i = 0; i < textureassets.size(); ++i)
        if (textureassets[i].name == name)
            return i;

    return TextureKey();
}

SamplerKey AssetCache::FindSampler(const StringID& name) const {
    for (uintn i = 0; i < samplerassets.size(); ++i)
        if (samplerassets[i].name == name)
            return i;

    return SamplerKey();
}

ShaderKey AssetCache::FindShader(const StringID& name, ShaderAssetType type) const {
    for (uintn i = 0; i < shaderassets.size(); ++i) {
        const ShaderAsset& shader = shaderassets[i];
        if (shader.name == name && shader.type == type)
            return i;
    }

    return ShaderKey();
}

GeoAsset AssetCache::GetGeoAsset(const GeoKey& key) const {
    Assert_(key.index < geoassets.size());
    return geoassets[key.index];
}

ImageAsset AssetCache::GetImageAsset(const ImageKey& key) const {
    Assert_(key.index < imageassets.size());

    return imageassets[key.index];
}

const TextureAsset& AssetCache::GetTextureAsset(const TextureKey& key) const {
    Assert_(key.index < textureassets.size());

    return textureassets[key.index];
}

SamplerAsset AssetCache::GetSamplerAsset(const SamplerKey& key) const {
    Assert_(key.index < samplerassets.size());

    return samplerassets[key.index];
}

ImageResource AssetCache::GetImage(const ImageKey& key) const {
    Assert_(key.index < imageresources.size());

    return imageresources[key.index];
}

BLASResource AssetCache::GetBLAS(const GeoKey& key) const {
    Assert_(key.index < blasresources.size());

    BLASResource resource = blasresources[key.index];

    return resource;
}

void AssetCache::Shutdown(SessionContext &scontext) {
    for (uintn i = 0; i < geoassets.size(); ++i)
        FreeGeo(geoassets[i]);
    geoassets.clear();
    geodatas.clear();

    DestroyBuffer(scontext.pc, geoindexresource);
    DestroyBuffer(scontext.pc, geovertexresource);
    DestroyBuffer(scontext.pc, geovcolorresource);
    DestroyBuffer(scontext.pc, geosrgbvcolorresource);
    DestroyBuffer(scontext.pc, geotex1coordresource);
    DestroyBuffer(scontext.pc, geotangentresource);

    imageassets.clear();
    for (auto& image : imageresources)
        DestroyImage(scontext.pc, image);
    imageresources.clear();

    shaderassets.clear();
    for (auto& shader : shaderresources)
        DestroyShader(scontext.pc, shader);
    shaderresources.clear();

    for (auto& accel : blasresources)
        DestroyBLAS(scontext.pc, accel);
    blasresources.clear();
}

}