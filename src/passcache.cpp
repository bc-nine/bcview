#include "passcache.h"
#include "imageview.h"
#include "sessioncontext.h"
#include "framebufferlayout.h"

namespace bc9 {

PassCache::PassCache() {
};

void PassCache::Initialize(SessionContext &scontext) {
}

void PassCache::Shutdown(SessionContext &scontext) {
}

void PassCache::RegisterPass(const StringID& passname) {
    if (framebufferlayouts.find(passname) != framebufferlayouts.end())
        Throw_(Error("%s: pass already registered in pass cache", passname.str()));

    passimageviewmaps[passname] = ImageViewMap();
    framebufferlayouts[passname] = FramebufferLayout();
}

void PassCache::ResetPass(const StringID& passname) {
    if (framebufferlayouts.find(passname) == framebufferlayouts.end())
        Throw_(Error("%s: can't reset - pass not registered in pass cache", passname.str()));

    passimageviewmaps[passname] = ImageViewMap();
    framebufferlayouts[passname] = FramebufferLayout();
}

void PassCache::RegisterTarget(const StringID& passname, const StringID& targetname) {
    if (passimageviewmaps.find(passname) == passimageviewmaps.end())
        Throw_(Error("%s: pass not registered in pass cache", passname.str()));
    if (passimageviewmaps[passname].find(targetname) != passimageviewmaps[passname].end())
        Throw_(Error("%s: pass target already registered in pass cache", passname.str()));

    passimageviewmaps[passname][targetname] = nullptr;
}

void PassCache::UpdateFramebufferLayout(const StringID& passname, FramebufferLayout& layout) {
    auto iter = framebufferlayouts.find(passname);
    if (iter == framebufferlayouts.end())
        Throw_(Error("%s: pass not registered in pass cache", passname.str()));

    (*iter).second = layout;
}

void PassCache::UpdateTarget(const StringID& passname, const StringID& targetname,
                             ImageView* imageview) {
    if (passimageviewmaps.find(passname) == passimageviewmaps.end())
        Throw_(Error("%s: pass not registered in pass cache", passname.str()));
    auto iter = passimageviewmaps[passname].find(targetname);
    if (iter == passimageviewmaps[passname].end())
        Throw_(Error("%s: pass target not registered in pass cache", passname.str()));

    (*iter).second = imageview;
}


void PassCache::GetTarget(const StringID& passname, const StringID& targetname,
                          ImageView** imageview) {
    if (passimageviewmaps.find(passname) == passimageviewmaps.end())
        Throw_(Error("%s: pass not registered in pass cache", passname.str()));
    auto iter = passimageviewmaps[passname].find(targetname);
    if (iter == passimageviewmaps[passname].end())
        Throw_(Error("%s: pass target not registered in pass cache", passname.str()));

    *imageview = (*iter).second;
}

void PassCache::GetDisplayTarget(const StringID& targetname, ImageView** imageview) {
    if (passimageviewmaps.find("display") == passimageviewmaps.end())
        GetTarget("main", targetname, imageview);

    GetTarget("display", targetname, imageview);
}

void PassCache::GetFramebufferLayout(const StringID& passname, FramebufferLayout& layout) {
    auto iter = framebufferlayouts.find(passname);
    if (iter == framebufferlayouts.end())
        Throw_(Error("%s: pass not registered in pass cache", passname.str()));

    layout = (*iter).second;
}

void PassCache::GetDisplayFramebufferLayout(FramebufferLayout& layout) {
    if (framebufferlayouts.find("display") == framebufferlayouts.end())
        GetFramebufferLayout("main", layout);

    GetFramebufferLayout("display", layout);
}

}