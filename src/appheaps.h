#pragma once

#include "bclib/heaps.h"

namespace bc9 {

// changes here must be synced with the heaps definition in HeapSpace!
enum HeapID {
    eHeapGlobalBuddy,
    eHeapSessionLinear,
    eHeapGlobalLinear,
    eHeapGlobalStack,
    eHeapExternal,
    eHeapVulkan,
    eHeapJSON,
    eHeapXXHash,
    eHeapIMGui,
    eHeapSDL,
    eHeapCount
};

class AppHeapSpace : public HeapSpace {
public:
    AppHeapSpace();

private:
    BuddyHeap globalbuddy;
    LinearHeap sessionlinear;
    LinearHeap globallinear;
    StackHeap globalstack;
    ExternalHeap external;
    BuddyHeap vulkan;
    LinearHeap json;
    LinearHeap xxhash;
    LinearHeap imgui;
    BuddyHeap sdl;
};

}

// if we ignore the old throw() versions deprecated in c++11, and ignore the
// class-specific and placement version of new/delete, then there are 4 versions
// of new and 8 versions of delete. oh also ignore nothrow versions. phew.
extern void* operator new(size_t size);
extern void* operator new(size_t size, size_t alignment);
extern void* operator new[](size_t size);
extern void* operator new[](size_t size, size_t alignment);
extern void operator delete(void* ptr);
extern void operator delete(void* ptr, std::align_val_t alignment);
extern void operator delete(void* ptr, std::size_t size);
extern void operator delete(void* ptr, std::size_t size, std::align_val_t alignment);
extern void operator delete[](void *ptr);
extern void operator delete[](void* ptr, std::align_val_t alignment);
extern void operator delete[](void* ptr, std::size_t size);
extern void operator delete[](void* ptr, std::size_t size, std::align_val_t alignment);