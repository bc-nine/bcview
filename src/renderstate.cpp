#include "renderstate.h"
#include "bclib/stdjson.h"
#include <fstream>
#include <iomanip>

namespace bc9 {

Result ReadRenderState(const char* filename, RenderState& renderstate) {
    stdjson rsjson;
    Return_(ReadJSONFile(filename, rsjson));
    Return_(ReqUInt(rsjson, "frame", renderstate.frame));
    Return_(ReqUInt(rsjson, "rt_pathlength", renderstate.rt_pathlength));
    Return_(ReqUInt(rsjson, "rt_maxaccum", renderstate.rt_maxaccum));

    return Success();
}

Result WriteRenderState(const char* filename, const RenderState& renderstate) {
    stdjson state;
    state["frame"] = renderstate.frame;
    state["rt_pathlength"] = renderstate.rt_pathlength;
    state["rt_maxaccum"] = renderstate.rt_maxaccum;

    std::ofstream jstream(filename);
    jstream << std::setw(4) << state << std::endl;

    return Success();
}

bool operator==(const RenderState& lhs, const RenderState& rhs) {
    return (lhs.frame == rhs.frame) &&
           (lhs.rt_pathlength == rhs.rt_pathlength) &&
           (lhs.rt_maxaccum == rhs.rt_maxaccum);
}

}