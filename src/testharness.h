#pragma once

#include "bclib/stringid.h"
#include "bclib/fstring.h"
#include "bclib/errorutils.h"
#include "bclib/stdvector.h"
#include "imageresource.h"
#include "cachekeys.h"
#include "pipelinecache.h"
#include "bufferresource.h"

#include "EASTL/initializer_list.h"

namespace bc9 {

class Session;
struct RenderState;

class FunctionalTest;

class TestHarness {
public:
    enum Mode {
        eNone,
        eCaptureReference,
        eRunTest
    };
    static const FString ImageReferenceDir;

    TestHarness(const FString& worldname, Mode mode);
    ~TestHarness();

    void KeyPressed(uchar key);

    void Initialize(Session& session);
    void Shutdown(Session& session);

    Result Tick(Session& session, float dt, bool& paused, bool& done);

private:
    FunctionalTest* test = nullptr;
};


class FunctionalTest {
public:
    enum ImageDisplayMode {
        eImageDisplayCapture,
        eImageDisplayReference,
        eImageDisplayOutput,
        eImageDisplayModeCount
    };

    FunctionalTest (const FString& worldname, TestHarness::Mode mode,
                    std::initializer_list<uintn> frames);

    virtual void Initialize(Session& session);
    virtual void Shutdown(Session& session);

    virtual Result Tick(Session& session, uintn frame, float dt, bool& paused, bool& done);

    virtual void KeyPressed(uchar key);

    const FString worldname;
    #if winvk_
        const FString platform = "winvk";
    #elif wind12_
        const FString platform = "wind12";
    #endif

protected:
    Result RunTick(Session& session, uintn frame, float dt, bool& paused, bool& done);

    Result ReadImage(uintn frame, stdvector<uchar>& data, uintn& width, uintn& height,
                     uintn& format, RenderState& state);
    Result WriteImage(uintn frame, uchar* data, uint64 size, uintn width, uintn height,
                      uintn format, RenderState& state);

    TestHarness::Mode mode;

    Result testresult;

    ImageResource refimage, capimage, outputimage;

    stdvector<uintn> frames;

    ImageDisplayMode displaymode = eImageDisplayOutput;

private:
    ShaderKey computekey;
    Pipeline pipeline;

    BufferResource indexcbuf;
};

}