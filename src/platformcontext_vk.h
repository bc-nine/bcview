#pragma once

#include "volk.h"
#include "glm/glm.hpp"

#include "memallocator_vk.h"

#include "rhitypes.h"
#include "bclib/stdvector.h"
#include "bclib/stdarray.h"
#include "bclib/hashutils.h"
#include "platform_vk.h"
#include "imageresource_vk.h"
#include "resourceutil.h"
#include "rendertarget.h"
#include "bufferresource_vk.h"
#include "commandbuffer_vk.h"
#include "window_vk.h"
#include "util_vk.h"

namespace bc9 {

class World;
class Window;
class Surface;
class XRHeadset;
class AssetCache;
class SessionContext;
struct BufferResource;

extern VkBool32 DebugReportCallback(VkDebugReportFlagsEXT flags,
                                    VkDebugReportObjectTypeEXT objecttype,
                                    uint64 object,
                                    sizen location,
                                    int32 messagecode,
                                    const char* playerprefix,
                                    const char* pmessage,
                                    void* userdata);

class PlatformContext {
public:
    // public interface
    PlatformContext(const World& world, const AssetCache& assetcache, Surface& surface,
                    XRHeadset& headset, uintn xrmode);

    void Initialize(Window& window, OutputDevice device, const World& world);
    void Shutdown();

    void PostInitialize(SessionContext& scontext);

    CommandBuffer BeginSingleTimeCommands();
    void EndSingleTimeCommands(CommandBuffer& cmdbuffer);

    void BeginFrame();
    void EndFrame();

    uintn GetSwapIndex() const;

    // platform independent interface

    void DeviceWaitIdle();

    void SwapBuffers();

    // platform dependent interface
    void BindBindlessInputSet(PipelineType pipelinetype);

    void AddBindlessStorageBuffer(BufferResource& buffer);
    void AddBindlessSampledImage(ImageView& imageview);
    void AddBindlessStorageImage(ImageView& imageview);
    void AddBindlessAccelStructure(TLASResource& tlas);

    void BuildVkInstanceCreateInfo(stdvector<const char*>& instanceextensions,
                                   VkInstanceCreateInfo& createinfo, bool usedebugutils);
    void BuildVkDeviceCreateInfo(stdvector<const char*>& deviceextensions,
                                 VkDeviceCreateInfo& createinfo);

    void SetupDebugUtilsCallback();
    void SetupDebugReportCallback();

    void RequireDeviceExtensions(stdvector<const char*>& deviceextensions);

    void RequireBaseInstanceExtensions(stdvector<const char *> &extensions, bool usedebugutils);
    void RequireSDLInstanceExtensions(SDL_Window *window, stdvector<const char *> &extensions);

    bool CreateLogicalDevice(VkDeviceCreateInfo& createinfo);

    void CreateDeviceQueues();

private:
    void CreateBindlessDescriptorSets();

    stdvector<const char*> validationlayers;

    VkDebugUtilsMessengerEXT debugmessenger;
    VkDebugReportCallbackEXT dbgreportcallback;

public:
    uintn xrmode = 0;

    bool enableraytracing = false;

    SDL_Window *sdlwin = nullptr;
    OutputDevice outputdevice = eOutputDeviceCount;

    VkInstance instance;

    VkSampleCountFlagBits msaasamples;

    VkPhysicalDevice physicaldevice;

    VkDevice device;

    VkQueue presentqueue, graphicsqueue, asynccomputequeue;

    void* deviceextensionchain = nullptr;
    void* instancepnextchain = nullptr;

    VkPhysicalDeviceRayTracingPipelineFeaturesKHR rtpipelinefeatures {};
    VkPhysicalDeviceAccelerationStructureFeaturesKHR asfeatures {};
    VkPhysicalDeviceRayQueryFeaturesKHR rqfeatures {};

    stdvector<const char*> instanceextensions, deviceextensions;

    uintn bufferidx = 0;

    VkDescriptorPool imguidescriptorpool;

    MemAllocatorVk memallocator;

    Hasher hasher;

    VkCommandPool commandpool;
    stdvector<VkCommandBuffer> commandbuffers;

    struct {
        CommandBuffer cmdbuffer;
        VkDescriptorSet bindlessset;
        VkSemaphore imageavailablesemaphore;
        VkSemaphore renderfinishedsemaphore;
        VkFence inflightfence;
    } current;

    stdarray<VkSemaphore, kFrameRenderBufferCount> imageavailablesemaphores;
    stdarray<VkSemaphore, kFrameRenderBufferCount> renderfinishedsemaphores;
    stdarray<VkFence, kFrameRenderBufferCount> inflightfences;

    VkPhysicalDeviceProperties deviceproperties;

    VkDescriptorSet bindlesssets[kFrameRenderBufferCount];

    stdvector<VkSampler> samplers;

    uint64 currentcpuframe = 1;

    const AssetCache& assetcache;

    Surface& surface;

    XRHeadset& headset;

    VkCommandPool tmpcmdpool;

    VkDescriptorPool descriptorpool;

    QueueFamilyIndices indices;

    // index 0 is the push descriptor set for cbuffers, index 1 is the bindless set
    VkDescriptorSetLayout setlayouts[2];
    VkPipelineLayout pipelinelayout;

    BufferResource pfconstantsbuf;

private:
    uintn nextstoragebufferidx = 0;
    uintn nextsampledimageidx = 0;
    uintn nextstorageimageidx = 0;
    uintn nextaccelstructureidx = 0;
};

}
