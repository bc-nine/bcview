#include "cameracontrols.h"
#include "window.h"
#include "bclib/stdallocators.h"
#include "bclib/fileutils.h"
#include "bclib/fstring.h"

#include "session.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <shellapi.h> // For CommandLineToArgvW
#include "DXGIDebug.h"
#include <windowsx.h>
#include "imgui_impl_win32.h"

extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hwnd, UINT msg,
                                                             WPARAM wparam, LPARAM lparam);

// In order to define a function called CreateWindow, the windows macro needs to be undefined.
#if defined(CreateWindow)
#undef CreateWindow
#endif

#include <atlstr.h>
#include <wrl.h>

using namespace Microsoft::WRL;
using namespace bc9;
using namespace std;
using namespace glm;

Window g_window;
Session *g_session = nullptr;
CameraControls* g_controls = nullptr;
FString g_datadir;
bool g_initialized = false;

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

static void ReportLiveObjects() {
    Microsoft::WRL::ComPtr<IDXGIDebug> dxgicontroller;
    DXGIGetDebugInterface1(0, IID_PPV_ARGS(&dxgicontroller));
    dxgicontroller->ReportLiveObjects(DXGI_DEBUG_ALL,
        DXGI_DEBUG_RLO_FLAGS(DXGI_DEBUG_RLO_DETAIL | DXGI_DEBUG_RLO_IGNORE_INTERNAL));
}

void RegisterWindowClass(HINSTANCE hinst, const wchar_t* windowclassname) {
    // Register a window class for creating our render window with.
    WNDCLASSEXW windowclass = {};

    windowclass.cbSize = sizeof(WNDCLASSEX);
    windowclass.style = CS_HREDRAW | CS_VREDRAW;
    windowclass.lpfnWndProc = &WndProc;
    windowclass.cbClsExtra = 0;
    windowclass.cbWndExtra = 0;
    windowclass.hInstance = hinst;
    windowclass.hIcon = ::LoadIcon(hinst, NULL);
    windowclass.hCursor = ::LoadCursor(NULL, IDC_ARROW);
    windowclass.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    windowclass.lpszMenuName = NULL;
    windowclass.lpszClassName = windowclassname;
    windowclass.hIconSm = ::LoadIcon(hinst, NULL);

    static ATOM atom = ::RegisterClassExW(&windowclass);
    assert(atom > 0);
}

HWND CreateWindow(const wchar_t* windowclassname, HINSTANCE hinst,
                  const wchar_t* windowtitle, uint32_t width, uint32_t height) {
    int screenwidth = ::GetSystemMetrics(SM_CXSCREEN);
    int screenheight = ::GetSystemMetrics(SM_CYSCREEN);

    RECT windowrect = { 0, 0, static_cast<LONG>(width), static_cast<LONG>(height) };
    ::AdjustWindowRect(&windowrect, WS_OVERLAPPEDWINDOW, FALSE);

    int windowwidth = windowrect.right - windowrect.left;
    int windowheight = windowrect.bottom - windowrect.top;

    // Center the window within the screen. Clamp to 0, 0 for the top-left corner.
    int windowx = Max(0, (screenwidth - windowwidth) / 2);
    int windowy = Max(0, (screenheight - windowheight) / 2);

    HWND hwnd = ::CreateWindowExW(
        NULL,
        windowclassname,
        windowtitle,
        WS_OVERLAPPEDWINDOW,
        windowx,
        windowy,
        windowwidth,
        windowheight,
        NULL,
        NULL,
        hinst,
        nullptr
    );

    assert(hwnd && "Failed to create window");

    return hwnd;
}

CameraControls* CreateCameraControls(const StringID& mode, const glm::vec2& speed, World* world) {
    CameraControls* controls = nullptr;
    if (mode == "ttz"_sid)
        controls = new TiltTwirlControls(speed, world);
    else if (mode == "fly"_sid)
        controls = new FlyControls(speed);
    else
        Throw_(Error("invalid camera controls mode"));
    controls->SetCamera(world->GetActiveCamera());

    return controls;
}

void Cleanup(Session*& session, Window& window) {
    if (window.win) {
        DestroyWindow(window.win);
        window.win = nullptr;
    }

    if (session) {
        session->Shutdown();
        delete session;
        session = nullptr;
    }

    Heaps().ResetSessionHeaps();
}

void ReloadSession(Session*& session, CameraControls*& controls,
                   const char* dataroot, Window& window) {
    g_initialized = false;
    while (!g_initialized) {
        Cleanup(session, window);

        stdjson worldjson;
        FString worldfile("%s%s", dataroot, "\\world.json");
        Throw_(ReadJSONFile(worldfile.str(), worldjson));
        glm::uvec2 size;
        Throw_(OptUInt2(worldjson["render-settings"], "surface-size", uvec2(1500, 1100), size));

        g_window.win = CreateWindow(window.classname, window.hinstance, L"BCVIEW (DirectX 12)",
                                    size.x, size.y);

        ::ShowWindow(window.win, SW_SHOW);

        try {
            session = new Session(dataroot, worldjson);
            session->Initialize(window);

            if (controls)
                delete controls;

            StringID mode(session->world.GetCameraControlsMode());
            glm::vec2 speed(session->world.GetCameraControlsSpeed());
            controls = CreateCameraControls(mode, speed, &session->world);

            g_initialized = true;
        }
        catch (const runtime_error& error) {
            FString msg("%s. \nPlease fix the error and click OK to retry.", error.what());
            if (MessageBoxA(NULL, msg.str(), "Error", MB_RETRYCANCEL) == IDCANCEL)
                exit(0);
        }

    }
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam) {
    WindowEvent event;
    if (g_initialized && g_session) {

        if (ImGui_ImplWin32_WndProcHandler(hwnd, message, wparam, lparam))
            return true;

        if (g_session->renderer.ProcessEvent(event))
            return ::DefWindowProcW(hwnd, message, wparam, lparam);

        switch (message) {
            case WM_LBUTTONDOWN: {
                ButtonEvent bevent;
                bevent.position.x = GET_X_LPARAM(lparam);
                bevent.position.y = GET_Y_LPARAM(lparam);
                g_controls->LeftButtonDown(bevent);
            }
            break;

            case WM_MOUSEMOVE: {
                if (wparam & MK_LBUTTON) {
                    MotionEvent mevent;
                    mevent.position.x = GET_X_LPARAM(lparam);
                    mevent.position.y = GET_Y_LPARAM(lparam);
                    g_controls->LeftButtonMotion(mevent);
                }
            }
            break;

            case WM_KEYUP: {
                KeyEvent event;
                switch (wparam) {
                    case VK_LEFT:
                    event.key = eLeftKey;
                    g_controls->KeyUp(event);
                    break;

                    case VK_RIGHT:
                    event.key = eRightKey;
                    g_controls->KeyUp(event);
                    break;

                    case VK_UP:
                    event.key = eUpKey;
                    g_controls->KeyUp(event);
                    break;

                    case VK_DOWN:
                    event.key = eDownKey;
                    g_controls->KeyUp(event);
                    break;

                    case 'S':
                    event.key = eSKey;
                    g_controls->KeyUp(event);
                    break;

                    case 'W':
                    event.key = eWKey;
                    g_controls->KeyUp(event);
                    break;

                    case 'D':
                    event.key = eDKey;
                    g_controls->KeyUp(event);
                    break;

                    case 'A':
                    event.key = eAKey;
                    g_controls->KeyUp(event);
                    break;

                    case 'E':
                    event.key = eEKey;
                    g_controls->KeyUp(event);
                    break;

                    case 'C':
                    event.key = eCKey;
                    g_controls->KeyUp(event);
                    break;

                    case 'O':
                    event.key = eOKey;
                    g_controls->KeyUp(event);
                    break;
                }
            }
            break;

            case WM_SYSKEYDOWN:
            case WM_KEYDOWN: {
                KeyEvent event;
                switch (wparam) {
                    case VK_LEFT:
                    event.key = eLeftKey;
                    g_controls->KeyDown(event);
                    break;

                    case VK_RIGHT:
                    event.key = eRightKey;
                    g_controls->KeyDown(event);
                    break;

                    case VK_UP:
                    event.key = eUpKey;
                    g_controls->KeyDown(event);
                    break;

                    case VK_DOWN:
                    event.key = eDownKey;
                    g_controls->KeyDown(event);
                    break;

                    case 'S':
                    event.key = eSKey;
                    g_controls->KeyDown(event);
                    break;

                    case 'W':
                    event.key = eWKey;
                    g_controls->KeyDown(event);
                    break;

                    case 'D':
                    event.key = eDKey;
                    g_controls->KeyDown(event);
                    break;

                    case 'A':
                    event.key = eAKey;
                    g_controls->KeyDown(event);
                    break;

                    case 'E':
                    event.key = eEKey;
                    g_controls->KeyDown(event);
                    break;

                    case 'C':
                    event.key = eCKey;
                    g_controls->KeyDown(event);
                    break;

                    case 'O':
                    event.key = eOKey;
                    g_controls->KeyDown(event);
                    break;

                    case 'R':
                    ReloadSession(g_session, g_controls, g_datadir.str(), g_window);
                    break;

                    case 'M':
                    g_session->world.GetActiveCamera()->Dump();
                    break;

                    case VK_ESCAPE:
                    g_session->context.ToggleOverlay();
                    break;

                    //::PostQuitMessage(0);

                    case VK_RETURN:
                        //if ( alt )
                        //{
                    //case VK_F11:
                        //SetFullscreen(g_session->renderer.scontext.pc.mswin, !g_fullscreen);
                        //}
                    break;
                }
            }
            break;

            // The default window procedure will play a system notification sound
            // when pressing the Alt+Enter keyboard combination if this message is
            // not handled.
            case WM_SYSCHAR:
            break;

            case WM_SIZE: {
                RECT clientrect = {};
                ::GetClientRect(g_session->renderer.scontext.pc.mswin, &clientrect);

                int width = clientrect.right - clientrect.left;
                int height = clientrect.bottom - clientrect.top;

                g_session->renderer.ViewportResized(width, height);
            }
            break;

            case WM_DESTROY:
                ::PostQuitMessage(0);
                break;

            default:
                return ::DefWindowProcW(hwnd, message, wparam, lparam);
        }
    }
    else {
        return ::DefWindowProcW(hwnd, message, wparam, lparam);
    }

    return 0;
}

int CALLBACK wWinMain(HINSTANCE hinstance, HINSTANCE hprevinstance, PWSTR lpcmdline, int ncmdshow){
    int argc = 0;
    LPWSTR *argv = CommandLineToArgvW(lpcmdline, &argc);

    if(argc < 1) {
        printf("usage: bcview.exe <world name>\n");
        exit(1);
    }

    // Windows 10 Creators update adds Per Monitor V2 DPI awareness context.
    // Using this awareness context allows the client area of the window
    // to achieve 100% scaling while still allowing non-client window content to
    // be rendered in a DPI sensitive fashion.
    SetThreadDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);

    // Window class name. Used for registering / creating the window.
    g_window.classname = L"DX12WindowClass";
    //ParseCommandLineArguments();
    RegisterWindowClass(hinstance, g_window.classname);
    g_window.hinstance = hinstance;

    // why OH WHY can't i do FString name = CW2A(argv[0]) ? why? WHY?
    FString name;
    name = CW2A(argv[0]);
    g_datadir = FString("P:/data/wind12/%s", name.str());

    ReloadSession(g_session, g_controls, g_datadir.str(), g_window);

    MSG msg = {};
    while (msg.message != WM_QUIT) {
        if (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
            ::TranslateMessage(&msg);
            ::DispatchMessage(&msg);
        }

        static auto prevtime = std::chrono::high_resolution_clock::now();
        auto currtime = std::chrono::high_resolution_clock::now();
        float dt = chrono::duration<float,
            chrono::seconds::period>(currtime - prevtime).count();
        prevtime = chrono::high_resolution_clock::now();

        g_controls->Update(dt);

        g_session->renderer.DrawFrame(dt);
    }

    g_session->Shutdown();
    delete g_session;

    delete g_controls;

    ReportLiveObjects();

    return 0;
}