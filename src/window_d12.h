#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

namespace bc9 {

class Window {
public:
    HWND win;
    const wchar_t* classname;
    HINSTANCE hinstance;
};

class WindowEvent {
public:

};

}