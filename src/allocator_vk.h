#pragma once

#include "appheaps.h"
#include "volk.h"
#include "bclib/stdutility.h"

namespace bc9 {

inline void* vkalloc(void* userdata, size_t size, size_t align, VkSystemAllocationScope scope) {
    return Heaps().Buddy(eHeapVulkan).Allocate(size, align);
}

inline void vkfree(void* userdata, void* memory) {
    if (memory)
        Heaps().Buddy(eHeapVulkan).Deallocate(memory);
}

inline void* vkrealloc(void* userdata, void* orig, size_t size, size_t align,
                       VkSystemAllocationScope scope) {
    return Heaps().Buddy(eHeapVulkan).Reallocate(orig, size, align);
}

inline void vkinternalalloc(void* userdata, size_t size, VkInternalAllocationType type,
                            VkSystemAllocationScope scope) {
    Heaps().External(eHeapExternal).Allocate(size);
}

inline void vkinternalfree(void* userdata, size_t size, VkInternalAllocationType type,
                           VkSystemAllocationScope scope) {
    Heaps().External(eHeapExternal).Deallocate(size);
}

VkAllocationCallbacks *VkAllocators();

}