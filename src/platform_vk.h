#pragma once

// unfortunately, if you enable validation, you need to enable at least *one*
// of the modes below it, otherwise the features struct is empty, which is
// illegal (and there's no "filler" value i can use).
#define ENABLE_VALIDATION                 1
#define ENABLE_GPU_VALIDATION             0
// also unfortunately, if you want to use printf right now, you need to include
//     vk::RawBufferLoad(0);
// somewhere in the shader (ugh).
#define ENABLE_PRINTF_VALIDATION          0
#define ENABLE_BEST_PRACTICES_VALIDATION  0
#define ENABLE_SYNCHRONIZATION_VALIDATION 1

#define ENABLE_PIPELINE_CACHE 0