#pragma once

#include "cachekeys.h"

namespace bc9 {

struct Drawable {
    StringID name = "null";
    GeoKey geokey;
    uint32 materialidx = 0;
    int32 normalsense = 1;
};

}