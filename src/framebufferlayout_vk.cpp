#include "framebufferlayout.h"
#include "platformcontext.h"
#include "util_vk.h"
#include "allocator_vk.h"
#include "resourceutil.h"

namespace bc9 {

FramebufferLayout::FramebufferLayout() : layoutdata(1) {}

FramebufferLayout::FramebufferLayout(uintn colortargets, uintn msaasamples,
                                     FramebufferLayoutFlags flags,
                                     MultiviewMode multiview) :
colortargets(colortargets),
flags(flags),
layoutdata(msaasamples)
{
    layers = (multiview == eMultiviewDisabled) ? 1 : multiview;
    multiviewenabled = (multiview != eMultiviewDisabled);
}

void FramebufferLayout::AddColorDependency(VkImageLayout layout) {
    switch (layout) {
        case VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL:
        dependencies.insert(eSubpassDependencyColorAttachToShaderRead);
        break;

        case VK_IMAGE_LAYOUT_PRESENT_SRC_KHR:
        dependencies.insert(eSubpassDependencyToColorAttach);
        break;

        case VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL:
        dependencies.insert(eSubpassDependencyFromColorAttach);
        break;

        default:
        Assert_(0);
        break;
    }
}

bool FramebufferLayout::UseDepthStencilTarget() const {
    return flags & eFramebufferLayoutUseDepthStencilTarget;
}

void FramebufferLayout::AddColorLayout(PlatformContext& context, ColorLayout& color) {
    if (colorattachmentrefs.size() == colortargets)
        Throw_(Error("too many color targets"));

    bool noresolve = color.flags & eColorLayoutNoResolve;
    bool shaderreadable = color.flags & eColorLayoutShaderReadable;
    bool colorpresenting = color.flags & eColorLayoutPresenting;

    VkImageLayout initiallayout = VK_IMAGE_LAYOUT_UNDEFINED;
    VkImageLayout finallayout = VK_IMAGE_LAYOUT_UNDEFINED;
    VkImageLayout resolveinitiallayout = VK_IMAGE_LAYOUT_UNDEFINED;
    VkImageLayout resolvefinallayout = VK_IMAGE_LAYOUT_UNDEFINED;
    if (layoutdata.msaasamples > 1) {
        // can't present a multisample framebuffer, so we can skip
        // consideration of that particular image layout.
        Assert_((noresolve == false) || (colorpresenting == false));

        if (noresolve) {
            if (shaderreadable) {
                initiallayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                finallayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            }
            else {
                initiallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                finallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            }
        }
        else {
            if (shaderreadable) {
                initiallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                finallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                resolveinitiallayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                resolvefinallayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            }
            else {
                initiallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                finallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                resolveinitiallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
                resolvefinallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            }
        }
    }
    else {
        // doesn't make sense to disable resolves if we aren't even
        // doing multisampling in the first place, so let's enforce this.
        Assert_(noresolve == false);
        if (colorpresenting) {
            initiallayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
            finallayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        }
        else if (shaderreadable) {
            initiallayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
            finallayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        }
        else {
            initiallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
            finallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        }
    }

    VkAttachmentDescription colorattachment = {};
    Throw_(GetVkFormat(color.format, colorattachment.format));
    Throw_(GetVkSampleCount(layoutdata.msaasamples, colorattachment.samples));
    Throw_(GetVkAttachmentLoadOp(color.flags, colorattachment.loadOp));
    Throw_(GetVkAttachmentStoreOp(color.flags, colorattachment.storeOp));
    colorattachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    colorattachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    colorattachment.initialLayout = initiallayout;
    colorattachment.finalLayout = finallayout;

    if (colorattachment.loadOp == VK_ATTACHMENT_LOAD_OP_CLEAR)
        hasclearattachment = true;

    VkAttachmentReference colorattachmentref = {};
    colorattachmentref.attachment = (uint32) attachments.size();
    colorattachmentref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    if (layoutdata.msaasamples > 1)
        attachmentindicesmsaa[color.name] = (uintn)attachments.size();
    else
        attachmentindices[color.name] = (uintn)attachments.size();
    attachments.push_back(colorattachment);

    colorattachmentrefs.push_back(colorattachmentref);

    if (finallayout != VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
        AddColorDependency(finallayout);

    // Right now, this is for the OpenXR swapchain render target. I need to figure
    // out a better way to handle these, as this whole approach is a bit janky,
    // but basically, for now, this is how I go about identifying it. When LunarG
    // added QueueSubmit synchronization validation checks by default (see
    // feb 24 2024), I started getting validation errors on the swapchain target.
    // The solution is to put (the equivalent of) a pipeline barrier in to avoid
    // the write after write hazzard of trying to do a layout transition while
    // still doing color attachment writes.
    if (layoutdata.msaasamples == 1 &&
        initiallayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL &&
        finallayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL) {
        AddColorDependency(finallayout);
    }

    if (layoutdata.msaasamples > 1 && !noresolve) {
        VkImageLayout finallayout = VK_IMAGE_LAYOUT_UNDEFINED;
        if (colorpresenting)
            finallayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
        else if (shaderreadable)
            finallayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        else
            finallayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
        VkAttachmentDescription colorresolveattachment = {};
        colorresolveattachment.format = colorattachment.format;
        colorresolveattachment.samples = VK_SAMPLE_COUNT_1_BIT;
        colorresolveattachment.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorresolveattachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
        colorresolveattachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        colorresolveattachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
        colorresolveattachment.initialLayout = resolveinitiallayout;
        colorresolveattachment.finalLayout = resolvefinallayout;

        VkAttachmentReference colorresolveattachmentref = {};
        colorresolveattachmentref.attachment = (uint32) attachments.size();
        colorresolveattachmentref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        attachmentindices[color.name] = (uintn)attachments.size();
        attachments.push_back(colorresolveattachment);

        colorresolveattachmentrefs.push_back(colorresolveattachmentref);

        if (finallayout != VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
            AddColorDependency(finallayout);
    }

    colorlayouts.push_back(color);
    if (colorpresenting)
        presenting = true;
}

uintn
FramebufferLayout::GetSampleCount() const {
    return layoutdata.msaasamples;
}

uintn FramebufferLayout::GetColorTargetCount() const {
    return (uintn) colorlayouts.size();
}

void FramebufferLayout::GetColorLayout(uintn idx, ColorLayout& color) const {
    Assert_(idx < colorlayouts.size());
    color = colorlayouts[idx];
}

void FramebufferLayout::GetColorLayout(const StringID& name, ColorLayout& color) const {
    uintn idx;
    GetColorIndex(name, idx);
    GetColorLayout(idx, color);
}

void FramebufferLayout::GetDepthStencilLayout(DepthStencilLayout& _dslayout) const {
    _dslayout = dslayout;
}

void FramebufferLayout::GetColorIndex(const StringID& name, uintn& index) const {
    for (uintn i = 0; i < colorlayouts.size(); ++i) {
        if (colorlayouts[i].name == name) {
            index = i;
            return;
        }
    }

    Throw_(Error("FramebufferLayout: couldn't find color layout '%s'", name.str()));
}

void
FramebufferLayout::AddDepthStencilLayout(PlatformContext& context, DepthStencilLayout& _dslayout) {
    dslayout = _dslayout;

    if ((flags & eFramebufferLayoutUseDepthStencilTarget) == 0)
        Throw_(Error("can't add depth/stencil target"));
    else if (attachmentindices.find(SID_("depth")) != attachmentindices.end())
        Throw_(Error("depth target already added"));

    // we don't support the new depth/stencil resolve features of vulkan yet. also,
    // clearly, even though the intent is there to support separate depth/stencil,
    // the functionality isn't fully fleshed out yet, as can be seen below.
    VkImageLayout finallayout = VK_IMAGE_LAYOUT_UNDEFINED;
    // doesn't make sense to disable resolves if we aren't even
    // doing multisampling in the first place, so let's enforce this.
    Assert_((dslayout.flags & eDepthStencilLayoutDepthNoResolve) == 0);
    Assert_((dslayout.flags & eDepthStencilLayoutStencilNoResolve) == 0);
    if (dslayout.flags & eDepthStencilLayoutDepthShaderReadable)
        finallayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_READ_ONLY_OPTIMAL;
    else
        finallayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentDescription depthattachment = {};
    Throw_(GetVkFormat(dslayout.format, depthattachment.format));
    Throw_(GetVkSampleCount(layoutdata.msaasamples, depthattachment.samples));
    Throw_(GetVkAttachmentDepthLoadOp(dslayout.flags, depthattachment.loadOp));
    Throw_(GetVkAttachmentDepthStoreOp(dslayout.flags, depthattachment.storeOp));
    Throw_(GetVkAttachmentStencilLoadOp(dslayout.flags, depthattachment.stencilLoadOp));
    Throw_(GetVkAttachmentStencilStoreOp(dslayout.flags, depthattachment.stencilStoreOp));

    VkImageLayout initiallayout = VK_IMAGE_LAYOUT_UNDEFINED;
    if (depthattachment.loadOp == VK_ATTACHMENT_LOAD_OP_LOAD ||
        depthattachment.stencilLoadOp == VK_ATTACHMENT_LOAD_OP_LOAD) {
        initiallayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    }

    depthattachment.initialLayout = initiallayout;
    depthattachment.finalLayout = finallayout;

    depthattachmentref = {};
    depthattachmentref.attachment = (uint32) attachments.size();
    depthattachmentref.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    attachmentindices[SID_("depth")] = (uintn)attachments.size();
    attachments.push_back(depthattachment);

    if (layoutdata.msaasamples > 1 && dsresolvelayout != eImageLayoutUndefined) {
        // not yet implemented
    }
}

void FramebufferLayout::Initialize(PlatformContext& context, const StringID& name) {
    VkSubpassDescription subpass = {};
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.colorAttachmentCount = (uint32)colorattachmentrefs.size();
    subpass.pColorAttachments = colorattachmentrefs.data();
    if (flags & eFramebufferLayoutUseDepthStencilTarget)
        subpass.pDepthStencilAttachment = &depthattachmentref;
    if (colorresolveattachmentrefs.size())
        subpass.pResolveAttachments = colorresolveattachmentrefs.data();

    stdvector<VkSubpassDependency> spdependencies;
    Assert_(dependencies.size() <= 1); // until synchronization validation layer can handle > 1
    for (auto dependency : dependencies) {
        VkSubpassDependency spdependency = {};
        if (dependency == eSubpassDependencyColorAttachToShaderRead) {
            spdependency.srcSubpass = 0;
            spdependency.dstSubpass = VK_SUBPASS_EXTERNAL;

            spdependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            spdependency.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            spdependency.dstStageMask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
            spdependency.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
            spdependencies.push_back(spdependency);
        }
        // Notice the symmetry here, from outside in...
        else if (dependency == eSubpassDependencyToColorAttach) {
            spdependency.srcSubpass = VK_SUBPASS_EXTERNAL;
            spdependency.dstSubpass = 0;

            spdependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            spdependency.srcAccessMask = 0;
            spdependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            spdependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            spdependencies.push_back(spdependency);
        }
        // ...and from inside out
        else if (dependency == eSubpassDependencyFromColorAttach) {
            spdependency.srcSubpass = 0;
            spdependency.dstSubpass = VK_SUBPASS_EXTERNAL;

            spdependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            spdependency.srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
            spdependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
            spdependency.dstAccessMask = 0;
            spdependencies.push_back(spdependency);
        }
        else {
            // Dodgy...
            Assert_(0);
        }
    }

    uint32 viewmask = 0x0;
    VkRenderPassMultiviewCreateInfo multiviewinfo = {};

    VkRenderPassCreateInfo renderpassinfo = {};
    renderpassinfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderpassinfo.attachmentCount = static_cast<uint32>(attachments.size());
    renderpassinfo.pAttachments = attachments.data();
    renderpassinfo.subpassCount = 1;
    renderpassinfo.pSubpasses = &subpass;
    renderpassinfo.dependencyCount = (uint32) spdependencies.size();
    renderpassinfo.pDependencies = spdependencies.data();

    if (multiviewenabled) {
        for (uintn i = 0; i < layers; ++i)
            viewmask |= (1 << i);

        multiviewinfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_MULTIVIEW_CREATE_INFO;
        multiviewinfo.subpassCount = renderpassinfo.subpassCount;
        multiviewinfo.pViewMasks = &viewmask;

        renderpassinfo.pNext = &multiviewinfo;
    }

    Assert_(renderpassinfo.subpassCount == 1); // otherwise e.g. viewmask would need updating
                                               // for the other subpasses, but code above is
                                               // only set up for a single subpass for now.

    Throw_(CreateRenderPass(context, renderpassinfo, layoutdata.renderpass, name));
}

uintn FramebufferLayout::GetMultiviewLayerCount() const {
    return layers;
}

bool FramebufferLayout::MultiviewEnabled() const {
    return multiviewenabled;
}

uint32 FramebufferLayout::GetColorClearAttachmentIndex(const StringID& name) const {
    auto& indices = (layoutdata.msaasamples > 1) ? attachmentindicesmsaa : attachmentindices;

    auto iter(indices.find(name));
    if (iter != indices.end())
        return (*iter).second;

    return kInvalidUInt32;
}

uint32
FramebufferLayout::GetColorAttachmentIndex(const StringID& name, bool msaaindex) const {
    auto& indices = (msaaindex) ? attachmentindicesmsaa : attachmentindices;

    auto iter(indices.find(name));
    if (iter != indices.end())
        return (*iter).second;

    return kInvalidUInt32;
}

uint32 FramebufferLayout::GetDepthIndex() const {
    auto iter(attachmentindices.find(SID_("depth")));
    if (iter != attachmentindices.end())
        return (*iter).second;

    return kInvalidUInt32;
}

uint32 FramebufferLayout::GetDepthClearIndex() const {
    auto iter(attachmentindices.find(SID_("depth")));
    if (iter != attachmentindices.end())
        return (*iter).second;

    return kInvalidUInt32;
}

void FramebufferLayout::Shutdown(PlatformContext& context) {
    vkDestroyRenderPass(context.device, layoutdata.renderpass, VkAllocators());
}

}