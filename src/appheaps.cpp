#include "appheaps.h"
#include "bclib/stdallocators.h"

namespace bc9 {

HeapSpace &Heaps() {
    static AppHeapSpace heapspace;

    return heapspace;
}

// i needed 4096 for buddy and session linear to load all of the am212 scenes,
// but can probably get by with much less than that otherwise.
AppHeapSpace::AppHeapSpace() :
globalbuddy(MB(1024),   eHeapGlobalBuddy,   "global (buddy)", false),
sessionlinear(MB(4096), eHeapSessionLinear, "session (linear)"),
globallinear(MB(8),     eHeapGlobalLinear,  "global (linear)", false),
globalstack(MB(1024),   eHeapGlobalStack,   "global (stack)", false),
external(MB(1),         eHeapExternal,      "external"),
vulkan(MB(128),          eHeapVulkan,        "vulkan"),
json(MB(256),           eHeapJSON,          "nlohmann::json"),
xxhash(MB(1),           eHeapXXHash,        "xxhash"),
imgui(MB(16),           eHeapIMGui,         "imgui"),
sdl(MB(1),              eHeapSDL,           "sdl", false)
{
    RegisterHeap(&globalbuddy, eHeapGlobalBuddy);
    RegisterHeap(&sessionlinear, eHeapSessionLinear);
    RegisterHeap(&globallinear, eHeapGlobalLinear);
    RegisterHeap(&globalstack, eHeapGlobalStack);
    RegisterHeap(&external, eHeapExternal);
    RegisterHeap(&vulkan, eHeapVulkan);
    RegisterHeap(&json, eHeapJSON);
    RegisterHeap(&xxhash, eHeapXXHash);
    RegisterHeap(&imgui, eHeapIMGui);
    RegisterHeap(&sdl, eHeapSDL);
    Assert_(HeapCount() == eHeapCount);
}

StdAllocator* DefaultAllocator() {
    return &GlobalBuddy();
}

uint32 GlobalStack() {
    return eHeapGlobalStack;
}

StdAllocator& GlobalLinear() {
    static StdLinearAllocator stdlinalloc(Heaps().Linear(eHeapGlobalLinear));

    return stdlinalloc;
}

StdAllocator& SessionLinear() {
    static StdLinearAllocator stdlinalloc(Heaps().Linear(eHeapSessionLinear));

    return stdlinalloc;
}

StdAllocator& GlobalBuddy() {
    static StdBuddyAllocator stdbuddyalloc(Heaps().Buddy(eHeapGlobalBuddy));

    return stdbuddyalloc;
}

}

void* operator new(size_t size) {
    return bc9::DefaultAllocator()->Allocate(size);
}

void* operator new(size_t size, size_t alignment) {
    return bc9::DefaultAllocator()->Allocate(size, alignment);
}

void* operator new[](size_t size) {
    return bc9::DefaultAllocator()->Allocate(size);
}

void* operator new[](size_t size, size_t alignment) {
    return bc9::DefaultAllocator()->Allocate(size, alignment);
}

void operator delete(void* ptr) {
    return bc9::DefaultAllocator()->Deallocate(ptr);
}

void operator delete(void* ptr, std::align_val_t alignment) {
    return bc9::DefaultAllocator()->Deallocate(ptr);
}

void operator delete(void* ptr, std::size_t size) {
    return bc9::DefaultAllocator()->Deallocate(ptr);
}

void operator delete[](void *ptr) {
    return bc9::DefaultAllocator()->Deallocate(ptr);
}

void operator delete[](void* ptr, std::align_val_t alignment) {
    return bc9::DefaultAllocator()->Deallocate(ptr);
}

void operator delete[](void* ptr, std::size_t size) {
    return bc9::DefaultAllocator()->Deallocate(ptr);
}
