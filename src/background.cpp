#include "background.h"
#include "cachekeys.h"
#include "assetcache.h"

namespace bc9 {

Background::Background(const stdjson &bgjson, const AssetCache& assetcache) {
    Throw_(ReqString(bgjson, "name", name));

    if (bgjson.contains("color1")) {
        mode = eBackgroundGradient;
        Throw_(ReqFloat4(bgjson, "color1", color1));
        Throw_(ReqFloat4(bgjson, "color2", color2));
        Throw_(ReqString(bgjson, "color-space", space));
    }
    else if (bgjson.contains("color-map")) {
        mode = eBackgroundMap;
        StringID colormap;
        Throw_(ReqString(bgjson, "color-map", colormap));
        TextureKey key = assetcache.FindTexture(colormap);
        Assert_(key != kInvalidUInt);
        const TextureAsset& texture = assetcache.GetTextureAsset(key);
        mapdata.imageidx = texture.ImageIndex();
        mapdata.sampleridx = texture.SamplerIndex();
    }
    else {
        mode = eBackgroundSolid;
        Throw_(ReqFloat4(bgjson, "color", color1));
        Throw_(ReqString(bgjson, "color-space", space));
    }
}

StringID Background::Name() const {
    return name;
}

BackgroundMapData Background::MapData() const {
    return mapdata;
}

glm::vec3 Background::Color(const glm::vec3& color) const {
    glm::vec3 c = color;

    if (space == "srgb"_sid) {
        // go from sRGB to linear
        c.x = powf(color.x, 2.233333f);
        c.y = powf(color.y, 2.233333f);
        c.z = powf(color.z, 2.233333f);
    }

    return c;
}

BackgroundMode Background::Mode() const {
    return mode;
}

glm::vec4 Background::Color1() const {
    return glm::vec4(Color(color1), color1.w);
}

glm::vec4 Background::Color2() const {
    return glm::vec4(Color(color2), color2.w);
}

}
