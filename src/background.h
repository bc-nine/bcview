#pragma once

#include "glm/glm.hpp"
#include "bclib/stdjson.h"

namespace bc9 {

class AssetCache;

enum BackgroundMode {
    eBackgroundSolid,
    eBackgroundGradient,
    eBackgroundMap,
    eBackgroundModeCount
};

struct BackgroundMapData {
    uintn sampleridx = kInvalidUInt;
    uintn imageidx = kInvalidUInt;
};

class Background {
public:
    Background() {};
    Background(const stdjson &cjson, const AssetCache& assetcache);

    BackgroundMode Mode() const;

    glm::vec4 Color1() const;
    glm::vec4 Color2() const;

    BackgroundMapData MapData() const;

    StringID Name() const;

private:
    StringID name;
    glm::vec3 Color(const glm::vec3& color) const;

    BackgroundMode mode = eBackgroundSolid;
    glm::vec4 color1 = { 1,1,1,1 }, color2 = { 1,1,1,1 };
    StringID space = "linear";

    BackgroundMapData mapdata;
};

}