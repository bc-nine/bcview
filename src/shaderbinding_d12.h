#pragma once

#include <climits>
#include "bclib/stdjson.h"
#include <d3dcommon.h>
#include "rhitypes.h"

namespace bc9 {

class ShaderBinding {
public:
    // note that a shader binding may be part of multiple stages (vertex, pixel, etc).
    // when created from json, they're initialized with a single stage (since each shader
    // has its own json), but other parts of the code that look at the whole pipeline
    // may see that the same binding is used in multiple shader stages, and there we
    // simply reuse this structure, but with all relevant stages identified.
    ShaderBinding(const stdjson &mjson, ShaderStage stage);
    ShaderBinding();

    bool Valid() const;

    bool operator==(const ShaderBinding& rhs);

    bool multistage = false;
    unsigned int bindpoint = UINT_MAX;
    unsigned int space = UINT_MAX;
    unsigned int count = UINT_MAX;

    D3D_SHADER_INPUT_TYPE type;
    D3D_SRV_DIMENSION dimension;

    ShaderStages stages = eShaderStageInvalid;
};

}
