#pragma once

#include "resourceutil.h"

// TODO: this guy looks like it could be moved over to rendertarget.h now...

namespace bc9 {

class ColorTarget {
public:
    ImageResource msaatarget;
    ImageResource target;
};

class SwapchainTarget {
public:
    ImageResource targets[kMaxSwapchainImageCount];
};

// might at some point need to support separate resources for depth and stencil.
class DepthStencilTarget {
public:
    ImageResource target;
};

}