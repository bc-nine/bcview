#pragma once

#include "bclib/podtypes.h"

namespace bc9 {

struct GeoDrawData {
    uint32 indexoffset = kInvalidUInt32;

    // only one of indices/vertices will be filled.
    uint32 indexcount = 0;
    uint32 vertexcount = 0;
};

struct GeoData {
    uint32 pntoffset = kInvalidUInt32;
    uint32 coloroffset = kInvalidUInt32;
    uint32 srgbcoloroffset = kInvalidUInt32;
    uint32 tex1coordoffset = kInvalidUInt32;
    uint32 tangentoffset = kInvalidUInt32;

    GeoDrawData drawdata;
};

}