#pragma once

#include "constants.h"

#include <wrl.h>
#include <d3d12.h>
#include "d3dx12.h"
#include "bclib/podtypes.h"

namespace bc9 {

enum D12BufferViewType {
    eD12BufferViewIndex,
    eD12BufferViewConstant,
    eD12BufferViewInput,
    eD12BufferViewOutput,
    eD12BufferViewTypeCount
};

struct BufferResource {
    // platform-independent interface
    BufferResource() {}
    // "instances" refers to the number of copies of data in the buffer, so
    // e.g. a double-buffered setup would specify 2, tripled-buffered 3, etc.
    // set to 1 if only a single copy of the data is required.
    // use a stride of kInvalidUInt32 if stride doesn't apply (e.g. cbuffers).
    BufferResource(uint64 instancesize, uint32 instances, uint32 stride) :
                                                                   instancesize(instancesize),
                                                                   instances(instances),
                                                                   stride(stride) {
        assert(instancesize);
        assert(instances);
        assert(stride);
        assert(stride == kInvalidUInt32 || (instancesize % stride == 0));
    }

    void* Map(uint8 instance) {
        assert(mappedmem);
        return (char*)(mappedmem) + instance * instancesize;
    }

    uint64 GetSize() const { return instances*instancesize; }
    uint64 GetInstanceSize() const { return instancesize; }

    uint32 GetStride() const { return stride; }
    uint32 GetInstances() const { return instances; }
    uint64 GetElements() const { return stride == kInvalidUInt32 ? 1 : instancesize / stride; }

    operator bool() { return instancesize != 0; }

    // platform-specific stuff
    void* mappedmem = nullptr;
    D12BufferViewType viewtype;
    union {
        D3D12_INDEX_BUFFER_VIEW ibv;
        D3D12_CONSTANT_BUFFER_VIEW_DESC cbv;
        D3D12_SHADER_RESOURCE_VIEW_DESC  srv;
        D3D12_UNORDERED_ACCESS_VIEW_DESC uav;
    };
    Microsoft::WRL::ComPtr<ID3D12Resource> resource;

private:
    uint64 instancesize = 0;
    uint32 instances = 0;
    uint32 stride = 0;
};

}
