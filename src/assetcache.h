#pragma once

#include "bclib/stdjson.h"
#include "bclib/geoasset.h"
#include "imageasset.h"
#include "samplerasset.h"
#include "shaderasset.h"
#include "textureasset.h"
#include "bufferresource.h"
#include "imageresource.h"
#include "shaderresource.h"
#include "accelresource.h"
#include "cachekeys.h"
#include "geodata.h"

#include "bclib/stdvector.h"

namespace bc9 {

class SessionContext;

class AssetCache {
public:
    AssetCache(const stdjson& graphjson);

    void Initialize(SessionContext &scontext);
    void Shutdown(SessionContext &scontext);

    ImageResource  GetImage(const ImageKey& key) const;
    ShaderResource GetShader(const ShaderKey& key) const;
    BufferResource GetGeoVertexBuffer() const;
    BufferResource GetGeoIndexBuffer() const;
    BufferResource GetGeoVertexColorBuffer() const;
    BufferResource GetGeoSRGBVertexColorBuffer() const;
    BufferResource GetGeoTex1CoordBuffer() const;
    BufferResource GetGeoTangentBuffer() const;
    BLASResource   GetBLAS(const GeoKey& key) const;

    GeoKey     FindGeo(const StringID& name) const;
    ImageKey   FindImage(const StringID& name) const;
    SamplerKey FindSampler(const StringID& name) const;
    TextureKey FindTexture(const StringID& name) const;
    ShaderKey  FindShader(const StringID& name, ShaderAssetType type) const;

    GeoAsset            GetGeoAsset(const GeoKey& key) const;
    ImageAsset          GetImageAsset(const ImageKey& key) const;
    SamplerAsset        GetSamplerAsset(const SamplerKey& key) const;
    const TextureAsset& GetTextureAsset(const TextureKey& key) const;
    const ShaderAsset*  GetShaderAsset(const ShaderKey& key) const;

    uintn GetSamplerCount() const;
    uintn GetImageCount() const;
    uintn GetGeoTotalIndexCount() const;

    void GetGeoData(const GeoKey& key, GeoData& geodata) const;
    void GetGeoDrawData(const GeoKey& key, GeoDrawData& geodrawdata) const;

private:
    uintn geototalindexcount = 0;

    Result LoadAssets(const stdjson& gjson);

    void BuildBottomAccelStructures(SessionContext &scontext);

    stdvector<GeoAsset> geoassets;
    stdvector<GeoData> geodatas;

    BufferResource geoindexresource;
    BufferResource geovertexresource;
    BufferResource geovcolorresource;
    BufferResource geosrgbvcolorresource;
    BufferResource geotex1coordresource;
    BufferResource geotangentresource;

    stdvector<TextureAsset> textureassets;

    stdvector<ImageAsset> imageassets;
    stdvector<ImageResource> imageresources;

    stdvector<SamplerAsset> samplerassets;

    stdvector<ShaderAsset> shaderassets;
    stdvector<ShaderResource> shaderresources;

    stdvector<BLASResource> blasresources;
};

}
