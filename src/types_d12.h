#pragma once

#include "d3dx12.h"

namespace bc9 {

class PlatformContext;

// I previously had each of these handles maintain a "starthandle", so that they could
// be "reset" after doing offsets, but this turned out to be a bad design, because now
// there's two handles instead of one, and assignments/copy ctors are complicated because
// you don't necessarily want the start of the target handle to be the same as the
// start of the dest handle, etc. Just gross. You can get by just fine by doing an
// assignment first, and then offseting the copied handle, leaving the original intact.
// Basically, treat them like pointers.

class SRVHandle {
public:
    SRVHandle();

    void Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu);
    void Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu, D3D12_GPU_DESCRIPTOR_HANDLE gpu);
    void Offset(const PlatformContext& context, unsigned int units = 1);

    CD3DX12_CPU_DESCRIPTOR_HANDLE cpuhandle, cpustart;
    CD3DX12_GPU_DESCRIPTOR_HANDLE gpuhandle, gpustart;

    bool gpuenabled = false;
};

class SamplerHandle {
public:
    SamplerHandle();

    void Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu);
    void Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu, D3D12_GPU_DESCRIPTOR_HANDLE gpu);
    void Offset(const PlatformContext& context, unsigned int units = 1);

    CD3DX12_CPU_DESCRIPTOR_HANDLE cpuhandle, cpustart;
    CD3DX12_GPU_DESCRIPTOR_HANDLE gpuhandle, gpustart;

    bool gpuenabled = false;
};

class RTVHandle {
public:
    RTVHandle();

    void Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu);
    void Offset(const PlatformContext& context, unsigned int units = 1);
    bool operator==(const RTVHandle& rhs) const { return cpuhandle == rhs.cpuhandle; }

    CD3DX12_CPU_DESCRIPTOR_HANDLE cpuhandle;
};

class DSVHandle {
public:
    DSVHandle();

    void Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu);
    void Offset(const PlatformContext& context, unsigned int units = 1);

    CD3DX12_CPU_DESCRIPTOR_HANDLE cpuhandle;
};

}