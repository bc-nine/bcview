#include "skybox.h"

#include "assetcache.h"
#include "sessioncontext.h"
#include "world.h"

namespace bc9 {

Skybox::Skybox(SessionContext& scontext) {
    vertexkey = scontext.assetcache.FindShader("skybox", eShaderAssetVertex);
    pixelkey = scontext.assetcache.FindShader("skybox", eShaderAssetPixel);
    bool hasshader = (vertexkey != kInvalidUInt && pixelkey != kInvalidUInt);

    geokey = scontext.assetcache.FindGeo("skybox");
    bool hasgeo = (geokey != kInvalidUInt);

    enabled = hasshader && hasgeo;
}

void Skybox::Initialize(SessionContext& scontext, FramebufferLayoutData& layoutdata) {
    if (!enabled)
        return;

    CreateConstantBufferParams params(scontext.pc, sizeof(SkyboxConstants), "skybox-constants");
    Throw_(CreateBuffer(scontext.pc, params, cbuffer));

    GeoData geodata;
    scontext.assetcache.GetGeoData(geokey, geodata);
    constants = { .pntoffset = geodata.pntoffset };

    cbufferset.custom0 = &cbuffer;
    drawdata.pipelinebase = GraphicsPipelineBase(scontext.pc, vertexkey, pixelkey);
    drawdata.geodrawdata = geodata.drawdata;
    drawdata.cbufferset = &cbufferset;
}

void Skybox::Shutdown(SessionContext& scontext) {
    if (!enabled)
        return;

    DestroyBuffer(scontext.pc, cbuffer);
}

void Skybox::Update(SessionContext& scontext) {
    if (!enabled)
        return;

    const Background& background = scontext.world.GetActiveBackground();
    if (background.Mode() == eBackgroundMap) {
        hasmap = true;
        BackgroundMapData mapdata = background.MapData();
        constants.sampleridx = mapdata.sampleridx;
        constants.imageidx = mapdata.imageidx;
        cbuffer.UpdateAndSwap(&constants);
    }
    else {
        hasmap = false;
    }
}

bool Skybox::Active() const {
    return enabled && hasmap;
}

}