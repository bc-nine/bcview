#include "overlay.h"
#include "imgui.h"
#include "appheaps.h"
#include "sessioncontext.h"
#include "world.h"
#include "imgui_impl_sdl2.h"
#include "imgui_impl_vulkan.h"



namespace bc9 {

void Overlay::UpdateMenuBar(SessionContext& scontext) {
    if (ImGui::BeginMenu("App")) {
        ImGui::Checkbox("Memory Usage", &memoryview);
        ImGui::Checkbox("ImGui Demo", &demoview);
        ImGui::EndMenu();
    }

    scontext.world.OverlayUpdateMenuBar(scontext);
}

void Overlay::Display(SessionContext& scontext) {
    if (memoryview)
        DisplayMemoryUsage();
    if (demoview)
        DisplayDemo(scontext);

    scontext.world.OverlayUpdate(scontext);
}

void Overlay::DisplayDemo(SessionContext& scontext) {
    ImGui::ShowDemoWindow();
}

void Overlay::DisplayMemoryUsage() {
    ImGui::Begin("CPU Memory Usage");
    unsigned int heapcount = Heaps().Count();
    ImGui::Columns(3, "mycolumns3", false);  // 3-ways, no border
    ImGui::SetColumnWidth(0, 120);
    ImGui::SetColumnWidth(1, 160);
    ImGui::Text("Heap");
    ImGui::NextColumn();
    ImGui::Text("Used");
    ImGui::NextColumn();
    ImGui::Text("Size");
    ImGui::NextColumn();
    ImGui::Separator();
    static char buf[512];
    size_t totalused = 0, totalsize = 0;

    static ImColor green(0, 255, 0);
    static ImColor yellow(255, 255, 0);
    static ImColor red(255, 0, 0);

    for (unsigned int i = 0 ; i < Heaps().Count(); ++i) {
        HeapInfo info(Heaps().GetInfo(i));
        totalused += info.used;
        totalsize += info.heapsize;
        ImColor healthcol;
        if (info.health < 0.1f)
            healthcol = red;
        else if (info.health < 0.25f)
            healthcol = yellow;
        else
            healthcol = green;
        ImGui::PushStyleColor(ImGuiCol_Text, (ImVec4)healthcol);
        ImGui::Text("%s", info.heapname);
        ImGui::PopStyleColor(1);
        ImGui::NextColumn();
        float health = 1.0f - float(info.used) / info.heapsize;
        if (health < 0.1f)
            healthcol = red;
        else if (health < 0.25f)
            healthcol = yellow;
        else
            healthcol = green;
        size_t mb = info.used / 1048576;
        size_t rem = (info.used % 1048576);
        size_t mbfrac = int((float(rem) / 1048576.0f) * 10.0f);
        sprintf_s(buf, 512, "%zu.%zu MB (%zu)", mb, mbfrac, info.used);
        ImGui::PushStyleColor(ImGuiCol_Text, (ImVec4)healthcol);
        ImGui::Text(buf);
        ImGui::PopStyleColor(1);
        ImGui::NextColumn();
        mb = info.heapsize / 1048576;
        rem = (info.heapsize % 1048576);
        mbfrac = int((float(rem) / 1048576.0f) * 10.0f);
        sprintf_s(buf, 512, "%zu.%zu MB", mb, mbfrac);
        ImGui::Text(buf);
        ImGui::NextColumn();
    }
    ImGui::Separator();
    ImGui::Text("total");
    ImGui::NextColumn();
    size_t mb = totalused / 1048576;
    size_t rem = (totalused % 1048576);
    size_t mbfrac = int((float(rem) / 1048576.0f) * 10.0f);
    sprintf_s(buf, 512, "%zu.%zu MB (%zu)", mb, mbfrac, totalused);
    ImGui::Text(buf);
    ImGui::NextColumn();
    mb = totalsize / 1048576;
    rem = (totalsize % 1048576);
    mbfrac = int((float(rem) / 1048576.0f) * 10.0f);
    sprintf_s(buf, 512, "%zu.%zu MB", mb, mbfrac);
    ImGui::Text(buf);

    ImGui::End();
}

void Overlay::BeginImGuiFrame(SessionContext& scontext) {
    ImGui_ImplVulkan_NewFrame();
    ImGui_ImplSDL2_NewFrame(scontext.pc.sdlwin);
    ImGui::NewFrame();
}

void Overlay::EndImGuiFrame(SessionContext& scontext) {
    ImGui::Render();
    ImGui_ImplVulkan_RenderDrawData(ImGui::GetDrawData(), scontext.pc.current.cmdbuffer.vkobj);
}

bool Overlay::ProcessWindowEvent(SessionContext& scontext, WindowEvent event) {
    if (scontext.EnableOverlay()) {
        ImGui_ImplSDL2_ProcessEvent(event.evt);
        ImGuiIO& io = ImGui::GetIO();
        return (io.WantCaptureMouse || io.WantCaptureKeyboard);
    }

    return false;
}


}
