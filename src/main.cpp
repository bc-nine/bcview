#include "renderer.h"
#include "session.h"
#include "window.h"
#include "bclib/stdjson.h"
#include "bclib/timeutils.h"
#include "testharness.h"

#define STB_DEFINE
#include "stb.h"

using namespace glm;
using namespace bc9;

using namespace std;

#define PRINT_FRAME_TIME 0

const FString ScreenshotDir = "C:\\Users\\bcollins\\Desktop";

void Cleanup(Session*& session, Window& window) {
    window.Shutdown();

    if (session) {
        if (session->testharness) {
            session->testharness->Shutdown(*session);
            delete session->testharness;
        }
        session->Shutdown();
        delete session;
        session = nullptr;
    }

    Heaps().ResetSessionHeaps();
}

void ReloadSession(Session*& session, const FString& worldname, const char *dataroot,
                   Window &window, uintn xrmode, TestHarness::Mode testmode) {
    bool initialized = false;
    while (!initialized) {
        stdjson bundlejson;
        FString bundlefile("%s\\%s", dataroot, "bundle.json");
        Throw_(ReadJSONFile(bundlefile.str(), bundlejson));

        bool found = false;
        glm::uvec2 size(1500, 1100);
        Throw_(OptUInt2(bundlejson["world"]["render-settings"], "surface-size", size, found));

        // FIXME: temp!
        if (xrmode > 0) {
            size.x = 2064;
            size.y = 2096;
        }
        window.Initialize("BCVIEW", size.x, size.y);

        OutputDevice outdevice = (xrmode == 0) ? eOutputSystem : eOutputQuest2;

        session = new Session(bundlejson, xrmode);
        session->Initialize(window, outdevice);

        if (testmode != TestHarness::eNone) {
            session->testharness = new TestHarness(worldname, testmode);
            session->testharness->Initialize(*session);
        }

        if (session->world.RayTracingEnabled())
            window.SetName("BCView - Ray Tracing");

        initialized = true;
    }
}

#define MAX_PATH_LEN 4096
char g_pathbuf[MAX_PATH_LEN];
StringID g_program;

void Usage() {
    stb_splitpath(g_pathbuf, (char*)g_program.str(), STB_FILE);
    printf("usage: %s [--xrmode=0|1|2][--run-test|--capture-reference] <world name>\n", g_pathbuf);
    exit(1);
}

int main(int argc, char *argv[]) {
    g_program = argv[0];
    if(argc < 2 || argc > 5)
        Usage();

    bool runtest = false;
    bool capturereference = false;
    uintn xrmode = 0;
    FString worldname;

    uint32 posarg = 0;
    for (uint32 i = 1; i < uint32(argc); ++i) {
        StringID arg = argv[i];
        if (arg == "--run-test"_sid) {
            runtest = true;
        }
        else if (arg == "--capture-reference"_sid) {
            capturereference = true;
        }
        else if (arg == "--xrmode=1"_sid) {
            xrmode = 1;
        }
        else if (arg == "--xrmode=2"_sid) {
            xrmode = 2;
        }
        else if (arg.str()[0] == '-') {
            Usage();
        }
        else {
            if (posarg == 0)
                worldname = argv[i];
            else
                Usage();
            ++posarg;
        }
    }

    TestHarness::Mode testmode = TestHarness::eNone;
    if (runtest && capturereference)
        Usage();
    else if (runtest)
        testmode = TestHarness::eRunTest;
    else if (capturereference)
        testmode = TestHarness::eCaptureReference;

    // Ensure required positional args were retrieved (only needed if opt args take args)
    if (worldname.str() == nullptr)
        Usage();

#if winvk_
    FString dataroot = FString("P:/data/winvk/%s", worldname.str());
#elif wind12_
    FString dataroot = FString("P:/data/wind12/%s", worldname.str());
#endif

    Window window(ScreenshotDir, worldname);

    bool quit = false;

    Session* session = nullptr;

    bool requestrestart = false;

    Heaps().ResetSessionHeaps();

    try {
        do {
            ReloadSession(session, worldname, dataroot.str(), window, xrmode, testmode);

            bool paused = false;

            while (!quit) {
                bool exitrenderloop = false;
                window.ProcessEvents(session, quit, exitrenderloop, requestrestart, paused);

                static auto prevtime = std::chrono::high_resolution_clock::now();
                auto currtime = std::chrono::high_resolution_clock::now();

                float dt = chrono::duration<float, std::milli>(currtime - prevtime).count();

                prevtime = chrono::high_resolution_clock::now();

            #if PRINT_FRAME_TIME
                static double sum = 0;
                static int frame =  0;
                static constexpr unsigned int period = 128;

                if (frame % period == 0) {
                    static double frac = 1.0/period;
                    double ft = frac * sum;
                    printf("CPU frame time: %0.2f\n", ft);
                    sum = 0.0;
                    frame = 0;
                }
                sum += dt;
                ++frame;
            #endif
                if (session->testharness) {
                    Result res = session->testharness->Tick(*session, dt, paused, quit);
                    if (!res)
                        window.ShowErrorBox("Test Error", res.Message());
                }
                else {
                    if (exitrenderloop) // was e.g. 'r' key pressed? handle before ProcessEvents()
                        break;
                    session->cameracontrols.Update(session->context, dt);
                    if (xrmode > 0) {
                        session->headset.ProcessEvents(session->context.pc, exitrenderloop,
                                                       requestrestart);
                        if (exitrenderloop)
                            break;
                        if (session->headset.SessionRunning()) {
                            //session->headset.PollActions();
                            session->renderer.RenderFrame(session->context, dt);
                        }
                    }
                    else {
                        session->renderer.RenderFrame(session->context, dt);
                    }
                }
            }

            Cleanup(session, window);
        } while (!quit && requestrestart);

        return 0;
    }
    catch (const runtime_error& error) {
        FString msg("%s. \nPlease fix the error and click OK to retry.", error.what());
        window.ShowErrorBox("Error", msg);
        return 1;
    }
}