#pragma once

#include "platformcontext.h"
#include "passcache.h"
#include "pipelinecache.h"

namespace bc9 {

class World;
class IMDraw;
class Window;
class MaterialCache;
class DrawableCache;
class ModelCache;
class NodeCache;
class Renderer;
class XRHeadset;
class Surface;

class SessionContext {
public:
    SessionContext (World& world,
                    IMDraw& imdraw,
                    AssetCache& assetcache,
                    Surface& surface,
                    XRHeadset& headset,
                    MaterialCache& materialcache,
                    DrawableCache& drawablecache,
                    NodeCache& nodecache,
                    Renderer& renderer,
                    uintn xrmode);

    void Initialize(Window& window, OutputDevice device);
    void Shutdown();

    void PostInitialize();

    bool EnableOverlay();
    void ToggleOverlay();

    AssetCache& assetcache;

    PlatformContext pc;

    World& world;

    IMDraw& imdraw;

    MaterialCache& materialcache;

    DrawableCache& drawablecache;

    NodeCache& nodecache;

    PassCache passcache;

    PipelineCache pipelinecache;

    Renderer& renderer;

    bool enableoverlay = false;
};

}
