#pragma once

struct SDL_Window;

#include "SDL_events.h"
#include "bclib/fstring.h"

namespace bc9 {

class Session;

class Window {
public:
    Window(const FString& screenshotdir, const FString& worldname);

    void Initialize(const FString& name, uintn width, uintn height);
    void Shutdown();

    void SetName(const FString& name);

    void ProcessEvents(Session* session, bool& quit, bool& exitrenderloop, bool& requestrestart,
                       bool& paused);

    void ShowErrorBox(const FString& title, const FString& message);

    SDL_Window *sdlobj = nullptr;

    const FString screenshotdir;
    const FString worldname;
};

class WindowEvent {
public:
    WindowEvent(SDL_Event *evt) : evt(evt) {}
    SDL_Event *evt;
};

}