#include "commandbuffer_vk.h"

#include "world.h"
#include "sessioncontext.h"
#include "computecommands_vk.h"

namespace bc9 {

void CommandBuffer::BeginGraphics(SessionContext& scontext) {
    PlatformContext& context = scontext.pc;
    VkCommandBufferBeginInfo beginInfo = {};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    beginInfo.pInheritanceInfo = nullptr; // Optional

    if (vkBeginCommandBuffer(vkobj, &beginInfo) != VK_SUCCESS) {
        throw std::runtime_error("failed to begin recording command buffer!");
    }

    // BEWARE renderdoc may crash during capture load if you have ray tracing things enabled!
    //
    // note that for graphics, that's handled in BeginRenderPass() (see mar 11 2023)
    scontext.pc.BindBindlessInputSet(ePipelineCompute);
    if (scontext.world.RayTracingEnabled())
        scontext.pc.BindBindlessInputSet(ePipelineRayTracing);

    Compute::PushConstantBuffers(scontext.pc, { .engine0 = &context.pfconstantsbuf });
    if (scontext.world.RayTracingEnabled())
        Shader::PushConstantBuffers(scontext.pc, { .engine0 = &context.pfconstantsbuf },
                                    ePipelineRayTracing);
}

void CommandBuffer::EndGraphics(SessionContext& scontext) {
    if (vkEndCommandBuffer(vkobj) != VK_SUCCESS)
        throw std::runtime_error("failed to record command buffer!");
}

}