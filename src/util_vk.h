#pragma once

#include "shaderasset.h"
#include "samplerasset.h"
#include "volk.h"
#include "accelresource_vk.h"

namespace bc9 {

class PlatformContext;

constexpr unsigned int MaxLODLevels = 16; // 32768x32768 image size

#define VkCheck_(R) do { auto r = R; if (r != VK_SUCCESS) VkCheck(r); } while(0)

inline void VkCheck(VkResult vkr) {
    __debugbreak();

    throw std::runtime_error("vulkan error");
}

struct RayTracingScratchBuffer {
    uint64_t deviceaddress = 0;
    BufferResource buffer;
};

struct SwapchainSupportDetails {
    VkSurfaceCapabilitiesKHR capabilities;
    stdvector<VkSurfaceFormatKHR> formats;
    stdvector<VkPresentModeKHR> presentmodes;
};

struct QueueFamilyIndices {
    uint32_t graphicsfamily = -1;
    uint32_t presentfamily = -1;
    uint32_t asynccomputefamily = -1;

    bool IsComplete() {
        return graphicsfamily >= 0 && presentfamily >= 0;
    }
};

#if ENABLE_VALIDATION
template <typename T>
void NameVulkanObject(VkDevice device, VkObjectType objecttype, T objecthandle,
                      const char* objectname) {
    if (vkSetDebugUtilsObjectNameEXT) {
        VkDebugUtilsObjectNameInfoEXT nameinfo{};
        nameinfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_OBJECT_NAME_INFO_EXT;
        nameinfo.objectType = objecttype;
        nameinfo.objectHandle = (uint64)objecthandle;
        nameinfo.pObjectName = objectname;
        vkSetDebugUtilsObjectNameEXT(device, &nameinfo);
    }
    if (vkDebugMarkerSetObjectNameEXT) {
        VkDebugMarkerObjectNameInfoEXT nameinfo{};
        nameinfo.sType = VK_STRUCTURE_TYPE_DEBUG_MARKER_OBJECT_NAME_INFO_EXT;
        nameinfo.objectType = (VkDebugReportObjectTypeEXT) objecttype;
        nameinfo.object = (uint64) objecthandle;
        nameinfo.pObjectName = objectname;
        vkDebugMarkerSetObjectNameEXT(device, &nameinfo);
    }
}
#else
    #define NameVulkanObject(device, objecttype, objecthandle, objectname)
#endif

VkTransformMatrixKHR GetTransformMatrix(glm::mat4x3 m);

uint64 AlignedInstanceSize(PlatformContext& context, BufferResourceType type, uint64 size);

uint64_t GetBufferDeviceAddress(PlatformContext& context, VkBuffer buffer);

Result GetMemoryPropertyFlags(BufferCPUAccessMode accessmode, VkMemoryPropertyFlags& flags);
Result GetMemoryPropertyFlags(ImageCPUAccessMode accessmode, VkMemoryPropertyFlags& flags);

Result GetPipelineBindPoint(PipelineType pipelinetype, VkPipelineBindPoint &bindpoint);

Result GetVkSamplerFilter(SamplerFilter& filter, VkFilter& vkfilter);
Result GetVkSamplerMipmapFilter(SamplerMipmapFilter& filter, VkSamplerMipmapMode& vkfilter);
Result GetVkSamplerMode(SamplerMode& mode, VkSamplerAddressMode& vkmode);

Result GetVkCompareOp(CompareOp op, VkCompareOp& vkop);

Result GetVkAttachmentLoadOp(ColorLayoutFlags, VkAttachmentLoadOp& vkop);
Result GetVkAttachmentStoreOp(ColorLayoutFlags, VkAttachmentStoreOp& vkop);

Result GetVkAttachmentDepthLoadOp(DepthStencilLayoutFlags, VkAttachmentLoadOp& vkop);
Result GetVkAttachmentDepthStoreOp(DepthStencilLayoutFlags, VkAttachmentStoreOp& vkop);

Result GetVkAttachmentStencilLoadOp(DepthStencilLayoutFlags, VkAttachmentLoadOp& vkop);
Result GetVkAttachmentStencilStoreOp(DepthStencilLayoutFlags, VkAttachmentStoreOp& vkop);

Result GetVkStencilOp(StencilOp op, VkStencilOp& vkop);

Result GetVkImageLayout(ImageLayout layout, VkImageLayout& vklayout);

Result GetVkCullMode(CullMode mode, VkCullModeFlags& flags);

Result GetVkPolygonMode(PolygonMode mode, VkPolygonMode& vkmode);

Result GetVkPrimitiveTopology(PrimitiveTopology topology, VkPrimitiveTopology& vktopology);

Result GetVkSampleCount(unsigned int numsamples, VkSampleCountFlagBits& bits);

Result GetDescriptorType(const StringID &strtype, VkDescriptorType& dtype);

Result GetLocationFormat(const StringID &strformat, VkFormat& format);

Result GetMemoryType(VkPhysicalDeviceMemoryProperties availableproperties,
                     uint32_t typebits, VkMemoryPropertyFlags properties, uint32_t &index);

Result GetVkFormat(ResourceFormat format, VkFormat& vkformat);
Result GetResourceFormat(VkFormat vkformat, ResourceFormat& format);

Result GetVkShaderStage(ShaderAssetType type, VkShaderStageFlagBits& stage);

Result CreateSampler(PlatformContext& context, VkSamplerCreateInfo& createinfo,
                     VkSampler& sampler, const StringID& name);

Result CreateRenderPass(PlatformContext& context, VkRenderPassCreateInfo& renderpassinfo,
                        VkRenderPass& renderpass, const StringID& name);

Result CreateDescriptorSetLayout(PlatformContext& context,
                                 VkDescriptorSetLayoutCreateInfo& createinfo,
                                 VkDescriptorSetLayout& layout, const StringID& name);

Result CreatePipelineLayout(PlatformContext& context, VkPipelineLayoutCreateInfo& createinfo,
                            VkPipelineLayout& layout, const StringID& name);

Result CreateAccelStructureBuffer(PlatformContext& context,
                                  BufferResource &accelbuffer,
                                  VkAccelerationStructureBuildSizesInfoKHR& buildsizeinfo);

Result CreateScratchBuffer(PlatformContext& context, VkDeviceSize size,
                           RayTracingScratchBuffer& scratchbuffer);
void DeleteScratchBuffer(PlatformContext& context, RayTracingScratchBuffer& scratchbuffer);

void PrintDeviceExtensions(VkPhysicalDevice device);

Result FindQueueFamilies(VkPhysicalDevice device, VkSurfaceKHR vksurface,
                         QueueFamilyIndices& indices);

bool IsDeviceSuitable(VkPhysicalDevice device, VkSurfaceKHR vksurface,
                      const stdvector<const char*>& deviceextensions);

VkPhysicalDevice PickPhysicalDevice(VkInstance instance, VkSurfaceKHR vksurface,
                                    stdvector<const char*>& deviceextensions);

bool ChoosePhysicalDevice(VkInstance instance, VkSurfaceKHR vksurface,
                          stdvector<const char*>& deviceextensions,
                          VkPhysicalDevice& physicaldevice);

bool CheckDeviceExtensionSupport(VkPhysicalDevice device,
                                 const stdvector<const char*>& deviceextensions);

SwapchainSupportDetails QuerySwapchainSupport(VkPhysicalDevice device, VkSurfaceKHR vksurface);

}