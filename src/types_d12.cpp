#include "types_d12.h"
#include "platformcontext.h"

namespace bc9 {

SRVHandle::SRVHandle() {
}

void SRVHandle::Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu) {
    cpustart = cpuhandle = cpu;
}

void SRVHandle::Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu, D3D12_GPU_DESCRIPTOR_HANDLE gpu) {
    cpustart = cpuhandle = cpu;
    gpustart = gpuhandle = gpu;

    gpuenabled = true;
}

void SRVHandle::Offset(const PlatformContext& context, unsigned int units) {
    cpuhandle.Offset(units, context.srvdescsize);
    if (gpuenabled)
        gpuhandle.Offset(units, context.srvdescsize);
}

SamplerHandle::SamplerHandle() {
}

void SamplerHandle::Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu) {
    cpustart = cpuhandle = cpu;
}

void SamplerHandle::Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu, D3D12_GPU_DESCRIPTOR_HANDLE gpu) {
    cpustart = cpuhandle = cpu;
    gpustart = gpuhandle = gpu;

    gpuenabled = true;
}

void SamplerHandle::Offset(const PlatformContext& context, unsigned int units) {
    cpuhandle.Offset(units, context.samplerdescsize);
    if (gpuenabled)
        gpuhandle.Offset(units, context.samplerdescsize);
}

RTVHandle::RTVHandle() {
}

void RTVHandle::Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu) {
    cpuhandle = cpu;
}

void RTVHandle::Offset(const PlatformContext& context, unsigned int units) {
    cpuhandle.Offset(units, context.rtvdescsize);
}

DSVHandle::DSVHandle() {
}

void DSVHandle::Set(D3D12_CPU_DESCRIPTOR_HANDLE cpu) {
    cpuhandle = cpu;
}

void DSVHandle::Offset(const PlatformContext& context, unsigned int units) {
    cpuhandle.Offset(units, context.dsvdescsize);
}

}