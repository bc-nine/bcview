#pragma once

#include "pipelines.h"
#include "shadercommands.h"

class PlatformContext;

namespace bc9 {

namespace Compute {

struct DispatchParams {
    uintn xgroups = 0;
    uintn ygroups = 1;
    uintn zgroups = 1;
};

void PushConstantBuffers(PlatformContext& context, const CBufferSet& cbufferset);
void PushConstantBuffers(PlatformContext& context, CommandBuffer& cmdbuffer,
                         const CBufferSet& cbufferset);

void Compute(PlatformContext& context,
             Pipeline& pipeline,
             const CBufferSet& cbufferset,
             const DispatchParams& params);

void Compute(PlatformContext& context,
             CommandBuffer& cmdbuffer,
             Pipeline& pipeline,
             const CBufferSet& cbufferset,
             const DispatchParams& params);

}

}
