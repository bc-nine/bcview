
#include "platformcontext.h"
#include "heaps_d12.h"
#include "util_d12.h"
#include "d3dx12.h"
#include "bclib/dxutils.h"

namespace bc9 {

D12Heaps::D12Heaps() {
}

void D12Heaps::Initialize(Microsoft::WRL::ComPtr<ID3D12Device2> device) {
    D3D12_DESCRIPTOR_HEAP_DESC srvheapdesc = {};
    srvheapdesc.NumDescriptors = 1000;
    srvheapdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
    srvheapdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
    DxThrow_(device->CreateDescriptorHeap(&srvheapdesc, IID_PPV_ARGS(&srvheap)));
    startsrv.Set(srvheap->GetCPUDescriptorHandleForHeapStart(),
                 srvheap->GetGPUDescriptorHandleForHeapStart());

    D3D12_DESCRIPTOR_HEAP_DESC rtvheapdesc = {};
    rtvheapdesc.NumDescriptors = 10;
    rtvheapdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
    rtvheapdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    DxThrow_(device->CreateDescriptorHeap(&rtvheapdesc, IID_PPV_ARGS(&rtvheap)));
    startrtv.Set(rtvheap->GetCPUDescriptorHandleForHeapStart());

    D3D12_DESCRIPTOR_HEAP_DESC dsvheapdesc = {};
    dsvheapdesc.NumDescriptors = 10;
    dsvheapdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
    dsvheapdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
    DxThrow_(device->CreateDescriptorHeap(&dsvheapdesc, IID_PPV_ARGS(&dsvheap)));
    startdsv.Set(dsvheap->GetCPUDescriptorHandleForHeapStart());

    D3D12_DESCRIPTOR_HEAP_DESC samplerheapdesc = {};
    samplerheapdesc.NumDescriptors = 1000;
    samplerheapdesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER;
    samplerheapdesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
    DxThrow_(device->CreateDescriptorHeap(&samplerheapdesc, IID_PPV_ARGS(&samplerheap)));
    startsampler.Set(samplerheap->GetCPUDescriptorHandleForHeapStart(),
                     samplerheap->GetGPUDescriptorHandleForHeapStart());

    ResetSRVHeap();
    ResetRTVHeap();
    ResetDSVHeap();
    ResetSamplerHeap();
}

void D12Heaps::ResetSRVHeap() {
    nextsrv = startsrv;
}

void D12Heaps::ResetRTVHeap() {
    nextrtv = startrtv;
}

void D12Heaps::ResetDSVHeap() {
    nextdsv = startdsv;
}

void D12Heaps::ResetSamplerHeap() {
    nextsampler = startsampler;
}

SRVHandle D12Heaps::AllocateSRV(const PlatformContext& context, unsigned int descriptorcount) {
    SRVHandle handle(nextsrv);

    nextsrv.Offset(context, descriptorcount);

    return handle;
}

RTVHandle D12Heaps::AllocateRTV(const PlatformContext& context, unsigned int descriptorcount) {
    RTVHandle handle(nextrtv);

    nextrtv.Offset(context, descriptorcount);

    return handle;
}

DSVHandle D12Heaps::AllocateDSV(const PlatformContext& context, unsigned int descriptorcount) {
    DSVHandle handle(nextdsv);

    nextdsv.Offset(context, descriptorcount);

    return handle;
}

SamplerHandle D12Heaps::AllocateSampler(const PlatformContext& context,
                                        unsigned int descriptorcount) {
    SamplerHandle handle(nextsampler);

    nextsampler.Offset(context, descriptorcount);

    return handle;
}

bool D12Heaps::RTVHeapReset() const {
    return startrtv == nextrtv;
}

void D12Heaps::Shutdown() {
}

}
