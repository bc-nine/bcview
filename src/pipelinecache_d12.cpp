#include "pipelinecache.h"
#include "bclib/geoasset.h"
#include "assetcache.h"
#include "sessioncontext.h"
#include "util_d12.h"
#include "bclib/dxutils.h"

namespace bc9 {

typedef Microsoft::WRL::ComPtr<ID3D12PipelineState> PipelineObject;

static PipelineObject CreateGraphicsPipeline(const SessionContext &scontext,
                                             const GraphicsPipelineBase &base,
                                             const GraphicsPipelineState &state) {
    PipelineObject pipeline;

    const ShaderResource vresource = scontext.assetcache.GetShader(base.vertexkey);
    const ShaderResource fresource = scontext.assetcache.GetShader(base.pixelkey);

    struct PipelineStateStream
    {
        CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE pRootSignature;
        CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT InputLayout;
        CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY PrimitiveTopologyType;
        CD3DX12_PIPELINE_STATE_STREAM_VS VS;
        CD3DX12_PIPELINE_STATE_STREAM_PS PS;
        CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL depthstencil;
        CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat;
        CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS RTVFormats;
        CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER rasterizer;
        CD3DX12_PIPELINE_STATE_STREAM_SAMPLE_DESC msaa;
    } pipelinestatestream;

    D3D12_DEPTH_STENCIL_DESC dsdesc;
    dsdesc.DepthEnable = state.depthtest;
    dsdesc.DepthWriteMask = state.depthwrite ? D3D12_DEPTH_WRITE_MASK_ALL :
                                                 D3D12_DEPTH_WRITE_MASK_ZERO;
    dsdesc.StencilEnable = state.stenciltest;
    D3D12_COMPARISON_FUNC d12depthcomparefunc;
    Throw_(GetD12CompareOp(state.depthcompareop, d12depthcomparefunc));
    dsdesc.DepthFunc = d12depthcomparefunc;

    dsdesc.StencilReadMask = state.readmask;
    dsdesc.StencilWriteMask = state.writemask;

    // stencil front
    D3D12_STENCIL_OP d12frontfailop, d12frontpassop, d12frontdepthfailop;
    Throw_(GetD12StencilOp(state.frontfailop, d12frontfailop));
    Throw_(GetD12StencilOp(state.frontpassop, d12frontpassop));
    Throw_(GetD12StencilOp(state.frontdepthfailop, d12frontdepthfailop));
    dsdesc.FrontFace.StencilFailOp = d12frontfailop;
    dsdesc.FrontFace.StencilDepthFailOp = d12frontdepthfailop;
    dsdesc.FrontFace.StencilPassOp = d12frontpassop;

    D3D12_COMPARISON_FUNC d12frontcompareop;
    Throw_(GetD12CompareOp(state.frontcompareop, d12frontcompareop));
    dsdesc.FrontFace.StencilFunc =  d12frontcompareop;

    // stencil back
    D3D12_STENCIL_OP d12backfailop, d12backpassop, d12backdepthfailop;
    Throw_(GetD12StencilOp(state.backfailop, d12backfailop));
    Throw_(GetD12StencilOp(state.backpassop, d12backpassop));
    Throw_(GetD12StencilOp(state.backdepthfailop, d12backdepthfailop));
    dsdesc.BackFace.StencilFailOp = d12backfailop;
    dsdesc.BackFace.StencilDepthFailOp = d12backdepthfailop;
    dsdesc.BackFace.StencilPassOp = d12backpassop;

    D3D12_COMPARISON_FUNC d12backcompareop;
    Throw_(GetD12CompareOp(state.backcompareop, d12backcompareop));
    dsdesc.BackFace.StencilFunc =  d12backcompareop;

    CD3DX12_DEPTH_STENCIL_DESC xdepthstencil(dsdesc);

    D3D12_FILL_MODE fillmode;
    Throw_(GetPolygonMode(state.polygonmode, fillmode));
    D3D12_RASTERIZER_DESC rastdesc;
    rastdesc.FillMode = fillmode;

    D3D12_CULL_MODE d12cullmode;
    Throw_(GetD12CullMode(state.cullmode, d12cullmode));
    rastdesc.CullMode = d12cullmode;
    rastdesc.FrontCounterClockwise = TRUE;
    rastdesc.DepthBias = 0;
    rastdesc.DepthBiasClamp = 0.0f;
    rastdesc.SlopeScaledDepthBias = 0.0f;
    rastdesc.DepthClipEnable = TRUE;
    rastdesc.MultisampleEnable = FALSE;
    rastdesc.AntialiasedLineEnable = FALSE;
    rastdesc.ForcedSampleCount = 0;
    rastdesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

    CD3DX12_RASTERIZER_DESC xrastdesc(rastdesc);

    DXGI_SAMPLE_DESC msaadesc = {};
    msaadesc.Count = state.msaasamples;
    msaadesc.Quality = 0;
    pipelinestatestream.msaa = msaadesc;

    D3D12_RT_FORMAT_ARRAY rtvformats = {};
    rtvformats.NumRenderTargets = state.colorlayoutcount;
    for (unsigned int i = 0; i < state.colorlayoutcount; ++i)
        Throw_(GetDXGIFormat(scontext.pc, state.colorlayouts[i].format,rtvformats.RTFormats[i]));

    pipelinestatestream.pRootSignature = base.layout.rootsignature.Get();
    pipelinestatestream.InputLayout = { },
    pipelinestatestream.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
    pipelinestatestream.VS = CD3DX12_SHADER_BYTECODE(vresource.blob.Get());
    if (fresource.blob)
        pipelinestatestream.PS = CD3DX12_SHADER_BYTECODE(fresource.blob.Get());
    if (state.dslayoutcount)
        Throw_(GetDXGIFormat(scontext.pc, state.dslayout.format, pipelinestatestream.DSVFormat));
    pipelinestatestream.RTVFormats = rtvformats;
    pipelinestatestream.rasterizer = xrastdesc;
    pipelinestatestream.depthstencil = xdepthstencil;

    D3D12_PIPELINE_STATE_STREAM_DESC pipelinestatestreamdesc = {
        sizeof(PipelineStateStream), &pipelinestatestream
    };
    DxThrow_(scontext.pc.device->CreatePipelineState(&pipelinestatestreamdesc,
                                                      IID_PPV_ARGS(&pipeline)));

    return pipeline;
}

PipelineBaseKey
PipelineCache::RegisterBaseGraphicsPipeline(const SessionContext &scontext,
                                            const GraphicsPipelineBase &base,
                                            const GraphicsPipelineState &state) {
    PipelineBaseKey basekey;
    basekey.val = base.GetHash();

    auto iter(basegraphicspipelines.find(basekey));

    if (iter == basegraphicspipelines.end()) {
        Pipeline pipeline;
        pipeline.obj = CreateGraphicsPipeline(scontext, base, state);
        basegraphicspipelines[basekey] = std_make_pair(base, pipeline);
        PipelineStateKey statekey;
        statekey.val = state.GetHash(scontext.pc);
        pipelines[std_make_pair(basekey, statekey)] = pipeline;
    }

    return basekey;
}

Pipeline PipelineCache::GetGraphicsPipeline(const SessionContext &scontext,
                                            PipelineBaseKey basekey,
                                            const GraphicsPipelineState &state) {
    Pipeline pipeline;

    PipelineStateKey statekey;
    statekey.val = state.GetHash(scontext.pc);
    PipelineKeys key(std_make_pair(basekey, statekey));

    auto biter(basegraphicspipelines.find(basekey));
    assert(biter != basegraphicspipelines.end());
    const GraphicsPipelineBase &base((*biter).second.first);

    auto iter(pipelines.find(key));
    if (iter == pipelines.end()) {
        pipeline.obj = CreateGraphicsPipeline(scontext, base, state);
        pipelines[std_make_pair(basekey, statekey)] = pipeline;
    }
    else {
        pipeline = (*iter).second;
    }

    return pipeline;
}

void PipelineCache::Initialize(const SessionContext& scontext) {
    // TODO: get pipeline libraries going for disk caching
}

void PipelineCache::Shutdown(const SessionContext& scontext) {
    // TODO: get pipeline libraries going for disk caching
}

}
