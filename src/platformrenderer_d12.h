#pragma once

#include "bclib/stdvector.h"
#include "bclib/stdarray.h"
#include "drawdata_d12.h"
#include "constants.h"
#include "rendertarget.h"
#include "framebuffer.h"
#include "types_d12.h"

namespace bc9 {

class InputSet;
class Renderer;
class InputLayout;
class WindowEvent;
class SessionContext;
class FramebufferLayout;
struct PerFrameConstants;

class PlatformRenderer {
public:
    PlatformRenderer(Renderer& renderer);

    void Initialize(SessionContext& scontext);
    void Shutdown(SessionContext& scontext);

    void PostInitialize(SessionContext& scontext);

    bool ViewportResized(SessionContext& scontext, unsigned int width, unsigned int height);

    void BeginFrame(SessionContext& scontext);
    void EndFrame(SessionContext& scontext);

    void BeginGraphicsCommands(SessionContext& scontext);
    void EndGraphicsCommands(SessionContext& scontext);

    void BindInputSet(SessionContext& scontext, InputSet& inputset);

    void Render(SessionContext& scontext);
    void EndRender(SessionContext& scontext);

    void Update(SessionContext& scontext, PerFrameConstants& pfconstants);

    void DeviceWaitIdle(SessionContext& scontext);

    void SingleDraw(SessionContext& scontext, const Pipeline& pipeline, const DrawData& dd);

    bool ProcessEvent(SessionContext& scontext, WindowEvent& event);

    void BindPipeline(SessionContext& scontext, Pipeline& pipeline);

    void SetConstants(SessionContext& scontext, const Pipeline& pipeline, const DrawData& dd);

    void BindInputSet(SessionContext& scontext, PipelineType pipelinetype, InputSet& inputset);
    void BindIndexBuffer(SessionContext& scontext, BufferResource& indexbuffer, uint64_t offset);

    void BindBindlessInputSet(SessionContext& scontext, PipelineType pipelinetype);

    void BeginRenderPass(SessionContext& scontext, Framebuffer& framebuffer);
    void EndRenderPass(SessionContext& scontext, Framebuffer& framebuffer);

    void BeginImGuiFrame(SessionContext& scontext);
    void EndImGuiFrame(SessionContext& scontext);

    void CreateBindlessInputLayout(SessionContext& scontext, InputLayout& layout,
                                   PipelineType ptype);
    void CreateBindlessInputSet(SessionContext& scontext, InputLayout& layout, InputSet& inputset);

    D3D12_RECT scissorrect;

    D3D12_VIEWPORT viewport;

    BufferResource pfconstantsbuf;

    Microsoft::WRL::ComPtr<ID3D12CommandAllocator> commandallocators[kFrameRenderBufferCount];

    stdarray<RTVHandle, kFrameRenderBufferCount> swaphandles;

    bool initialized = false;

    Renderer& renderer;
};

}