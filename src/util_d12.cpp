#include "util_d12.h"
#include "platformcontext.h"
#include <stdexcept>

namespace bc9 {

Result GetD12CompareOp(CompareOp op, D3D12_COMPARISON_FUNC& d12op) {
    // ugh, d3d starts the enums at 1
    d12op = (D3D12_COMPARISON_FUNC) (op+1);

    return Success();
}

Result GetD12StencilOp(StencilOp op, D3D12_STENCIL_OP& d12op) {
    // ugh, d3d starts the enums at 1
    d12op = (D3D12_STENCIL_OP) (op+1);

    return Success();
}

Result GetD12CullMode(CullMode mode, D3D12_CULL_MODE& d12mode) {
    // ugh, d3d starts the enums at 1
    d12mode = (D3D12_CULL_MODE) (mode+1);

    return Success();
}

Result GetResourceState(ImageLayout layout, D3D12_RESOURCE_STATES& state) {
    switch (layout) {
        case eImageLayoutUndefined:
        // this might be incorrect, may need to revisit...
        state = D3D12_RESOURCE_STATE_GENERIC_READ;
        return Success();
        break;

        case eImageLayoutPresent:
        state = D3D12_RESOURCE_STATE_PRESENT;
        return Success();
        break;

        case eImageLayoutColorOutput:
        state = D3D12_RESOURCE_STATE_RENDER_TARGET;
        return Success();
        break;

        case eImageLayoutShaderReadOnly:
        state = D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE |
                D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
        return Success();
        break;

        case eImageLayoutShaderReadWrite:
        state = D3D12_RESOURCE_STATE_UNORDERED_ACCESS;
        return Success();
        break;

        case eImageLayoutDepthStencilOutput:
        state = D3D12_RESOURCE_STATE_DEPTH_WRITE;
        return Success();
        break;

        case eImageLayoutDepthStencilReadOnly:
        state = D3D12_RESOURCE_STATE_DEPTH_READ;
        return Success();
        break;

        default:
        return Error("GetResourceState(): invalid image layout specified");
        break;
    }

    return Success();
}

Result GetDescriptorRangeType(D3D_SHADER_INPUT_TYPE inputtype,
                              D3D12_DESCRIPTOR_RANGE_TYPE &rangetype) {
    switch (inputtype) {
        case D3D_SIT_CBUFFER:
        rangetype = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
        break;

        case D3D_SIT_TBUFFER:
        case D3D_SIT_TEXTURE:
        case D3D_SIT_STRUCTURED:
        case D3D_SIT_BYTEADDRESS:
        case D3D_SIT_RTACCELERATIONSTRUCTURE:
        rangetype = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
        break;

        case D3D_SIT_SAMPLER:
        rangetype = D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER;
        break;

        case D3D_SIT_UAV_RWTYPED:
        case D3D_SIT_UAV_RWSTRUCTURED:
        case D3D_SIT_UAV_RWBYTEADDRESS:
        case D3D_SIT_UAV_APPEND_STRUCTURED:
        case D3D_SIT_UAV_CONSUME_STRUCTURED:
        case D3D_SIT_UAV_RWSTRUCTURED_WITH_COUNTER:
        case D3D_SIT_UAV_FEEDBACKTEXTURE:
        rangetype = D3D12_DESCRIPTOR_RANGE_TYPE_UAV;
        break;

        default:
        return Error("GetDescriptorRangeType(): unknown input type");
        break;
    }

    return Success();
}

Result GetDXGIFormat(const PlatformContext& context, const ResourceFormat format,
                     DXGI_FORMAT& dxgiformat) {
    switch (format) {
        case eResourceFormatR8UInt:
        dxgiformat = DXGI_FORMAT_R8_UINT;
        break;

        case eResourceFormatRGBA8UNorm:
        dxgiformat = DXGI_FORMAT_R8G8B8A8_UNORM;
        break;

        case eResourceFormatRGBA8SRGB:
        dxgiformat = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
        break;

        case eResourceFormatRGBA32Float:
        dxgiformat = DXGI_FORMAT_R32G32B32A32_FLOAT;
        break;

        case eResourceFormatDepth24UNormStencil8UInt:
        dxgiformat = DXGI_FORMAT_D24_UNORM_S8_UINT;
        break;

        default:
        return Error("GetDXGIFormat(): unknown format");
        break;
    }

    return Success();
}

Result GetPolygonMode(PolygonMode mode, D3D12_FILL_MODE& d12mode) {
    switch (mode) {
        case ePolygonModeFill:
        d12mode = D3D12_FILL_MODE_SOLID;
        break;

        case ePolygonModeLine:
        d12mode = D3D12_FILL_MODE_WIREFRAME;
        break;

        case ePolygonModePoint:
        return Error("point polygon mode not valid for directx 12");
        break;

        default:
        return Error("invalid polygon mode specified");
        break;
    }

    return Success();
}

Result GetShaderInputType(const StringID& strtype, D3D_SHADER_INPUT_TYPE& type) {
    unsigned int value = -1;

    if (strtype == "D3D_SIT_CBUFFER"_sid)
        value = D3D_SIT_CBUFFER;
    else if (strtype == "D3D_SIT_STRUCTURED"_sid)
        value = D3D_SIT_STRUCTURED;
    else if (strtype == "D3D_SIT_SAMPLER"_sid)
        value = D3D_SIT_SAMPLER;
    else if (strtype == "D3D_SIT_TEXTURE"_sid)
        value = D3D_SIT_TEXTURE;

    if (value == -1)
        return Error("%s: invalid shader input type", strtype.str());

    type = (D3D_SHADER_INPUT_TYPE) value;

    return Success();
}

Result GetSRVDimension(const StringID& strdim, D3D_SRV_DIMENSION& dim) {
    unsigned int value = -1;

    if (strdim == "D3D_SRV_DIMENSION_UNKNOWN"_sid)
        value = D3D_SRV_DIMENSION_UNKNOWN;
    else if (strdim == "D3D_SRV_DIMENSION_TEXTURE2DARRAY"_sid)
        value = D3D_SRV_DIMENSION_TEXTURE2DARRAY;
    else if (strdim == "D3D_SRV_DIMENSION_BUFFER"_sid)
        value = D3D_SRV_DIMENSION_BUFFER;
    else if (strdim == "D3D_SRV_DIMENSION_TEXTURE2D"_sid)
        value = D3D_SRV_DIMENSION_TEXTURE2D;

    if (value == -1)
        return Error("%s: invalid srv dimension", strdim.str());

    dim = (D3D_SRV_DIMENSION) value;

    return Success();
}

}