#include "platformcontext_d12.h"
#include "util_d12.h"
#include "window_d12.h"
#include <dxgi1_6.h>
#include "platform_d12.h"
#include "d3dx12.h"
#include <wrl.h>
#include "constants.h"
#include "imageresource.h"
#include "sessioncontext.h"
#include "world.h"
#include "imgui.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx12.h"
#include "bclib/timeutils.h"
#include "externalallocators.h"
#include "bclib/dxutils.h"

using namespace Microsoft::WRL;

namespace bc9 {

PlatformContext::PlatformContext(const World& world) :
vsync(world.GetVSync()) {
}

static void EnableDebugLayer() {
#if defined(_DEBUG)
    // Always enable the debug layer before doing anything DX12 related
    // so all possible errors generated while creating DX12 objects
    // are caught by the debug layer.
    ComPtr<ID3D12Debug> debuginterface;
    DxThrow_(D3D12GetDebugInterface(IID_PPV_ARGS(&debuginterface)));
    debuginterface->EnableDebugLayer();

    ComPtr<ID3D12Debug> spdebugcontroller0;
    ComPtr<ID3D12Debug1> spdebugcontroller1;

    DxThrow_(D3D12GetDebugInterface(IID_PPV_ARGS(&spdebugcontroller0)));
    DxThrow_(spdebugcontroller0->QueryInterface(IID_PPV_ARGS(&spdebugcontroller1)));
    spdebugcontroller1->SetEnableGPUBasedValidation(true);
#endif
}

static void InitImGui(SessionContext& scontext, HWND window) {
    ImGui_ImplWin32_Init(window);

    FramebufferLayout flayout;
    D3D12_RENDER_TARGET_VIEW_DESC colordesc = {};
    scontext.passcache.GetDisplayFramebufferLayout(flayout);
    flayout.GetColorDescription(0, colordesc);

    SRVHandle handle = scontext.pc.heaps.AllocateSRV(scontext.pc, 1);

    ImGui_ImplDX12_Init(scontext.pc.device.Get(), scontext.pc.swapchainimagecount,
                        colordesc.Format, scontext.pc.heaps.srvheap.Get(), handle.cpuhandle,
                        handle.gpuhandle);
}

static unsigned int CheckMSAA(Microsoft::WRL::ComPtr<ID3D12Device2>& device,
                              unsigned int msaasamples) {
    D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS msaadesc = {};
    msaadesc.SampleCount = msaasamples;
    msaadesc.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
    msaadesc.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;

    while (msaadesc.SampleCount > 1) {
        HRESULT hr = device->CheckFeatureSupport(D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS,
                                                 &msaadesc, sizeof(msaadesc));
        if (SUCCEEDED(hr) && msaadesc.NumQualityLevels > 0)
            break;
        msaadesc.SampleCount--;
    }

    return msaadesc.SampleCount;
}

static bool CheckTearingSupport() {
    BOOL allowtearing = FALSE;

    // Rather than create the DXGI 1.5 factory interface directly, we create the
    // DXGI 1.4 interface and query for the 1.5 interface. This is to enable the
    // graphics debugging tools which will not support the 1.5 factory interface
    // until a future update.
    ComPtr<IDXGIFactory4> factory4;
    if (SUCCEEDED(CreateDXGIFactory1(IID_PPV_ARGS(&factory4)))) {
        ComPtr<IDXGIFactory5> factory5;
        if (SUCCEEDED(factory4.As(&factory5))) {
            if (FAILED(factory5->CheckFeatureSupport(
                DXGI_FEATURE_PRESENT_ALLOW_TEARING,
                &allowtearing, sizeof(allowtearing)))) {
                allowtearing = FALSE;
            }
        }
    }

    return allowtearing == TRUE;
}

static ComPtr<IDXGIAdapter4> GetAdapter(bool usewarp) {
    ComPtr<IDXGIFactory4> dxgifactory;
    UINT createfactoryflags = 0;

#if defined(_DEBUG)
    createfactoryflags = DXGI_CREATE_FACTORY_DEBUG;
#endif

    DxThrow_(CreateDXGIFactory2(createfactoryflags, IID_PPV_ARGS(&dxgifactory)));

    ComPtr<IDXGIAdapter1> dxgiadapter1;
    ComPtr<IDXGIAdapter4> dxgiadapter4;

    if (usewarp) {
        DxThrow_(dxgifactory->EnumWarpAdapter(IID_PPV_ARGS(&dxgiadapter1)));
        DxThrow_(dxgiadapter1.As(&dxgiadapter4));
    }
    else {
        SIZE_T maxdedicatedvideomemory = 0;
        for (UINT i = 0; dxgifactory->EnumAdapters1(i, &dxgiadapter1) != DXGI_ERROR_NOT_FOUND;++i){
            DXGI_ADAPTER_DESC1 dxgiadapterdesc1;
            dxgiadapter1->GetDesc1(&dxgiadapterdesc1);

            // Check to see if the adapter can create a D3D12 device without actually
            // creating it. The adapter with the largest dedicated video memory
            // is favored.
            if ((dxgiadapterdesc1.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) == 0 &&
                SUCCEEDED(D3D12CreateDevice(dxgiadapter1.Get(),
                    D3D_FEATURE_LEVEL_11_0, __uuidof(ID3D12Device), nullptr)) &&
                dxgiadapterdesc1.DedicatedVideoMemory > maxdedicatedvideomemory )
            {
                maxdedicatedvideomemory = dxgiadapterdesc1.DedicatedVideoMemory;
                DxThrow_(dxgiadapter1.As(&dxgiadapter4));
            }
        }
    }

    return dxgiadapter4;
}

static ComPtr<ID3D12Device2> CreateDevice(ComPtr<IDXGIAdapter4> adapter) {
    ComPtr<ID3D12Device2> d3d12device2;
    DxThrow_(D3D12CreateDevice(adapter.Get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&d3d12device2)));

    // Enable debug messages in debug mode.
#if defined(_DEBUG)
    ComPtr<ID3D12InfoQueue> pinfoqueue;
    if (SUCCEEDED(d3d12device2.As(&pinfoqueue))) {
        pinfoqueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_CORRUPTION, TRUE);
        pinfoqueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_ERROR, TRUE);
        pinfoqueue->SetBreakOnSeverity(D3D12_MESSAGE_SEVERITY_WARNING, TRUE);
        // Suppress whole categories of messages
        //D3D12_MESSAGE_CATEGORY categories[] = {};

        // Suppress messages based on their severity level
        D3D12_MESSAGE_SEVERITY severities[] = {
            D3D12_MESSAGE_SEVERITY_INFO
        };

        // Suppress individual messages by their ID
        //D3D12_MESSAGE_ID DenyIds[] = {
            //D3D12_MESSAGE_ID_CLEARRENDERTARGETVIEW_MISMATCHINGCLEARVALUE,   // I'm really not sure how to avoid this message.
            //D3D12_MESSAGE_ID_MAP_INVALID_NULLRANGE,                         // This warning occurs when using capture frame while graphics debugging.
            //D3D12_MESSAGE_ID_UNMAP_INVALID_NULLRANGE,                       // This warning occurs when using capture frame while graphics debugging.
        //};

        D3D12_INFO_QUEUE_FILTER newfilter = {};
        //NewFilter.DenyList.NumCategories = _countof(categories);
        //NewFilter.DenyList.pCategoryList = categories;
        newfilter.DenyList.NumSeverities = _countof(severities);
        newfilter.DenyList.pSeverityList = severities;
        newfilter.DenyList.NumIDs = 0;//_countof(DenyIds);
        newfilter.DenyList.pIDList = nullptr;//DenyIds;

        DxThrow_(pinfoqueue->PushStorageFilter(&newfilter));
    }
#endif

    return d3d12device2;
}

static ComPtr<ID3D12CommandQueue> CreateCommandQueue(ComPtr<ID3D12Device2> device,
                                                     D3D12_COMMAND_LIST_TYPE type) {
    ComPtr<ID3D12CommandQueue> d3d12commandqueue;

    D3D12_COMMAND_QUEUE_DESC desc = {};
    desc.Type =     type;
    desc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
    desc.Flags =    D3D12_COMMAND_QUEUE_FLAG_NONE;
    desc.NodeMask = 0;

    DxThrow_(device->CreateCommandQueue(&desc, IID_PPV_ARGS(&d3d12commandqueue)));

    return d3d12commandqueue;
}

static ComPtr<ID3D12Fence> CreateFence(ComPtr<ID3D12Device2> device) {
    ComPtr<ID3D12Fence> fence;

    DxThrow_(device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence)));

    return fence;
}

static HANDLE CreateEventHandle() {
    HANDLE fenceevent;

    fenceevent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
    assert(fenceevent && "Failed to create fence event.");

    return fenceevent;
}

static uint64_t Signal(ComPtr<ID3D12CommandQueue> commandqueue, ComPtr<ID3D12Fence> fence,
                       uint64_t& fencevalue) {
    uint64_t fencevalueforsignal = ++fencevalue;
    DxThrow_(commandqueue->Signal(fence.Get(), fencevalueforsignal));

    return fencevalueforsignal;
}

static void
WaitForFenceValue(ComPtr<ID3D12Fence> fence, uint64_t fencevalue, HANDLE fenceevent,
                  std::chrono::milliseconds duration = std::chrono::milliseconds::max()) {
    if (fence->GetCompletedValue() < fencevalue) {
        DxThrow_(fence->SetEventOnCompletion(fencevalue, fenceevent));
        ::WaitForSingleObject(fenceevent, static_cast<DWORD>(duration.count()));
    }
}

static void Flush(ComPtr<ID3D12CommandQueue> commandqueue, ComPtr<ID3D12Fence> fence,
                  uint64_t& fencevalue, HANDLE fenceevent) {
    uint64_t fencevalueforsignal = Signal(commandqueue, fence, fencevalue);
    WaitForFenceValue(fence, fencevalueforsignal, fenceevent);
}

void PlatformContext::Initialize(Window& window, const World& world) {
    mswin = window.win;

    // swapchain images start off in the 'present' state.
    for (unsigned int i = 0; i < PlatformContext::swapchainimagecount; ++i)
        swapchainstates[i] = D3D12_RESOURCE_STATE_PRESENT;

    EnableDebugLayer();

    tearingsupported = CheckTearingSupport();

    if (!vsync and !tearingsupported)
        Throw_(Error("vsync disabled but tearing unsupported"));

    ComPtr<IDXGIAdapter4> dxgiadapter4 = GetAdapter(usewarp);

    device = CreateDevice(dxgiadapter4);

    heaps.Initialize(device);

    msaasamples = CheckMSAA(device, world.GetMSAASamples());

    graphicsqueue = CreateCommandQueue(device, D3D12_COMMAND_LIST_TYPE_DIRECT);

    DxThrow_(device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT,
                                          IID_PPV_ARGS(&tmpcmdallocator)));

    srvdescsize = device->GetDescriptorHandleIncrementSize(
        D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
    rtvdescsize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
    dsvdescsize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
    samplerdescsize = device->GetDescriptorHandleIncrementSize(
        D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER);

    fence = CreateFence(device);
    fenceevent = CreateEventHandle();

    ImGui::SetAllocatorFunctions(imguialloc, imguifree, nullptr);
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
}

void PlatformContext::Shutdown() {
    WaitForIdle();

    ImGui_ImplDX12_Shutdown();
    ImGui_ImplWin32_Shutdown();
    ImGui::DestroyContext();

    ::CloseHandle(fenceevent);
    heaps.Shutdown();
}

void PlatformContext::BeginResourceBarriers() {
    resourcebarriers.clear();
}

void PlatformContext::AddSwapchainResourceBarrier(unsigned int swapidx,
                                                  D3D12_RESOURCE_STATES state) {
    if (state != swapchainstates[swapidx]) {
        CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(
            swapchainimages[swapidx].Get(), swapchainstates[swapidx], state);
        resourcebarriers.push_back(barrier);
        swapchainstates[swapidx] = state;
    }
}

void PlatformContext::AddResourceBarrier(ImageResource& image, D3D12_RESOURCE_STATES state) {
    if (state != image.state) {
        CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(
            image.resource.Get(), image.state, state);
        resourcebarriers.push_back(barrier);
        image.state = state;
    }
}

void PlatformContext::EndResourceBarriers(SessionContext& scontext) {
    if (resourcebarriers.size()) {
        PlatformContext::CommandBuffer cmdlist = scontext.pc.BeginSingleTimeCommands();
        EndResourceBarriers(cmdlist);
        scontext.pc.EndSingleTimeCommands(cmdlist);
    }
}

void
PlatformContext::EndResourceBarriers(CommandBuffer& cmdlist){
    if (resourcebarriers.size())
        cmdlist.d12obj->ResourceBarrier((UINT)resourcebarriers.size(),
                                        resourcebarriers.data());
}


PlatformContext::CommandBuffer PlatformContext::BeginSingleTimeCommands() {
    CommandBuffer cmdlist;

    DxThrow_(device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT,
                  tmpcmdallocator.Get(), nullptr, IID_PPV_ARGS(&cmdlist.d12obj)));

    return cmdlist;
}

void PlatformContext::EndSingleTimeCommands(CommandBuffer &cmdlist) {
    DxThrow_(cmdlist.d12obj->Close());

    ID3D12CommandList* const cmdlists[] = { cmdlist.d12obj.Get() };
    graphicsqueue->ExecuteCommandLists(_countof(cmdlists), cmdlists);

    ComPtr<ID3D12Fence> fence;
    DxThrow_(device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence)));

    HANDLE fenceevent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
    assert(fenceevent && "error: failed to create fence event.");

    DxThrow_(graphicsqueue->Signal(fence.Get(), 1));
    DxThrow_(fence->SetEventOnCompletion(1, fenceevent));

    WaitForSingleObject(fenceevent, INFINITE);

    ::CloseHandle(fenceevent);
}

void PlatformContext::PostInitialize(SessionContext& scontext) {
    InitImGui(scontext, mswin);
}

void PlatformContext::WaitForIdle() {
    Flush(graphicsqueue, fence, fencevalue, fenceevent);
}

}