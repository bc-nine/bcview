#include "testharness.h"
#include "bclib/stringid.h"
#include "session.h"
#include "direct.h"
#include "bclib/fileutils.h"
#include "renderstate.h"
#include "glm/glm.hpp"
#include "computecommands.h"
#include "bclib/stdallocators.h"

#if winvk_
#include "util_vk.h"
#include "resourceutil_vk.h"
#elif wind12_
#endif

#define TickPreamble_() \
    testresult = Success(); \
    if (frame > frames.back() + 1) { \
        done = true; \
        return Success(); \
    }

namespace bc9 {

const FString TestHarness::ImageReferenceDir = "P:\\assets\\test-images";

struct ImageMSEConstants {
    uintn resultsidx;
    glm::uvec3 pad1;
    uintn capimageidx;
    glm::uvec3 pad2;
    uintn refimageidx;
    glm::uvec3 pad3;
    uintn outputidx;
    glm::uvec3 pad4;
};

FunctionalTest::FunctionalTest(const FString& worldname, TestHarness::Mode mode,
                               std::initializer_list<uintn> frames) :
worldname(worldname),
mode(mode),
frames(frames) {
}

void FunctionalTest::KeyPressed(uchar key) {
    switch (key){
        case '1':
        displaymode = eImageDisplayReference;
        break;

        case '2':
        displaymode = eImageDisplayCapture;
        break;

        case '3':
        displaymode = eImageDisplayOutput;
        break;
    }
}

Result FunctionalTest::ReadImage(uintn frame, stdvector<uchar>& data, uintn& width, uintn& height,
                                 uintn& format, RenderState& state) {
    FString imagepath1("%s\\%s", TestHarness::ImageReferenceDir.str(), worldname.str());
    FString imagepath2("%s\\%s", imagepath1.str(), platform.str());
    FString imagepath("%s\\%s_%s_%d.rgba", imagepath2.str(),
                      worldname.str(), platform.str(), frame);
    FString statepath("%s\\%s_%s_%d.json", imagepath2.str(),
                      worldname.str(), platform.str(), frame);

    Result r = ReadRenderState(statepath.str(), state);
    if (!r)
        return r;

    return ReadRGBAImage(imagepath.str(), data, width, height, format);
}

Result FunctionalTest::WriteImage(uintn frame, uchar* data, uint64 size, uintn width, uintn height,
                                  uintn format, RenderState& state) {
    FString imagepath1("%s\\%s", TestHarness::ImageReferenceDir.str(), worldname.str());
    _mkdir(imagepath1.str());
    FString imagepath2("%s\\%s", imagepath1.str(), platform.str());
    _mkdir(imagepath2.str());
    FString imagepath("%s\\%s_%s_%d.rgba", imagepath2.str(),
                      worldname.str(), platform.str(), frame);
    FString statepath("%s\\%s_%s_%d.json", imagepath2.str(),
                      worldname.str(), platform.str(), frame);

    Result r = WriteRenderState(statepath.str(), state);
    if (!r)
        return r;

    return WriteRGBAImage(imagepath.str(), data, size, width, height, format);
}

void FunctionalTest::Initialize(Session& session) {
    computekey = session.assetcache.FindShader("image-mse", eShaderAssetCompute);

    ComputePipelineBase base(session.context.pc, computekey);
    pipeline = session.context.pipelinecache.GetComputePipeline(session.context.pc, base);
}

void FunctionalTest::Shutdown(Session& session) {
    if (refimage.vkobj) {
        DestroyImage(session.context.pc, refimage);
        refimage.vkobj = nullptr;
    }
    if (capimage.vkobj) {
        DestroyImage(session.context.pc, capimage);
        capimage.vkobj = nullptr;
    }
    if (outputimage.vkobj) {
        DestroyImage(session.context.pc, outputimage);
        outputimage.vkobj = nullptr;
    }
}

Result FunctionalTest::Tick(Session& session, uintn frame, float dt, bool& paused, bool& done) {
    TickPreamble_();

    return RunTick(session, frame, dt, paused, done);
}

static bool DoCapture(const stdvector<uintn>& frames, uintn frame) {
    for (uintn elem : frames)
        if (elem == frame)
            return true;

    return false;
}

// so this is unfortunate, but there is a single platform dependent function needed for
// test harness, namely RunTick(). instead of trying to split it out in different files,
// i'll just put it here.

#if winvk_

Result FunctionalTest::RunTick(Session& session, uintn frame, float dt, bool& paused, bool& done) {
    if (mode == TestHarness::eCaptureReference) {
        if (DoCapture(frames, frame)) {
            session.renderer.CaptureImage(session.context,
                                          [=](RenderState& state, uchar* data, uintn stride,
                                              uintn width, uintn height, uintn format) {
                uint16 w = uint16_(width);
                uint16 h = uint16_(height);
                uint64 size = height*stride;
                Throw_(WriteImage(state.frame, data, h*stride, w, h, format, state));
            });
        }

        session.renderer.RenderFrame(session.context, dt);
    }
    else {
        Assert_(mode == TestHarness::eRunTest);
        if (DoCapture(frames, frame)) {
            // pay attention to this pass-by-reference here...
            session.renderer.CaptureImage(session.context,
                                          [&](RenderState& state, uchar* data, uintn stride,
                                              uintn width, uintn height, uintn format) {
                // clean up any old resources
                if (refimage.vkobj)
                    DestroyImage(session.context.pc, refimage);
                if (capimage.vkobj)
                    DestroyImage(session.context.pc, capimage);
                if (outputimage.vkobj)
                    DestroyImage(session.context.pc, outputimage);

                // read the reference image first
                StdStackAllocator stackalloc(GlobalStack());
                stdvector<uchar> refdata(stackalloc);
                RenderState refstate;
                uintn refwidth, refheight, refformat;
                Throw_(ReadImage(state.frame, refdata, refwidth, refheight, refformat, refstate));

                // if reference render state doesn't match captured render state, bail early
                // because no point in continuing if that's the case.
                PrintI_("ref format: %d    cap format: %d", format, refformat);
                if (format != refformat) {
                    testresult = Error("reference format != capture format");
                    return;
                }
                if (width != refwidth) {
                    testresult = Error("reference width != capture width");
                    return;
                }
                if (height != refheight) {
                    testresult = Error("reference height != capture height");
                    return;
                }

                CommandBuffer cmdbuffer = session.context.pc.BeginSingleTimeCommands();

                vkCmdBindDescriptorSets(cmdbuffer.vkobj,
                                        VK_PIPELINE_BIND_POINT_COMPUTE,
                                        session.context.pc.pipelinelayout, 1, 1,
                                        &session.context.pc.current.bindlessset, 0, nullptr);

                // copy reference image into the gpu image resource
                CreateImageParams refparams(ResourceFormat(format), width, height, 1,
                                            "referenceimage");
                refparams.SetUsageSampled();
                refparams.SetUsageCopyDestination();
                refparams.SetInitialLayout(eImageLayoutTransferDestination);
                Throw_(CreateImage(session.context.pc, &cmdbuffer, refparams, refimage));

                uintn refbufsize = height*stride;
                CreateBufferParams refbufparams(session.context.pc, eBufferResourceGeneral,
                                                refbufsize, "refbuffer");
                refbufparams.SetUsageCopySource();
                refbufparams.SetAccessMode(eBufferCPUAccessWrite);
                refbufparams.SetInitialData(&refdata[0], refbufsize);
                BufferResource refbuffer;
                Throw_(CreateBuffer(session.context.pc, refbufparams, refbuffer));
                VkBufferImageCopy refbufimgcopy{};
                refbufimgcopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                refbufimgcopy.imageSubresource.mipLevel = 0;
                refbufimgcopy.imageSubresource.baseArrayLayer = 0;
                refbufimgcopy.imageSubresource.layerCount = 1;
                refbufimgcopy.imageExtent.width = width;
                refbufimgcopy.imageExtent.height = height;
                refbufimgcopy.imageExtent.depth = 1;

                vkCmdCopyBufferToImage(cmdbuffer.vkobj, refbuffer.vkobj, refimage.vkobj,
                                       VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &refbufimgcopy);

                CreateVkImageParams vkrefparams{};
                Throw_(refparams.Get(session.context.pc, vkrefparams));
                TransitionVkImageParams tparams { .computemasks = true };
                tparams.usecmdbuffer = &cmdbuffer;
                tparams.barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
                tparams.barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                tparams.barrier.image = refimage.vkobj;
                tparams.barrier.subresourceRange = vkrefparams.viewcreateinfo.subresourceRange;
                tparams.srcstagemask = VK_PIPELINE_STAGE_TRANSFER_BIT;
                tparams.dststagemask = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
                TransitionImage(session.context.pc, tparams);

                // next, copy captured image into the gpu image resource
                CreateImageParams capparams(ResourceFormat(format), width, height, 1,
                                            "captureimage");
                capparams.SetUsageSampled();
                capparams.SetUsageCopyDestination();
                capparams.SetInitialLayout(eImageLayoutTransferDestination);
                Throw_(CreateImage(session.context.pc, &cmdbuffer, capparams, capimage));

                uintn capbufsize = height*stride;
                CreateBufferParams capbufparams(session.context.pc, eBufferResourceGeneral,
                                                capbufsize, "capbuffer");
                capbufparams.SetAccessMode(eBufferCPUAccessWrite);
                capbufparams.SetUsageCopySource();
                capbufparams.SetInitialData(data, capbufsize);
                BufferResource capbuffer;
                Throw_(CreateBuffer(session.context.pc, capbufparams, capbuffer));
                VkBufferImageCopy capbufimgcopy{};
                capbufimgcopy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
                capbufimgcopy.imageSubresource.mipLevel = 0;
                capbufimgcopy.imageSubresource.baseArrayLayer = 0;
                capbufimgcopy.imageSubresource.layerCount = 1;
                capbufimgcopy.imageExtent.width = width;
                capbufimgcopy.imageExtent.height = height;
                capbufimgcopy.imageExtent.depth = 1;
                vkCmdCopyBufferToImage(cmdbuffer.vkobj, capbuffer.vkobj, capimage.vkobj,
                                       VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &capbufimgcopy);

                tparams.barrier.image = capimage.vkobj;
                TransitionImage(session.context.pc, tparams);

                // next, set up results buffer
                CreateBufferParams resparams(session.context.pc, eBufferResourceStructuredOutput,
                                             width*height*sizeof(float), "results");
                resparams.SetAccessMode(eBufferCPUAccessRead);
                BufferResource resbuffer;
                Throw_(CreateBuffer(session.context.pc, resparams, resbuffer));

                // finally, set up compute shader, passing in input image etc, and execute
                CreateImageParams params(eResourceFormatR8G8B8A8_UNORM, width, height, 1,
                                         "storageimage");
                params.SetUsageStorage();
                params.SetUsageSampled();
                params.SetUsageCopySource();
                params.SetInitialLayout(eImageLayoutShaderReadWrite);
                Throw_(CreateImage(session.context.pc, &cmdbuffer, params, outputimage));

                CreateConstantBufferParams bufparams(session.context.pc,
                                                     sizeof(ImageMSEConstants),
                                                     "image-mse-constants");
                ImageMSEConstants constants;
                constants.resultsidx = resbuffer.DescriptorIndex();
                constants.capimageidx = capimage.view.DescriptorIndex();
                constants.refimageidx = refimage.view.DescriptorIndex();
                constants.outputidx = outputimage.rwview.DescriptorIndex();
                bufparams.SetInitialData(&constants, sizeof(ImageMSEConstants));
                Throw_(CreateBuffer(session.context.pc, bufparams, indexcbuf));

                // these numbers must stay in sync with numthreads in image-mse-compute.hlsl!
                Compute::DispatchParams dispatchparams{};
                uintn xthreads = 8;
                uintn ythreads = 8;
                dispatchparams.xgroups = width  / xthreads + ((width  % xthreads > 0) ? 1 : 0);
                dispatchparams.ygroups = height / ythreads + ((height % ythreads > 0) ? 1 : 0);

                Compute::Compute(session.context.pc, cmdbuffer, pipeline,
                                 { &indexcbuf }, dispatchparams);

                CreateVkImageParams vkparams{};
                Throw_(params.Get(session.context.pc, vkparams));
                tparams.computemasks = false;
                tparams.barrier.oldLayout = VK_IMAGE_LAYOUT_GENERAL,
                tparams.barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                tparams.barrier.image = outputimage.vkobj;
                tparams.barrier.subresourceRange = vkparams.viewcreateinfo.subresourceRange;
                tparams.srcstagemask = VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT;
                tparams.dststagemask = VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
                tparams.barrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
                tparams.barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
                TransitionImage(session.context.pc, tparams);

                session.context.pc.EndSingleTimeCommands(cmdbuffer);

                float error = 0.;
                float *fdata = (float*) resbuffer.memory.mappedmem;
                for (uintn j = 0; j < height; ++j)
                    for (uintn i = 0; i < width; ++i)
                        error += fdata[width*j + i];
                error /= (width*height);
                PrintI_("MSE: %.05f", error);

                DestroyBuffer(session.context.pc, capbuffer);
                DestroyBuffer(session.context.pc, refbuffer);
                DestroyBuffer(session.context.pc, resbuffer);

                DestroyBuffer(session.context.pc, indexcbuf);

                testresult = Success();
            });

            session.renderer.RenderFrame(session.context, dt);

            if (!testresult)
                return testresult;

            displaymode = eImageDisplayOutput;
            session.renderer.RenderImage(session.context, outputimage);

            paused = true;
        }
        else if (paused) {
            if (displaymode == eImageDisplayReference)
                session.renderer.RenderImage(session.context, refimage);
            else if (displaymode == eImageDisplayCapture)
                session.renderer.RenderImage(session.context, capimage);
            else
                session.renderer.RenderImage(session.context, outputimage);
        }
        else {
            session.renderer.RenderFrame(session.context, dt);
        }
    }

    return Success();
}

#elif wind12_
// put DirectX 12 RunTick() here...
#endif

struct TestSimple : public FunctionalTest {
    TestSimple(TestHarness::Mode mode) : FunctionalTest("simple", mode, {4}) {}

    Result Tick(Session& session, uintn frame, float dt, bool& paused, bool& done) {
        TickPreamble_();

        return RunTick(session, frame, dt, paused, done);
    }
};

struct TestIndexed : public FunctionalTest {
    TestIndexed(TestHarness::Mode mode) : FunctionalTest("indexed", mode, {4}) {}

    Result Tick(Session& session, uintn frame, float dt, bool& paused, bool& done) {
        TickPreamble_();

        if (frame == frames[0]) {
        }

        return RunTick(session, frame, dt, paused, done);
    }
};

struct TestRTShapes : public FunctionalTest {
    TestRTShapes(TestHarness::Mode mode) : FunctionalTest("rtshapes", mode, {4}) {}

    Result Tick(Session& session, uintn frame, float dt, bool& paused, bool& done) {
        TickPreamble_();

        if (frame == frames[0]) {
            glm::quat q(glm::angleAxis(0.5f, glm::vec3(0.f, 1.f, 0.f)));
            uintn idx = session.world.ActiveCameraNode();
            session.nodecache.SetNodeRotation(idx, q);
        }

        return RunTick(session, frame, dt, paused, done);
    }
};

struct TestRTWeekendCH8 : public FunctionalTest {
    TestRTWeekendCH8(TestHarness::Mode mode) : FunctionalTest("rtweekend-ch8", mode, {512}) {}
};

struct TestRTWeekendCH9 : public FunctionalTest {
    TestRTWeekendCH9(TestHarness::Mode mode) : FunctionalTest("rtweekend-ch9", mode, {512}) {}
};

struct TestRTWeekendCH10 : public FunctionalTest {
    TestRTWeekendCH10(TestHarness::Mode mode) : FunctionalTest("rtweekend-ch10", mode, {512}) {}
};

struct TestRTWeekendCH12 : public FunctionalTest {
    TestRTWeekendCH12(TestHarness::Mode mode) : FunctionalTest("rtweekend-ch12", mode, {16384}) {}
};

struct TestRTWeekendCH13 : public FunctionalTest {
    TestRTWeekendCH13(TestHarness::Mode mode) : FunctionalTest("rtweekend-ch13", mode, {512}) {}
};

struct TestBoxVertexColors : public FunctionalTest {
    TestBoxVertexColors(TestHarness::Mode mode) : FunctionalTest("box-vertex-colors", mode, {16}){}
};

TestHarness::TestHarness(const FString& worldname, Mode mode) {
    switch (worldname.sid().Hash()) {
        case "indexed"_sid:
        test = new TestIndexed(mode);
        break;

        case "simple"_sid:
        test = new TestSimple(mode);
        break;

        case "rtshapes"_sid:
        test = new TestRTShapes(mode);
        break;

        case "rtweekend-ch8"_sid:
        test = new TestRTWeekendCH8(mode);
        break;

        case "rtweekend-ch9"_sid:
        test = new TestRTWeekendCH9(mode);
        break;

        case "rtweekend-ch10"_sid:
        test = new TestRTWeekendCH10(mode);
        break;

        case "rtweekend-ch12"_sid:
        test = new TestRTWeekendCH12(mode);
        break;

        case "rtweekend-ch13"_sid:
        test = new TestRTWeekendCH13(mode);
        break;

        case "box-vertex-colors"_sid:
        test = new TestBoxVertexColors(mode);
        break;
    }
}

void TestHarness::KeyPressed(uchar key) {
    Assert_(test);
    test->KeyPressed(key);
}

void TestHarness::Initialize(Session& session) {
    Assert_(test);
    test->Initialize(session);
}

void TestHarness::Shutdown(Session& session) {
    Assert_(test);
    test->Shutdown(session);
}

Result TestHarness::Tick(Session& session, float dt, bool& paused, bool& done) {
    Assert_(test);

    return test->Tick(session, uint32_(session.context.pc.currentcpuframe), dt, paused, done);
}

TestHarness::~TestHarness() {
}

}