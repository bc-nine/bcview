#include "rhitypes.h"

namespace bc9 {

Result GetShaderStage(ShaderAssetType type, ShaderStage& stage) {
    switch (type) {
        case eShaderAssetVertex:
        stage = eShaderStageVertex;
        break;

        case eShaderAssetPixel:
        stage = eShaderStagePixel;
        break;

        case eShaderAssetCompute:
        stage = eShaderStageCompute;
        break;

        case eShaderAssetRay:
        stage = eShaderStageRay;
        break;

        case eShaderAssetMiss:
        stage = eShaderStageMiss;
        break;

        case eShaderAssetClosestHit:
        stage = eShaderStageClosestHit;
        break;

        default:
        return Error("GetShaderStage(): unknown shader asset type");
        break;
    }

    return Success();
}

Result GetResourceFormatProperties(ResourceFormat format, uintn& channels, uintn& texelsize) {
    switch (format) {
        case eResourceFormatR8_UNORM:
        case eResourceFormatR8_UINT:
        channels = 1;
        texelsize = 1;
        break;

        case eResourceFormatR8G8B8A8_UNORM:
        case eResourceFormatR8G8B8A8_SRGB:
        channels = 4;
        texelsize = 4;
        break;

        case eResourceFormatR32_UINT:
        case eResourceFormatR32_SFLOAT:
        channels = 1;
        texelsize = 4;
        break;

        case eResourceFormatR32G32B32A32_SFLOAT:
        channels = 4;
        texelsize = 16;
        break;

        default:
        channels = texelsize = 0;
        return Error("GetResourceFormatTexelSize(): unknown resource format");
        break;
    }

    return Success();
}

}