#include "shaderbinding.h"

#include "util_vk.h"

namespace bc9 {

ShaderBinding::ShaderBinding(const stdjson &bjson, ShaderStage stage) : stages(stage) {
    set = bjson["set"].get<unsigned int>();
    binding = bjson["binding"].get<unsigned int>();
    count = bjson["count"].get<unsigned int>();
    StringID strtype;
    Throw_(ReqString(bjson, "type", strtype));
    Throw_(GetDescriptorType(strtype, dtype));
}

ShaderBinding::ShaderBinding() {
}

bool ShaderBinding::Valid() const {
    return dtype < VK_DESCRIPTOR_TYPE_MAX_ENUM;
}

bool
ShaderBinding::operator==(const ShaderBinding& rhs) {
    return set == rhs.set && binding == rhs.binding && count == rhs.count && dtype == rhs.dtype;
}

}
