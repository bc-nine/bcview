#pragma once

#include <cstddef>
#include "appheaps.h"

extern "C" void* xxhashalloc(size_t size);
extern "C" void xxhashfree(void *ptr);

extern void* jsonalloc(size_t size);
extern void jsonfree(void *ptr);

extern void* imguialloc(size_t size, void*);
extern void imguifree(void* ptr, void*);

inline void* sdlalloc(size_t size) {
    return bc9::Heaps().Buddy(bc9::eHeapSDL).Allocate(size);
}

inline void* sdlcalloc(size_t num, size_t size) {
    void* ptr = bc9::Heaps().Buddy(bc9::eHeapSDL).Allocate(num*size);
    memset(ptr, 0, size);
    return ptr;
}

inline void* sdlrealloc(void* ptr, size_t size) {
    return bc9::Heaps().Buddy(bc9::eHeapSDL).Reallocate(ptr, size);
}

inline void sdlfree(void* ptr) {
    if (ptr)
        bc9::Heaps().Buddy(bc9::eHeapSDL).Deallocate(ptr);
}