#pragma once

#include "bclib/fstring.h"
#include "assetcache.h"
#include "materialcache.h"
#include "drawablecache.h"
#include "nodecache.h"
#include "world.h"
#include "imdraw.h"
#include "window.h"
#include "renderer.h"
#include "cameracontrols.h"
#include "xrheadset.h"
#include "sessioncontext.h"
#include "surface.h"

namespace bc9 {

class TestHarness;

class Session {
public:
    Session(const stdjson& bundle, uintn xrmode);

    void Initialize(Window &window, OutputDevice device); // temporary
    void Shutdown();

    AssetCache assetcache;

    World world;

    Surface surface;
    XRHeadset headset;

    MaterialCache materialcache;

    DrawableCache drawablecache;

    NodeCache nodecache;

    Renderer renderer;

    IMDraw imdraw;

    CameraControls cameracontrols;

    SessionContext context;

    TestHarness* testharness = nullptr;
};

}