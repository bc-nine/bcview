#pragma once

#if winvk_
#include "shaderresource_vk.h"
#elif wind12_
#include "shaderresource_d12.h"
#endif
