#include "nodecache.h"
#include "drawablecache.h"
#include "renderbins.h"
#include "bclib/stdjson.h"
#include "bclib/mathutils.h"
#include "sessioncontext.h"

using namespace std;
using namespace glm;

namespace bc9 {

uintn NodeCache::FindNode(const StringID& name) const {
    uintn nodecount = uintn_(nodes.size());
    for (uintn n = 0; n < nodecount; ++n)
        if (nodes[n].name == name)
            return n;

    return kInvalidUInt;
}

const Camera* NodeCache::NodeCamera(uintn nodeidx) const {
    Assert_(nodeidx < nodes.size());

    const Camera* camera = nodes[nodeidx].camera;
    Assert_(camera);

    return camera;
}

uintn NodeCache::CameraNodeIndex(const StringID& name) const {
    uintn cameranodecount = uintn_(cameranodes.size());
    for (uintn idx = 0; idx < cameranodecount; ++idx) {
        uintn nodeidx = cameranodes[idx];
        if (nodes[nodeidx].name == name)
            return nodeidx;
    }

    return kInvalidUInt;
}

StringID NodeCache::CameraNodeName(uintn zindex) const {
    Assert_(zindex < cameranodes.size());
    Assert_(cameranodes[zindex] < nodes.size());

    return nodes[cameranodes[zindex]].name;
}

uintn NodeCache::CameraNodeCount() const {
    return uintn_(cameranodes.size());
}

void NodeCache::SetNodeTranslation(uintn nodeidx, const glm::vec3& t) {
    Assert_(nodeidx < nodes.size());

    nodes[nodeidx].translation = t;
}

void NodeCache::SetNodeRotation(uintn nodeidx, const glm::quat& r) {
    Assert_(nodeidx < nodes.size());

    nodes[nodeidx].rotation = r;
}

void NodeCache::SetNodeScale(uintn nodeidx, const glm::vec3& s) {
    Assert_(nodeidx < nodes.size());

    nodes[nodeidx].scale = s;
}

glm::vec3 NodeCache::NodeTranslation(uintn nodeidx) const {
    Assert_(nodeidx < nodes.size());

    return nodes[nodeidx].translation;
}

glm::quat NodeCache::NodeRotation(uintn nodeidx) const {
    Assert_(nodeidx < nodes.size());

    return nodes[nodeidx].rotation;
}

glm::vec3 NodeCache::NodeScale(uintn nodeidx) const {
    Assert_(nodeidx < nodes.size());

    return nodes[nodeidx].scale;
}

glm::mat4 NodeCache::NodeTransform(uintn nodeidx) {
    Assert_(nodeidx < nodes.size());

    return nodes[nodeidx].transform;
}

glm::mat4 NodeCache::NodeInverseTransform(uintn nodeidx) {
    Assert_(nodeidx < nodes.size());

    return nodes[nodeidx].invtransform;
}

// it's important to understand that the "view" transforms are the inverse of the
// node transforms. in other words, if you treat the camera as an object that can
// be positioned and oriented in space with the node transform, then the view
// matrix that corresponds to that is the inverse of the node transform (and of
// course the inverse of the view matrix is simply the original node transform).
glm::mat4 NodeCache::ViewTransform(uintn nodeidx) {
    Assert_(nodeidx < nodes.size());

    return nodes[nodeidx].invtransform;
}

glm::mat4 NodeCache::ViewInverseTransform(uintn nodeidx) {
    Assert_(nodeidx < nodes.size());

    return nodes[nodeidx].transform;
}

void NodeCache::TraverseNode(SessionContext& scontext, stdvector<BakedInstance>& instances,
                             uintn& instanceidx, Node& node, const glm::mat4x3& xform) {
    glm::mat4x3 nodexform = Transform3D(node.translation, node.rotation, node.scale);
    glm::mat4x3 cumxform = xform * glm::mat4(nodexform);

    if (node.model) {
        for (auto dkey : node.model->drawables) {
            BakedInstance& instance = instances[instanceidx];
            instance.drawablekey = dkey;
            instance.transform = cumxform;
            ++instanceidx;
            Assert_(instanceidx <= instances.size());
        }
    }

    uint32 numchildren = node.GetChildCount();
    for (uint32 childidx = 0; childidx < numchildren; ++childidx) {
        Node& childnode = nodes[node.children[childidx]];
        TraverseNode(scontext, instances, instanceidx, childnode, cumxform);
    }
}
void NodeCache::Bake(SessionContext& scontext, stdvector<BakedInstance>& instances) {
    uintn instanceidx = 0;
    instances.resize(scontext.nodecache.DrawableInstanceCount());
    TraverseNode(scontext, instances, instanceidx, nodes[0], glm::mat4x3(1.0));
    Assert_(instanceidx == instances.size());
}

void NodeCache::TraverseModel(SessionContext& scontext, RenderBins& renderbins,
                              const Model& model, uint32 modeldataidx) {
    for (auto dkey : model.drawables)
        scontext.drawablecache.TraverseDrawable(scontext, renderbins, dkey, modeldataidx);
}

void NodeCache::TraverseNode(SessionContext& scontext, RenderBins& renderbins,
                             Node& node, const glm::mat4x3& xform,
                             const glm::mat4x3& invxform) {
    glm::mat4x3 nodexform = Transform3D(node.translation, node.rotation, node.scale);
    glm::mat4x3 invnodexform = InverseTransform3D(node.translation, node.rotation, node.scale);

    glm::mat4x3 cumxform = xform * glm::mat4(nodexform);
    glm::mat4x3 invcumxform = invnodexform * glm::mat4(invxform);

    node.transform = glm::mat4(cumxform);
    node.invtransform = glm::mat4(invcumxform);

    if (node.model) {
        ModelNodeData& modeldata = modeldatas[node.modeldataidx];
        modeldata.transform = node.transform;
        modeldata.invtransform = node.invtransform;
        TraverseModel(scontext, renderbins, *node.model, node.modeldataidx);
    }

    uint32 numchildren = node.GetChildCount();
    for (uint32 childidx = 0; childidx < numchildren; ++childidx) {
        Node& childnode = nodes[node.children[childidx]];
        TraverseNode(scontext, renderbins, childnode, cumxform, invcumxform);
    }
}

NodeCache::NodeCache(const stdjson& graphjson) {
    bool found = false;

    const stdjson nodejson = graphjson["nodes"];
    const stdjson modeljson = graphjson["models"];
    const stdjson camerajson = graphjson["cameras"];

    uintn modelnodecount;
    uintn cameranodecount;
    const stdjson countjson = graphjson["counts"];
    Throw_(ReqUInt(countjson, "modelnodes", modelnodecount));
    Throw_(ReqUInt(countjson, "cameranodes", cameranodecount));
    Throw_(ReqUInt(countjson, "drawableinstances", drawableinstancecount));

    uintn cameracount = uintn_(camerajson.size());
    cameras.resize(cameracount);

    uintn cameranodeidx = 0;
    cameranodes.resize(cameranodecount);

    uint32 cidx = 0;
    for (stdjson::const_iterator it = camerajson.begin(); it != camerajson.end(); ++it, ++cidx) {
        auto& cjson(*it);
        Camera& camera(cameras[cidx]);
        camera = Camera(cjson);
    }

    size_t modelcount = modeljson.size();
    models.resize(modelcount);

    uint32 midx = 0;
    for (stdjson::const_iterator it = modeljson.begin(); it != modeljson.end(); ++it, ++midx) {
        auto& mjson(*it);
        Model& model(models[midx]);
        Throw_(OptString(mjson, "name", model.name, found));

        stdjson djson(mjson["drawables"]);
        model.drawables.resize(djson.size());
        uint32 didx = 0;
        for (stdjson::const_iterator it = djson.begin(); it != djson.end(); ++it, ++didx) {
            DrawableKey dkey((*it).get<uintn>());
            model.drawables[didx] = dkey;
        }
    }

    uintn nodecount = uintn_(nodejson.size());
    nodes.resize(nodecount);

    uint32 modeldataidx = 0;
    modeldatas.resize(modelnodecount);

    uint32 nidx = 0;
    for (stdjson::const_iterator it = nodejson.begin(); it != nodejson.end(); ++it, ++nidx) {
        auto& njson(*it);
        Node& node(nodes[nidx]);

        Throw_(OptString(njson, "name", node.name, found));
        Throw_(OptFloat3(njson, "translation", node.translation, found));
        vec4 quaternion = { 0., 0., 0., 1. }; // our format (w last)
        Throw_(OptFloat4(njson, "rotation", quaternion, found));
        node.rotation = { quaternion[3], quaternion[0], quaternion[1], quaternion[2] };
        Throw_(OptFloat3(njson, "scale", node.scale, found));
        if (njson.contains("model")) {
            uint32 midx;
            Throw_(ReqUInt(njson, "model", midx));
            Assert_(midx < models.size(), "key index %u too large", midx);
            node.model = &models[midx];
            node.modeldataidx = modeldataidx++;
            Assert_(modeldataidx <= modelnodecount);
        }
        if (njson.contains("camera")) {
            uint32 cidx;
            Throw_(ReqUInt(njson, "camera", cidx));
            Assert_(cidx < cameras.size(), "key index %u too large", cidx);
            node.camera = &cameras[cidx];
            Assert_(cameranodeidx < cameranodecount);
            cameranodes[cameranodeidx++] = nidx;
        }
        if (njson.contains("children")) {
            stdjson cjson(njson["children"]);
            node.children.resize(cjson.size());
            uint32 cidx = 0;
            for (stdjson::const_iterator it = cjson.begin(); it != cjson.end(); ++it, ++cidx)
                node.children[cidx] = (*it).get<uintn>();
        }
    }

    Assert_(modeldataidx == modelnodecount);
    Assert_(cameranodeidx == cameranodecount);
}

uintn NodeCache::DrawableInstanceCount() const {
    return drawableinstancecount;
}

uintn NodeCache::Update(SessionContext& scontext, RenderBins& renderbins) {
    TraverseNode(scontext, renderbins, nodes[0], glm::mat4x3(1.0), glm::mat4x3(1.0));

    // TODO: should i only update when changed here?
    return modelnodebuf.UpdateAndSwap(modeldatas.data());
}

void NodeCache::Initialize(SessionContext &scontext) {
    CreateInstancedBufferParams params(scontext.pc, eBufferResourceStructuredInput,
                                       modeldatas.size(), sizeof(ModelNodeData), "modeldatas");
    Throw_(CreateBuffer(scontext.pc, params, modelnodebuf));
}

void NodeCache::Shutdown(SessionContext &scontext) {
    DestroyBuffer(scontext.pc, modelnodebuf);
}

}