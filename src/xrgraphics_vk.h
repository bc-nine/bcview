#pragma once

#define XR_USE_GRAPHICS_API_VULKAN

#include "volk.h"
#include "rhitypes.h"
#include "openxr/openxr_platform.h"
#include "bclib/stdvector.h"
#include "imageresource_vk.h"
#include "glm/glm.hpp"

namespace bc9 {

class Window;
class Surface;
class Framebuffer;
class PlatformContext;
class XRHeadset;

class XRGraphics {

friend class XRHeadset;

public:
    void Initialize(PlatformContext& context, Surface& surface, Window& window);
    void Shutdown(PlatformContext& context);

    void BeginRenderPass(PlatformContext& context, Framebuffer& framebuffer);
    void EndRenderPass(PlatformContext& context);

    const XrBaseInStructure* GetGraphicsBinding() const;

    int64 SetColorSwapchainFormat(const stdvector<int64>& runtimeformats);

    uint32 GetSupportedSwapchainSampleCount(const XrViewConfigurationView&) const;

    XrResult LoadSwapImages(PlatformContext& context, XrSwapchain swapchain, uintn imagecount);

    // platform dependent interface
    virtual XrResult CreateVulkanInstanceKHR(PlatformContext& context,
                                             const XrVulkanInstanceCreateInfoKHR* createinfo,
                                             VkInstance* vkinstance);

    virtual XrResult CreateVulkanDeviceKHR(PlatformContext& context,
                                           const XrVulkanDeviceCreateInfoKHR* createinfo);

    virtual XrResult GetVulkanGraphicsRequirements2KHR(PlatformContext& context,
                                                       XrGraphicsRequirementsVulkan2KHR* reqs);

    virtual XrResult GetVulkanGraphicsDevice2KHR(PlatformContext& context,
                                                 const XrVulkanGraphicsDeviceGetInfoKHR* getinfo,
                                                 VkPhysicalDevice* vkphysicaldevice);

    virtual XrStructureType GetGraphicsBindingType() const;

    XrGraphicsBindingVulkan2KHR graphicsbinding { XR_TYPE_GRAPHICS_BINDING_VULKAN2_KHR };

    XrInstance xrinstance = {};
    XrSystemId xrsystemid = XR_NULL_SYSTEM_ID;

private:
    uint32 xrviewcount = 0;
    ResourceFormat xrswapchainformat;
    VkFormat xrvkswapchainformat;
    glm::uvec2 xrswapchaindim;
    uint32 xrswapchainimagecount = 0;
    uint32 xrswapidx = 0;
    stdvector<ImageResource> xrswapchainimages;
};

}