#pragma once

#if winvk_
#include "pipelines_vk.h"
#elif wind12_
#include "pipelines_d12.h"
#endif