#include "camera.h"
#include "bclib/mathutils.h"
#define GLM_ENABLE_EXPERIMENTAL
#include "glm/gtx/norm.hpp"

using namespace glm;

namespace bc9 {

Camera::Camera() {
}

Camera::Camera(const stdjson &cjson) {
    bool found = false;

    CameraType cameratype  = eCameraTypePerspective;

    Throw_(OptString(cjson, "name", name, found));

    if (cjson.contains("type")) {
        if (cjson["type"] == "orthographic")
            cameratype = eCameraTypeOrthographic;
        else if (cjson["type"] == "perspective")
            cameratype = eCameraTypePerspective;
        else
            throw std::runtime_error("unknown camera type");
    }

    Construct(cjson, cameratype);
}

float Camera::GetApertureSize() const {
    return aperturesize;
}

float Camera::GetFocusDistance() const {
    return focusdistance;
}

void Camera::Construct(const stdjson &cjson, CameraType intype) {
    bool found = false;

    type = intype;

    Throw_(ReqFloat(cjson, "near", nearplane));
    if (intype == eCameraTypePerspective) {
        Throw_(ReqFloat(cjson, "vertical-fov", vfovdeg));
        Throw_(OptFloat(cjson, "aperture-size", aperturesize, found));
        Throw_(OptFloat(cjson, "focus-distance", focusdistance, found));
    }
    else {
        Throw_(ReqFloat(cjson, "far", farplane));
        Throw_(ReqFloat(cjson, "height", height));
        Throw_(ReqFloat(cjson, "width", width));
    }
}

mat4 Camera::ProjectionTransform(float aspect) const {
    // see mar 1 2023 for notes on these
    assert(aspect > 0);
    if (type == eCameraTypePerspective)
        return InfReversedZPerspective(glm::radians(vfovdeg), aspect, nearplane);

    return InfReversedZOrthographic(aspect*height, height);
}

vec3 Camera::GetForwardVector(const glm::quat& rot) {
    vec3 fwd(0.f, 0.f, -1.f);
    return rotate(rot, fwd);
}

vec3 Camera::GetRightVector(const glm::quat& rot) {
    vec3 right(1.f, 0.f, 0.f);
    return rotate(rot, right);
}

void Camera::Print(const char *str, const mat4 &xf) const {
    // per glm docs, the [] operator on a matrix type returns a column vector.
    for (unsigned int i = 0; i < 4; ++i)
        printf("%s % 0.4f,  % 0.4f,  % 0.4f,  % 0.4f\n", str,
               xf[0][i], xf[1][i], xf[2][i], xf[3][i]);
}

void Camera::Print(const char *str, const vec3 &v) const {
    printf("%s%0.6f, %0.6f, %0.6f\n", str, v.x, v.y, v.z);
}

void Camera::Dump() const {
    //Print("position: ", pos);
    //Print("forward: ", GetForwardVector(rot));
}

}
