#include "sessioncontext.h"
#include "materialcache.h"
#include "drawablecache.h"
#include "nodecache.h"
#include "world.h"
#include "imdraw.h"
#include "renderer.h"
#include "assetcache.h"

namespace bc9 {

SessionContext::SessionContext(World& world,
                               IMDraw& imdraw,
                               AssetCache& assetcache,
                               Surface& surface,
                               XRHeadset& headset,
                               MaterialCache& materialcache,
                               DrawableCache& drawablecache,
                               NodeCache& nodecache,
                               Renderer& renderer,
                               uintn xrmode) :
assetcache(assetcache),
pc(world, assetcache, surface, headset, xrmode),
world(world),
imdraw(imdraw),
materialcache(materialcache),
drawablecache(drawablecache),
nodecache(nodecache),
renderer(renderer) {
}

void SessionContext::Initialize(Window& window, OutputDevice device) {
    world.Initialize(*this);
    pc.Initialize(window, device, world);
    assetcache.Initialize(*this);
    passcache.Initialize(*this);
    materialcache.Initialize(*this);
    drawablecache.Initialize(*this);
    nodecache.Initialize(*this);
    pipelinecache.Initialize(this->pc);
    renderer.Initialize(*this);
}

void SessionContext::PostInitialize() {
    pc.PostInitialize(*this);
    materialcache.PostInitialize(*this);
    //imdraw.Initialize(*this);
}

void SessionContext::Shutdown() {
    //imdraw.Shutdown(*this);
    renderer.Shutdown(*this);
    pipelinecache.Shutdown(this->pc);
    nodecache.Shutdown(*this);
    drawablecache.Shutdown(*this);
    materialcache.Shutdown(*this);
    passcache.Shutdown(*this);
    assetcache.Shutdown(*this);
    pc.Shutdown();
    world.Shutdown(*this);
}

bool SessionContext::EnableOverlay() {
    return enableoverlay;
}

void SessionContext::ToggleOverlay() {
    enableoverlay = !enableoverlay;
}

}
