#pragma once

#include "volk.h"
#include "bclib/stdvector.h"
#include "rhitypes.h"
#include <functional>
#include "imageresource_vk.h"
#include "bufferresource_vk.h"
#include "glm/glm.hpp"

namespace bc9 {

class Window;
struct RenderState;
class PlatformContext;

class Surface {

friend class PlatformContext;

public:
    Surface(bool raytracing, ColorSpaceType colorspace, bool vsync);

    void Initialize(PlatformContext& context, Window& window);
    void Shutdown(PlatformContext& context);

    bool ResizeView(PlatformContext& context, uintn width, uintn height);

    void GetViewSize(uintn& width, uintn& height) const;

    void CaptureImage(PlatformContext& context);
    void ProcessCapture(PlatformContext& context,
                        std::function<void(RenderState&, uchar*, uintn, uintn, uintn, uintn)> cb,
                        RenderState& state);

    // can CreateSwapchain be part of Initialize?
    void CreateSwapchain(PlatformContext& context, Window* window,
                         uintn width = 0, uintn height = 0);

    void GetDeviceExtensions(stdvector<const char*>& deviceextensions);

    static void GetInstanceExtensions(Window& window, stdvector<const char*>& instanceextensions);

    ResourceFormat GetSwapchainFormat() const;

    uintn GetSwapchainImageCount() const;

    ImageResource GetSwapchainImage(uintn idx) const;

    VkSurfaceKHR vkobj = nullptr;

private:

    void RecreateSwapchain(PlatformContext& context, uintn width, uintn height);

    void ResizeReadbackImage(PlatformContext& context, uintn width, uintn height);

    void CleanupSwapchain(PlatformContext& context);

    bool raytracing;
    ColorSpaceType colorspace;
    bool vsync;

    ImageResource readbackimg;
    BufferResource readbackbuf;
    VkFormat vkreadbackfmt;
    uintn readbackchannels, readbacktexelsize;

    glm::uvec2 swapchaindim;
    VkFormat vkswapchainformat;
    ResourceFormat swapchainformat;
    uintn swapchainimagecount = 0;
    uint32 swapidx;
    VkSwapchainKHR swapchain;
    stdvector<ImageResource> swapchainimages;
};

}
