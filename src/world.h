#pragma once

#include "bclib/stdvector.h"
#include "bclib/stdmap.h"
#include "rhitypes.h"
#include "node.h"
#include "camera.h"
#include "background.h"
#include "bclib/stringid.h"
#include "cachekeys.h"

namespace bc9 {

class AssetCache;
class SessionContext;

enum RendererMode {
    eRendererRaster,
    eRendererRayTrace,
    eRendererModeCount
};

enum WorldUpdateBits {
    eWorldBackground = 0x1
};

class World {
public:
    World(const stdjson& worldjson, AssetCache &assetcache);

    uint64 Update();

    void Initialize(SessionContext &scontext);
    void Shutdown(SessionContext &scontext);

    uintn ActiveCameraNode() const;
    void SetActiveCameraNode(uintn activecameranode);

    void SetActiveBackground(uintn activebackgroundidx);
    const Background& GetActiveBackground() const;

    RendererMode GetRendererMode() const;

    glm::vec4 GetClearColor() const;

    uintn GetMSAASamples() const;

    bool RayTracingEnabled() const;

    ColorSpaceType GetSurfaceColorSpace() const;

    uintn GetRayTraceMaxAccum() const;
    uintn GetRayTraceBounces() const;

    bool GetVSync() const;

    void OverlayUpdateMenuBar(SessionContext& scontext);
    void OverlayUpdate(SessionContext& scontext);

private:
    AssetCache &assetcache;

    stdvector<Background> backgrounds;

    StringID activecameranode;
    uintn activecamnodeidx = kInvalidUInt;

    uintn activebackgroundidx = kInvalidUInt;

    glm::vec3 startcampos;
    glm::quat startcamrot;
    bool hasstartcampos = false, hasstartcamrot = false;

    uintn msaasamples = 1;

    ColorSpaceType surfacespace = eColorSpaceSRGB;

    uintn raytracemaxaccum = 100000;
    uintn raytracebounces = 10;

    bool vsync = true;

    bool transformsview = false, invtransformsview = false;

    uint64 update = 0;

    RendererMode renderermode = eRendererRaster;
};

}