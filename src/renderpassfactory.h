#pragma once

#include "renderpass.h"

namespace bc9 {

class SessionContext;

enum RenderPassType {
    eRenderPassPrePass,
    eRenderPassMainHDR,
    eRenderPassDisplay,
    eRenderPassRayTrace,
    eRenderPassCount
};

class RenderPassFactory {
public:
    RenderPass* CreateRenderPass(SessionContext& scontext, RenderPassType rptype);
};

}