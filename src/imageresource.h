#pragma once

#if winvk_
#include "imageresource_vk.h"
#elif wind12_
#include "imageresource_d12.h"
#endif

// useful even if empty, as it prevents higher level code from having to platform switch as above.