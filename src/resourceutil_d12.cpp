#include "resourceutil.h"
#include "util_d12.h"
#include <assert.h>
#include "bclib/stdallocators.h"
#include "bclib/mathutils.h"
#include "rendertarget.h"
#include "framebufferlayout.h"
#include "bclib/dxutils.h"

#include "DirectXTex.h"
#include "atlstr.h" // for CStringW (conversion from char to wide char)
#include "d3dcompiler.h"

using namespace Microsoft::WRL;

namespace bc9 {

struct CreateD12BufferParams {
    UINT64 instancesize = 0;
    UINT32 instances = 0;
    UINT32 stride = 0;
    D3D12_HEAP_TYPE heaptype = D3D12_HEAP_TYPE_DEFAULT;
    D3D12_HEAP_FLAGS heapflags = D3D12_HEAP_FLAG_NONE;
    D3D12_RESOURCE_FLAGS resflags = D3D12_RESOURCE_FLAG_NONE;
    D3D12_RESOURCE_STATES resstates = D3D12_RESOURCE_STATE_COMMON;
    void* initdata = nullptr;
    uint64 initdatasize = 0;
    const char* name = nullptr;
};

void CopyBuffer(PlatformContext &context,
                BufferResource dst, uint64 dstoffset,
                BufferResource src, uint64 srcoffset,
                uint64 numbytes) {
    PlatformContext::CommandBuffer cmdlist = context.BeginSingleTimeCommands();

    cmdlist->CopyBufferRegion(dst.resource.Get(), dstoffset, src.resource.Get(), srcoffset,
                              numbytes);

    context.EndSingleTimeCommands(cmdlist);
}

// all buffer creation should be going through this routine
BufferResource CreateBuffer(PlatformContext &context, CreateD12BufferParams &params) {
    BufferResource buffer(params.instancesize, params.instances, params.stride);

    ComPtr<ID3D12Resource> resource;

    CD3DX12_HEAP_PROPERTIES heapprops(params.heaptype);
    CD3DX12_RESOURCE_DESC desc = CD3DX12_RESOURCE_DESC::Buffer(buffer.GetSize(), params.resflags);
    DxThrow_(context.device->CreateCommittedResource(
              &heapprops,
              params.heapflags,
              &desc,
              params.resstates,
              nullptr,
              IID_PPV_ARGS(&resource)));

    // TODO: look into why the name set here doesn't show up in d3d validation errors
    //       (but it does in pix)
    buffer.resource = resource;
    buffer.resource->SetPrivateData(WKPDID_D3DDebugObjectName, uint32(strlen(params.name)),
                                    params.name);

    if (params.heaptype & D3D12_HEAP_TYPE_UPLOAD)
        DxThrow_(buffer.resource->Map(0, nullptr, &buffer.mappedmem));

    if (params.initdata) {
        uint64 initdatasize = params.initdatasize ? params.initdatasize : buffer.GetInstanceSize();
        for(uint32 i = 0; i < params.instances; ++i) {
            if (buffer.mappedmem) {
                void* data  = buffer.Map(i);
                memcpy(data, params.initdata, initdatasize);
            }
            else {
                CreateBufferParams stagingparams(context, initdatasize, eBufferResourceInput,
                                                 "staging buffer (initdata)");
                stagingparams.SetAccessMode(eBufferCPUAccessWrite);
                stagingparams.SetUsageCopySource(true);

                BufferResource staging = CreateBuffer(context, stagingparams);

                void* data = staging.Map(0);
                unsigned char* ptr = (unsigned char*) data;
                memcpy(ptr, params.initdata, (size_t) initdatasize);

                CopyBuffer(context, buffer, i*buffer.GetInstanceSize(),
                           staging, 0, initdatasize);
            }
        }
    }

    return buffer;
}

BufferResource CreateBuffer(PlatformContext &context, const CreateBufferParams &params) {
    CreateD12BufferParams d12params{};

    assert(params.type != eBufferResourceTypeCount);
    assert(params.accessmode != eBufferCPUAccessModeCount);

    uint64 instancesize = params.instancesize;

    if (params.type == eBufferResourceConstant) {
        assert(params.stride == kInvalidUInt32);
        assert(params.accessmode == eBufferCPUAccessWrite);
        instancesize = Align<uint64>(params.instancesize,
                                     D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT);
    }
    else if (params.type == eBufferResourceTexture) {
        assert(params.accessmode == eBufferCPUAccessWrite);
        instancesize = Align<uint64>(params.instancesize,
                                     D3D12_CONSTANT_BUFFER_DATA_PLACEMENT_ALIGNMENT);
    }

    d12params.instancesize = instancesize;
    d12params.instances = params.instances;
    d12params.stride = params.stride;
    d12params.resflags  = D3D12_RESOURCE_FLAG_NONE;
    d12params.heapflags = D3D12_HEAP_FLAG_NONE;
    d12params.name = params.name;

    d12params.initdata = params.initdata;
    d12params.initdatasize = params.initdatasize ? params.initdatasize : d12params.instancesize;

    if (params.accessmode == eBufferCPUAccessWrite) {
        d12params.heaptype = D3D12_HEAP_TYPE_UPLOAD;
        d12params.resstates = D3D12_RESOURCE_STATE_GENERIC_READ;
    }
    else if (params.accessmode == eBufferCPUAccessNone) {
        d12params.heaptype = D3D12_HEAP_TYPE_DEFAULT;
        d12params.resstates = D3D12_RESOURCE_STATE_COMMON;
    }
    else if (params.accessmode == eBufferCPUAccessRead) {
        std::runtime_error("readback not yet implemented");
    }
    else {
        std::runtime_error("invalid buffer access mode");
    }

    if (params.GetUsageCopyDestination()) {
        d12params.resstates |= D3D12_RESOURCE_STATE_COPY_DEST;
    }
    else if (params.GetUsageCopySource()) {
        d12params.resstates |= D3D12_RESOURCE_STATE_COPY_SOURCE;
    }

    // TODO: seems like you want to transition to this more limited resource state, look into this...
    //if (params.vertexvisible)
    //    d12params.resstates |= D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE;
    //if (params.pixelvisible)
    //    d12params.resstates |= D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;

    // TODO: make a function to encapsulate srv descriptions
    BufferResource buffer = CreateBuffer(context, d12params);

    if (params.type == eBufferResourceConstant) {
        buffer.viewtype = eD12BufferViewConstant;
        buffer.cbv.BufferLocation = buffer.resource->GetGPUVirtualAddress();
        buffer.cbv.SizeInBytes = uint32_(buffer.GetInstanceSize());
    }
    else if (params.type == eBufferResourceTexture) {
        // i think this one gets an srv... not sure though
    }
    else if (params.type == eBufferResourceInput) {
        buffer.viewtype = eD12BufferViewInput;
        buffer.srv.Format = DXGI_FORMAT_UNKNOWN;
        buffer.srv.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
        buffer.srv.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
        buffer.srv.Buffer.FirstElement = 0;
        buffer.srv.Buffer.NumElements = uint32_(buffer.GetElements());
        buffer.srv.Buffer.StructureByteStride = buffer.GetStride();
        buffer.srv.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
    }
    else if (params.type == eBufferResourceStructuredInput) {
        buffer.viewtype = eD12BufferViewInput;
        buffer.srv.Format = DXGI_FORMAT_UNKNOWN;
        buffer.srv.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
        buffer.srv.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
        buffer.srv.Buffer.FirstElement = 0;
        buffer.srv.Buffer.NumElements = uint32_(buffer.GetElements());
        buffer.srv.Buffer.StructureByteStride = buffer.GetStride();
        buffer.srv.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
    }
    else if (params.type == eBufferResourceByteAddressInput) {
    }
    else if (params.type == eBufferResourceOutput) {
    }
    else if (params.type == eBufferResourceStructuredOutput) {
    }
    else if (params.type == eBufferResourceByteAddressOutput) {
    }
    else if (params.type == eBufferResourceStructuredAppendOutput) {
    }
    else if (params.type == eBufferResourceStructuredConsumeOutput) {
    }
    else {
        throw std::runtime_error("unknown buffer resource type");
    }

    if (params.GetUsageIndexBuffer()) {
        buffer.viewtype = eD12BufferViewIndex;
        buffer.ibv.BufferLocation = buffer.resource->GetGPUVirtualAddress();
        buffer.ibv.SizeInBytes = uint32_(buffer.GetInstanceSize());
        buffer.ibv.Format = DXGI_FORMAT_R32_UINT;
    }

    return buffer;
}

BufferResource CreateIndexBuffer(PlatformContext &context,
                                 const stdvector<GeoAsset> &geos,
                                 stdvector<uint64_t> &geooffsets) {
    uint64 buffersize = 0;
    uint64 offset = 0;
    geooffsets.resize(geos.size());
    for (uint32 i = 0; i < geos.size(); ++i) {
        const GeoAsset& geo(geos[i]);
        geooffsets[i] = geo.ecount ? offset : ~(uint64(0));
        buffersize += (geo.esize * geo.ecount);
        offset += geo.ecount;
    }

    if (!buffersize)
        return BufferResource();

    CreateBufferParams bufparams(context, buffersize, eBufferResourceIndex, IdxTypeUInt32,
                                 "index buffer (engine)");
    bufparams.SetUsageCopyDestination(true);

    BufferResource index = CreateBuffer(context, bufparams);

    CreateBufferParams stagingparams(context, buffersize, eBufferResourceIndex,
                                     IdxTypeUInt32, "index buffer (engine staging)");
    stagingparams.SetAccessMode(eBufferCPUAccessWrite);
    bufparams.SetUsageCopySource(true);
    BufferResource staging = CreateBuffer(context, stagingparams);

    void* data;
    DxThrow_(staging.resource->Map(0, nullptr, &data));
    unsigned char* ptr = (unsigned char*) data;
    for (auto& geo : geos) {
        uint64_t subbuffersize = geo.ecount * geo.esize;
        memcpy(ptr, geo.elems32, (uint64_t) subbuffersize);
        ptr += subbuffersize;
    }
    assert(ptr == ((unsigned char*) data)+buffersize);
    staging.resource->Unmap(0, nullptr);

    PlatformContext::CommandBuffer cmdlist = context.BeginSingleTimeCommands();

    cmdlist->CopyResource(index.resource.Get(), staging.resource.Get());

    CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(index.resource.Get(),
        D3D12_RESOURCE_STATE_COPY_DEST,
        D3D12_RESOURCE_STATE_INDEX_BUFFER);
    cmdlist->ResourceBarrier(1, &barrier);

    context.EndSingleTimeCommands(cmdlist);

    return index;
}

BLASResource CreateBLAS(PlatformContext& context,
                        const GeoAsset& asset, const CreateBLASParams& blasparams) {
    BLASResource blas;

    return blas;
}

void DestroyBLAS(PlatformContext& context, BLASResource& blas) {
}

BufferResource CreateVertexBuffer(PlatformContext &context,
                                  const stdvector<GeoAsset> &geos,
                                  stdvector<uint64_t> &geooffsets) {
    uint64 buffersize = 0;
    uint64 offset = 0;
    geooffsets.resize(geos.size());
    for (uint32 i = 0; i < geos.size(); ++i) {
        const GeoAsset& geo(geos[i]);
        geooffsets[i] = offset;
        buffersize += (geo.vcount * geo.vsize);
        offset += geo.vcount;
    }

    assert(buffersize);

    CreateBufferParams bufparams(context, buffersize, eBufferResourceStructuredInput,
                                 GeoAsset::VertexSize, "vertex buffer (engine)");

    BufferResource vertex = CreateBuffer(context, bufparams);

    CreateBufferParams stagingparams(context, buffersize, eBufferResourceStructuredInput,
                                     GeoAsset::VertexSize, "vertex buffer (engine staging)");
    stagingparams.SetAccessMode(eBufferCPUAccessWrite);
    stagingparams.SetVisibilityVertex(true);
    BufferResource staging = CreateBuffer(context, stagingparams);

    void* data;
    DxThrow_(staging.resource->Map(0, nullptr, &data));
    unsigned char* ptr = (unsigned char*) data;
    for (auto& geo : geos) {
        unsigned int subbuffersize = geo.vcount * geo.vsize;
        memcpy(ptr, geo.vertices, (size_t) subbuffersize);
        ptr += subbuffersize;
    }
    assert(ptr == ((unsigned char*) data)+buffersize);
    staging.resource->Unmap(0, nullptr);

    PlatformContext::CommandBuffer cmdlist = context.BeginSingleTimeCommands();

    cmdlist->CopyResource(vertex.resource.Get(), staging.resource.Get());

    CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(vertex.resource.Get(),
                                              D3D12_RESOURCE_STATE_COPY_DEST,
                                              D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
    cmdlist->ResourceBarrier(1, &barrier);

    context.EndSingleTimeCommands(cmdlist);

    return vertex;
}

static void PopulateSRV(D3D12_RESOURCE_DESC& imageinfo, bool arrayimage, bool cube,
                        ImageResource& image) {
    image.srv = {};
    image.srv.Format = imageinfo.Format;
    image.srv.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

    if (imageinfo.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE1D) {
        if (arrayimage) {
            image.srv.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE1DARRAY;
            image.srv.Texture1DArray.MostDetailedMip = 0;
            image.srv.Texture1DArray.MipLevels = imageinfo.MipLevels;
            image.srv.Texture1DArray.FirstArraySlice = 0;
            image.srv.Texture1DArray.ArraySize = imageinfo.DepthOrArraySize;
            image.srv.Texture1DArray.ResourceMinLODClamp = 0;
        }
        else {
            image.srv.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE1D;
            image.srv.Texture1D.MostDetailedMip = 0;
            image.srv.Texture1D.MipLevels = imageinfo.MipLevels;
            image.srv.Texture1D.ResourceMinLODClamp = 0;
        }
    }
    else if (imageinfo.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE2D) {
        if (cube) {
            if (arrayimage) {

                image.srv.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBEARRAY;
                image.srv.TextureCubeArray.MostDetailedMip = 0;
                image.srv.TextureCubeArray.MipLevels = imageinfo.MipLevels;
                image.srv.TextureCubeArray.First2DArrayFace = 0;
                image.srv.TextureCubeArray.NumCubes = imageinfo.DepthOrArraySize / 6;
                image.srv.TextureCubeArray.ResourceMinLODClamp = 0;
            }
            else {
                image.srv.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
                image.srv.TextureCube.MostDetailedMip = 0;
                image.srv.TextureCube.MipLevels = imageinfo.MipLevels;
                image.srv.TextureCube.ResourceMinLODClamp = 0;
            }
        }
        else {
            unsigned int samples = imageinfo.SampleDesc.Count;
            if (arrayimage) {
                if (samples > 1) {
                    image.srv.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DMSARRAY;
                      image.srv.Texture2DMSArray.FirstArraySlice = 0;
                      image.srv.Texture2DMSArray.ArraySize = imageinfo.DepthOrArraySize;
                }
                else {
                    image.srv.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DARRAY;
                    image.srv.Texture2DArray.MostDetailedMip = 0;
                    image.srv.Texture2DArray.MipLevels = imageinfo.MipLevels;
                    image.srv.Texture2DArray.FirstArraySlice = 0;
                    image.srv.Texture2DArray.ArraySize = imageinfo.DepthOrArraySize;
                    image.srv.Texture2DArray.PlaneSlice = 0;
                    image.srv.Texture2DArray.ResourceMinLODClamp = 0;
                }
            }
            else {
                if (samples > 1) {
                    image.srv.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DMS;
                    // no fields
                }
                else {
                    image.srv.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
                    image.srv.Texture2D.MostDetailedMip = 0;
                    image.srv.Texture2D.MipLevels = imageinfo.MipLevels;
                    image.srv.Texture2D.PlaneSlice = 0;
                    image.srv.Texture2D.ResourceMinLODClamp = 0;
                }
            }
        }
    }
    else if (imageinfo.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE3D) {
        image.srv.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE3D;
        image.srv.Texture3D.MostDetailedMip = 0;
        image.srv.Texture3D.MipLevels = imageinfo.MipLevels;
        image.srv.Texture3D.ResourceMinLODClamp = 0;
    }
    else {
        Throw_(Error("internal error: invalid image dimension"));
    }
}

void CreateImage(PlatformContext& context, CreateImageParams& params, ImageResource& image) {
    image.state = params.initial;
    CD3DX12_HEAP_PROPERTIES heapprops(D3D12_HEAP_TYPE_DEFAULT);
    DxThrow_(context.device->CreateCommittedResource(
              &heapprops, D3D12_HEAP_FLAG_NONE,
              &params.imageinfo, params.initial, params.clear, IID_PPV_ARGS(&image.resource)));

    PopulateSRV(params.imageinfo, params.arrayimage, params.cube, image);
}

void CreateImage(PlatformContext &context, TextureAsset &asset, ImageResource& image) {
    DirectX::ScratchImage imagedata;
    DxThrow_(DirectX::LoadFromDDSFile(CStringW(asset.datapath.str()),
                                      DirectX::DDS_FLAGS_NONE, nullptr, imagedata));

    const DirectX::TexMetadata& metadata = imagedata.GetMetadata();
    asset.width = (unsigned int) metadata.width;
    asset.height = (unsigned int) metadata.height;
    asset.depth = (unsigned int) metadata.depth;
    asset.arraysize = (unsigned int) metadata.arraySize;
    asset.miplevels = (unsigned int) metadata.mipLevels;
    asset.pmalpha = metadata.IsPMAlpha();

    assert(asset.colorspace != eColorSpaceTypeCount);
    switch (metadata.format) {
        case DXGI_FORMAT_R8G8B8A8_UNORM:
        asset.format = (asset.colorspace == eColorSpaceSRGB) ? eResourceFormatR8G8B8A8_SRGB :
                                                               eResourceFormatR8G8B8A8_UNORM;
        break;

        default:
            Throw_(Error("%s: unsupported texture format", asset.name.str()));
        break;
    }

    D3D12_RESOURCE_DESC texturedesc{};

    DXGI_FORMAT format;
    Ignore_(GetDXGIFormat(asset.format, format)); // already validated above
    texturedesc.Format = format;
    texturedesc.Width = metadata.width;
    texturedesc.Height = (UINT) metadata.height;
    texturedesc.Flags = D3D12_RESOURCE_FLAG_NONE;
    texturedesc.DepthOrArraySize = (UINT16)metadata.arraySize;
    texturedesc.MipLevels = (UINT16)metadata.mipLevels;
    texturedesc.SampleDesc.Count = 1;
    texturedesc.SampleDesc.Quality = 0;
    // update this if we end up supporting more than { 1d-array, 2d-array, cube-array } formats.
    // TODO make this infer dimension just like CreateImageParams() does.
    texturedesc.Dimension = (asset.type == eTextureAssetType1DArray) ?
                                   D3D12_RESOURCE_DIMENSION_TEXTURE1D :
                                   D3D12_RESOURCE_DIMENSION_TEXTURE2D;
    texturedesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
    texturedesc.Alignment = 0;

    D3D12_HEAP_PROPERTIES defaultproperties;
    defaultproperties.Type = D3D12_HEAP_TYPE_DEFAULT;
    defaultproperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
    defaultproperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
    defaultproperties.CreationNodeMask = 0;
    defaultproperties.VisibleNodeMask = 0;

    // target resource
    image.state = D3D12_RESOURCE_STATE_COPY_DEST;
    DxThrow_(context.device->CreateCommittedResource(&defaultproperties,
                                                      D3D12_HEAP_FLAG_NONE,
                                                      &texturedesc,
                                                      image.state, NULL,
                                                      IID_PPV_ARGS(&image.resource)));

    UINT64 texmemorysize = 0;
    constexpr unsigned int MaxTextureSubresourceCount = kMaxArrayTextureSlices*kMaxArrayMipLevels;
    UINT numrows[MaxTextureSubresourceCount];
    UINT64 rowsizesinbytes[MaxTextureSubresourceCount];
    D3D12_PLACED_SUBRESOURCE_FOOTPRINT layouts[MaxTextureSubresourceCount];
    const UINT64 numsubresources = metadata.mipLevels * metadata.arraySize;

    context.device->GetCopyableFootprints(&texturedesc, 0, (UINT32)numsubresources, 0, layouts,
                                             numrows, rowsizesinbytes, &texmemorysize);

    // staging resource
    // TODO: switch this to using platform-independent CreateBuffer() routine
    CreateD12BufferParams bufparams = {};
    bufparams.instancesize = texmemorysize;
    bufparams.stride       = kInvalidUInt32;
    bufparams.instances    = 1;
    bufparams.heaptype     = D3D12_HEAP_TYPE_UPLOAD;
    bufparams.resstates    = D3D12_RESOURCE_STATE_GENERIC_READ;
    bufparams.resflags     = D3D12_RESOURCE_FLAG_NONE;
    bufparams.heapflags    = D3D12_HEAP_FLAG_NONE;
    bufparams.name = "staging buffer (CreateImage())";
    BufferResource staging = CreateBuffer(context, bufparams);

    UINT8 *mappedbuf = nullptr;
    staging.resource->Map(0, NULL, reinterpret_cast<void**>(&mappedbuf));

    for (UINT64 arrayidx = 0; arrayidx < metadata.arraySize; arrayidx++)
    {
        for (UINT64 mipidx = 0; mipidx < metadata.mipLevels; mipidx++)
        {
            const UINT64 subresidx = mipidx + (arrayidx * metadata.mipLevels);

            const D3D12_PLACED_SUBRESOURCE_FOOTPRINT& subreslayout = layouts[subresidx];
            const UINT64 subresheight = numrows[subresidx];
            const UINT64 subrespitch = Align<UINT64>(subreslayout.Footprint.RowPitch,
                                                     D3D12_TEXTURE_DATA_PITCH_ALIGNMENT);
            const UINT64 subresdepth = subreslayout.Footprint.Depth;
            UINT8* destsubresmem = mappedbuf + subreslayout.Offset;

            for (UINT64 sliceidx = 0; sliceidx < subresdepth; sliceidx++)
            {
                const DirectX::Image* subImage = imagedata.GetImage(mipidx, arrayidx, sliceidx);
                const UINT8* sourcesubresmem = subImage->pixels;

                for (UINT64 height = 0; height < subresheight; height++)
                {
                    memcpy(destsubresmem, sourcesubresmem, Min(subrespitch, subImage->rowPitch));
                    destsubresmem += subrespitch;
                    sourcesubresmem += subImage->rowPitch;
                }
            }
        }
    }

    PlatformContext::CommandBuffer cmdlist = context.BeginSingleTimeCommands();

    for (UINT64 subresidx = 0; subresidx < numsubresources; subresidx++)
    {
        D3D12_TEXTURE_COPY_LOCATION destination = {};
        destination.pResource = image.resource.Get();
        destination.Type = D3D12_TEXTURE_COPY_TYPE_SUBRESOURCE_INDEX;
        destination.SubresourceIndex = (UINT32)subresidx;

        D3D12_TEXTURE_COPY_LOCATION source = {};
        source.pResource = staging.resource.Get();
        source.Type = D3D12_TEXTURE_COPY_TYPE_PLACED_FOOTPRINT;
        source.PlacedFootprint = layouts[subresidx];
        //source.PlacedFootprint.Offset = 0;

        cmdlist->CopyTextureRegion(&destination, 0, 0, 0, &source, nullptr);
    }

    CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(image.resource.Get(),
        D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
    cmdlist->ResourceBarrier(1, &barrier);

    context.EndSingleTimeCommands(cmdlist);

    bool cube = false;
    bool arrayimage = false;
    if (asset.type == eTextureAssetType1DArray ||
        asset.type == eTextureAssetType2DArray ||
        asset.type == eTextureAssetTypeCubeArray) {
        arrayimage = true;
    }
    PopulateSRV(texturedesc, arrayimage, cube, image);
}

Result CreateShader(PlatformContext &context, const ShaderAsset &asset,
                    const CreateShaderParams &params, ShaderResource& shader) {
    DxThrow_(D3DCreateBlob(asset.blob.size(), &shader.blob));
    memcpy(shader.blob->GetBufferPointer(), asset.blob.data(), asset.blob.size());

    return Success();
}

void DestroyImage(PlatformContext &context, ImageResource &image) {
}

void DestroyBuffer(PlatformContext &context, BufferResource &buffer) {
}

void DestroyShader(PlatformContext &context, ShaderResource &shader) {
}

void CreateColorTarget(PlatformContext& context, FramebufferLayout& layout,
                       CreateColorTargetParams& params, ColorTarget& color) {
    unsigned int samples = layout.GetSampleCount();

    ColorLayout colorlayout;
    layout.GetColorLayout(params.name, colorlayout);
    CreateImageParams imageparams(context, colorlayout.format, params.width, params.height,
                                  samples, params.name.str());

    color.resolving = layout.Multisampled() && !colorlayout.NoResolve();
    color.presenting = colorlayout.Presenting();
    color.shaderreadable = colorlayout.ShaderReadable();

    if (colorlayout.LoadOpClear()) {
        glm::vec4 clearcolor(params.GetClearColor());
        imageparams.SetUsageColorTarget(clearcolor);
        color.clearcolor[0] = clearcolor.x;
        color.clearcolor[1] = clearcolor.y;
        color.clearcolor[2] = clearcolor.z;
        color.clearcolor[3] = clearcolor.w;
    }
    else {
        imageparams.SetUsageColorTarget();
    }

    // msaa target
    // TODO maybe I need a eColorLayoutMSAAShaderReadable too? Otherwise how
    //      would you mark the msaa target as a shader input, if that's
    //      something you wanted to do? (which is possible in the api) Hmm,
    //      actually, I think it should actually be ok to just have the
    //      eColorLayoutShaderReadable, because if you were resolving, you
    //      likely wouldn't be using the msaa image as input (possible I
    //      guess, but not likely), and if you're not resolving and you
    //      request that, then it would def be msaa.
    if (layout.Multisampled())
        CreateImage(context, imageparams, color.msaatarget);

    // resolve target (or non-msaa target if msaa is off)
    if (colorlayout.flags & eColorLayoutShaderReadable)
        imageparams.SetUsageSampled();

    // set samples to 1 for the resolve (or just non-msaa) target
    imageparams.imageinfo.SampleDesc.Count = 1;

    if (!colorlayout.Presenting() && (!layout.Multisampled() || !colorlayout.NoResolve()))
        CreateImage(context, imageparams, color.target);
}

void DestroyColorTarget(PlatformContext& context, ColorTarget& color) {
    if (color.msaatarget.resource)
        DestroyImage(context, color.msaatarget);
    if (color.target.resource)
        DestroyImage(context, color.target);
}

void CreateDepthStencilTarget(PlatformContext& context, FramebufferLayout& layout,
                              CreateDepthStencilTargetParams& params,
                              DepthStencilTarget& dstarget) {
    unsigned int samples = layout.GetSampleCount();

    DepthStencilLayout dslayout;
    layout.GetDepthStencilLayout(dslayout);
    CreateImageParams imageparams(context, dslayout.format,
                                  params.width, params.height, samples, "dstarget");

    if (dslayout.DepthEnabled()) {
        if (dslayout.DepthLoadOpClear()) {
            float cleardepth(params.GetClearDepth());
            imageparams.SetUsageDepthTarget(cleardepth);
            dstarget.cleardepth = cleardepth;
        }
        else {
            imageparams.SetUsageDepthTarget();
        }
    }

    if (dslayout.StencilEnabled()) {
        if (dslayout.StencilLoadOpClear()) {
            uint32_t clearstencil(params.GetClearStencil());
            imageparams.SetUsageStencilTarget(clearstencil);
            dstarget.clearstencil = clearstencil;
        }
        else {
            imageparams.SetUsageStencilTarget();
        }
    }
    // TODO: deal with setting up depth targets for reading from shaders

    CreateImage(context, imageparams, dstarget.target);
}

void DestroyDepthStencilTarget(PlatformContext& context, DepthStencilTarget& dstarget) {
    DestroyImage(context, dstarget.target);
}

}