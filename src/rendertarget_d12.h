#pragma once

#include "resourceutil.h"

namespace bc9 {

class ColorTarget {
public:
    FLOAT clearcolor[4];
    // TODO: should some of these states be moved to ColorLayout?
    bool resolving = false;
    bool presenting = false;
    bool shaderreadable = false;
    ImageResource msaatarget;
    ImageResource target;
};

// might at some point need to support separate resources for depth and stencil.
class DepthStencilTarget {
public:
    float cleardepth = 0;
    uint32_t clearstencil = 0;
    ImageResource target;
};

}