#include "imdraw.h"
#include "sessioncontext.h"
#include "renderer.h"
#include "bclib/miscutils.h"

namespace bc9 {

static constexpr uint32_t vbsize = MB(8);
static constexpr uint32_t ibsize = MB(8);

IMDraw::IMDraw(Renderer& renderer) :
renderer(renderer) {
}

void IMDraw::Shutdown(SessionContext& scontext) {
    DestroyBuffer(scontext.pc, constantsbuf);
    DestroyBuffer(scontext.pc, indexbuf);
    DestroyBuffer(scontext.pc, vertexbuf);
    DestroyBuffer(scontext.pc, gpuvertexbuf);
}

void IMDraw::Initialize(SessionContext& scontext) {
#if 0
    vertexkey = scontext.assetcache.FindShader("imdraw", eShaderAssetVertex);
    pixelkey = scontext.assetcache.FindShader("imdraw", eShaderAssetPixel);

    FramebufferLayout fblayout;
    scontext.passcache.GetFramebufferLayout("display", fblayout);

    pipelinestate = GraphicsPipelineState(scontext.pc, fblayout);
    GraphicsPipelineBase base(scontext.pc, vertexkey, pixelkey);
    basekey = scontext.pipelinecache.RegisterBaseGraphicsPipeline(scontext, base, pipelinestate);

    CreateBufferParams cparams(scontext.pc, sizeof(IMConstants), kFrameRenderBufferCount,
                               eBufferResourceConstant, "constants (imdraw)");
    cparams.SetVisibilityPixel(true);
    cparams.SetAccessMode(eBufferCPUAccessWrite);
    Throw_(CreateBuffer(scontext.pc, cparams, constantsbuf));

    CreateBufferParams idxparams(scontext.pc, ibsize, kFrameRenderBufferCount,
                                 eBufferResourceIndex, GeoAssetIndexUInt32, "index buffer (imdraw)");
    idxparams.SetAccessMode(eBufferCPUAccessWrite);
    idxparams.SetVisibilityVertex(true);
    Throw_(CreateBuffer(scontext.pc, idxparams, indexbuf));

    CreateBufferParams vtxparams(scontext.pc, vbsize, kFrameRenderBufferCount,
                                 eBufferResourceStructuredInput, GeoAsset::PNTSize,
                                 "vertex buffer (imdraw)");
    vtxparams.SetAccessMode(eBufferCPUAccessWrite);
    vtxparams.SetVisibilityVertex(true);
    Throw_(CreateBuffer(scontext.pc, vtxparams, vertexbuf));

    CreateBufferParams gpuvtxparams(scontext.pc, vbsize, eBufferResourceStructuredOutput,
                                    GeoAsset::PNTSize, "gpu vertex buffer (imdraw)");
    gpuvtxparams.SetUsageCopyDestination(true);
    // FIXME: this initial value think is jank
    float initvalue = 100000.0f;
    gpuvtxparams.SetInitialData(&initvalue, sizeof(initvalue));
    Throw_(CreateBuffer(scontext.pc, gpuvtxparams, gpuvertexbuf));
#endif
}

BufferResource& IMDraw::GPUVertexBuffer() {
    return gpuvertexbuf;
}

void IMDraw::DrawTriangle(SessionContext& scontext) {
#if 0
    unsigned char* backidxptr = (unsigned char*)indexbuf.Map(backidx);
    unsigned char* backvtxptr = (unsigned char*)vertexbuf.Map(backidx);

    uint32_t* inds = (uint32_t*) backidxptr;
    *inds++ = 0; *inds++ = 1; *inds++ = 2;

    float* verts = (float*) backvtxptr;
    *verts++ = -0.5f; *verts++ = -0.5f; *verts++ = 0.0f; *verts++ = 0.0f;
    *verts++ = 1.0f; *verts++ = 0.0f; *verts++ = 0.0f; *verts++ = 0.0f;

    *verts++ = 0.5f; *verts++ = -0.5f; *verts++ = 0.0f; *verts++ = 0.0f;
    *verts++ = 1.0f; *verts++ = 0.0f; *verts++ = 0.0f; *verts++ = 0.0f;

    *verts++ = 0.0f; *verts++ = 0.5f; *verts++ = 0.0f; *verts++ = 0.0f;
    *verts++ = 1.0f; *verts++ = 0.0f; *verts++ = 0.0f; *verts++ = 0.0f;

    GraphicsPipelineState gfxstate = pipelinestate;
    gfxstate.polygonmode = ePolygonModeLine;
    //gfxstate.topology = ePrimitiveLineList;
    gfxstate.blendingenabled = true;
    Pipeline pipeline(scontext.pipelinecache.GetGraphicsPipeline(scontext, basekey, gfxstate));

    DrawData dd;
    //dd.pipeline = pipeline;
    dd.geodata.indexcount = 3;
    //drawdatas.push_back(dd);
#endif
}

void IMDraw::DrawLines(SessionContext& scontext) {
#if 0
    GraphicsPipelineState gfxstate = pipelinestate;
    gfxstate.polygonmode = ePolygonModeLine;
    //gfxstate.topology = ePrimitivePointList;
    gfxstate.blendingenabled = true;
    Pipeline pipeline(scontext.pipelinecache.GetGraphicsPipeline(scontext, basekey, gfxstate));

    DrawData dd;
    //dd.pipeline = pipeline;
    // this number should match the geo generation loop in the shader
    dd.geodata.vertexcount = 1024;
    //drawdatas.push_back(dd);
#endif
}

uint32_t IMDraw::GetVertexBufferOffset() {
    return (1-backidx) * vbsize;
}

uint32_t IMDraw::GetIndexBufferOffset() {
    return (1-backidx) * ibsize;
}

void IMDraw::PreRender(SessionContext& scontext) {
#if winvk_
    VkBufferMemoryBarrier bufferbarrier { VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER, nullptr };
    bufferbarrier.srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT;
    bufferbarrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
    bufferbarrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    bufferbarrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    bufferbarrier.buffer = gpuvertexbuf.vkobj;
    bufferbarrier.offset = 0;
    bufferbarrier.size = gpuvertexbuf.GetSize();

    vkCmdPipelineBarrier(scontext.pc.current.cmdbuffer.vkobj,
                         VK_PIPELINE_STAGE_RAY_TRACING_SHADER_BIT_KHR,
                         VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
                         0,
                         0, nullptr,
                         1, &bufferbarrier,
                         0, nullptr);
#endif
}

void IMDraw::Render(SessionContext& scontext) {
// commented out because dd's don't have a pipeline anymore, so you'd actually generate that here
#if 0
    unsigned int bufferidx = scontext.pc.bufferidx;

    if (drawdatas.size()) {
        backidx = 1 - backidx;

        // FIXME: use new-style UpdateAndSwap() here!
        void* ptr = constantsbuf.Map(bufferidx);
        memcpy(ptr, &constants, sizeof(IMConstants));
        iminputset.SetBufferInstance(scontext, "imconstants", constantsbuf, bufferidx);
        //iminputset.SetBufferInstance(scontext, "imvertices", vertexbuf, 1 - backidx);

        pr.BindInputSet(scontext, ePipelineTypeGraphics, iminputset);
        pr.BindIndexBuffer(scontext, indexbuf, GetIndexBufferOffset());

        for (auto& dd : drawdatas) {
            pr.BindPipeline(scontext, dd.pipeline);
            pr.SingleDraw(scontext, dd);
        }

        drawdatas.clear();
    }
#endif
}

}