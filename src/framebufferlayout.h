#pragma once

#include "bclib/stringid.h"
#include "rhitypes.h"
#include "glm/glm.hpp"

namespace bc9 {

struct ColorLayout {
    // if you use eColorLayoutLoadOpPreserve see access flags in VkAttachmentLoadOp for details
    ColorLayout() {}
    ColorLayout(const StringID& name, ResourceFormat format, ColorLayoutFlags flags) :
        name(name), format(format), flags(flags) {}

    bool NoResolve() const { return flags & eColorLayoutNoResolve; }
    bool LoadOpClear() const { return flags & eColorLayoutLoadOpClear; }
    bool Presenting() const { return flags & eColorLayoutPresenting; }
    bool ShaderReadable() const { return flags & eColorLayoutShaderReadable; }

    bool Valid() { return format != eResourceFormatInvalid; }

    StringID name;
    ColorLayoutFlags flags;
    ResourceFormat format = eResourceFormatInvalid;
};

// the intention is that there's only ever 0 or 1 DepthStencilLayouts. if using
// eDepthStencilLayoutDepthStencilSeparate flag, then we can add support for
// maintaining the 2 buffers within the DepthStencilTarget. maintaining multiple
// resources inside DepthStencilTarget shouldn't be too much of a problem since we
// already do this for msaa, and the framebuffer code can encapsulate all of that.
// we use the format + flags to decide whether it's
// 1. depth only
// 2. stencil only
// 3. combined depth + stencil
// 4. separate depth + stencil
struct DepthStencilLayout {
    // if you use one of the preserve flags, check out VkAttachmentLoadOp notes
    DepthStencilLayout() {}
    DepthStencilLayout(ResourceFormat format, DepthStencilLayoutFlags flags) :
        format(format),
        flags(flags) {}

    bool DepthEnabled() const { return (flags & eDepthStencilLayoutDepthDisabled) == 0; }
    bool StencilEnabled() const { return flags & eDepthStencilLayoutStencilEnabled; }

    bool DepthLoadOpClear() const { return flags & eDepthStencilLayoutDepthLoadOpClear; }
    bool StencilLoadOpClear() const { return flags & eDepthStencilLayoutStencilLoadOpClear; }

    bool Valid() { return format != eResourceFormatInvalid; }

    DepthStencilLayoutFlags flags;
    ResourceFormat format = eResourceFormatInvalid;
};

}

#if winvk_
#include "framebufferlayout_vk.h"
#elif wind12_
#include "framebufferlayout_d12.h"
#endif