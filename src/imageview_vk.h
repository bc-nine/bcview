#pragma once

#include "volk.h"

namespace bc9 {

struct ImageView {
    VkImageView vkobj = VK_NULL_HANDLE;

    void SetDescriptorIndex(uintn index) { descriptoridx = index; }
    uint32 DescriptorIndex() const { return descriptoridx; }

private:
    uint32 descriptoridx = kInvalidUInt32;
};

}
