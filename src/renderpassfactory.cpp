#include "renderpassfactory.h"
#include "sessioncontext.h"
#include "renderpasses/mainhdrpass.h"
#include "renderpasses/displaypass.h"
#include "renderpasses/prepass.h"
#if winvk_
    #include "renderpasses/raytracepass.h"
#endif
#include <exception>

namespace bc9 {

RenderPass* RenderPassFactory::CreateRenderPass(SessionContext& scontext,
                                                RenderPassType rptype) {
    RenderPass* renderpass = nullptr;

    switch (rptype) {
        case eRenderPassPrePass:
        renderpass = new PrePass(scontext);
        break;

        case eRenderPassMainHDR:
        renderpass = new MainHDRPass(scontext);
        break;

        case eRenderPassDisplay:
        renderpass = new DisplayPass(scontext);
        break;

#if winvk_
        case eRenderPassRayTrace:
        renderpass = new RayTracePass(scontext);
        break;
#endif

        default:
        throw std::runtime_error("invalid render pass type");
        break;
    };

    return renderpass;
}

}