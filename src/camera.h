#pragma once

#include "bclib/stdjson.h"

#define GLM_FORCE_SWIZZLE
#include "glm/glm.hpp"
#include "glm/gtc/quaternion.hpp"
#include "rhitypes.h"

namespace bc9 {

class Camera {
public:
    Camera();
    Camera(const stdjson &cjson);

    glm::mat4 ViewTransform() const;
    glm::mat4 InverseViewTransform() const;

    glm::mat4 ProjectionTransform(float aspect) const;

    void SetPosition(const glm::vec3& pos);
    void SetOrientation(const glm::quat& q);

    glm::quat GetOrientation() const;
    glm::vec3 GetPosition() const;

    static glm::vec3 GetRightVector(const glm::quat& rot);
    static glm::vec3 GetForwardVector(const glm::quat& rot);

    float GetApertureSize() const;
    float GetFocusDistance() const;

    void Dump() const;

private:
    void Construct(const stdjson &cjson, CameraType type);

    void Print(const char *str, const glm::mat4 &transform) const;
    void Print(const char *str, const glm::vec3 &v) const;

    StringID name = "null";

    float nearplane = 1;
    float farplane = 10000;
    float vfovdeg = 35;
    float height = 1.;
    float width = 1.;
    float aperturesize = 0;
    float focusdistance = 1;

    CameraType type = eCameraTypeCount;
};

}