#include "samplerasset.h"
#include "bclib/stringid.h"

namespace bc9 {

Result GetSamplerMode(const StringID& strmode, SamplerMode& mode) {
    switch (strmode.Hash()) {
        case "repeat"_sid:
        mode = eSamplerRepeat;
        break;

        case "mirrored-repeat"_sid:
        mode = eSamplerMirroredRepeat;
        break;

        case "clamp-to-edge"_sid:
        mode = eSamplerClampToEdge;
        break;

        case "clamp-to-border"_sid:
        mode = eSamplerClampToBorder;
        break;

        case "mirror-clamp-to-edge"_sid:
        mode = eSamplerMirrorClampToEdge;
        break;

        default:
        return Error("%s: sampler mode not found", strmode.str());
        break;
    }

    return Success();
}

Result GetSamplerFilter(const StringID& strfilter, SamplerFilter& filter) {
    switch (strfilter.Hash()) {
        case "nearest"_sid:
        filter = eSamplerNearest;
        break;

        case "linear"_sid:
        filter = eSamplerLinear;
        break;

        default:
        return Error("%s: sampler filter not found", strfilter.str());
        break;
    }

    return Success();
}

Result GetSamplerMipmapFilter(const StringID& strfilter, SamplerMipmapFilter& filter) {
    switch (strfilter.Hash()) {
        case "nearest"_sid:
        filter = eSamplerMipmapNearest;
        break;

        case "linear"_sid:
        filter = eSamplerMipmapLinear;
        break;

        default:
            return Error("%s: sampler mipmap filter not found", strfilter.str());
        break;
    }

    return Success();
}

}